<?php
/**
 * @version		$Id: components.php 1.0 19-11-2009 Danijar
 * @package		Frontend Admin
 * @copyright	Copyright (C) 2009 CMSSpace. http://www.cmsspace.com
 * @license		GNU/GPL. http://www.gnu.org/licenses/gpl.html
 */
defined( '_JEXEC' ) or die( 'Restricted access' );
require_once(dirname(__FILE__).DS.'extension.php');
class AikModelComponents extends InstallerModel
{
	var $_type = 'component';
	function enable($eid=array())
	{
		$result	= false;
		if (!is_array($eid)) {
			$eid = array ($eid);
		}
		$db =& JFactory::getDBO();
		$table = & JTable::getInstance($this->_type);
		foreach ($eid as $id)
		{
			$table->load($id);
			$table->enabled = '1';
			$result |= $table->store();
		}
		return $result;
	}
	function disable($eid=array())
	{
		$result		= false;
		if (!is_array($eid)) {
			$eid = array ($eid);
		}
		$db =& JFactory::getDBO();
		$table = & JTable::getInstance($this->_type);
		foreach ($eid as $id)
		{
			$table->load($id);
			$table->enabled = '0';
			$result |= $table->store();
		}

		return $result;
	}

	function _loadItems()
	{
		global $mainframe, $option;

		jimport('joomla.filesystem.folder');
		$db =& JFactory::getDBO();

		$query = 'SELECT *' .
				' FROM #__components' .
				' WHERE parent = 0' .
				' ORDER BY iscore, name';
		$db->setQuery($query);
		$rows = $db->loadObjectList();
		$adminDir = JPATH_ADMINISTRATOR .DS. 'components';
		$siteDir = JPATH_SITE .DS. 'components';

		$numRows = count($rows);
		for($i=0;$i < $numRows; $i++)
		{
			$row =& $rows[$i];

			$folder = $adminDir.DS.$row->option;
			if (JFolder::exists($folder)) {
				$xmlFilesInDir = JFolder::files($folder, '.xml$');
			} else {
				$folder = $siteDir.DS.$row->option;
				if (JFolder::exists($folder)) {
					$xmlFilesInDir = JFolder::files($folder, '.xml$');
				} else {
					$xmlFilesInDir = null;
				}
			}

			if (count($xmlFilesInDir))
			{
				foreach ($xmlFilesInDir as $xmlfile)
				{
					if ($data = JApplicationHelper::parseXMLInstallFile($folder.DS.$xmlfile)) {
						foreach($data as $key => $value) {
							$row->$key = $value;
						}
					}
					$row->jname = JString::strtolower(str_replace(" ", "_", $row->name));
				}
			}
		}
		$this->setState('pagination.total', $numRows);

		if($this->_state->get('pagination.offset') > $this->_state->get('pagination.total')) {
			$this->setState('pagination.offset',0);
		}

		if($this->_state->get('pagination.limit') > 0) {
			$this->_items = array_slice( $rows, $this->_state->get('pagination.offset'), $this->_state->get('pagination.limit') );
		} else {
			$this->_items = $rows;
		}
	}
}