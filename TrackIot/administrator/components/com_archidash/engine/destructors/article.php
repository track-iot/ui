<?php


defined('_JEXEC') or die('Restricted Access');
jimport('joomla.application.component.model');
require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'engine'.DS.'destructors'.DS.'destructor.php');


/**
 * The following class destroyes all the joomla articles related to the dashboard
 * @version 1.0
 * @author  TobyTools.com (email:info@tobytools.com)(website: www.tobytools.com)
 * @copyright Copyright TobyTools.com 
 * @license GNU/GPL
 */


class ArchiDashDestructorArticle extends ArchiDashDestructor{


	function ArchiDashDestructorArticle($opts){
		parent::__construct();
		//$opts['dashboardid']
		$this->options = $opts;
		$this->table="#__content";
		$this->options['characterset']=$this->characterset;//set characters
		
	}
	
	function preCondition(){
		//no precondition
		return true;
	}

	function destroy(){
		global $mainframe;
		if($this->preCondition() && isset($this->options['dashboardid'])){
			
			require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'models'.DS.'article.php');
			$classname="ArchiDashModelArticle";
			$model = new $classname();
			
			/*Articles IDs related to dashboard*/
			$list = $model->selectAllRecordsByDashboardID($this->options['dashboardid']);
			if(!$list || count($list)<=0){
				//no element to destroy
				return;
			}
			
			
			/*DESTROY ARTICLES*/
			//work on #__content
			$db =& JFactory::getDBO();
			//to avoid problem with character set
			$db->setQuery($this->characterset);
			$db->query();
			
			$query = "DELETE FROM ".$db->nameQuote($this->table)." 
						WHERE 0 ";
			
			//create where clause
			$where=" ";
			foreach($list as $element){
				$id = $element['articleid'];
				$where.=" OR ".$db->nameQuote('id')." = ".$db->Quote($id);				
			}						
			$where.=" ";
			$query.=$where;			
			

			
			$db->setQuery($query);
			$db->query();
			
									
			/**/
			
			/*DELETE THE RELATED RECORDS INTO #__archidash_articles*/
			$model->deleteAllRecordsByDashboardID($this->options['dashboardid']);
			
			
			
			
		}
		else{			
			$this->completed=false;
			$this->errMsg.=get_class($this)."::destroy ".JText::_('NODASHBOARDID');
			$mainframe->enqueueMessage($this->errMsg,'error');
		}
	}
}


