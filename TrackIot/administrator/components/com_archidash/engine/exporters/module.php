<?php


defined('_JEXEC') or die('Restricted Access');
jimport('joomla.application.component.model');
require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'engine'.DS.'exporters'.DS.'exporter.php');


/**
 * The following class export the archimede modules 
 * @version 1.0
 * @author  TobyTools.com (email:info@tobytools.com)(website: www.tobytools.com)
 * @copyright Copyright TobyTools.com 
 * @license GNU/GPL
 */


class ArchiDashExporterModule extends ArchiDashExporter{
	
	private $datasourceIDs;
	
	/*
	 *class constructor 
	 */
	function ArchiDashExporterModule($opts){
		parent::__construct($opts);
		//check everything is allright
		if($this->completed){
			//options['modules'], is an arrays of module id
			if($this->options['modules']){
				$this->options['table']="#__modules";
				$this->options['tag']="module";	
				$this->datasourceIDs = array();		
			}
			else{
				$this->completed=false;
				$this->options=false;
				$this->errMsg=false;
				$this->datasourceIDs=false;
				global $mainframe;
				$mainframe->enqueueMessage(JText::_('NOMODULEID')." ".get_class($this),'error');
			}
			
		}
		else{
			global $mainframe;
			$mainframe->enqueueMessage(JText::_('NOOBJECT')." ".get_class($this),'error');
		}
		
	}
	
	public function export($params=false){
		if($this->completed){
			global $mainframe;						
			$moduleIDs = $this->options['modules'];
			
			$db =& JFactory::getDBO();
			
			//to avoid problem with character set
			$db->setQuery($this->options['characterset']);
			$db->query();
			
			//create the WHERE clause (0 OR id= OR id= ...) 
			$clause=" (0 ";
			foreach($moduleIDs as $id){
				$clause.=" OR ".$db->nameQuote('id').' = '.$db->Quote($id)." ";
			}
			$clause.=" ) ";
			
			/*the fields are the xml attributes*/
			$query='SELECT title as name, module as type, position, params  FROM '.$db->nameQuote($this->options['table']). ' WHERE '.$clause;
			$db->setQuery($query);
			$vet = $db->loadAssocList();
			
			//get the list of datasource ids
			$this->datasourceIDs = $this->getDataSourceList($vet);
			
			
			
			
			$xml="";
			foreach($vet as $module){
				$xml.=$this->exportModule($module);
			}
			return $xml;
						
			
		}
		else{
			return "";
		}				
	}
	
	
	
	protected function exportModule($module){
		$module['datasourcename'] = $this->getDataSourceName($module);
			$params = $this->getParams($module['params']);
		$module['datasourcetype'] = $params['datasourcetype'];		
		//we delete the datasource id from module params value	
			unset($params['datasourceid']);
		$paramString="";
		foreach($params as $key => $value){
			$paramString.="$key=$value\n";			
		}
			unset($module['params']);
		$module['params'] = $paramString;
		
		//create the xml string
		$xml="<{$this->options['tag']} ";				
		foreach($module as $key => $value){
	

			//check value
			if(strcasecmp(trim($value),"")==0){
				//skip
			}
			else{					
				//from utf-8 to html entities
				$value=$this->convertToHTMLEntities($value);
				
				if(strcasecmp(trim($key),"params")==0){
					$xml.=">";//close the head tag
					$xml.="$value";
				}else{
					$xml.=" $key=\"$value\" ";	
				}									
			}
			
		}
		$xml.="</{$this->options['tag']}>";				
		return $xml;	
	}
	
	
	protected function getDataSourceName($module){
		$params = $this->getParams($module['params']);
		$db =& JFactory::getDBO();
			
		//to avoid problem with character set
		$db->setQuery($this->options['characterset']);
		$db->query();
		
		$targetTable="";
		if(strcasecmp($params['datasourcetype'],"SQL")==0)$targetTable="#__arc_datasource_sql";
		if(strcasecmp($params['datasourcetype'],"Snippet")==0)$targetTable="#__arc_datasource_snippet";
		if(strcasecmp($params['datasourcetype'],"HTML")==0)$targetTable="#__arc_datasource_html";
		if(strcasecmp($params['datasourcetype'],"Article")==0)$targetTable="#__arc_datasource_article";
		
		$query='SELECT name  FROM '.$db->nameQuote($targetTable). ' WHERE '.$db->nameQuote('id').' = '.$db->Quote($params['datasourceid']);

		$db->setQuery($query);
		$vet = $db->loadAssoc();
		return $vet['name'];							
	}
	
	/**
	 * Starting by the parameter string, return an array representation of parameters
	 * 
	 * @param $string module parameters
	 * @return array representation of module parameters
	 */
	protected function getParams($string){
		if(!$string || strcasecmp(trim($string),"")==0){
			return false;	
		}
		else{
			//the params are stored as a string of couples (name=value)separated by \n characters
			$tmp = explode("\n",$string);
			/*create the array params which will be defined as for example 
				Array ( [datasourcetype] => SQL [datasourceid] => 1 [alive] => 0 [updatingtime] => [showupdatingtime] => 0 ... 
			 */
			$params = array();
			$i=0;
			foreach($tmp as $couple){
				//explode the couple by =
				//where com[0] is the parameter name, the rest part of array is the value
				$com = explode("=",$couple);
				//\n\n\n cause $com[0]="" we can skip it
				if(	strcasecmp(trim($com[0]),"")!=0	){
					$key = $com[0];			
					unset($com[0]);
					//this is necessary because the value can contain some = inside
					//example: parameter name is TITLE
					//parameter value is A = B = C
					$params[$key]=implode("=",$com);
					$i++;
				}
			}
			return $params;				
		}
	}
	
	/**
	 * Starting by the modules, return an array of datasources id as follow
	 *		$a[sql]
				id1 => true, id2=>true,...
			$a[html]
				id1 => true, id2=>true,...
			$a[article]
				id1 => true, id2=>true,...
			$a[snippet]
				id1 => true, id2=>true,...
	 * 
	 */
	private function getDataSourceList($modules){
		$datasources = array();
		foreach($modules as $module){
			$params = $this->getParams($module['params']);
			$key = strtolower($params['datasourcetype']);
			$dsid = $params['datasourceid'];
			$datasources[$key][$dsid]=true;			
		}
		return $datasources;
	}
	
	public function getDataSourceIDs(){
		return $this->datasourceIDs;
	}
	

}

