<?php


defined('_JEXEC') or die('Restricted Access');
jimport('joomla.application.component.model');
require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'engine'.DS.'destructors'.DS.'destructor.php');


/**
 * The following class destroyes the categories related to the dashboard
 * @version 1.0
 * @author  TobyTools.com (email:info@tobytools.com)(website: www.tobytools.com)
 * @copyright Copyright TobyTools.com 
 * @license GNU/GPL
 */


class ArchiDashDestructorCategory extends ArchiDashDestructor{

	/**
	 * SELECT b.title as name, count(*) as articles 
	 * FROM `jos_content` as a,`jos_categories` as b 
	 * WHERE a.catid=b.id AND (0 OR a.catid=1 OR a.catid=3 )
	 * GROUP BY a.catid
	 *
	 */
	
	/**
	 * 
	 * @var string, a variable to save the current category name 
	 */
	private $categoryName;
	

	function ArchiDashDestructorCategory($opts){
		parent::__construct();
		//$opts['dashboardid']
		$this->options = $opts;
		$this->table="#__categories";
		$this->options['characterset']=$this->characterset;//set characters
		$this->categoryName="";

		
	}

	//true, the class will use a specific precondition check, one for each category
	function preCondition(){
		return true;		
	}

	//destroy all categories of dashboard, if there are no more articles related with them
	function destroy(){
		global $mainframe;
		$preCondition = $this->preCondition();
		
		if(isset($this->options['dashboardid']) && $preCondition ){
			
			require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'models'.DS.'category.php');
			$classname="ArchiDashModelCategory";
			$model = new $classname();
			
			/*Category IDs related to dashboard*/
			$list = $model->selectAllRecordsByDashboardID($this->options['dashboardid']);
			if(!$list || count($list)<=0){
				//no element to destroy
				return;
			}
			
			foreach($list as $category){
										
				if($this->categoryPreCondition($category['categoryid'])){
					//destroy the record inside #__categories
					$this->destroyCategory($category['categoryid']);
				}
				else{
					//give a message to the user, the category is not deleted
					$this->errMsg=get_class($this)."::destroy (CATEGORY $this->categoryName) ".JText::_('NOPRECCATEGORY');
					$mainframe->enqueueMessage($this->errMsg,'notice');
				}
				/*Either cases need to clean the #__archidash_categories table*/
				$model->deleteRecord($category['id']);
				
			}
			
		}
		else{
			$this->completed=false;
			$this->errMsg.=get_class($this)."::destroy ".JText::_('NODASHBOARDID');
			$mainframe->enqueueMessage($this->errMsg,'error');
		}	
	
	}
	
	/**
	 * By giving a category id, it deletes the related record on #__categories
	 * 
	 * @param $catid category id
	 */
	private function destroyCategory($catid){
		
			require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'models'.DS.'category.php');
			$classname="ArchiDashModelCategory";
			$model = new $classname();
															
			
			$db =& JFactory::getDBO();
			//to avoid problem with character set
			$db->setQuery($this->characterset);
			$db->query();			
			$query = "DELETE FROM ".$db->nameQuote($this->table)." WHERE ".$db->nameQuote('id')." = ".$db->Quote($catid);										
			$db->setQuery($query);
			$db->query();												
			
						
	} 
	
	
	/**
	 * Since could be more than one category associated with a dashboard definition, it is necessary
	 * to check the precondition for each category.
	 * The pre-condition is NO articles related to the categori into the #__content table
	 * 
	 * @param  $catid the category id
	 * @return true if there are the pre-conditions to delete the category. False otherwise.
	 */
	private function categoryPreCondition($catid){
		$db =& JFactory::getDBO();
		//delete the menutype ONLY IF there are no menu items associated with it				
		$query=	'SELECT b.id as id, b.title as name, count(*) as articles 
					FROM '.$db->nameQuote('#__content').' as a, '.$db->nameQuote('#__categories').' as b  '.
				' WHERE '.$db->nameQuote('a.catid').' = '.$db->nameQuote('b.id').' AND '.$db->nameQuote('a.catid')." = ".$db->Quote($catid).' GROUP BY a.catid';
		

		$db->setQuery($query);
		$result = $db->loadAssoc();
		
		if(count($result)==0){

			return true;
		}
		else{
			//there are articles associated with the category
			$this->categoryName=$result['name'];
			return false;
		}
		
	}	
	
}



