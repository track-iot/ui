<?php
/**
 * @version		$Id: menutype.php 1.0 19-11-2009 Danijar
 * @package		Frontend Admin
 * @copyright	Copyright (C) 2009 CMSSpace. http://www.cmsspace.com
 * @license		GNU/GPL. http://www.gnu.org/licenses/gpl.html
 */
defined('_JEXEC') or die( 'Restricted access' );
jimport( 'joomla.application.component.model' );
class AikModelMenutype extends JModel
{
	var $_modelName = 'menutype';
	var $_table = null;
	function &getTable()
	{
		if ($this->_table == null) {
			$this->_table = & JTable::getInstance('menuTypes');
			if ($id = JRequest::getVar('id', false, '', 'int')) {
				$this->_table->load($id);
			}
		}
		return $this->_table;
	}
	function getMenus()
	{
		global $mainframe;

		$menus= array();
		$db = &$this->getDBO();
		$query = 'SELECT a.menutype, COUNT( a.menutype ) AS num' .
				' FROM #__menu AS a' .
				' WHERE a.published = 1' .
				' GROUP BY a.menutype';
		$db->setQuery( $query );
		$published = $db->loadObjectList( 'menutype' );

		$query = 'SELECT a.menutype, COUNT( a.menutype ) AS num' .
				' FROM #__menu AS a' .
				' WHERE a.published = 0' .
				' GROUP BY a.menutype';
		$db->setQuery( $query );
		$unpublished = $db->loadObjectList( 'menutype' );

		$query = 'SELECT a.menutype, COUNT( a.menutype ) AS num' .
				' FROM #__menu AS a' .
				' WHERE a.published = -2' .
				' GROUP BY a.menutype';
		$db->setQuery( $query );
		$trash = $db->loadObjectList( 'menutype' );

		$limit		= $mainframe->getUserStateFromRequest( 'global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int' );
		$limitstart = $mainframe->getUserStateFromRequest( 'com_menus.menus.limitstart', 'limitstart', 0, 'int' );

		$query = 'SELECT a.*, SUM(b.home) AS home' .
				' FROM #__menu_types AS a' .
				' LEFT JOIN #__menu AS b ON b.menutype = a.menutype' .
				' GROUP BY a.id';
		$db->setQuery( $query, $limitstart, $limit );
		$menuTypes	= $db->loadObjectList();

		$total		= count( $menuTypes );
		$i			= 0;
		for ($i = 0;  $i < $total; $i++) {
			$row = &$menuTypes[$i];

			$query = 'SELECT count( id )' .
					' FROM #__modules' .
					' WHERE module = "mod_mainmenu"' .
					' AND params LIKE '.$db->Quote('%menutype='.$row->menutype.'%');
			$db->setQuery( $query );
			$modules = $db->loadResult();

			if ( !$modules ) {
				$modules = '-';
			}
			$row->modules		= $modules;
			$row->published		= @$published[$row->menutype]->num ? $published[$row->menutype]->num : '-' ;
			$row->unpublished	= @$unpublished[$row->menutype]->num ? $unpublished[$row->menutype]->num : '-';
			$row->trash			= @$trash[$row->menutype]->num ? $trash[$row->menutype]->num : '-';
			$menus[] = $row;
		}
		return $menus;
	}

	function getPagination()
	{
		global $mainframe;

		$menutypes 	= MenusHelper::getMenuTypeList();
		$total		= count( $menutypes );
		$limit		= $mainframe->getUserStateFromRequest( 'global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int' );
		$limitstart = $mainframe->getUserStateFromRequest( 'com_menus.menus.limitstart', 'limitstart', 0, 'int' );

		jimport('joomla.html.pagination');
		$pagination = new JPagination( $total, $limitstart, $limit );
		return $pagination;
	}
	function getMenuItems()
	{
		$table = & $this->getTable();
		if ($table->menutype == '') {
			$table->menutype = JRequest::getString('menutype');
		}

		$db = &$this->getDBO();
		$query = 'SELECT a.name, a.id' .
				' FROM #__menu AS a' .
				' WHERE a.menutype = ' . $db->Quote( $table->menutype ) .
				' ORDER BY a.name';
		$db->setQuery( $query );
		$result = $db->loadObjectList();
		return $result;
	}
	function getModules( $type='' )
	{
		if ($type == '') {
			$type = $this->_table->menutype;
		}

		$db = &$this->getDBO();
		$query = 'SELECT id, title, params' .
				' FROM #__modules' .
				' WHERE module = "mod_mainmenu"' .
				' AND params LIKE ' . $db->Quote( '%menutype=' . $type . '%' );
		$db->setQuery( $query );
		$temp = $db->loadObjectList();

		$result = array();
		$n = count( $temp );
		for ($i = 0; $i < $n; $i++)
		{
			$params = new JParameter( $temp[$i]->params );
			if ($params->get( 'menutype' ) == $type) {
				 $result[] = $temp[$i];
			}
		}
		return $result;
	}
	function canDelete( $type='' )
	{
		if ($type == '') {
			$type = $this->_table->menutype;
		}
		if ($type == 'mainmenu') {
			$this->setError( JText::_( 'WARNDELMAINMENU' ) );
			return false;
		}
		return true;
	}
	function delete( $id = 0 )
	{
		$table = &$this->getTable();
		if ($id != 0) {
			$table->load( $id );
		}

		$db = &$this->getDBO();
		if (!$this->deleteByType( $table->menutype )) {
			$this->setError( $this->getError() );
			return false;
		}

		$moduleTable= &JTable::getInstance( 'module');
		$items		= &$this->getModules( $table->menutype );
		$modulesIds	= array();
		foreach ($items as $item)
		{
			if (!$moduleTable->delete( $item->id )) {
				$this->setError( $moduleTable->getErrorMsg() );
				return false;
			}
			$modulesIds[] = (int) $item->id;
		}

		if (count( $modulesIds )) {
			$query = 'DELETE FROM #__modules_menu' .
					' WHERE menuid = '.implode( ' OR moduleid = ', $modulesIds );
			$db->setQuery( $query );
			if (!$db->query()) {
				$this->setError( $menuTable->getErrorMsg() );
				return false;
			}
		}

		$result = $table->delete();

		return $result;
	}
	function deleteByType( $type = '' )
	{
		if (!$type) {
			return false;
		}
		$db = &$this->getDBO();
		$query = 'DELETE FROM #__menu' .
				' WHERE menutype = '.$db->Quote( $type );
		$db->setQuery( $query );
		if (!$db->query()) {
			$this->setError( $menuTable->getErrorMsg() );
			return false;
		}
		MenusHelper::cleanCache();
				
		return true;
	}
}
