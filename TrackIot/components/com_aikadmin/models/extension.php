<?php
/**
 * @version		$Id: extension.php 1.0 19-11-2009 Danijar
 * @package		Frontend Admin
 * @copyright	Copyright (C) 2009 CMSSpace. http://www.cmsspace.com
 * @license		GNU/GPL. http://www.gnu.org/licenses/gpl.html
 */
defined('_JEXEC') or die( 'Restricted access' );
jimport( 'joomla.application.component.model' );
class InstallerModel extends JModel
{
	var $_items = array();
	var $_pagination = null;
	function __construct()
	{
		global $mainframe;
		parent::__construct();
		$this->setState('pagination.limit',	$mainframe->getUserStateFromRequest('global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int'));
		$this->setState('pagination.offset',$mainframe->getUserStateFromRequest('com_extension.installer.limitstart.'.$this->_type, 'limitstart', 0, 'int'));
		$this->setState('pagination.total',	0);
	}
	function &getItems()
	{
		if (empty($this->_items)) {
			$this->_loadItems();
		}
		return $this->_items;
	}

	function &getPagination()
	{
		if (empty($this->_pagination)) {
			if (empty($this->_items)) {
				$this->_loadItems();
			}
			jimport('joomla.html.pagination');
			$this->_pagination = new JPagination($this->_state->get('pagination.total'), $this->_state->get('pagination.offset'), $this->_state->get('pagination.limit'));
		}
		return $this->_pagination;
	}
	function remove($eid=array())
	{
		global $mainframe;
		$failed = array ();
		if (!is_array($eid)) {
			$eid = array($eid => 0);
		}
		$db =& JFactory::getDBO();
		jimport('joomla.installer.installer');
		$installer = & JInstaller::getInstance();
		foreach ($eid as $id => $clientId)
		{
			$id		= trim( $id );
			$result	= $installer->uninstall($this->_type, $id, $clientId );
			if ($result === false) {
				$failed[] = $id;
			}
		}

		if (count($failed)) {
			$msg = JText::sprintf('UNINSTALLEXT', JText::_($this->_type), JText::_('Error'));
			$result = false;
		} else {
			$msg = JText::sprintf('UNINSTALLEXT', JText::_($this->_type), JText::_('Success'));
			$result = true;
		}

		$mainframe->enqueueMessage($msg);
		$this->setState('action', 'remove');
		$this->setState('name', $installer->get('name'));
		$this->setState('message', $installer->message);
		$this->setState('extension.message', $installer->get('extension.message'));

		return $result;
	}

	function _loadItems()
	{
		return JError::raiseError( 500, JText::_('Method Not Implemented'));
	}
}