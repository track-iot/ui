<?php


defined('_JEXEC') or die('Restricted Access');
jimport('joomla.application.component.model');
require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'engine'.DS.'factories'.DS.'factory.php');


/**
 * The following class create the section 
 * @version 1.0
 * @author  TobyTools.com (email:info@tobytools.com)(website: www.tobytools.com)
 * @copyright Copyright TobyTools.com 
 * @license GNU/GPL
 */


class ArchiDashFactorySection extends ArchiDashFactory{
	
	/*
	 *class constructor 
	 */
	function ArchiDashFactorySection($opts){
		parent::__construct($opts);
		//check everything is allright
		if($this->completed){
			
			$this->options['table']="#__sections";
			$this->options['tag']="section";		
		}
		else{
			global $mainframe;
			$mainframe->enqueueMessage(JText::_('NOOBJECT')." ".get_class($this),'error');
		}
		
	}
	/*
	 * create  section if it does not exist
	 */
	function create(){
		if($this->completed){
		
			global $mainframe;
			/*check if the section already exists.*/
			$db =& JFactory::getDBO();
				
			//to avoid problem with character set
			$db->setQuery($this->options['characterset']);
			$db->query();
			
			//section title
			$sectionTitle = $this->readDefinition($this->options['tag'],1,"name");
			//section alias
			$sectionAlias = $this->readDefinition($this->options['tag'],1,"alias");
			//section description
			$sectionDescription = $this->readDefinition($this->options['tag'],1,"description");
			
			//check the values are not empty
			if(strcasecmp(trim($sectionTitle),"")==0 || strcasecmp(trim($sectionAlias),"")==0 || strcasecmp(trim($sectionDescription),"")==0){
				$this->completed=false;
				$this->errMsg.=get_class($this)."::create ".JText::_('DEFINITIONNOTVALID');
				$mainframe->enqueueMessage($this->errMsg,'error');
				return false;
			}
			
			$query='SELECT * FROM '.$db->nameQuote($this->options['table']). ' WHERE '.$db->nameQuote('title').' = '.$db->Quote($sectionTitle);
			$db->setQuery($query);
			$vet = $db->loadAssoc();
			if(!$vet){
				//the section needs to be created
				$query = "INSERT INTO ".$db->nameQuote($this->options['table'])." (title,alias,scope,description,published,access) VALUES ('$sectionTitle','$sectionAlias','content','$sectionDescription','1','{$this->options['accesslevel']}')";			
				$db->setQuery($query);
				$result = $db->query();
				
				if(!$result){
					//something wrong
					$this->completed=false;
					$this->errMsg .= $db->getErrorNum()." : ".$db->getErrorMsg();
					$mainframe->enqueueMessage(JText::_( 'QUERYKO' ),'error');
					$mainframe->enqueueMessage($this->errMsg,'error');
					$mainframe->enqueueMessage(get_class($this)."::create",'error');
					return false;
				}
				else{
					//clean
					$this->errMsg .= false;
					//provide the element id
					$db->setQuery("SELECT LAST_INSERT_ID()");				
					$this->options['sectionid']= $db->loadResult();
				}
				
				
				//output message
				$this->msg.="<h3>".JText::_('Section')."</h3>$sectionTitle.<br/>";			
				
			}
			else{
				//the section already exists
				$this->options['sectionid']=$vet['id'];	
				//output message
				$this->msg.="<h3>".JText::_('Section')."</h3>***$sectionTitle.<br/>";
			}
			//save the information inside #__archidash_sections
			require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'models'.DS.'section.php');
			$classname="ArchiDashModelSection";
			$model = new $classname();
			$vet = array();
				$vet['sectionid']=$this->options['sectionid'];
				$vet['dashboardid']=$this->options['dashboardid'];
			
			$model->insertRecord($vet);
			//check there are errors for the creation of record with the model
			if($model->getErrMsg()!=false){
				$this->completed=false;
				$this->errMsg.=$model->getErrMsg();
			}
			
		}
		else{
			//no op
		}
	}

}

