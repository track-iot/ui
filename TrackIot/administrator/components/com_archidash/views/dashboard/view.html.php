<?php



// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view' );

/**
 * @version 1.0
 * @author  TobyTools.com (email:info@tobytools.com)(website: www.tobytools.com)
 * @copyright Copyright TobyTools.com 
 * @license GNU/GPL
 */

class ArchiDashViewDashboard extends JView{
	
	function display($tpl = null){
		require_once( JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'views'.DS.'dashboard'.DS.'tmpl'.DS.'dashboard.php' );
		$classname = "ArchiDashLayoutDashboard";
		$layout = new $classname();			
		$layout->display();
	}
	
	function edit($record=false){
		require_once( JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'views'.DS.'dashboard'.DS.'tmpl'.DS.'dashboard.php' );
		$classname = "ArchiDashLayoutDashboard";
		$layout = new $classname();						
		$layout->edit($record);
	}
	
	
	function remove($tpl = null){
		require_once( JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'views'.DS.'dashboard'.DS.'tmpl'.DS.'dashboard.php' );
		$classname = "ArchiDashLayoutDashboard";
		$layout = new $classname();			
		$layout->remove();
	}
	
	function add($definitions=false){
		require_once( JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'views'.DS.'dashboard'.DS.'tmpl'.DS.'dashboard.php' );
		$classname = "ArchiDashLayoutDashboard";
		$layout = new $classname();			
		$layout->add($definitions);
	}
	
	
	function details($details=false){
		require_once( JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'views'.DS.'dashboard'.DS.'tmpl'.DS.'dashboard.php' );
		$classname = "ArchiDashLayoutDashboard";
		$layout = new $classname();			
		$layout->details($details);
	}
	
	
}

?>

