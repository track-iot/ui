<?php


defined('_JEXEC') or die('Restricted Access');
jimport('joomla.application.component.model');
require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'engine'.DS.'destructors'.DS.'destructor.php');


/**
 * The following class destroyes the Section related to the dashboard
 * @version 1.0
 * @author  TobyTools.com (email:info@tobytools.com)(website: www.tobytools.com)
 * @copyright Copyright TobyTools.com 
 * @license GNU/GPL
 */


class ArchiDashDestructorSection extends ArchiDashDestructor{

	/**
	 * 
	 * @var string, used to save the current name of section
	 */
	private $sectionName;

	function ArchiDashDestructorSection($opts){
		parent::__construct();
		//$opts['dashboardid']
		$this->options = $opts;
		$this->table="#__sections";
		$this->options['characterset']=$this->characterset;//set characters
		$this->sectionName="";
		
	}
	
	function preCondition(){
		$db =& JFactory::getDBO();
		//delete the section ONLY IF there are no categories associated with it				
		$query=	'SELECT a.sectionid as id, b.title as title, count(*) as categories FROM '.$db->nameQuote('#__archidash_sections').' as a, '.$db->nameQuote('#__sections').' as b, '.$db->nameQuote('#__categories').' as c  '.
				' WHERE '.$db->nameQuote('dashboardid').' = '.$db->Quote($this->options['dashboardid']).
				' AND '.$db->nameQuote('a.sectionid').' = '.$db->nameQuote('b.id').
				' AND '.$db->nameQuote('b.id').' = '.$db->nameQuote('c.section').
				' GROUP BY a.sectionid';
		$db->setQuery($query);
		//check only for one record (no loadAssocList)
		$list = $db->loadAssoc();
		if(count($list)>0){
			$this->sectionName = $list['title'];
			return false;
		}
		else{
			return true;	
		}
		
	}

	function destroy(){
		global $mainframe;
		$preCondition = $this->preCondition();
		
		if(isset($this->options['dashboardid']) && $preCondition ){
			
			require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'models'.DS.'section.php');
			$classname="ArchiDashModelSection";
			$model = new $classname();
			
			/*SECTION IDs related to dashboard*/
			$list = $model->selectAllRecordsByDashboardID($this->options['dashboardid']);
			if(!$list || count($list)<=0){
				//no element to destroy
				return;
			}
			
			
			/*DESTROY SECTION*/
			//work on #__sections
			$db =& JFactory::getDBO();
			//to avoid problem with character set
			$db->setQuery($this->characterset);
			$db->query();
			
			$query = "DELETE FROM ".$db->nameQuote($this->table)." 
						WHERE 0 ";
			
			//create where clause
			$where=" ";
			foreach($list as $element){
				$id = $element['sectionid'];
				$where.=" OR ".$db->nameQuote('id')." = ".$db->Quote($id);				
			}
			$where.=" ";
			$query.=$where;			
			$db->setQuery($query);
			$db->query();
			
			
		
			
			/*DELETE THE RELATED RECORDS INTO #__archidash_archies*/
			$model->deleteAllRecordsByDashboardID($this->options['dashboardid']);
			
			
			
			
			
			
		}
		else{
			if(!isset($this->options['dashboardid'])){			
				$this->completed=false;
				$this->errMsg.=get_class($this)."::destroy ".JText::_('NODASHBOARDID');
				$mainframe->enqueueMessage($this->errMsg,'error');
			}
			if(!$preCondition){
				/*
				 *Even though there is no condition, it's necessary to clean the archidash tables 
				 */
				require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'models'.DS.'section.php');
				$classname="ArchiDashModelSection";
				$model = new $classname();
				$model->deleteAllRecordsByDashboardID($this->options['dashboardid']);
				/**/
				//alert the user
				$this->completed=false;				
				$this->errMsg.=get_class($this)."::destroy (SECTION ".$this->sectionName.") ".JText::_('NOPRECSECTION');
				$mainframe->enqueueMessage($this->errMsg,'notice');
			}
			
		}
	}
}



