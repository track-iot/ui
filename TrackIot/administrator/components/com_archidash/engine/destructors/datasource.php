<?php


defined('_JEXEC') or die('Restricted Access');
jimport('joomla.application.component.model');
require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'engine'.DS.'destructors'.DS.'destructor.php');


/**
 * The following class destroyes the archimede datasources related to the dashboard
 * @version 1.0
 * @author  TobyTools.com (email:info@tobytools.com)(website: www.tobytools.com)
 * @copyright Copyright TobyTools.com 
 * @license GNU/GPL
 */


class ArchiDashDestructorDataSource extends ArchiDashDestructor{


	function ArchiDashDestructorDataSource($opts){
		parent::__construct();
		//$opts['dashboardid']
		$this->options = $opts;
		$this->table="";
		$this->options['characterset']=$this->characterset;//set characters
		
	}
	
	function preCondition(){
		//no precondition
		return true;
	}
	
	
	

	function destroy(){
		global $mainframe;
		if($this->preCondition() && isset($this->options['dashboardid'])){
			
			require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'models'.DS.'datasource.php');
			$classname="ArchiDashModelDataSource";
			$model = new $classname();
			
			/*Datasource IDs related to dashboard*/
			$list = $model->selectAllRecordsByDashboardID($this->options['dashboardid']);
			if(!$list || count($list)<=0){
				//no element to destroy
				return;
			}
			
			foreach($list as $ds){
				if(strcasecmp(trim($ds['type']),"SQL")==0) $this->destroySQLDataSource($ds['datasourceid']);

				if(strcasecmp(trim($ds['type']),"HTML")==0) $this->destroyHTMLDataSource($ds['datasourceid']);
				
				if(strcasecmp(trim($ds['type']),"Snippet")==0) $this->destroySnippetDataSource($ds['datasourceid']);
				
				if(strcasecmp(trim($ds['type']),"Article")==0) $this->destroyArticleDataSource($ds['datasourceid']);
			}									
			/**/
			
			/*DELETE THE RELATED RECORDS INTO #__archidash_datasources*/
			$model->deleteAllRecordsByDashboardID($this->options['dashboardid']);
			
			
			
			
		}
		else{			
			$this->completed=false;
			$this->errMsg.=get_class($this)."::destroy ".JText::_('NODASHBOARDID');
			$mainframe->enqueueMessage($this->errMsg,'error');
		}
	}
	
	
	/**
	 * By giving the id, destroy the SQL datasource 
	 * 
	 * @param $id, the SQL datasource id
	 */
	private function destroySQLDataSource($id){
		$db =& JFactory::getDBO();
		
		//to avoid problem with character set
		$db->setQuery($this->characterset);
		$db->query();
		
		$query = "DELETE FROM ".$db->nameQuote("#__arc_datasource_sql")." 
					WHERE ".$db->nameQuote('id')." = ".$db->Quote($id);
		
		$db->setQuery($query);
		$db->query();
		
	}
	
	/**
	 * By giving the id, destroy the HTML datasource 
	 * 
	 * @param $id, the HTML datasource id
	 */
	private function destroyHTMLDataSource($id){
		$db =& JFactory::getDBO();
		
		//to avoid problem with character set
		$db->setQuery($this->characterset);
		$db->query();
		
		$query = "DELETE FROM ".$db->nameQuote("#__arc_datasource_html")." 
					WHERE ".$db->nameQuote('id')." = ".$db->Quote($id);
		
		$db->setQuery($query);
		$db->query();
		
	}
	
	/**
	 * By giving the id, destroy the Article datasource 
	 * 
	 * @param $id, the Article datasource id
	 */
	private function destroyArticleDataSource($id){
		$db =& JFactory::getDBO();
		
		//to avoid problem with character set
		$db->setQuery($this->characterset);
		$db->query();
		
		$query = "DELETE FROM ".$db->nameQuote("#__arc_datasource_article")." 
					WHERE ".$db->nameQuote('id')." = ".$db->Quote($id);
		
		$db->setQuery($query);
		$db->query();
		
	}
	/**
	 * By giving the id, destroy the Snippet datasource 
	 * 
	 * @param $id, the Snippet datasource id
	 */
	private function destroySnippetDataSource($id){
		$db =& JFactory::getDBO();
		
		//to avoid problem with character set
		$db->setQuery($this->characterset);
		$db->query();
		
		$query = "DELETE FROM ".$db->nameQuote("#__arc_datasource_snippet")." 
					WHERE ".$db->nameQuote('id')." = ".$db->Quote($id);
		
		$db->setQuery($query);
		$db->query();
		
	}
	
}
