DROP TABLE IF EXISTS #__archidash_dashboards;
DROP TABLE IF EXISTS #__archidash_articles;
DROP TABLE IF EXISTS #__archidash_categories;
DROP TABLE IF EXISTS #__archidash_sections;
DROP TABLE IF EXISTS #__archidash_menu_types;
DROP TABLE IF EXISTS #__archidash_menus;
DROP TABLE IF EXISTS #__archidash_datasources;
DROP TABLE IF EXISTS #__archidash_modules;
DROP TABLE IF EXISTS #__archidash_archies;
DROP TABLE IF EXISTS #__archidash_definitions;
DROP TABLE IF EXISTS #__archidash_years;
DROP TABLE IF EXISTS #__archidash_months;
DROP TABLE IF EXISTS #__archidash_hours;
DROP TABLE IF EXISTS #__archidash_calendar;




