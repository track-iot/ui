<?php


require_once('../config/lang/eng.php');
require_once('../tcpdf.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('LOR-TRACKIOT');

$pdf->SetSubject('Actuator_PDF');
$pdf->SetKeywords('TRACKIOT, FILE-PDF');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.'', PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------

// add a page
$pdf->AddPage();

// set font
$pdf->SetFont('helvetica', 'B', 20);




include('\wamp\www\TrackIot\BDD.php');

// create some HTML content





if (isset($_GET['id'])) 
{

    $id=$_GET['id'];
	
    
			   $pdf->SetTitle('Actuator_'.$id);
				$pdf->Write(0, "The card actuator's [ ".$id." ]", '', 0, 'L', true, 0, false, false, 0);
			
				$query='SELECT * FROM Actuators where id_actuator='.$id;
				
				
				$requete = mysql_query( $query, $cnx ) ;

				
				while( $donnees = mysql_fetch_array( $requete ) )

				{
			
					
					
					$html='<br><br><br><center><table border="1">
					
				
						  
						<tr>
							<td> <strong>ID :</strong> </td>
							<td align="center">	'.$donnees["id_actuator"].'</td>
						</tr>
						  
						  <tr>
								<td> <strong>Name :</strong></td> 
								 <td align="center">'.ucwords($donnees["name"]).'</td></tr>
								 
						 <tr>
							<td> <strong>Type :</strong> </td>							 
							<td align="center">	 '.strtoupper($donnees["type"]).'</td></tr>
							
						<tr>
							<td> <strong>Status :</strong> </td>							 
							<td align="center">	 '.strtoupper($donnees["state"]).'</td></tr>
							
				
							
						<tr>
							<td> <strong>Location :</strong> </td>							 
							<td align="center">	 '.ucwords($donnees["location"]).'</td></tr>
						<tr>
							<td> <strong>Vendor :</strong> </td>							 
							<td align="center">	 '.$donnees["vendor_id"].'</td></tr>
						<tr>
							<td> <strong>Product :</strong> </td>							 
							<td align="center">	 '.$donnees["product"].'</td></tr>
						<tr>
							<td> <strong>Serial :</strong> </td>							 
							<td align="center">	 '.$donnees["serial"].'</td></tr>
					
						<tr>
							<td> <strong>Comment :</strong> </td>							 
							<td align="center">	 '.ucwords($donnees["comment"]).'</td></tr>
						
										
						
					</table><center>';
			
		
				}
				
				
		
}


	



// set core font
$pdf->SetFont('helvetica', '', 10);

// output the HTML content
$pdf->writeHTML($html, true, 0, true, true);

$pdf->Ln();

// set UTF-8 Unicode font
$pdf->SetFont('dejavusans', '', 10);

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('Actuator_'.$id.'.pdf', 'I');

//============================================================+
// END OF FILE                                                
//============================================================+
