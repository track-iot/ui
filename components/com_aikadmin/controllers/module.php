<?php
/**
 * @version		$Id: module.php 1.0 19-11-2009 Danijar
 * @package		Frontend Admin
 * @copyright	Copyright (C) 2009 CMSSpace. http://www.cmsspace.com
 * @license		GNU/GPL. http://www.gnu.org/licenses/gpl.html
 */
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.controller' );

$client	= JRequest::getVar('client', 0, '', 'int');
if ($client == 1) {
	JSubMenuHelper::addEntry(JText::_('Site'), 'index.php?option=com_aikadmin&c=module&client_id=0');
	JSubMenuHelper::addEntry(JText::_('Administrator'), 'index.php?option=com_aikadmin&c=module&client=1', true );
} else {
	JSubMenuHelper::addEntry(JText::_('Site'), 'index.php?option=com_aikadmin&c=module&client_id=0', true );
	JSubMenuHelper::addEntry(JText::_('Administrator'), 'index.php?option=com_aikadmin&c=module&client=1');
}

class AikControllerModule extends JController
{
	function __construct()
	{
		parent::__construct(array('default_task' => 'view'));

		$this->registerTask( 'apply', 			'save' );
		$this->registerTask( 'unpublish', 		'publish' );
		$this->registerTask( 'orderup', 		'reorder' );
		$this->registerTask( 'orderdown', 		'reorder' );
		$this->registerTask( 'accesspublic', 	'access' );
		$this->registerTask( 'accessregistered','access' );
		$this->registerTask( 'accessspecial',	'access' );
	}
	function view()
	{
		global $mainframe;

		$db		=& JFactory::getDBO();
		$client	=& JApplicationHelper::getClientInfo(JRequest::getVar('client', '0', '', 'int'));
		$option	= 'com_aikadmin.module';

		$filter_order		= $mainframe->getUserStateFromRequest( 'com_module.filter_order',		'filter_order',		'm.position',	'cmd' );
		$filter_order_Dir	= $mainframe->getUserStateFromRequest( 'com_module.filter_order_Dir',	'filter_order_Dir',	'',				'word' );
		$filter_state		= $mainframe->getUserStateFromRequest( 'com_module.filter_state',		'filter_state',		'',				'word' );
		$filter_position	= $mainframe->getUserStateFromRequest( 'com_module.filter_position',	'filter_position',	'',				'cmd' );
		$filter_type		= $mainframe->getUserStateFromRequest( 'com_module.filter_type',		'filter_type',		'',				'cmd' );
		$filter_assigned	= $mainframe->getUserStateFromRequest( 'com_module.filter_assigned',	'filter_assigned',	'',				'cmd' );
		$search				= $mainframe->getUserStateFromRequest( 'com_module.search',			'search',			'',				'string' );
		$search				= JString::strtolower( $search );

		$limit		= $mainframe->getUserStateFromRequest( 'global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int' );
		$limitstart	= $mainframe->getUserStateFromRequest( 'com_module.limitstart', 'limitstart', 0, 'int' );

		JToolBarHelper::title( JText::_( 'Module Manager' ), 'module.png' );
		JToolBarHelper::publishList();
		JToolBarHelper::unpublishList();
		JToolBarHelper::custom( 'copy', 'copy.png', 'copy_f2.png', 'Copy', true );
		JToolBarHelper::deleteList();
		JToolBarHelper::editListX();
		JToolBarHelper::addNewX();
		JToolBarHelper::help( 'screen.modules' );
		JToolBarHelper::cpanel();
		
		$where[] = 'm.client_id = '.(int) $client->id;

		$joins[] = 'LEFT JOIN #__users AS u ON u.id = m.checked_out';
		$joins[] = 'LEFT JOIN #__groups AS g ON g.id = m.access';
		$joins[] = 'LEFT JOIN #__modules_menu AS mm ON mm.moduleid = m.id';

		if ( $filter_assigned ) {
			$joins[] = 'LEFT JOIN #__templates_menu AS t ON t.menuid = mm.menuid';
			$where[] = 't.template = '.$db->Quote($filter_assigned);
		}
		if ( $filter_position ) {
			$where[] = 'm.position = '.$db->Quote($filter_position);
		}
		if ( $filter_type ) {
			$where[] = 'm.module = '.$db->Quote($filter_type);
		}
		if ( $search ) {
			$where[] = 'LOWER( m.title ) LIKE '.$db->Quote( '%'.$db->getEscaped( $search, true ).'%', false );
		}
		if ( $filter_state ) {
			if ( $filter_state == 'P' ) {
				$where[] = 'm.published = 1';
			} else if ($filter_state == 'U' ) {
				$where[] = 'm.published = 0';
			}
		}

		$where 		= ' WHERE ' . implode( ' AND ', $where );
		$join 		= ' ' . implode( ' ', $joins );
		if ($filter_order == 'm.ordering') {
			$orderby = ' ORDER BY m.position, m.ordering '. $filter_order_Dir;
		} else {
			$orderby = ' ORDER BY '. $filter_order .' '. $filter_order_Dir .', m.ordering ASC';
		}

		$query = 'SELECT COUNT(DISTINCT m.id)'
		. ' FROM #__modules AS m'
		. $join
		. $where
		;
		$db->setQuery( $query );
		$total = $db->loadResult();

		jimport('joomla.html.pagination');
		$pageNav = new JPagination( $total, $limitstart, $limit );

		$query = 'SELECT m.*, u.name AS editor, g.name AS groupname, MIN(mm.menuid) AS pages'
		. ' FROM #__modules AS m'
		. $join
		. $where
		. ' GROUP BY m.id'
		. $orderby
		;
		$db->setQuery( $query, $pageNav->limitstart, $pageNav->limit );
		$rows = $db->loadObjectList();
		if ($db->getErrorNum()) {
			echo $db->stderr();
			return false;
		}

		$query = 'SELECT m.position AS value, m.position AS text'
		. ' FROM #__modules as m'
		. ' WHERE m.client_id = '.(int) $client->id
		. ' GROUP BY m.position'
		. ' ORDER BY m.position'
		;
		$positions[] = JHTML::_('select.option',  '0', '- '. JText::_( 'Select Position' ) .' -' );
		$db->setQuery( $query );
		$positions = array_merge( $positions, $db->loadObjectList() );
		$lists['position']	= JHTML::_('select.genericlist',   $positions, 'filter_position', 'class="inputbox" size="1" onchange="this.form.submit()"', 'value', 'text', "$filter_position" );

		$query = 'SELECT module AS value, module AS text'
		. ' FROM #__modules'
		. ' WHERE client_id = '.(int) $client->id
		. ' GROUP BY module'
		. ' ORDER BY module'
		;
		$db->setQuery( $query );
		$types[] 		= JHTML::_('select.option',  '0', '- '. JText::_( 'Select Type' ) .' -' );
		$types 			= array_merge( $types, $db->loadObjectList() );
		$lists['type']	= JHTML::_('select.genericlist',   $types, 'filter_type', 'class="inputbox" size="1" onchange="this.form.submit()"', 'value', 'text', "$filter_type" );

		$lists['state']	= JHTML::_('grid.state',  $filter_state );

		$query = 'SELECT DISTINCT(template) AS text, template AS value'.
				' FROM #__templates_menu' .
				' WHERE client_id = '.(int) $client->id;
		$db->setQuery( $query );
		$assigned[]		= JHTML::_('select.option',  '0', '- '. JText::_( 'Select Template' ) .' -' );
		$assigned 		= array_merge( $assigned, $db->loadObjectList() );
		$lists['assigned']	= JHTML::_('select.genericlist',   $assigned, 'filter_assigned', 'class="inputbox" size="1" onchange="this.form.submit()"', 'value', 'text', "$filter_assigned" );

		$lists['order_Dir']	= $filter_order_Dir;
		$lists['order']		= $filter_order;

		$lists['search']= $search;

		$view =& $this->getView( 'module' );
		$view->view( $rows, $client, $pageNav, $lists );
	}

	function copy()
	{
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$db 	=& JFactory::getDBO();
		$client	=& JApplicationHelper::getClientInfo(JRequest::getVar('client', '0', '', 'int'));
		$this->setRedirect( 'index.php?option=com_aikadmin&c=module&client='.$client->id );

		$cid	= JRequest::getVar( 'cid', array(), 'post', 'array' );
		$n		= count( $cid );

		if ($n == 0) {
			return JError::raiseWarning( 500, JText::_( 'No items selected' ) );
		}

		$row 	=& JTable::getInstance('module');
		$tuples	= array();

		foreach ($cid as $id)
		{
			$row->load( (int) $id );
			$row->title 		= JText::sprintf( 'Copy of', $row->title );
			$row->id 			= 0;
			$row->iscore 		= 0;
			$row->published 	= 0;

			if (!$row->check()) {
				return JError::raiseWarning( 500, $row->getError() );
			}
			if (!$row->store()) {
				return JError::raiseWarning( 500, $row->getError() );
			}
			$row->checkin();

			$row->reorder( 'position='.$db->Quote( $row->position ).' AND client_id='.(int) $client->id );

			$query = 'SELECT menuid'
			. ' FROM #__modules_menu'
			. ' WHERE moduleid = '.(int) $cid[0]
			;
			$db->setQuery( $query );
			$rows = $db->loadResultArray();

			foreach ($rows as $menuid) {
				$tuples[] = '('.(int) $row->id.','.(int) $menuid.')';
			}
		}

		if (!empty( $tuples ))
		{
			$query = 'INSERT INTO #__modules_menu (moduleid,menuid) VALUES '.implode( ',', $tuples );
			$db->setQuery( $query );
			if (!$db->query()) {
				return JError::raiseWarning( 500, $db->getError() );
			}
		}

		$msg = JText::sprintf( 'Items Copied', $n );
		$this->setRedirect( 'index.php?option=com_aikadmin&c=module&client='. $client->id, $msg );
	}

	function save()
	{
		JRequest::checkToken() or jexit( 'Invalid Token' );

		global $mainframe;

		$db		=& JFactory::getDBO();
		$client	=& JApplicationHelper::getClientInfo(JRequest::getVar('client', '0', '', 'int'));
		$this->setRedirect( 'index.php?option=com_aikadmin&c=module&client='.$client->id );

		$post	= JRequest::get( 'post' );
		$post['content']   = JRequest::getVar( 'content', '', 'post', 'string', JREQUEST_ALLOWRAW );
		$post['client_id'] = $client->id;

		$row =& JTable::getInstance('module');

		if (!$row->bind( $post, 'selections' )) {
			return JError::raiseWarning( 500, $row->getError() );
		}

		if (!$row->check()) {
			return JError::raiseWarning( 500, $row->getError() );
		}

		if (!$row->id) {
			$where = 'position='.$db->Quote( $row->position ).' AND client_id='.(int) $client->id ;
			$row->ordering = $row->getNextOrder( $where );
		}

		if (!$row->store()) {
			return JError::raiseWarning( 500, $row->getError() );
		}
		$row->checkin();

		$menus = JRequest::getVar( 'menus', '', 'post', 'word' );
		$selections = JRequest::getVar( 'selections', array(), 'post', 'array' );
		JArrayHelper::toInteger($selections);

		$query = 'DELETE FROM #__modules_menu'
		. ' WHERE moduleid = '.(int) $row->id
		;
		$db->setQuery( $query );
		if (!$db->query()) {
			return JError::raiseWarning( 500, $db->getError() );
		}
		if ( $menus == 'all' ) {
			$query = 'INSERT INTO #__modules_menu'
			. ' SET moduleid = '.(int) $row->id.' , menuid = 0'
			;
			$db->setQuery( $query );
			if (!$db->query()) {
				return JError::raiseWarning( 500, $db->getError() );
			}
		}
		else
		{
			foreach ($selections as $menuid)
			{
				if ( (int) $menuid >= 0 ) {
					$query = 'INSERT INTO #__modules_menu'
					. ' SET moduleid = '.(int) $row->id .', menuid = '.(int) $menuid
					;
					$db->setQuery( $query );
					if (!$db->query()) {
						return JError::raiseWarning( 500, $db->getError() );
					}
				}
			}
		}
		$cache =& JFactory::getCache();
		$cache->remove($row->id . '0', $row->module);
		$cache->remove($row->id . '1', $row->module);
		$cache->remove($row->id . '2', $row->module);
		$cache->clean( 'com_content' );
		
		$this->setMessage( JText::_( 'Item saved' ) );
		switch ($this->getTask())
		{
			case 'apply':
				$this->setRedirect( 'index.php?option=com_aikadmin&c=module&client='. $client->id .'&task=edit&id='. $row->id );
				break;
		}
	}
	function edit( )
	{
		$db 	=& JFactory::getDBO();
		$user 	=& JFactory::getUser();

		$client	=& JApplicationHelper::getClientInfo(JRequest::getVar('client', '0', '', 'int'));
		$module = JRequest::getVar( 'module', '', '', 'cmd' );
		$id 	= JRequest::getVar( 'id', 0, 'method', 'int' );
		$cid 	= JRequest::getVar( 'cid', array( $id ), 'method', 'array' );
		JArrayHelper::toInteger($cid, array(0));

				$moduleType = JRequest::getCmd( 'module' );
		$cid 		= JRequest::getVar( 'cid', array(0), '', 'array' );
		JArrayHelper::toInteger($cid, array(0));

		JToolBarHelper::title( JText::_( 'Module' ) . ': <small><small>[ '. JText::_( 'Edit' ) .' ]</small></small>', 'module.png' );

		if($moduleType == 'custom') {
			JToolBarHelper::Preview('index.php?option=com_aikadmin&c=modules&tmpl=component&client='.$client->id.'&pollid='.$cid[0]);
		}

		JToolBarHelper::save();
		JToolBarHelper::apply();
		if ( $cid[0] ) {
			JToolBarHelper::cancel( 'cancel', 'Close' );
		} else {
			JToolBarHelper::cancel();
		}
		JToolBarHelper::help( 'screen.modules.edit' );
		JToolBarHelper::cpanel();
		
		$model	= &$this->getModel('module');
		$model->setState( 'id',			$cid[0] );
		$model->setState( 'clientId',	$client->id );

		$lists 	= array();
		$row 	=& JTable::getInstance('module');
		$row->load( (int) $cid[0] );
		if ($row->isCheckedOut( $user->get('id') )) {
			$this->setRedirect( 'index.php?option=com_aikadmin&c=module&client='.$client->id );
			return JError::raiseWarning( 500, JText::sprintf( 'DESCBEINGEDITTED', JText::_( 'The module' ), $row->title ) );
		}

		$row->content = htmlspecialchars($row->content, ENT_COMPAT, 'UTF-8');

		if ( $cid[0] ) {
			$row->checkout( $user->get('id') );
		}
		if ($cid[0] == 0) {
			$row->position 	= 'left';
			$row->showtitle = true;
			$row->published = 1;
			$row->module 	= $module;
		}

		if ($client->id == 1)
		{
			$where 				= 'client_id = 1';
			$lists['client_id'] = 1;
			$path				= 'mod1_xml';
		}
		else
		{
			$where 				= 'client_id = 0';
			$lists['client_id'] = 0;
			$path				= 'mod0_xml';
		}

		$query = 'SELECT position, ordering, showtitle, title'
		. ' FROM #__modules'
		. ' WHERE '. $where
		. ' ORDER BY ordering'
		;
		$db->setQuery( $query );
		$orders = $db->loadObjectList();
		if ($db->getErrorNum()) {
			echo $db->stderr();
			return false;
		}

		$orders2 	= array();

		$l = 0;
		$r = 0;
		for ($i=0, $n=count( $orders ); $i < $n; $i++) {
			$ord = 0;
			if (array_key_exists( $orders[$i]->position, $orders2 )) {
				$ord =count( array_keys( $orders2[$orders[$i]->position] ) ) + 1;
			}

			$orders2[$orders[$i]->position][] = JHTML::_('select.option',  $ord, $ord.'::'.addslashes( $orders[$i]->title ) );
		}

		if ( $cid[0] ) {
			$query = 'SELECT menuid AS value'
			. ' FROM #__modules_menu'
			. ' WHERE moduleid = '.(int) $row->id
			;
			$db->setQuery( $query );
			$lookup = $db->loadObjectList();
			if (empty( $lookup )) {
				$lookup = array( JHTML::_('select.option',  '-1' ) );
				$row->pages = 'none';
			} elseif (count($lookup) == 1 && $lookup[0]->value == 0) {
				$row->pages = 'all';
			} else {
				$row->pages = null;
			}
		} else {
			$lookup = array( JHTML::_('select.option',  0, JText::_( 'All' ) ) );
			$row->pages = 'all';
		}

		if ( $row->access == 99 || $row->client_id == 1 || $lists['client_id'] ) {
			$lists['access'] 			= 'Administrator';
			$lists['showtitle'] 		= 'N/A <input type="hidden" name="showtitle" value="1" />';
			$lists['selections'] 		= 'N/A';
		} else {
			if ( $client->id == '1' ) {
				$lists['access'] 		= 'N/A';
				$lists['selections'] 	= 'N/A';
			} else {
				$lists['access'] 		= JHTML::_('list.accesslevel',  $row );

				$selections				= JHTML::_('menu.linkoptions');
				$lists['selections']	= JHTML::_('select.genericlist',   $selections, 'selections[]', 'class="inputbox" size="15" multiple="multiple"', 'value', 'text', $lookup, 'selections' );
			}
			$lists['showtitle'] = JHTML::_('select.booleanlist',  'showtitle', 'class="inputbox"', $row->showtitle );
		}

		$lists['published'] = JHTML::_('select.booleanlist',  'published', 'class="inputbox"', $row->published );

		$row->description = '';

		$lang =& JFactory::getLanguage();
		if ( $client->id != '1' ) {
			$lang->load( trim($row->module), JPATH_SITE );
		} else {
			$lang->load( trim($row->module) );
		}

		if ($row->module == 'custom') {
			$xmlfile = JApplicationHelper::getPath( $path, 'mod_custom' );
		} else {
			$xmlfile = JApplicationHelper::getPath( $path, $row->module );
		}

		$data = JApplicationHelper::parseXMLInstallFile($xmlfile);
		if ($data)
		{
			foreach($data as $key => $value) {
				$row->$key = $value;
			}
		}

		$params = new JParameter( $row->params, $xmlfile, 'module' );

		$view =& $this->getView( 'module' );
		$view->edit( $model, $row, $orders2, $lists, $params, $client );
	}
	function add()
	{
		global $mainframe;

		$modules	= array();
		$client		=& JApplicationHelper::getClientInfo(JRequest::getVar('client', '0', '', 'int'));

		JToolBarHelper::title( JText::_( 'Module' ) . ': <small><small>[ '. JText::_( 'New' ) .' ]</small></small>', 'module.png' );
		JToolBarHelper::customX( 'edit', 'forward.png', 'forward_f2.png', 'Next', true );
		JToolBarHelper::cancel();
		if ($client->name == 'site') {
			JToolBarHelper::help( 'screen.modulessite.edit' );
		} 
		else {
			JToolBarHelper::help( 'screen.modulesadministrator.edit');
		}
		JToolBarHelper::cpanel();
		
		if ($client->id == '1') {
			$path		= JPATH_ADMINISTRATOR.DS.'modules';
			$langbase	= JPATH_ADMINISTRATOR;
		} else {
			$path		= JPATH_ROOT.DS.'modules';
			$langbase	= JPATH_ROOT;
		}

		jimport('joomla.filesystem.folder');
		$dirs = JFolder::folders( $path );
		$lang =& JFactory::getLanguage();

		foreach ($dirs as $dir)
		{
			if (substr( $dir, 0, 4 ) == 'mod_')
			{
				$files 				= JFolder::files( $path.DS.$dir, '^([_A-Za-z0-9]*)\.xml$' );
				$module				= new stdClass;
				$module->file 		= $files[0];
				$module->module 	= str_replace( '.xml', '', $files[0] );
				$module->path 		= $path.DS.$dir;
				$modules[]			= $module;

				$lang->load( $module->module, $langbase );
			}
		}

		ModulesHelperXML::parseXMLModuleFile( $modules, $client );

		$n = count($modules);
		for ($i = 0; $i < $n; $i++) {
			$modules[$i]->name = JText::_(stripslashes($modules[$i]->name));
		}

		JArrayHelper::sortObjects( $modules, 'name' );

		$view =& $this->getView( 'module' );
		$view->add( $modules, $client );
	}
	function remove()
	{
		global $mainframe;

		JRequest::checkToken() or jexit( 'Invalid Token' );
		$db		=& JFactory::getDBO();
		$client	=& JApplicationHelper::getClientInfo(JRequest::getVar('client', '0', '', 'int'));
		$this->setRedirect( 'index.php?option=com_aikadmin&c=module&client='.$client->id );

		$cid	= JRequest::getVar( 'cid', array(), 'post', 'array' );
		JArrayHelper::toInteger( $cid );

		if (empty( $cid )) {
			return JError::raiseWarning( 500, 'No items selected' );
		}

		$cids = implode( ',', $cid );
		$query = 'DELETE FROM #__modules_menu'
			. ' WHERE moduleid IN ( '.$cids.' )'
			;
		$db->setQuery( $query );
		if (!$db->query()) {
			return JError::raiseError( 500, $db->getErrorMsg() );
		}
		$query = 'DELETE FROM #__modules'
			. ' WHERE id IN ('.$cids.')'
			;
		$db->setQuery( $query );
		if (!$db->query()) {
			return JError::raiseError( 500, $db->getErrorMsg() );
		}

		$this->setMessage( JText::sprintf( 'Items removed', count( $cid ) ) );
	}
	function publish()
	{
		global $mainframe;

		JRequest::checkToken() or jexit( 'Invalid Token' );

		$db 	=& JFactory::getDBO();
		$user 	=& JFactory::getUser();
		$client	=& JApplicationHelper::getClientInfo(JRequest::getVar('client', '0', '', 'int'));
		$this->setRedirect( 'index.php?option=com_aikadmin&c=module&client='.$client->id );

		$cache = & JFactory::getCache();
		$cache->clean( 'com_content' );

		$cid 	= JRequest::getVar( 'cid', array(), 'post', 'array' );
		JArrayHelper::toInteger($cid);

		$task	= $this->getTask();
		$publish	= ($task == 'publish');

		if (empty( $cid )) {
			return JError::raiseWarning( 500, 'No items selected' );
		}

		$cids = implode( ',', $cid );

		$query = 'UPDATE #__modules'
		. ' SET published = ' . intval( $publish )
		. ' WHERE id IN ( '.$cids.' )'
		. ' AND ( checked_out = 0 OR ( checked_out = '.(int) $user->get('id').' ) )'
		;
		$db->setQuery( $query );
		if (!$db->query()) {
			return JError::raiseWarning( 500, $db->getErrorMsg() );
		}

		if (count( $cid ) == 1) {
			$row =& JTable::getInstance('module');
			$row->checkin( $cid[0] );
		}
	}
	function cancel()
	{
		global $mainframe;

		JRequest::checkToken() or jexit( 'Invalid Token' );
		$db		=& JFactory::getDBO();
		$client	=& JApplicationHelper::getClientInfo(JRequest::getVar('client', '0', '', 'int'));
		$this->setRedirect( 'index.php?option=com_aikadmin&c=module&client='.$client->id );

		$row =& JTable::getInstance('module');
		$row->bind(JRequest::get('post'), 'selections params' );
		$row->checkin();
	}

	function reorder()
	{
		global $mainframe;

		JRequest::checkToken() or jexit( 'Invalid Token' );

		$db		=& JFactory::getDBO();
		$client	=& JApplicationHelper::getClientInfo(JRequest::getVar('client', '0', '', 'int'));
		$this->setRedirect( 'index.php?option=com_aikadmin&c=module&client='.$client->id );

		$cid 	= JRequest::getVar( 'cid', array(), 'post', 'array' );
		JArrayHelper::toInteger($cid);

		$task	= $this->getTask();
		$inc	= ($task == 'orderup' ? -1 : 1);

		if (empty( $cid )) {
			return JError::raiseWarning( 500, 'No items selected' );
		}

		$row =& JTable::getInstance('module');
		$row->load( (int) $cid[0] );

		$row->move( $inc, 'position = '.$db->Quote( $row->position ).' AND client_id='.(int) $client->id  );
	}
	function access()
	{
		global $mainframe;

		JRequest::checkToken() or jexit( 'Invalid Token' );

		$db		=& JFactory::getDBO();
		$client	=& JApplicationHelper::getClientInfo(JRequest::getVar('client', '0', '', 'int'));
		$this->setRedirect( 'index.php?option=com_aikadmin&c=module&client='.$client->id );

		$cid 	= JRequest::getVar( 'cid', array(), 'post', 'array' );
		JArrayHelper::toInteger($cid);

		$task	= JRequest::getCmd( 'task' );

		if (empty( $cid )) {
			return JError::raiseWarning( 500, 'No items selected' );
		}

		switch ( $task )
		{
			case 'accesspublic':
				$access = 0;
				break;

			case 'accessregistered':
				$access = 1;
				break;

			case 'accessspecial':
				$access = 2;
				break;
		}

		$row =& JTable::getInstance('module');
		$row->load( (int) $cid[0] );
		$row->access = $access;

		if ( !$row->check() ) {
			JError::raiseWarning( 500, $row->getError() );
		}
		if ( !$row->store() ) {
			JError::raiseWarning( 500, $row->getError() );
		}
	}

	function saveOrder()
	{
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$db		=& JFactory::getDBO();
		$client	=& JApplicationHelper::getClientInfo(JRequest::getVar('client', '0', '', 'int'));
		$this->setRedirect( 'index.php?option=com_aikadmin&c=module&client='.$client->id );

		$cid 	= JRequest::getVar( 'cid', array(), 'post', 'array' );
		JArrayHelper::toInteger($cid);

		if (empty( $cid )) {
			return JError::raiseWarning( 500, 'No items selected' );
		}

		$total		= count( $cid );
		$row 		=& JTable::getInstance('module');
		$groupings = array();

		$order 		= JRequest::getVar( 'order', array(0), 'post', 'array' );
		JArrayHelper::toInteger($order);

		for ($i = 0; $i < $total; $i++)
		{
			$row->load( (int) $cid[$i] );
			$groupings[] = $row->position;

			if ($row->ordering != $order[$i])
			{
				$row->ordering = $order[$i];
				if (!$row->store()) {
					return JError::raiseWarning( 500, $db->getErrorMsg() );
				}
			}
		}
		$groupings = array_unique( $groupings );
		foreach ($groupings as $group){
			$row->reorder('position = '.$db->Quote($group).' AND client_id = '.(int) $client->id);
		}

		$this->setMessage (JText::_( 'New ordering saved' ));
	}

	function preview()
	{
		$document =& JFactory::getDocument();
		$document->setTitle(JText::_('Module Preview'));

		$view =& $this->getView( 'module' );
		$view->preview( );
	}
}