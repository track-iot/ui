<?php


include ("jpgraph.php");
include ("jpgraph_line.php");

include('\wamp\www\TrackIot\BDD.php');

$tableau = array();
$tableauNombre = array();



// *****************************************************
// Extraction of data in the database
// **************************************************

$sql = <<<EOF
	SELECT  
		type AS TYPE,
		COUNT(id) AS NBR_READERS 
	FROM `Readers`
	GROUP BY type
EOF;

$Cnx = @mysql_connect(MYSQL_HOST, MYSQL_USER, MYSQL_PASS) or die('Problem with mysql connxion');

@mysql_select_db(MYSQL_DATABASE) or die('Problem of selecting the base');

$Query = @mysql_query($sql, $Cnx) or die('Problem of query');

while ($r = mysql_fetch_array($Query,  MYSQL_ASSOC)) {
	
	$tableau[] = "Type: " . $r['TYPE'];
	$tableauNombre[] = $r['NBR_READERS'];
}

$graph = new Graph(450,530);
$graph->SetScale("textlin");

$theme_class= new UniversalTheme;
$graph->SetTheme($theme_class);

//$graph->title->Set('Background Image');
$graph->SetBox(false);

$graph->yaxis->HideZeroLabel();
$graph->yaxis->HideLine(false);
$graph->yaxis->HideTicks(false,false);

$graph->xaxis->SetTickLabels($tableau);
$graph->ygrid->SetFill(false);
//$graph->SetBackgroundImage("tiger_bkg.png",BGIMG_FILLFRAME);

$p1 = new LinePlot($tableauNombre);
$graph->Add($p1);
$p1->value->Show();



$p1->SetColor("#55bbdd");
$p1->SetLegend('Number of the Readers');
$p1->mark->SetType(MARK_FILLEDCIRCLE,'',1.0);
$p1->mark->SetColor('#55bbdd');
$p1->mark->SetFillColor('#55bbdd');
$p1->SetCenter();



$graph->legend->SetFrameWeight(1);
$graph->legend->SetColor('#4E4E4E','#00A78A');
$graph->legend->SetMarkAbsSize(8);


// Output line
$graph->Stroke();



?>





