<?php


defined('_JEXEC') or die('Restricted Access');
jimport('joomla.application.component.model');
require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'engine'.DS.'factories'.DS.'factory.php');


/**
 * The following class create the datasources
 * @version 1.0
 * @author  TobyTools.com (email:info@tobytools.com)(website: www.tobytools.com)
 * @copyright Copyright TobyTools.com 
 * @license GNU/GPL
 */


class ArchiDashFactoryDataSource extends ArchiDashFactory{
	
	/*
	 *class constructor 
	 *$opts must be for example
	 *	$opts['dashboard']="STANDARD"
	 *	$opts['dashboardid']=1
	 *
	 *
	 */
	function ArchiDashFactoryDataSource($opts){
		parent::__construct($opts);
		if($this->completed){
			$this->options['defaultcode']="<table><thead><tr><th>".JText::_('DEFAULTTH')."</th></tr></thead><tbody><tr><td>".JText::_('DEFAULTTD')."</td></tr></tbody></table>";
			global $mainframe;
			/*read joomla database options these will be the default value if they are not specified for the sql datasource*/
			$this->options['driver']=$mainframe->getCfg("dbtype");
			$this->options['prefix']= $mainframe->getCfg("dbprefix");
			$this->options['host']=$mainframe->getCfg("host");
			$this->options['database']=$mainframe->getCfg("db");
			$this->options['user']=$mainframe->getCfg("user");
			$this->options['password']=$mainframe->getCfg("password");
			$this->options['dsaccesslevel']=0;//Public
			$this->options['query']="Select \"".JText::_('DEFAULTTD')."\" as \"".JText::_('DEFAULTTH')."\" ";
			$this->options['realtime']=0;
			
			$this->options['tablenumber']=0;
			$this->options['tableid']="";
			$this->options['stripped']=0;
			
			$this->options['articlename']="";
			
			$this->options['snippetcode']=$this->options['defaultcode'];
			
			/*get Archimede encryption key*/
			require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archimede'.DS.'models'.DS.'settings.php');
			$classname="ArchimedeModelSettings";
			$settings = new $classname();
			$vet = $settings->getSetting('encryption_key');
			$this->options['encryptionkey'] = $vet['value'];
		}
		else{
			global $mainframe;
			$mainframe->enqueueMessage(JText::_('NOOBJECT')." ".get_class($this),'error');
			
		}
	}
	
	
	
	
	/*
	 * create all the defined datasources for the specified dashboard
	 */
	function create(){
		if($this->completed){
			global $mainframe;
			
			$this->msg=JText::_('DSFACTOR');
		
			$this->createSQLDataSources();
			//check the operation is successfully completed
				if($this->completed==false){
					$mainframe->enqueueMessage(JText::_('BUILDFAILED')." ".get_class($this)."::createSQLDataSources",'error');
					return false;
				}

			$this->createSnippetDataSources();
			//check the operation is successfully completed
				if($this->completed==false){
					$mainframe->enqueueMessage(JText::_('BUILDFAILED')." ".get_class($this)."::createSnippetDataSources",'error');
					return false;
				}
				
				
			$this->createHTMLDataSources();
			//check the operation is successfully completed
				if($this->completed==false){
					$mainframe->enqueueMessage(JText::_('BUILDFAILED')." ".get_class($this)."::createHTMLDataSources",'error');
					return false;
				}
			
			$this->createArticleDataSources();
			//check the operation is successfully completed
				if($this->completed==false){
					$mainframe->enqueueMessage(JText::_('BUILDFAILED')." ".get_class($this)."::createArticleDataSources",'error');
					return false;
				}

				
		}
		else{
			//no op
		}
		
	}
	
	/**
	 * 
	 * create the SQL datasources for the specified dashboard
	 * 
	 * @return unknown_type
	 */
	protected function createSQLDataSources(){
		//set the correct table
		$this->options['table']="#__arc_datasource_sql";
		$this->options['tag']="sqlds";
		
		
		
		/*SQL DATASOURCES CREATION*/
		//find how many sql datasources are defined
		$num = (int)$this->readDefinition($this->options['tag'],false,false,true);
		for ($i=1; $i<=$num; $i++){
				//create the datasource by reading the definition
				$tmp = array();
					$tmp['name']=$this->readDefinition($this->options['tag'],$i,"name");
					$tmp['description']=$this->readDefinition($this->options['tag'],$i,"description");
					
					$tmp ['query'] = $this->readDefinition($this->options['tag'],$i);
						if(!$tmp['query'])$tmp['query'] = $this->options['query'];//default value
					
					$tmp['dsaccesslevel'] = $this->readDefinition($this->options['tag'],$i,"accesslevel");
						if(!$tmp['dsaccesslevel'])$tmp['dsaccesslevel'] = $this->options['dsaccesslevel'];//default value
					
					$tmp['driver'] = $this->readDefinition($this->options['tag'],$i,"driver");
						if(!$tmp['driver'])$tmp['driver'] = $this->options['driver'];//default value
						
					$tmp['realtime'] = $this->readDefinition($this->options['tag'],$i,"realtime");
						if(!$tmp['realtime'])$tmp['realtime'] = $this->options['realtime'];//default value
						
					
					$tmp['prefix'] = $this->readDefinition($this->options['tag'],$i,"prefix");
						if(!$tmp['prefix'])$tmp['prefix'] = $this->options['prefix'];//default value
						
					$tmp['host'] = $this->readDefinition($this->options['tag'],$i,"host");
						if(!$tmp['host'])$tmp['host'] = $this->options['host'];//default value
						
					$tmp['database'] = $this->readDefinition($this->options['tag'],$i,"database");
						if(!$tmp['database'])$tmp['database'] = $this->options['database'];//default value
						
					$tmp['user'] = $this->readDefinition($this->options['tag'],$i,"user");
						if(!$tmp['user'])$tmp['user'] = $this->options['user'];//default value
						
					$tmp['password'] = $this->readDefinition($this->options['tag'],$i,"password");
						if(!$tmp['password'])$tmp['password'] = $this->options['password'];//default value
					
					
					
					
				$id = $this->insertSQLDataSourceRecord($tmp);
				if($id){
					//save the id in #_archidash_datasources table
					require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'models'.DS.'datasource.php');
					$classname="ArchiDashModelDataSource";
					$model = new $classname();
					$vet = array();
						$vet['type']="SQL";
						$vet['datasourceid']=$id;
						$vet['dashboardid']=$this->options['dashboardid'];
			
					$model->insertRecord($vet);
					//check there are errors for the creation of record with the model
					if($model->getErrMsg()!=false){
						$this->completed=false;
						$this->errMsg.=$model->getErrMsg();
					}
					
					
					//message
					$this->msg.=$tmp['name']."<br/>";	
				}
				else{
					//no op
				}
				
				
		}
			
		
		
	}
	
	/**
	 * insert a new record into the table #__arc_datasource_sql
	 * 
	 * @return id of SQL datasource, false otherwise
	 */
	protected function insertSQLDataSourceRecord($vet){
		global $mainframe;
		/*define the default table code*/
		
		
		$query="
			INSERT INTO {$this->options['table']}(name,description,driver,host,dbname,user,password,prefix,query,code,realtime,access_level,published)
			VALUES
				('{$vet['name']}','{$vet['description']}','{$vet['driver']}', '{$vet['host']}', '{$vet['database']}', '{$vet['user']}', AES_ENCRYPT('{$vet['password']}','{$this->options['encryptionkey']}'),'{$vet['prefix']}',
				'{$vet['query']}','{$this->options['defaultcode']}','{$vet['realtime']}','{$vet['dsaccesslevel']}','1')
		";
		
		
		
		$db =& JFactory::getDBO();			
			
		//to avoid problem with character set
		$db->setQuery($this->options['characterset']);
		$db->query();
		
		$db->setQuery($query);
		$db->query();
		$result = $db->insertid();
		if(!$result){	
			//something wrong
			$this->completed=false;
			$this->errMsg .= $db->getErrorNum()." : ".$db->getErrorMsg();
			$mainframe->enqueueMessage(JText::_( 'QUERYKO' ),'error');
			$mainframe->enqueueMessage($this->errMsg,'error');
			$mainframe->enqueueMessage(get_class($this)."::insertSQLDataSourceRecord",'error');
			return false;
		}
		else{
			//clean
			$this->errMsg .= false;
			return $result;
		}
		
	}
	
	
	
	/**
	 * 
	 * create the HTML datasources for the specified dashboard
	 * 
	 * @return unknown_type
	 */
	protected function createHTMLDataSources(){
		
		//set the correct table
		$this->options['table']="#__arc_datasource_html";
		$this->options['tag']="htmlds";
		
		//find the numbers of html datasources defined 
		$num = (int)$this->readDefinition($this->options['tag'],false,false,true);
		for ($i=1; $i<=$num; $i++){
			
			//create the datasource by reading the definition
			$tmp = array();
				$tmp['name']=$this->readDefinition($this->options['tag'],$i,"name");
				
				$tmp['description']=$this->readDefinition($this->options['tag'],$i,"description");
				
				$tmp['dsaccesslevel'] = $this->readDefinition($this->options['tag'],$i,"accesslevel");
					if(!$tmp['dsaccesslevel'])$tmp['dsaccesslevel'] = $this->options['dsaccesslevel'];//default value
				
				$tmp['tablenumber'] = $this->readDefinition($this->options['tag'],$i,"tablenumber");
					if(!$tmp['tablenumber'])$tmp['tablenumber'] = $this->options['tablenumber'];//default value
				
				$tmp['tableid'] = $this->readDefinition($this->options['tag'],$i,"tableid");
					if(!$tmp['tableid'])$tmp['tableid'] = $this->options['tableid'];//default value
				
				$tmp ['address'] = $this->readDefinition($this->options['tag'],$i);
						if(!$tmp['address'])$tmp['address'] = $this->options['address'];//default value	
				
				$tmp['stripped'] = $this->readDefinition($this->options['tag'],$i,"stripped");
					if(!$tmp['stripped'])$tmp['stripped'] = $this->options['stripped'];//default value
				
				$tmp['realtime'] = $this->readDefinition($this->options['tag'],$i,"realtime");
						if(!$tmp['realtime'])$tmp['realtime'] = $this->options['realtime'];//default value
					
			
			
			$id = $this->insertHTMLDataSourceRecord($tmp);
			
			require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'models'.DS.'datasource.php');
			$classname="ArchiDashModelDataSource";
			$model = new $classname();
			$vet = array();
				$vet['type']="HTML";
				$vet['datasourceid']=$id;
				$vet['dashboardid']=$this->options['dashboardid'];
	
			$model->insertRecord($vet);
			//check there are errors for the creation of record with the model
			if($model->getErrMsg()!=false){
					$this->completed=false;
					$this->errMsg.=$model->getErrMsg();
			}
			
			//message
			$this->msg.=$tmp['name']."<br/>";	
			
		}
			
	}
	
	
	
	/**
	 * insert a new record into the table #__arc_datasource_html
	 * 
	 * @return id of HTML datasource, false otherwise
	 */
	
	protected function insertHTMLDataSourceRecord($vet){
		global $mainframe;
		/*define the default table code*/
			
		$query="
			INSERT INTO {$this->options['table']}(name,description,address,tableid,tablenumber,code,stripped,realtime,access_level,published)
			VALUES
				('{$vet['name']}','{$vet['description']}','{$vet['address']}', '{$vet['tableid']}','{$vet['tablenumber']}','{$this->options['defaultcode']}','{$vet['stripped']}','{$vet['realtime']}','{$vet['dsaccesslevel']}','1')
		";
		$db =& JFactory::getDBO();
					
			
		//to avoid problem with character set
		$db->setQuery($this->options['characterset']);
		$db->query();
		
		$db->setQuery($query);
		$db->query();
		$result = $db->insertid();
		if(!$result){	
			//something wrong
			$this->completed=false;
			$this->errMsg .= $db->getErrorNum()." : ".$db->getErrorMsg();
			$mainframe->enqueueMessage(JText::_( 'QUERYKO' ),'error');
			$mainframe->enqueueMessage($this->errMsg,'error');
			$mainframe->enqueueMessage(get_class($this)."::insertHTMLDataSourceRecord",'error');
			return false;
		}
		else{
			//clean
			$this->errMsg .= false;
			return $result;
		}
		
	}
	
	
	/**
	 * 
	 * create the Article datasources for the specified dashboard
	 * 
	 * @return unknown_type
	 */
	
	protected function createArticleDataSources(){
		//set the correct table
		$this->options['table']="#__arc_datasource_article";
		$this->options['tag']="articleds";
		
		//find the numbers of html datasources defined 
		$num = (int)$this->readDefinition($this->options['tag'],false,false,true);
		for ($i=1; $i<=$num; $i++){
			
			
			//create the datasource by reading the definition
			$tmp = array();
				$tmp['name']=$this->readDefinition($this->options['tag'],$i,"name");
				
				$tmp['description']=$this->readDefinition($this->options['tag'],$i,"description");
				
				$tmp['dsaccesslevel'] = $this->readDefinition($this->options['tag'],$i,"accesslevel");
					if(!$tmp['dsaccesslevel'])$tmp['dsaccesslevel'] = $this->options['dsaccesslevel'];//default value
				
				$tmp['tablenumber'] = $this->readDefinition($this->options['tag'],$i,"tablenumber");
					if(!$tmp['tablenumber'])$tmp['tablenumber'] = $this->options['tablenumber'];//default value
				
				$tmp['tableid'] = $this->readDefinition($this->options['tag'],$i,"tableid");
					if(!$tmp['tableid'])$tmp['tableid'] = $this->options['tableid'];//default value
				
				$tmp ['articlename'] = $this->readDefinition($this->options['tag'],$i,"articlename");
						if(!$tmp['articlename'])$tmp['articlename'] = $this->options['articlename'];//default value	
				
				$tmp['stripped'] = $this->readDefinition($this->options['tag'],$i,"stripped");
					if(!$tmp['stripped'])$tmp['stripped'] = $this->options['stripped'];//default value
				
				$tmp['realtime'] = $this->readDefinition($this->options['tag'],$i,"realtime");
						if(!$tmp['realtime'])$tmp['realtime'] = $this->options['realtime'];//default value
					
			
			
			$id = $this->insertArticleDataSourceRecord($tmp);
			
			
			
			
			//save the id in #_archidash_datasources table
			require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'models'.DS.'datasource.php');
			$classname="ArchiDashModelDataSource";
			$model = new $classname();
			$vet = array();
				$vet['type']="Article";
				$vet['datasourceid']=$id;
				$vet['dashboardid']=$this->options['dashboardid'];
	
			$model->insertRecord($vet);
			//check there are errors for the creation of record with the model
			if($model->getErrMsg()!=false){
				$this->completed=false;
				$this->errMsg.=$model->getErrMsg();
			}
			
			
			//message
			$this->msg.=$tmp['name']."<br/>";	
			
		}
		
	}
	
	
	
	
	
	/**
	 * insert a new record into the table #__arc_datasource_article
	 * 
	 * @return id of Article datasource, false otherwise
	 */
	
	protected function insertArticleDataSourceRecord($vet){
		global $mainframe;
		/*define the default table code*/
		$db =& JFactory::getDBO();
		
		//finde the article id
		
		$query='SELECT id FROM '.$db->nameQuote("#__content"). ' WHERE '.$db->nameQuote('title').' = '.$db->Quote($vet['articlename']);
		$db->setQuery($query);
		$tmp = $db->loadAssoc();
		$vet['articleid']=$tmp['id'];
		
		$query="
			INSERT INTO {$this->options['table']}(name,description,articleid,tableid,tablenumber,code,stripped,realtime,access_level,published)
			VALUES
				('{$vet['name']}','{$vet['description']}','{$vet['articleid']}', '{$vet['tableid']}','{$vet['tablenumber']}','{$this->options['defaultcode']}','{$vet['stripped']}','{$vet['realtime']}','{$vet['dsaccesslevel']}','1')
		";
		
					
			
		//to avoid problem with character set
		$db->setQuery($this->options['characterset']);
		$db->query();
		
		$db->setQuery($query);
		$db->query();
		$result = $db->insertid();
		if(!$result){	
			//something wrong
			$this->completed=false;
			$this->errMsg .= $db->getErrorNum()." : ".$db->getErrorMsg();
			$mainframe->enqueueMessage(JText::_( 'QUERYKO' ),'error');
			$mainframe->enqueueMessage($this->errMsg,'error');
			$mainframe->enqueueMessage(get_class($this)."::insertArticleDataSourceRecord",'error');
			return false;
		}
		else{
			//clean
			$this->errMsg .= false;
			return $result;
		}
		
	}
	
	protected function createSnippetDataSources(){
		//set the correct table
		$this->options['table']="#__arc_datasource_snippet";
		$this->options['tag']="snippetds";
		
		//find the numbers of html datasources defined 
		$num = (int)$this->readDefinition($this->options['tag'],false,false,true);
		
		for ($i=1; $i<=$num; $i++){
			
			//create the datasource by reading the definition
			$tmp = array();
				$tmp['name']=$this->readDefinition($this->options['tag'],$i,"name");
				
				$tmp['description']=$this->readDefinition($this->options['tag'],$i,"description");
				
				$tmp['dsaccesslevel'] = $this->readDefinition($this->options['tag'],$i,"accesslevel");
					if(!$tmp['dsaccesslevel'])$tmp['dsaccesslevel'] = $this->options['dsaccesslevel'];//default value
				
				$tmp ['snippetcode'] = $this->readDefinition($this->options['tag'],$i);
						if(!$tmp['snippetcode'])$tmp['snippetcode'] = $this->options['snippetcode'];//default value	
				
					
			
			
			$id = $this->insertSnippetDataSourceRecord($tmp);
			
			
			
			//save the id in #_archidash_datasources table
			require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'models'.DS.'datasource.php');
			$classname="ArchiDashModelDataSource";
			$model = new $classname();
			$vet = array();
				$vet['type']="Snippet";
				$vet['datasourceid']=$id;
				$vet['dashboardid']=$this->options['dashboardid'];
	
			$model->insertRecord($vet);
			//check there are errors for the creation of record with the model
			if($model->getErrMsg()!=false){
				$this->completed=false;
				$this->errMsg.=$model->getErrMsg();
			}
			
			//message
			$this->msg.=$tmp['name']."<br/>";
			
		}
		
	}
	
	/**
	 * insert a new record into the table #__arc_datasource_snippet
	 * 
	 * @return id of Snippet datasource, false otherwise
	 */
	
	protected function insertSnippetDataSourceRecord($vet){
		global $mainframe;
		/*define the default table code*/
		
		
		$query="
			INSERT INTO {$this->options['table']}(name,description,code,access_level,published)
			VALUES
				('{$vet['name']}','{$vet['description']}','{$vet['snippetcode']}','{$vet['dsaccesslevel']}','1')
		";
		
		$db =& JFactory::getDBO();			
			
		//to avoid problem with character set
		$db->setQuery($this->options['characterset']);
		$db->query();
		
		$db->setQuery($query);
		$db->query();
		$result = $db->insertid();
		if(!$result){	
			//something wrong
			$this->completed=false;
			$this->errMsg .= $db->getErrorNum()." : ".$db->getErrorMsg();
			$mainframe->enqueueMessage(JText::_( 'QUERYKO' ),'error');
			$mainframe->enqueueMessage($this->errMsg,'error');
			$mainframe->enqueueMessage(get_class($this)."::insertSnippetDataSourceRecord",'error');
			return false;
		}
		else{
			//clean
			$this->errMsg .= false;
			return $result;
		}
		
	}
	
	
	
	
}
?>


