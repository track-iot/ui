<?php


defined('_JEXEC') or die('Restricted Access');
jimport('joomla.application.component.model');
require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'engine'.DS.'factories'.DS.'factory.php');


/**
 * The following class create the category 
 * @version 1.0
 * @author  TobyTools.com (email:info@tobytools.com)(website: www.tobytools.com)
 * @copyright Copyright TobyTools.com 
 * @license GNU/GPL
 */


class ArchiDashFactoryMenu extends ArchiDashFactory{
	
	/*
	 *class constructor 
	 */
	function ArchiDashFactoryMenu($opts){
		parent::__construct($opts);
		//check everything is allright
		if($this->completed){
			
			$this->options['table']="#__menu";
			$this->options['tag']="category";		
		}
		else{
			global $mainframe;
			$mainframe->enqueueMessage(JText::_('NOOBJECT')." ".get_class($this),'error');
		}
	}
	/*
	 * create section if it does not exist
	 */
	function create(){
		global $mainframe;	
		/*Menus CREATION*/
		//find the list of categories definition for the specified dashboard
		$num = (int)$this->readDefinition($this->options['tag'],false,false,true);
		
		//menutype
		$menutype = $this->readDefinition("menu",1,"menutype");
		
		if(strcasecmp(trim($menutype),"")==0 ){
			$this->completed=false;
			$this->errMsg.=get_class($this)."::create ".JText::_('DEFINITIONNOTVALID');
			$mainframe->enqueueMessage($this->errMsg,'error');
			return false;
		}
		$this->msg.="<h3>".JText::_('Menu')." ".JText::_('Items')."</h3>";
		for ($i=1; $i<=$num; $i++){
						
			//category title
			$name = $this->readDefinition($this->options['tag'],$i,"name");
			//category alias
			$alias = $this->readDefinition($this->options['tag'],$i,"alias");
			
			if(strcasecmp(trim($name),"")==0 || strcasecmp(trim($alias),"")==0 ){
				$this->completed=false;
				$this->errMsg.=get_class($this)."::create ".JText::_('DEFINITIONNOTVALID');
				$mainframe->enqueueMessage($this->errMsg,'error');
				//exit from the foreach
				break;
			}
			
			//menu creation
			$this->createMenu($menutype,$name,$alias,$i);		
		}
	}
	
	/**
	 * given menutype, name and alias of category the method creates the record inside #__menus
	 * @param  $name
	 * @param  $alias
	 * @param  $description
	 * @return unknown_type
	 */
	protected function createMenu($menutype,$name,$alias,$ordering){
		global $mainframe;
		$menuid=false;
		
		
		/*check if the category already exists.*/
		$db =& JFactory::getDBO();
			
		//to avoid problem with character set
		$db->setQuery($this->options['characterset']);
		$db->query();
		
		$query='SELECT * FROM '.$db->nameQuote("#__categories"). ' WHERE '.$db->nameQuote('title').' = '.$db->Quote($name);
		$db->setQuery($query);
		$vet = $db->loadAssoc();
		
		
		$query = "INSERT INTO ".$db->nameQuote($this->options['table'])."(menutype,name,alias,link,type,published,componentid,access,ordering) VALUES ('$menutype','$name','$alias','index.php?option=com_content&view=category&layout=blog&id={$vet['id']}','component','1','20','{$this->options['accesslevel']}','$ordering')";
		$db->setQuery($query);
		$result = $db->query();
			
		
		
		if(!$result){
			//something wrong
			$this->completed=false;
			$this->errMsg .= $db->getErrorNum()." : ".$db->getErrorMsg();
			$mainframe->enqueueMessage(JText::_( 'QUERYKO' ),'error');
			$mainframe->enqueueMessage($this->errMsg,'error');
			$mainframe->enqueueMessage(get_class($this)."::create",'error');
			return false;
		}
		else{
			//clean
			$this->errMsg = false;
			//provide the element id
			$db->setQuery("SELECT LAST_INSERT_ID()");				
			$menuid= $db->loadResult();
			
		}
				
			
		//output message
		$this->msg.="$name<br/>";		
			
		
		
		//save the information inside #__archidash_sections
		require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'models'.DS.'menu.php');
		$classname="ArchiDashModelMenu";
		$model = new $classname();
		$vet = array();
			$vet['menuid']=$menuid;
			$vet['dashboardid']=$this->options['dashboardid'];
		
		$model->insertRecord($vet);
		//check there are errors for the creation of record with the model
		if($model->getErrMsg()!=false){
			$this->completed=false;
			$this->errMsg.=$model->getErrMsg();
		}
		
	}

}



