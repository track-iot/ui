<?php
/**
 * @version		$Id: module.php 1.0 19-11-2009 Danijar
 * @package		Frontend Admin
 * @copyright	Copyright (C) 2009 CMSSpace. http://www.cmsspace.com
 * @license		GNU/GPL. http://www.gnu.org/licenses/gpl.html
 */
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.model' );
class AikModelModule extends JModel
{
	var $_xml;

	function &getModule()
	{
		static $instance;

		if (!$instance)
		{
			$instance = $this->getTable( 'Module', 'JTable' );
			if ($id = $this->getState( 'id' )) {
				$instance->load( (int) $id );
			}
		}
		return $instance;
	}

	function &_getXML()
	{
		if (!$this->_xml)
		{
			$clientId	= $this->getState( 'clientId', 0 );
			$path		= ($clientId == 1) ? 'mod1_xml' : 'mod0_xml';
			$module		= &$this->getModule();

			if ($module->module == 'custom') {
				$xmlpath = JApplicationHelper::getPath( $path, 'mod_custom' );
			} else {
				$xmlpath = JApplicationHelper::getPath( $path, $module->module );
			}

			if (file_exists($xmlpath))
			{
				$xml =& JFactory::getXMLParser('Simple');
				if ($xml->loadFile($xmlpath)) {
					$this->_xml = &$xml;
				}
			}
		}
		return $this->_xml;
	}

	function &getParams()
	{
		$module	=& $this->getModule();
		$params	= new JParameter($module->params);

		if ($xml =& $this->_getXML())
		{
			if ($ps = & $xml->document->params) {
				foreach ($ps as $p)
				{
					$params->setXML( $p );
				}
			}
		}
		return $params;
	}

	function getPositions()
	{
		jimport('joomla.filesystem.folder');

		$client =& JApplicationHelper::getClientInfo($this->getState('clientId'));
		if ($client === false) {
			return false;
		}
		$db	=& JFactory::getDBO();
		$query = 'SELECT DISTINCT(template) AS text, template AS value'.
				' FROM #__templates_menu' .
				' WHERE client_id = '.(int) $client->id;
		$db->setQuery( $query );
		$templates = $db->loadObjectList();

		$query = 'SELECT DISTINCT(position)'.
				' FROM #__modules' .
				' WHERE client_id = '.(int) $client->id;
		$db->setQuery( $query );
		$positions = $db->loadResultArray();
		$positions = (is_array($positions)) ? $positions : array();

		for ($i = 0, $n = count($templates); $i < $n; $i++ )
		{
			$path = $client->path.DS.'templates'.DS.$templates[$i]->value;

			$xml =& JFactory::getXMLParser('Simple');
			if ($xml->loadFile($path.DS.'templateDetails.xml'))
			{
				$p =& $xml->document->getElementByPath('positions');
				if (is_a($p, 'JSimpleXMLElement') && count($p->children()))
				{
					foreach ($p->children() as $child)
					{
						if (!in_array($child->data(), $positions)) {
							$positions[] = $child->data();
						}
					}
				}
			}
		}

		if(defined('_JLEGACY') && _JLEGACY == '1.0')
		{
			$positions[] = 'left';
			$positions[] = 'right';
			$positions[] = 'top';
			$positions[] = 'bottom';
			$positions[] = 'inset';
			$positions[] = 'banner';
			$positions[] = 'header';
			$positions[] = 'footer';
			$positions[] = 'newsflash';
			$positions[] = 'legals';
			$positions[] = 'pathway';
			$positions[] = 'breadcrumb';
			$positions[] = 'user1';
			$positions[] = 'user2';
			$positions[] = 'user3';
			$positions[] = 'user4';
			$positions[] = 'user5';
			$positions[] = 'user6';
			$positions[] = 'user7';
			$positions[] = 'user8';
			$positions[] = 'user9';
			$positions[] = 'advert1';
			$positions[] = 'advert2';
			$positions[] = 'advert3';
			$positions[] = 'debug';
			$positions[] = 'syndicate';
		}

		$positions = array_unique($positions);
		sort($positions);

		return $positions;
	}
}
