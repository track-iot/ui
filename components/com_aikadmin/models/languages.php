<?php
/**
 * @version		$Id: languages.php 1.0 19-11-2009 Danijar
 * @package		Frontend Admin
 * @copyright	Copyright (C) 2009 CMSSpace. http://www.cmsspace.com
 * @license		GNU/GPL. http://www.gnu.org/licenses/gpl.html
 */
defined( '_JEXEC' ) or die( 'Restricted access' );
require_once(dirname(__FILE__).DS.'extension.php');
jimport( 'joomla.filesystem.folder' );
class AikModelLanguages extends InstallerModel
{
	var $_type = 'language';
	function __construct()
	{
		global $mainframe;
		parent::__construct();

		$this->setState('filter.string', $mainframe->getUserStateFromRequest( "com_languages.installer.languages.string", 'filter', '', 'string' ));
		$this->setState('filter.client', $mainframe->getUserStateFromRequest( "com_languages.installer.languages.client", 'client', -1, 'int' ));
	}

	function _loadItems()
	{
		global $mainframe, $option;

		$db = &JFactory::getDBO();

		if ($this->_state->get('filter.client') < 0) {
			$client = 'all';
			$langBDir = JLanguage::getLanguagePath(JPATH_SITE);
			$langDirs = JFolder::folders($langBDir);

			for ($i=0; $i < count($langDirs); $i++)
			{
				$lang = new stdClass();
				$lang->folder = $langDirs[$i];
				$lang->client = 0;
				$lang->baseDir = $langBDir;
				$languages[] = $lang;
			}
			$langBDir = JLanguage::getLanguagePath(JPATH_ADMINISTRATOR);
			$langDirs = JFolder::folders($langBDir);

			for ($i=0; $i < count($langDirs); $i++)
			{
				$lang = new stdClass();
				$lang->folder = $langDirs[$i];
				$lang->client = 1;
				$lang->baseDir = $langBDir;
				$languages[] = $lang;
			}
		}
		else
		{
			$clientInfo =& JApplicationHelper::getClientInfo($this->_state->get('filter.client'));
			$client = $clientInfo->name;
			$langBDir = JLanguage::getLanguagePath($clientInfo->path);
			$langDirs = JFolder::folders($langBDir);

			for ($i=0, $n=count($langDirs); $i < $n; $i++)
			{
				$lang = new stdClass();
				$lang->folder = $langDirs[$i];
				$lang->client = $clientInfo->id;
				$lang->baseDir = $langBDir;

				if ($this->_state->get('filter.string')) {
					if (strpos($lang->folder, $this->_state->get('filter.string')) !== false) {
						$languages[] = $lang;
					}
				} else {
					$languages[] = $lang;
				}
			}
		}

		$rows = array();
		$rowid = 0;
		foreach ($languages as $language)
		{
			$files = JFolder::files( $language->baseDir.DS.$language->folder, '^([-_A-Za-z]*)\.xml$' );
			foreach ($files as $file)
			{
				$data = JApplicationHelper::parseXMLLangMetaFile($language->baseDir.DS.$language->folder.DS.$file);

				$row 			= new StdClass();
				$row->id 		= $rowid;
				$row->client_id = $language->client;
				$row->language 	= $language->baseDir.DS.$language->folder;

				if (!is_array($data)) {
					continue;
				}
				foreach($data as $key => $value) {
					$row->$key = $value;
				}
				$clientVals =& JApplicationHelper::getClientInfo($row->client_id);
				$lang = JComponentHelper::getParams('com_languages');
				if ( $lang->get($clientVals->name, 'en-GB') == basename( $row->language ) ) {
					$row->published	= 1;
				} else {
					$row->published = 0;
				}

				$row->checked_out = 0;
				$row->jname = JString::strtolower( str_replace( " ", "_", $row->name ) );
				$rows[] = $row;
				$rowid++;
			}
		}
		$this->setState('pagination.total', count($rows));
		if($this->_state->get('pagination.offset') > $this->_state->get('pagination.total')) {
			$this->setState('pagination.offset',0);
		}

		if($this->_state->get('pagination.limit') > 0) {
			$this->_items = array_slice( $rows, $this->_state->get('pagination.offset'), $this->_state->get('pagination.limit') );
		} else {
			$this->_items = $rows;
		}
	}
	function remove($eid=array())
	{
		global $mainframe;
		$lang =& JFactory::getLanguage();
		$lang->load('com_languages');
		$failed = array ();
		if (!is_array($eid)) {
			$eid = array ($eid);
		}
		$this->_loadItems();
		$db =& JFactory::getDBO();
		jimport('joomla.installer.installer');
		$installer	=& JInstaller::getInstance($db, $this->_type);

		foreach ($eid as $id)
		{
			$item = $this->_items[$id];
			$client	=& JApplicationHelper::getClientInfo($item->client_id);
			$params = JComponentHelper::getParams('com_languages');
			$tag	= basename($item->language);
			if ( $params->get($client->name, 'en-GB') == $tag ) {
				$failed[]	= $id;
				JError::raiseWarning('', JText::_('UNINSTALLLANGPUBLISHEDALREADY'));
				return;
			}

			$result = $installer->uninstall( 'language', $item->language );
			if ($result === false) {
				$failed[] = $id;
			}
		}

		if (count($failed)) {
			$msg = JText::sprintf('UNINSTALLEXT', JText::_($this->_type), JText::_('Error'));
			$result = false;
		} else {
			$msg = JText::sprintf('UNINSTALLEXT', JText::_($this->_type), JText::_('Success'));
			$result = true;
		}

		$mainframe->enqueueMessage($msg);
		$this->setState('action', 'remove');
		$this->setState('message', $installer->message);
		$this->_loadItems();
		return $result;
	}
}