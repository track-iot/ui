<?php
include ("jpgraph.php");
include ("jpgraph_pie.php");
include ("jpgraph_pie3d.php");

define('MYSQL_HOST', 'localhost');
define('MYSQL_USER', 'root');
define('MYSQL_PASS', '');
define('MYSQL_DATABASE', 'tracklot');

$tableauAnnees = array();
$tableauNombreVentes = array();

// *****************************************************
// Extraction des donn�es dans la base de donn�es 
// **************************************************

$sql = <<<EOF
	SELECT  
		type AS TYPE,
		COUNT(id) AS NBR_READERS  
	FROM `readers`
	GROUP BY type
EOF;

$mysqlCnx = @mysql_connect(MYSQL_HOST, MYSQL_USER, MYSQL_PASS) or die('Pb de connxion mysql');

@mysql_select_db(MYSQL_DATABASE) or die('Pb de s�lection de la base');

$mysqlQuery = @mysql_query($sql, $mysqlCnx) or die('Pb de requ�te');

while ($row = mysql_fetch_array($mysqlQuery,  MYSQL_ASSOC)) {
	// Ajouter ann�e devant, c'est pour la l�gende
	$tableauAnnees[] = "Type: " . $row['TYPE'];
	$tableauNombreVentes[] = $row['NBR_READERS'];
}

// **************************************
// Cr�ation du graphique
// *****************************************

// On sp�cifie la largeur et la hauteur du graph
$graph = new PieGraph(1000,700);

// Ajouter une ombre au conteneur
$graph->SetShadow();

// Donner un titre
$graph->title->Set("The percentage of readers by type");

// Quelle police et quel style pour le titre
// Prototype: function SetFont($aFamily,$aStyle=FS_NORMAL,$aSize=10)
// 1. famille
// 2. style
// 3. taille
$graph->title->SetFont(FF_GEORGIA,FS_BOLD, 12);

// Cr�er un camembert 
$pie = new PiePlot3D($tableauNombreVentes);

// Quelle partie se d�tache du reste
$pie->ExplodeSlice(2);

// Sp�cifier des couleurs personnalis�es... #FF0000 ok
$pie->SetSliceColors(array('red', 'blue', 'green'));

// L�gendes qui accompagnent le graphique, ici chaque ann�e avec sa couleur
$pie->SetLegends($tableauAnnees);

// Position du graphique (0.5=centr�)
$pie->SetCenter(0.5);

// Type de valeur (pourcentage ou valeurs)
//$pie->SetValueType(PIE_VALUE_ABS);

// Personnalisation des �tiquettes pour chaque partie
$pie->value->SetFormat('%d readers');

// Personnaliser la police et couleur des �tiquettes
$pie->value->SetFont(FF_ARIAL,FS_NORMAL, 9);
$pie->value->SetColor('blue');

// ajouter le graphique PIE3D au conteneur 
$graph->Add($pie);

// Provoquer l'affichage
$graph->Stroke();






$data = array(40,60,21,33);

// Create the Pie Graph. 
$graph = new PieGraph(350,250);

$theme_class= new VividTheme;
$graph->SetTheme($theme_class);

// Set A title for the plot
$graph->title->Set("A Simple 3D Pie Plot");

// Create
$p1 = new PiePlot3D($data);
$graph->Add($p1);

$p1->ShowBorder();
$p1->SetColor('black');
$p1->ExplodeSlice(1);
$graph->Stroke();


?> 