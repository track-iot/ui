<?php

defined('_JEXEC') or die('Restricted Access');




/**
 * Abstract class for the Factory classes of ArchiDash
 * @version 1.0
 * @author  TobyTools.com (email:info@tobytools.com)(website: www.tobytools.com)
 * @copyright Copyright TobyTools.com 
 * @license GNU/GPL
 */


abstract class ArchiDashFactory {
	
	/**
	 * This variable it's true only if the create operation is completed successfully.
	 * The create operation is a work unit performed against a Joomla installation, 
	 * and treated in a coherent and reliable way independent of other ones.
	 * 
	 * 
	 * @var boolean
	 */
	protected $completed;
	
	
	/**
	 * associative array with the options for the class
	 * At least it contains
	 * $options['definition'] the definition associated with the factory object
	 * $options['accesslevel'] the access level for the resource associated with the class
	 * $options['table'] the specific table used by the factory class
	 * $options['tag'] the definition tag associated with the class
	 * $options['characterset'] the character set for the database
	 * 
	 * more info
	 * 
	 * $options['dashboardid']
	 * $options['sectionid']
	 * 
	 * 
	 */
	protected $options;
	
	/**
	 * 
	 * @var string, the error message (if any) associated with the last operation performed
	 */
	protected $errMsg;
	
	
	/**
	 * 
	 * @var string, the messages associated with the last operation performed
	 */
	protected $msg;
	
	
	
	
	
	/**
	 * 
	 * @var ArchiDashDefinitionReader object
	 */
	protected $reader;
	
	/*
	 *class constructor 
	 */
	protected function ArchiDashFactory($opt=false){
		
		if(isset($opt['definition'])){
			$this->options=$opt;
			$this->options['table']="";
			$this->options['tag']="";
			$this->options['characterset']="SET NAMES 'utf8'";
			
			//is set to true, and if there is an error it is set to false
			$this->completed=true;
			$this->errMsg=false;
			$this->msg="";
			
			require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'engine'.DS.'utilities'.DS.'reader.php');
			$classname="ArchiDashDefinitionReader";
			$this->reader = new $classname($this->options['definition']);	
		}
		else{
			$this->errMsg=JText::_('NODEFINITION');
			$this->msg=false;
			$this->table=false;
			$this->tag=false;
			$this->characterset=false;
			$this->completed = false;
			$this->reader=false;
			$this->options=false;
		}
		
		
		
	}
	
	
	/*
	 *Read the definition and gets the value. 
	 */
	function readDefinition($tag=false,$index=1,$attribute=false,$length=false,$nested=false){
		if(!$length){
			return $this->reader->readDefinition($tag,$index,$attribute,$nested);
		}
		else{
			//read the length
			return $this->reader->getLength($tag);
		}
	}
	
	/*
	 *Create the resources belonging to dashborad 
	 */
	abstract function create();
	
	
	
	/**
	 * 
	 * @return string the error message
	 */
	function getErrMsg(){
		return $this->errMsg;
	}
	
	/**
	 * 
	 * @return string the message
	 */
	function getMsg(){
		return $this->msg;
	}
	
	/**
	 * check if there are all requirements to use ArchiDash
	 * 
	 * @return true,false
	 */
	function preRequirementsCheck(){
		$lang = & JFactory::getLanguage();
		$lang->load('com_archimede', JPATH_ADMINISTRATOR);
		$archimedeVersion = JText::_('ARCHIMEDEVERSION');
		
		if(version_compare($archimedeVersion,"11.5.17")>=0){
			
			$db =& JFactory::getDBO();
			$query = "SELECT name FROM ".$db->nameQuote("#__components")." WHERE ".$db->nameQuote('name')." = ".$db->Quote("Archimede");
			$db->setQuery($query);
			$installed = $db->loadResult();
			
			if($installed){	
				//check there are installed the mandatory modules defined in config.ini							
				$allOK=false;
				$tmp = parse_ini_file("config.ini");
				$modules=$tmp['module'];
				foreach($modules as $mod){
					$allOK = file_exists(JPATH_SITE.DS."modules".DS."$mod");
					if(!$allOK)break;	
				}
				return $allOK;
				 
				
							
			}
			else{
				
				return false;
			}									
		} 
		else{
			
			return false;
		}
	}
	
	function getOptions(){
		return $this->options;
	}
	function isCompleted(){
		return $this->completed;
	}
	
	
}




?>
