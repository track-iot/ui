<?php 
/**
 * @version		$Id: delete.php 1.0 19-11-2009 Danijar
 * @package		Frontend Admin
 * @copyright	Copyright (C) 2009 CMSSpace. http://www.cmsspace.com
 * @license		GNU/GPL. http://www.gnu.org/licenses/gpl.html
 */
defined('_JEXEC') or die('Restricted access'); ?>

<script language="javascript" type="text/javascript">
<!--
	function submitbutton(pressbutton) {
		if (pressbutton == 'doDeleteMenu') {

			submitform( pressbutton );
		} else {
			submitform( pressbutton );
		}
	}
//-->
</script>
<form action="index.php" method="post" name="adminForm">
	<div>
		<div style="width:30%;float:left">
			<?php if ( $this->modules ) : ?>
				<strong><?php echo JText::_( 'Module(s) being Deleted' ); ?>:</strong>
				<ol>
					<?php foreach ( $this->modules as $module ) :	?>
					<li><?php echo $module->title; ?></li>
					<?php endforeach; ?>
				</ol>
			<?php endif; ?>
		</div>
		<div style="width:30%;float:left">
			<strong><?php echo JText::_( 'Menu Items being Deleted' ); ?>:</strong>
			<ol>
				<?php foreach ($this->menuItems as $item) : ?>
				<li><?php echo $item->name; ?></li>
				<?php endforeach; ?>
			</ol>
		</div>
		<div class="clr"></div>
	</div>

	<input type="hidden" name="id" value="<?php echo $this->table->id; ?>" />
	<input type="hidden" name="option" value="com_aikadmin" />
	<input type="hidden" name="c" value="menus" />
	<input type="hidden" name="task" value="" />
	<?php echo JHTML::_( 'form.token' ); ?>
</form>