<?php


defined('_JEXEC') or die('Restricted Access');
jimport('joomla.application.component.model');
require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'engine'.DS.'exporters'.DS.'exporter.php');


/**
 * The following class export the articles 
 * @version 1.0
 * @author  TobyTools.com (email:info@tobytools.com)(website: www.tobytools.com)
 * @copyright Copyright TobyTools.com 
 * @license GNU/GPL
 */


class ArchiDashExporterArticle extends ArchiDashExporter{
	
	/*
	 *class constructor 
	 */
	function ArchiDashExporterArticle($opts){
		parent::__construct($opts);
		//check everything is allright
		if($this->completed){
			//options['sectionid'], since here you find all the categories related to
			if($this->options['sectionid']){
				$this->options['table']="#__content";
				$this->options['tag']="article";			
			}
			else{
				$this->completed=false;
				$this->options=false;
				$this->errMsg=false;
				global $mainframe;
				$mainframe->enqueueMessage(JText::_('NOSECTIONID')." ".get_class($this),'error');
			}
			
		}
		else{
			global $mainframe;
			$mainframe->enqueueMessage(JText::_('NOOBJECT')." ".get_class($this),'error');
		}
		
	}
	
	public function export($params=false){
		if($this->completed){
			global $mainframe;						
			$db =& JFactory::getDBO();
			
			//to avoid problem with character set
			$db->setQuery($this->options['characterset']);
			$db->query();

			/*the fields are the xml attributes*/
			//$query='SELECT title as name, alias, description  FROM '.$db->nameQuote($this->options['table']). ' WHERE '.$db->nameQuote('section').' = '.$db->Quote($this->options['sectionid']);
			$query ='
				SELECT a.title AS name, a.alias AS alias, s.title AS section, c.title AS category, a.introtext AS introtext, `fulltext`
				FROM 	'.$db->nameQuote($this->options['table']).' AS a,
						'.$db->nameQuote("#__sections").' AS s,
						'.$db->nameQuote("#__categories").' AS c
				WHERE	'.$db->nameQuote('a.sectionid').' = '.$db->Quote($this->options['sectionid']).' AND 
						'.$db->nameQuote('a.sectionid').' = '.$db->nameQuote('s.id').' AND
						'.$db->nameQuote('a.sectionid').' = '.$db->nameQuote('c.section').' AND
						'.$db->nameQuote('a.catid').' = '.$db->nameQuote('c.id').'    
						
			';
			
			$db->setQuery($query);
			$vet = $db->loadAssocList();			
			
			if(!$vet){
				//something wrong
				if($db->getErrorNum()==0){
					//it is not an error, the problem is there is no record with the specified id
					$this->errMsg = false;
					$this->completed=false;
					$mainframe->enqueueMessage(JText::_( 'NOSECTION' ),'error');
					$mainframe->enqueueMessage(get_class($this)."::export",'error');	
				}
				else{
					//MYSQL error
					$this->completed=false;
					$this->errMsg = $db->getErrorNum()." : ".$db->getErrorMsg();
					$mainframe->enqueueMessage(JText::_( 'QUERYKO' ),'error');
					$mainframe->enqueueMessage($this->errMsg,'error');
					$mainframe->enqueueMessage(get_class($this)."::export",'error');					
				}
			}			
			else{	
				$xml="";			
				foreach($vet as $article){
					$xml.=$this->exportArticle($article);
				}
				return $xml;								
			}			
		}
		else{
			return "";
		}				
	}
	
	protected function exportArticle($vet){
		
		//create the xml string
		$xml="<{$this->options['tag']} ";				
		foreach($vet as $key => $value){
			//check value
			if(strcasecmp(trim($value),"")==0){
				//skip
			}
			else{						
				//from utf-8 to html entities			
				$value=$this->convertToHTMLEntities($value);
				//check if it's attribute or nested tag
				if(strcasecmp(trim($key),"introtext")==0){
					$xml.=">";//close the head tag
					$xml.="<introtext>$value</introtext>";
				}elseif(strcasecmp(trim($key),"fulltext")==0){
					$xml.="<fulltext>$value</fulltext>";
				}else{
					$xml.=" $key=\"$value\" ";	
				}
				
			}
		}
		$xml.="</{$this->options['tag']}>";				
		return $xml;
	}
	

}

