<?php
/**
 * @version		$Id: template.php 1.0 19-11-2009 Danijar
 * @package		Frontend Admin
 * @copyright	Copyright (C) 2009 CMSSpace. http://www.cmsspace.com
 * @license		GNU/GPL. http://www.gnu.org/licenses/gpl.html
 */
jimport( 'joomla.application.component.controller' );
class AikControllerTemplate extends JController
{
	function __construct()
	{
		parent::__construct(array('default_task' => 'viewTemplates'));
		$this->registerTask( 'apply', 		'save');
		$this->registerTask( 'apply_source', 		'save_source');
		$this->registerTask( 'apply_css', 		'save_css');
		$this->registerTask( 'default', 		'publish');
	}
	function viewTemplates()
	{
		global $mainframe, $option;
		$db		=& JFactory::getDBO();
		$client	=& JApplicationHelper::getClientInfo(JRequest::getVar('client', '0', '', 'int'));
		$limit		= $mainframe->getUserStateFromRequest('global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int');
		$limitstart = $mainframe->getUserStateFromRequest('com_template.'.$client->id.'.limitstart', 'limitstart', 0, 'int');

		JToolBarHelper::title( JText::_( 'Template Manager' ), 'thememanager' );

		if ($client->id == '1') {
			JToolBarHelper::makeDefault('publish');
		} else {
			JToolBarHelper::makeDefault();
		}
		JToolBarHelper::editListX( 'edit', 'Edit' );
		JToolBarHelper::help( 'screen.templates' );
		JToolBarHelper::cpanel();
		
		$select[] 			= JHTML::_('select.option', '0', JText::_('Site'));
		$select[] 			= JHTML::_('select.option', '1', JText::_('Administrator'));
		$lists['client'] 	= JHTML::_('select.genericlist',  $select, 'client', 'class="inputbox" size="1" onchange="document.adminForm.submit();"', 'value', 'text', $client->id);

		$tBaseDir = $client->path.DS.'templates';
		$rows = array();
		$rows = TemplatesHelper::parseXMLTemplateFiles($tBaseDir);
		for($i = 0; $i < count($rows); $i++)  {
			$rows[$i]->assigned		= TemplatesHelper::isTemplateAssigned($rows[$i]->directory);
			$rows[$i]->published	= TemplatesHelper::isTemplateDefault($rows[$i]->directory, $client->id);
		}

		jimport('joomla.html.pagination');
		$page = new JPagination(count($rows), $limitstart, $limit);

		$rows = array_slice($rows, $page->limitstart, $page->limit);

		$view =& $this->getView( 'template' );
		$view->showTemplates($rows, $lists, $page, $option, $client);
	}
	function preview()
	{
		
		$template	= JRequest::getVar('id', '', 'method', 'cmd');
		$option 	= JRequest::getCmd('option');
		$client		=& JApplicationHelper::getClientInfo(JRequest::getVar('client', '0', '', 'int'));

		JToolBarHelper::title( JText::_( 'Template Manager' ), 'thememanager' );
		JToolBarHelper::back();
		JToolBarHelper::cpanel();
		
		
		if (!$template)
		{
			return JError::raiseWarning( 500, JText::_('Template not specified') );
		}
		jimport('joomla.client.helper');
		JClientHelper::setCredentialsFromRequest('ftp');

		$view =& $this->getView( 'template' );
		$view->previewTemplate($template, true, $client, $option);
	}
	function publish()
	{
		global $mainframe;

		JRequest::checkToken() or jexit( 'Invalid Token' );

		$db		= & JFactory::getDBO();
		$cid	= JRequest::getVar('cid', array(), 'method', 'array');
		$cid	= array(JFilterInput::clean(@$cid[0], 'cmd'));
		$option	= JRequest::getCmd('option');
		$client	=& JApplicationHelper::getClientInfo(JRequest::getVar('client', '0', '', 'int'));

		if ($cid[0])
		{
			$query = 'DELETE FROM #__templates_menu' .
					' WHERE client_id = '.(int) $client->id .
					' AND (menuid = 0 OR template = '.$db->Quote($cid[0]).')';
			$db->setQuery($query);
			$db->query();

			$query = 'INSERT INTO #__templates_menu' .
					' SET client_id = '.(int) $client->id .', template = '.$db->Quote($cid[0]).', menuid = 0';
			$db->setQuery($query);
			$db->query();
		}

		$mainframe->redirect('index.php?option='.$option.'&c=template&client='.$client->id);
	}

	function edit()
	{
		jimport('joomla.filesystem.path');

		$db			= & JFactory::getDBO();
		$cid		= JRequest::getVar('cid', array(), 'method', 'array');
		$cid		= array(JFilterInput::clean(@$cid[0], 'cmd'));
		$template	= $cid[0];
		$option		= JRequest::getCmd('option');
		$client		=& JApplicationHelper::getClientInfo(JRequest::getVar('client', '0', '', 'int'));

		JToolBarHelper::title( JText::_( 'Template' ) . ': <small><small>[ '. JText::_( 'Edit' ) .' ]</small></small>', 'thememanager' );
		JToolBarHelper::custom('preview', 'preview.png', 'preview_f2.png', 'Preview', false, false);
		JToolBarHelper::custom( 'edit_source', 'html.png', 'html_f2.png', 'Edit HTML', false, false );
		JToolBarHelper::custom( 'choose_css', 'css.png', 'css_f2.png', 'Edit CSS', false, false );
		JToolBarHelper::save( 'save' );
		JToolBarHelper::apply();
		JToolBarHelper::cancel( 'cancel', 'Close' );
		JToolBarHelper::cpanel();
		
		if (!$cid[0]) {
			return JError::raiseWarning( 500, JText::_('Template not specified') );
		}

		$tBaseDir	= JPath::clean($client->path.DS.'templates');

		if (!is_dir( $tBaseDir . DS . $template )) {
			return JError::raiseWarning( 500, JText::_('Template not found') );
		}
		$lang =& JFactory::getLanguage();
		$lang->load( 'tpl_'.$template, JPATH_ADMINISTRATOR );

		$ini	= $client->path.DS.'templates'.DS.$template.DS.'params.ini';
		$xml	= $client->path.DS.'templates'.DS.$template.DS.'templateDetails.xml';
		$row	= TemplatesHelper::parseXMLTemplateFile($tBaseDir, $template);

		jimport('joomla.filesystem.file');
		if (JFile::exists($ini)) {
			$content = JFile::read($ini);
		} else {
			$content = null;
		}

		$params = new JParameter($content, $xml, 'template');

		$assigned = TemplatesHelper::isTemplateAssigned($row->directory);
		$default = TemplatesHelper::isTemplateDefault($row->directory, $client->id);

		if($client->id == '1')  {
			$lists['selections'] =  JText::_('Cannot assign an administrator template');
		} else {
			$lists['selections'] = TemplatesHelper::createMenuList($template);
		}

		if ($default) {
			$row->pages = 'all';
		} elseif (!$assigned) {
			$row->pages = 'none';
		} else {
			$row->pages = null;
		}

		jimport('joomla.client.helper');
		$ftp =& JClientHelper::setCredentialsFromRequest('ftp');

		$view =& $this->getView( 'template' );
		$view->editTemplate($row, $lists, $params, $option, $client, $ftp, $template);
	}

	function save()
	{
		global $mainframe;

		JRequest::checkToken() or jexit( 'Invalid Token' );
		$db			 = & JFactory::getDBO();

		$template	= JRequest::getVar('id', '', 'method', 'cmd');
		$option		= JRequest::getVar('option', '', '', 'cmd');
		$client		=& JApplicationHelper::getClientInfo(JRequest::getVar('client', '0', '', 'int'));
		$menus		= JRequest::getVar('selections', array(), 'post', 'array');
		$params		= JRequest::getVar('params', array(), 'post', 'array');
		$default	= JRequest::getBool('default');
		JArrayHelper::toInteger($menus);

		if (!$template) {
			$mainframe->redirect('index.php?option='.$option.'&c=template&client='.$client->id, JText::_('Operation Failed').': '.JText::_('No template specified.'));
		}

		jimport('joomla.client.helper');
		JClientHelper::setCredentialsFromRequest('ftp');
		$ftp = JClientHelper::getCredentials('ftp');

		$file = $client->path.DS.'templates'.DS.$template.DS.'params.ini';

		jimport('joomla.filesystem.file');
		if (JFile::exists($file) && count($params))
		{
			$registry = new JRegistry();
			$registry->loadArray($params);
			$txt = $registry->toString();

			if (!$ftp['enabled'] && JPath::isOwner($file) && !JPath::setPermissions($file, '0755')) {
				JError::raiseNotice('SOME_ERROR_CODE', JText::_('Could not make the template parameter file writable'));
			}

			$return = JFile::write($file, $txt);

			if (!$ftp['enabled'] && JPath::isOwner($file) && !JPath::setPermissions($file, '0555')) {
				JError::raiseNotice('SOME_ERROR_CODE', JText::_('Could not make the template parameter file unwritable'));
			}

			if (!$return) {
				$mainframe->redirect('index.php?option='.$option.'&c=template&client='.$client->id, JText::_('Operation Failed').': '.JText::sprintf('Failed to open file for writing.', $file));
			}
		}

		$query = 'DELETE FROM #__templates_menu' .
				' WHERE client_id = 0' .
				' AND template = '.$db->Quote( $template );
		$db->setQuery($query);
		$db->query();

		if ($default) {
			$menus = array( 0 );
		}

		foreach ($menus as $menuid)
		{
			if ((int) $menuid >= 0)
			{
				$query = 'DELETE FROM #__templates_menu' .
						' WHERE client_id = 0' .
						' AND menuid = '.(int) $menuid;
				$db->setQuery($query);
				$db->query();

				$query = 'INSERT INTO #__templates_menu' .
						' SET client_id = 0, template = '. $db->Quote( $template ) .', menuid = '.(int) $menuid;
				$db->setQuery($query);
				$db->query();
			}
		}

		$task = JRequest::getCmd('task');
		if($task == 'apply') {
			$mainframe->redirect('index.php?option='.$option.'&c=template&task=edit&cid[]='.$template.'&client='.$client->id);
		} else {
			$mainframe->redirect('index.php?option='.$option.'&c=template&client='.$client->id);
		}
	}

	function cancel()
	{
		global $mainframe;

		$option	= JRequest::getCmd('option');
		$client	=& JApplicationHelper::getClientInfo(JRequest::getVar('client', '0', '', 'int'));

		jimport('joomla.client.helper');
		JClientHelper::setCredentialsFromRequest('ftp');

		$mainframe->redirect('index.php?option='.$option.'&c=template&client='.$client->id);
	}

	function edit_source()
	{
		global $mainframe;

		$option		= JRequest::getCmd('option');
		$client		=& JApplicationHelper::getClientInfo(JRequest::getVar('client', '0', '', 'int'));
		$template	= JRequest::getVar('id', '', 'method', 'cmd');
		$file		= $client->path.DS.'templates'.DS.$template.DS.'index.php';

		JToolBarHelper::title( JText::_( 'Template HTML Editor' ), 'thememanager' );
		JToolBarHelper::save( 'save_source' );
		JToolBarHelper::apply( 'apply_source' );
		JToolBarHelper::cancel('edit');
		JToolBarHelper::help( 'screen.templates' );
		JToolBarHelper::cpanel();

		jimport('joomla.filesystem.file');
		$content = JFile::read($file);

		if ($content !== false)
		{
			jimport('joomla.client.helper');
			$ftp =& JClientHelper::setCredentialsFromRequest('ftp');

			$content = htmlspecialchars($content, ENT_COMPAT, 'UTF-8');
			$view =& $this->getView( 'template' );
			$view->editTemplateSource($template, $content, $option, $client, $ftp);
		} else {
			$msg = JText::sprintf('Operation Failed Could not open', $file);
			$mainframe->redirect('index.php?option='.$option.'&c=template&client='.$client->id, $msg);
		}
	}

	function save_source()
	{
		global $mainframe;

		JRequest::checkToken() or jexit( 'Invalid Token' );

		$option			= JRequest::getCmd('option');
		$client			=& JApplicationHelper::getClientInfo(JRequest::getVar('client', '0', '', 'int'));
		$template		= JRequest::getVar('id', '', 'method', 'cmd');
		$filecontent	= JRequest::getVar('filecontent', '', 'post', 'string', JREQUEST_ALLOWRAW);

		if (!$template) {
			$mainframe->redirect('index.php?option='.$option.'&c=template&client='.$client->id, JText::_('Operation Failed').': '.JText::_('No template specified.'));
		}

		if (!$filecontent) {
			$mainframe->redirect('index.php?option='.$option.'&c=template&client='.$client->id, JText::_('Operation Failed').': '.JText::_('Content empty.'));
		}

		jimport('joomla.client.helper');
		JClientHelper::setCredentialsFromRequest('ftp');
		$ftp = JClientHelper::getCredentials('ftp');

		$file = $client->path.DS.'templates'.DS.$template.DS.'index.php';

		if (!$ftp['enabled'] && !JPath::setPermissions($file, '0755')) {
			JError::raiseNotice('SOME_ERROR_CODE', JText::_('Could not make the template file writable'));
		}

		jimport('joomla.filesystem.file');
		$return = JFile::write($file, $filecontent);

		if (!$ftp['enabled'] && !JPath::setPermissions($file, '0555')) {
			JError::raiseNotice('SOME_ERROR_CODE', JText::_('Could not make the template file unwritable'));
		}
		if ($return)
		{
			$task = JRequest::getCmd('task');
			switch($task)
			{
				case 'apply_source':
					$mainframe->redirect('index.php?option='.$option.'&c=template&client='.$client->id.'&task=edit_source&id='.$template, JText::_('Template source saved'));
					break;

				case 'save_source':
				default:
					$mainframe->redirect('index.php?option='.$option.'&c=template&client='.$client->id.'&task=edit&cid[]='.$template, JText::_('Template source saved'));
					break;
			}
		}
		else {
			$mainframe->redirect('index.php?option='.$option.'&c=template&client='.$client->id, JText::_('Operation Failed').': '.JText::sprintf('Failed to open file for writing.', $file));
		}
	}

	function choose_css()
	{
		global $mainframe;

		$option 	= JRequest::getCmd('option');
		$template	= JRequest::getVar('id', '', 'method', 'cmd');
		$client		=& JApplicationHelper::getClientInfo(JRequest::getVar('client', '0', '', 'int'));

		JToolBarHelper::title( JText::_( 'Template CSS Editor' ), 'thememanager' );
		JToolBarHelper::custom( 'edit_css', 'edit.png', 'edit_f2.png', 'Edit', true );
		JToolBarHelper::cancel('edit');
		JToolBarHelper::cpanel();
		
		$dir = $client->path.DS.'templates'.DS.$template.DS.'css';
		jimport('joomla.filesystem.folder');
		$files = JFolder::files($dir, '\.css$', false, false);
		jimport('joomla.client.helper');
		JClientHelper::setCredentialsFromRequest('ftp');

		$view =& $this->getView( 'template' );
		$view->chooseCSSFiles($template, $dir, $files, $option, $client);
	}

	function edit_css()
	{
		global $mainframe;
		$option		= JRequest::getCmd('option');
		$client		=& JApplicationHelper::getClientInfo(JRequest::getVar('client', '0', '', 'int'));
		$template	= JRequest::getVar('id', '', 'method', 'cmd');
		$filename	= JRequest::getVar('filename', '', 'method', 'cmd');

		JToolBarHelper::title( JText::_( 'Template Manager' ), 'thememanager' );
		JToolBarHelper::save( 'save_css' );
		JToolBarHelper::apply( 'apply_css');
		JToolBarHelper::cancel('choose_css');
		JToolBarHelper::cpanel();
		
		jimport('joomla.filesystem.file');

		if (JFile::getExt($filename) !== 'css') {
			$msg = JText::_('Wrong file type given, only CSS files can be edited.');
			$mainframe->redirect('index.php?option='.$option.'&c=template&client='.$client->id.'&task=choose_css&id='.$template, $msg, 'error');
		}

		$content = JFile::read($client->path.DS.'templates'.DS.$template.DS.'css'.DS.$filename);

		if ($content !== false)
		{
			jimport('joomla.client.helper');
			$ftp =& JClientHelper::setCredentialsFromRequest('ftp');

			$content = htmlspecialchars($content, ENT_COMPAT, 'UTF-8');
			$view =& $this->getView( 'template' );
			$view->editCSSSource($template, $filename, $content, $option, $client, $ftp);
		}
		else
		{
			$msg = JText::sprintf('Operation Failed Could not open', $client->path.$filename);
			$mainframe->redirect('index.php?option='.$option.'&c=template&client='.$client->id, $msg);
		}
	}

	function save_css()
	{
		global $mainframe;
		JRequest::checkToken() or jexit( 'Invalid Token' );

		$option			= JRequest::getCmd('option');
		$client			=& JApplicationHelper::getClientInfo(JRequest::getVar('client', '0', '', 'int'));
		$template		= JRequest::getVar('id', '', 'post', 'cmd');
		$filename		= JRequest::getVar('filename', '', 'post', 'cmd');
		$filecontent	= JRequest::getVar('filecontent', '', 'post', 'string', JREQUEST_ALLOWRAW);

		if (!$template) {
			$mainframe->redirect('index.php?option='.$option.'&c=template&client='.$client->id, JText::_('Operation Failed').': '.JText::_('No template specified.'));
		}

		if (!$filecontent) {
			$mainframe->redirect('index.php?option='.$option.'&c=template&client='.$client->id, JText::_('Operation Failed').': '.JText::_('Content empty.'));
		}

		jimport('joomla.client.helper');
		JClientHelper::setCredentialsFromRequest('ftp');
		$ftp = JClientHelper::getCredentials('ftp');

		$file = $client->path.DS.'templates'.DS.$template.DS.'css'.DS.$filename;

		if (!$ftp['enabled'] && JPath::isOwner($file) && !JPath::setPermissions($file, '0755')) {
			JError::raiseNotice('SOME_ERROR_CODE', JText::_('Could not make the css file writable'));
		}

		jimport('joomla.filesystem.file');
		$return = JFile::write($file, $filecontent);

		if (!$ftp['enabled'] && JPath::isOwner($file) && !JPath::setPermissions($file, '0555')) {
			JError::raiseNotice('SOME_ERROR_CODE', JText::_('Could not make the css file unwritable'));
		}

		if ($return)
		{
			$task = JRequest::getCmd('task');
			switch($task)
			{
				case 'apply_css':
					$mainframe->redirect('index.php?option='.$option.'&c=template&client='.$client->id.'&task=edit_css&id='.$template.'&filename='.$filename,  JText::_('File Saved'));
					break;

				case 'save_css':
				default:
					$mainframe->redirect('index.php?option='.$option.'&c=template&client='.$client->id.'&task=edit&cid[]='.$template, JText::_('File Saved'));
					break;
			}
		}
		else {
			$mainframe->redirect('index.php?option='.$option.'&c=template&client='.$client->id.'&id='.$template.'&task=choose_css', JText::_('Operation Failed').': '.JText::sprintf('Failed to open file for writing.', $file));
		}
	}
}
