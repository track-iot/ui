<?php


defined('_JEXEC') or die('Restricted Access');
jimport('joomla.application.component.model');
require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'engine'.DS.'exporters'.DS.'exporter.php');


/**
 * The following class export the section 
 * @version 1.0
 * @author  TobyTools.com (email:info@tobytools.com)(website: www.tobytools.com)
 * @copyright Copyright TobyTools.com 
 * @license GNU/GPL
 */


class ArchiDashExporterDashboard extends ArchiDashExporter{
	
	/*
	 *class constru ctor 
	 */
	function ArchiDashExporterDashboard($opts){
		parent::__construct($opts);
		//check everything is allright
		if($this->completed){

			if($this->options['dashboard']){
				$this->options['table']="";
				$this->options['tag']="dashboard";			
			}
			else{
				$this->completed=false;
				$this->options=false;
				$this->errMsg=false;
				global $mainframe;
				$mainframe->enqueueMessage(JText::_('NODASHBOARDINFO')." ".get_class($this),'error');
			}
			
		}
		else{
			global $mainframe;
			$mainframe->enqueueMessage(JText::_('NOOBJECT')." ".get_class($this),'error');
		}
		
	}
	
	public function export ($params=false){
		if($this->completed){
			require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'engine'.DS.'exporters'.DS.'section.php');
			require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'engine'.DS.'exporters'.DS.'category.php');
			require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'engine'.DS.'exporters'.DS.'article.php');
			require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'engine'.DS.'exporters'.DS.'menu.php');
			require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'engine'.DS.'exporters'.DS.'module.php');
			require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'engine'.DS.'exporters'.DS.'datasource.php');
			
			$xml="";
			
			$classname = "ArchiDashExporterSection";
			//sectionid expected
			$exporter = new $classname($this->options);
			$xml .= $exporter->export();
			
			$classname = "ArchiDashExporterCategory";
			//sectionid expected
			$exporter = new $classname($this->options);
			$xml .= $exporter->export();
			
			$classname = "ArchiDashExporterArticle";
			//sectionid expected
			$exporter = new $classname($this->options);
			$xml .= $exporter->export();
			
			$classname = "ArchiDashExporterMenu";
			//menu expected (name,description)
			$exporter = new $classname($this->options);
			$xml .= $exporter->export();
			
			$classname = "ArchiDashExporterModule";
			//modules expected (arrays of moduleids)
			$exporter = new $classname($this->options);
			$xml .= $exporter->export();
			
			$this->options['datasources'] = $exporter->getDataSourceIDs();
			$classname = "ArchiDashExporterDatasource";
			//datasources array expected (sql array of ids, article array of ids, ...)
			$exporter = new $classname($this->options);
			$xml .= $exporter->export();
			
			$dashboard = $this->options['dashboard'];
			$tag = "dashboard";
			
			$version = JText::_('ARCHIDASHVERSION');
			
			$result= "<$tag name=\"{$dashboard['name']}\" description=\"{$dashboard['description']}\" accesslevel=\"{$dashboard['accesslevel']}\" version=\"$version\">";
			$result.=$xml;
			$result.="</$tag>";
			
			return $result;
		}
		else{
			return "";
		}
	}
}

