<?php


defined('_JEXEC') or die('Restricted Access');
jimport('joomla.application.component.model');
require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'engine'.DS.'exporters'.DS.'exporter.php');


/**
 * The following class export the section 
 * @version 1.0
 * @author  TobyTools.com (email:info@tobytools.com)(website: www.tobytools.com)
 * @copyright Copyright TobyTools.com 
 * @license GNU/GPL
 */


class ArchiDashExporterMenu extends ArchiDashExporter{
	
	/*
	 *class constructor 
	 */
	function ArchiDashExporterMenu($opts){
		parent::__construct($opts);
		//check everything is allright
		if($this->completed){
			//options['menu']. It is an array with (name, description)
			if($this->options['menu']){
				$this->options['table']="";
				$this->options['tag']="menu";			
			}
			else{
				$this->completed=false;
				$this->options=false;
				$this->errMsg=false;
				global $mainframe;
				$mainframe->enqueueMessage(JText::_('NOMENU')." ".get_class($this),'error');
			}
			
		}
		else{
			global $mainframe;
			$mainframe->enqueueMessage(JText::_('NOOBJECT')." ".get_class($this),'error');
		}
		
	}
	
	
	
	public function export($params=false){
		if($this->completed){
			global $mainframe;
			$vet = $this->options['menu'];
			$vet['menutype']=$this->getMenuType($vet['name']);
			
			//create the xml string
			$xml="<{$this->options['tag']} ";
			foreach($vet as $key => $value){
				//check value
				if(strcasecmp(trim($value),"")==0){
					//skip
				}
				else{					
					//from utf-8 to html entities
					$value=$this->convertToHTMLEntities($value);
					$xml.=" $key=\"$value\" ";
				}
			}
			$xml.=" />";				
				return $xml;						
					
		}
		else{
			return "";
		}				
	}
	
	protected  function getMenuType($name){
		//take the name and delete the white spaces
		return str_replace(" ","",$name);
	}
	
}


