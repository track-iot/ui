<?php
/**
 * @version		$Id: directory.php 1.0 19-11-2009 Danijar
 * @package		Frontend Admin
 * @copyright	Copyright (C) 2009 CMSSpace. http://www.cmsspace.com
 * @license		GNU/GPL. http://www.gnu.org/licenses/gpl.html
 */
defined('_JEXEC') or die('Restricted access');

jimport('joomla.filesystem.folder');
$cparams = JComponentHelper::getParams ('com_media');
$config =& JFactory::getConfig();
?>
<fieldset class="adminform">
	<legend><?php echo JText::_( 'Directory Permissions' ); ?></legend>
		<table class="adminlist">
		<thead>
			<tr>
				<th width="650">
					<?php echo JText::_( 'Directory' ); ?>
				</th>
				<th>
					<?php echo JText::_( 'Status' ); ?>
				</th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="2">
					&nbsp;
				</td>
			</tr>
		</tfoot>
		<tbody>
			<?php
			AikViewAdmin::writableCell( 'administrator/backups' );
			AikViewAdmin::writableCell( 'administrator/components' );
			AikViewAdmin::writableCell( 'administrator/language' );

			$admin_langs = JFolder::folders(JPATH_ADMINISTRATOR.DS.'language');
			foreach ($admin_langs as $alang)
			{
				AikViewAdmin::writableCell( 'administrator/language/'.$alang );
			}

			AikViewAdmin::writableCell( 'administrator/modules' );
			AikViewAdmin::writableCell( 'administrator/templates' );
			AikViewAdmin::writableCell( 'components' );
			AikViewAdmin::writableCell( 'images' );
			AikViewAdmin::writableCell( 'images/banners' );
			AikViewAdmin::writableCell( $cparams->get('image_path'));
			AikViewAdmin::writableCell( 'language' );

			$site_langs	= JFolder::folders(JPATH_SITE.DS.'language');
			foreach ($site_langs as $slang)
			{
				AikViewAdmin::writableCell( 'language/'.$slang );
			}

			AikViewAdmin::writableCell( 'media' );
			AikViewAdmin::writableCell( 'modules' );
			AikViewAdmin::writableCell( 'plugins' );
			AikViewAdmin::writableCell( 'plugins/content' );
			AikViewAdmin::writableCell( 'plugins/editors' );
			AikViewAdmin::writableCell( 'plugins/editors-xtd' );
			AikViewAdmin::writableCell( 'plugins/search' );
			AikViewAdmin::writableCell( 'plugins/system' );
			AikViewAdmin::writableCell( 'plugins/user' );
			AikViewAdmin::writableCell( 'plugins/xmlrpc' );
			AikViewAdmin::writableCell( 'templates' );
			AikViewAdmin::writableCell( JPATH_SITE.DS.'cache', 0, '<strong>'. JText::_( 'Cache Directory' ) .'</strong> ' );
			AikViewAdmin::writableCell( JPATH_ADMINISTRATOR.DS.'cache', 0, '<strong>'. JText::_( 'Cache Directory' ) .'</strong> ' );
			AikViewAdmin::writableCell( $config->getValue('config.log_path', JPATH_ROOT.DS.'log'),0, '<strong>'. JText::_( 'Log Directory' ) .
							' ($log_path)</strong> ');
			AikViewAdmin::writableCell( $config->getValue('config.tmp_path', JPATH_ROOT.DS.'tmp'),0, '<strong>'. JText::_( 'Temp Directory' ) .
							' ($tmp_path)</strong> ');
			?>
		</tbody>
		</table>
</fieldset>
