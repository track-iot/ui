<?php


defined('_JEXEC') or die('Restricted Access');
jimport('joomla.application.component.model');
require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'engine'.DS.'factories'.DS.'factory.php');


/**
 * The following class create the articles 
 * @version 1.0
 * @author  TobyTools.com (email:info@tobytools.com)(website: www.tobytools.com)
 * @copyright Copyright TobyTools.com 
 * @license GNU/GPL
 */


class ArchiDashFactoryArticle extends ArchiDashFactory{
	
	/*
	 *class constructor 
	 */
	function ArchiDashFactoryArticle($opts){
		parent::__construct($opts);
		//check everything is allright
		if($this->completed){
			
			$this->options['table']="#__content";
			$this->options['tag']="article";		
		}
		else{
			global $mainframe;
			$mainframe->enqueueMessage(JText::_('NOOBJECT')." ".get_class($this),'error');
		}
	}
	/*
	 * create section if it does not exist
	 */
	function create(){
		if($this->completed){	
			global $mainframe;
			
			$num = (int)$this->readDefinition($this->options['tag'],false,false,true);
			/*check there is at least one article*/
			if($num<=0){
				$this->completed=false;
				$this->errMsg.=get_class($this)."::create ".JText::_('DEFINITIONNOTVALID');
				$mainframe->enqueueMessage($this->errMsg,'error');
				return false;
			}			
			
			$this->msg.="<h3>".JText::_('Article')."</h3>";
			
			for ($i=1; $i<=$num; $i++){
				
				
				//article title
				$title = $this->readDefinition($this->options['tag'],$i,"name");
				//article alias
				$alias = $this->readDefinition($this->options['tag'],$i,"alias");
				//article introtext
				
				$introtext = $this->readDefinition($this->options['tag'],$i,false,false,"introtext");
				//article fulltext
				$fulltext = $this->readDefinition($this->options['tag'],$i,false,false,"fulltext");
				//article section
				$section = $this->readDefinition($this->options['tag'],$i,"section");
				//article category
				$category = $this->readDefinition($this->options['tag'],$i,"category");
								
						
				
				
				//check the values are not empty, no introtext and no fulltext allowed
				if(	strcasecmp(trim($title),"")==0 || strcasecmp(trim($alias),"")==0 ||
				
				strcasecmp(trim($section),"")==0 || strcasecmp(trim($category),"")==0 
					){
					$this->completed=false;
					$this->errMsg.=get_class($this)."::create ".JText::_('DEFINITIONNOTVALID')."<br/><br/>
							Article $title<br/><br/>							
					";
					$mainframe->enqueueMessage($this->errMsg,'error');
					//exit from the foreach
					continue;//skip next one
				}
				
				//category creation
				$this->createArticle($title,$alias,$introtext,$fulltext,$section,$category);		
			}
		}
		else{
			//no op
		}
		
	}
	
	/**
	 * 
	 * @param unknown_type $title
	 * @param unknown_type $alias
	 * @param unknown_type $introtext
	 * @param unknown_type $fulltext
	 * @param unknown_type $section
	 * @param unknown_type $category
	 * @return unknown_type
	 */
	protected function createArticle($title,$alias,$introtext,$fulltext,$section,$category){
		global $mainframe;
		$articleid=false;
		
		/*check if the category already exists.*/
		$db =& JFactory::getDBO();
			
		//to avoid problem with character set
		$db->setQuery($this->options['characterset']);
		$db->query();
		
		/*
		 	SELECT s.id as sectionid, c.id as categoryid FROM jos_sections AS s, jos_categories AS c
			WHERE s.title = "ArchiDash" AND c.title = "Standard Dashboard" AND c.section = s.id
		 */
		
		//select the section and category ids
		
		$query='SELECT  s.id as sectionid, c.id as categoryid 
				FROM '.$db->nameQuote("#__sections").' as s,'.$db->nameQuote("#__categories").' as c 
				WHERE '.$db->nameQuote('s.title').' = '.$db->Quote($section).' AND '.
						$db->nameQuote('c.title').' = '.$db->Quote($category).' AND '.
						$db->nameQuote('c.section').' = '.$db->nameQuote('s.id');
		$db->setQuery($query);
		$vet = $db->loadAssoc();
		
		
		$cid=$vet['categoryid'];
		$sid=$vet['sectionid'];
		
		//check the values are not empty
		if( $cid==false || $sid==false){
			$this->completed=false;
			$this->errMsg.=get_class($this)."::createArticle $title".JText::_('SECCATNOTVALID').". Section $section Category $category ";
			$mainframe->enqueueMessage($this->errMsg,'error');
			//exit from the foreach
			return false;
		}
	
		
		
		$user =& JFactory::getUser();
		$userid = $user->get( 'id' );
		
		
		
		//create the article
		$query = "INSERT INTO ".$db->nameQuote($this->options['table'])." (title,alias,introtext,`fulltext`,state,sectionid,catid,created_by,access,created) VALUES ('$title','$alias','$introtext','$fulltext','1','$sid','$cid','$userid','{$this->options['accesslevel']}',NOW())";			
		
		
		
		$db->setQuery($query);
		$result = $db->query();
		
		if(!$result){
			//something wrong
			$this->completed=false;
			$this->errMsg .= $db->getErrorNum()." : ".$db->getErrorMsg();
			$mainframe->enqueueMessage(JText::_( 'QUERYKO' ),'error');
			$mainframe->enqueueMessage($this->errMsg,'error');
			$mainframe->enqueueMessage(get_class($this)."::create",'error');
			return false;
		}
		else{
			//clean
			$this->errMsg .= false;
			//provide the element id
			$db->setQuery("SELECT LAST_INSERT_ID()");				
			$articleid= $db->loadResult();
			
		}
			
		
		//output message
		$this->msg.="$title<br/>";			
		
	
		
		
		//save the information inside #__archidash_articles
		require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'models'.DS.'article.php');
		$classname="ArchiDashModelArticle";
		$model = new $classname();
		$vet = array();
			$vet['articleid']=$articleid;
			$vet['dashboardid']=$this->options['dashboardid'];
		
		$model->insertRecord($vet);
		//check there are errors for the creation of record with the model
		if($model->getErrMsg()!=false){
			$this->completed=false;
			$this->errMsg.=$model->getErrMsg();
		}
		
	}

}



