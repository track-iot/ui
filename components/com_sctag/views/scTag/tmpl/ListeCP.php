<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Listes d�roulantes pilot�es par JavaScript Asynchrone et une Base de donn�es</title>

<script type="text/javascript">
var Ld1Id='';
var Ld2Id='';
var id_liste='';

function ValideLd2(val) {
	Ld1Id=val; //id_d�partement
	id_liste='2';//Utilis� dans la fonction ChargeLd() pour identifier la liste d�roulante
	var LD_URL = 'ValideLd2.php?Ld1='+Ld1Id;
	ObjetXHR(LD_URL)
	// R�initialisation de Ld3 si modification de LD1 apr�s passage en Ld2
	if (Ld2Id!='') {ValideLd3('');	}
}

function ValideLd3(val) {
	Ld2Id=val; //id_commune
	id_liste='3'; //Utilis� dans la fonction ChargeLd() pour identifier la liste d�roulante
	var LD_URL = 'ValideLd3.php?Ld1='+Ld1Id+'&Ld2='+Ld2Id;
	if (Ld2Id=='') {var LD_URL = 'ValideLd3.php';}	
	ObjetXHR(LD_URL)
}

function ObjetXHR(LD_URL) {
	//creation de l'objet XMLHttpRequest
	if (window.XMLHttpRequest) { // Mozilla,...
		xmlhttp=new XMLHttpRequest();
		if (xmlhttp.overrideMimeType) {
			xmlhttp.overrideMimeType('text/xml');
		}	
		xmlhttp.onreadystatechange=ChargeLd;
		xmlhttp.open("GET", LD_URL, true);
		xmlhttp.send(null);
	} else if (window.ActiveXObject) { //IE 
		xmlhttp=new ActiveXObject('Microsoft.XMLHTTP'); 
		if (xmlhttp) {
			xmlhttp.onreadystatechange=ChargeLd;
			xmlhttp.open('GET', LD_URL, false);
			xmlhttp.send();
		}
	}
	// Bouton non apparent car modification de LD1 ou Ld2
	document.getElementById('buttons').style.display='none';
}

// fonction pour manipuler l'appel asynchrone
function ChargeLd() {
	if (xmlhttp.readyState==4) { 
		if (xmlhttp.status==200) { 
			//span id="niv2" ou "niv3"
			document.getElementById('niv'+id_liste).innerHTML=xmlhttp.responseText; 
			if (xmlhttp.responseText.indexOf('disabled')<=0) {
				//focus sur liste d�roulante 2 ou 3
				document.getElementById('Liste'+id_liste).focus(); 
			}	
		}
	}
}

function Affiche_Btn() {
	document.getElementById('buttons').style.display='inline';
}
</script>

<style>
#buttons {
	display: none;
}
</style>
</head>

<body>
<noscript>
<p>Cette page n�cessite que JavaScript soit activ�; dans votre navigateur
</noscript>
<div id="Les3LD">
  <p>Selectionnez votre choix dans les listes d&eacute;roulantes:</p>
  <form method="get" action="ListeCP.php">
    <?php
    include 'ValideLd1.php'; ?>&nbsp; <!--Pour remplir la liste d�roulante 1-->
    <span id="niv2">
    <?php include 'ValideLd2.php'; ?></span>&nbsp; <!--Pour remplir la liste d�roulante 2-->
    <span id="niv3">
    <?php include 'ValideLd3.php'; ?></span>&nbsp; <!--Pour remplir la liste d�roulante 3-->
    <span id="buttons">
    <input type="submit" value="Valider">
    </span>
  </form>
</div>
</body>
</html>