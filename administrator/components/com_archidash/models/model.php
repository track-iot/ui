<?php


defined('_JEXEC') or die('Restricted Access');
jimport('joomla.application.component.model');


/**
 * The ArchiDashModel abstract class is the general class for the model classes
 * @version 1.0
 * @author  TobyTools.com (email:info@tobytools.com)(website: www.tobytools.com)
 * @copyright Copyright TobyTools.com 
 * @license GNU/GPL
 */


abstract class ArchiDashModel extends JModel{
	
	/**
	 * 
	 * @var string, the database table associated with the model class
	 */
	protected $table;
	/**
	 * 
	 * @var string, the error message (if any) associated with the last operation performed
	 */
	protected $errMsg;
	/**
	 * 
	 * @var string, specifying the character set
	 */
	protected $characterset;
	
	/**
	 * ArchiDashModel class constructor
	 * 
	 * */
	protected function ArchimedeModel(){
		parent::__construct();
		$this->errMsg = false;
		//to manage the utf8 with MYSQL
		$this->characterset="SET NAMES 'utf8'";
	}
	
	
	
	/**
	 * Select the information of the record with the specified id and returns an associative array.
	 * @param string $id, the id of record you want to get.
	 * @return array, the associative array of record with the specified id;
	 * */
	
	function selectRecord($id=false){
		global $mainframe;
		
		if($id!=false){
			$db =& JFactory::getDBO();
			
			//to avoid problem with character set
			$db->setQuery($this->characterset);
			$db->query();
			
			
			$query='SELECT * FROM '.$db->nameQuote($this->table). ' WHERE '.$db->nameQuote('id').' = '.$db->Quote($id);
			$db->setQuery($query);
			$vet = $db->loadAssoc();
			
			if(!$vet){
				//something wrong
				if($db->getErrorNum()==0){
					//it is not an error, the problem is there is no record with the specified id
					$this->errMsg = false;
				}
				else{
					//MYSQL error
					$this->errMsg = $db->getErrorNum()." : ".$db->getErrorMsg();
					$mainframe->enqueueMessage(JText::_( 'QUERYKO' ),'error');
					$mainframe->enqueueMessage($this->errMsg,'error');
					$mainframe->enqueueMessage(get_class($this)."::selectRecord",'error');
					
				}
			}			
			else{
				
			}
			
			return $vet;
		}
		else{
			$this->errMsg=JText::_( 'ARGNOVALID' );
			$mainframe->enqueueMessage(JText::_( 'ARGNOVALID' ),'error');
			$mainframe->enqueueMessage(get_class($this)."::selectRecord",'error');
			return false;
		}	
	}
	
	
	
	/**
	 * 
	 * Insert a new record starting by the associative array.
	 * @param array $vet the associative array with the information of the new record. N.B. The id is useless, the function lets MYSQL manage the id auto increment 
	 * @return A database resource if successful, FALSE if not.
	 * 
	 * */
	
	abstract function insertRecord($vet=false);
	
	
	
	/**
	 * 
	 * Updates a record starting by the associative array. E.g.  Array ( [id] => 1 [articleid] => 7 [tableid] => someid ).
	 * @param array $vet the associative array with the new information of record. 
	 * @return A database resource if successful, FALSE if not.
	 * 
	 * */
	
	abstract function updateRecord($vet=false);
	
	/**
	 * Deletes the record with the specified id. If the id does not exist the method does nothing
	 * @param string $id, the id of record you want to delete.
	 * @return A database resource if successful, FALSE if not.
	 * 
	 */
	
	function deleteRecord($id=false){
		global $mainframe;
		if($id!=false){
			$db =& JFactory::getDBO();
			$query = "DELETE FROM ".$db->nameQuote($this->table)." WHERE ".$db->nameQuote('id')." = ".$db->Quote($id);
			$db->setQuery($query);
			$result = $db->query();
			if(!$result){
				//something wrong
				if($db->getErrorNum()==0){
					//it is not an error, the problem is there is no record with the specified id
					$this->errMsg = false;
				}
				else{
					//MYSQL error
					$this->errMsg = $db->getErrorNum()." : ".$db->getErrorMsg();
					$mainframe->enqueueMessage(JText::_( 'QUERYKO' ),'error');
					$mainframe->enqueueMessage($this->errMsg,'error');
					$mainframe->enqueueMessage(get_class($this)."::deleteRecord",'error');
					
				}
			}
			else{
				//clean messages
				$this->errMsg=false;
				return $result;
			}
		}
		else{
			$this->errMsg=JText::_( 'ARGNOVALID' );
			$mainframe->enqueueMessage(JText::_( 'ARGNOVALID' ),'error');
			$mainframe->enqueueMessage(get_class($this)."::deleteRecord",'error');
			return false;
		}	
	}
	
	
	
	function deleteRecordsByField($field,$value){
		global $mainframe;
		if($field!=false){
			$db =& JFactory::getDBO();
			$query = "DELETE FROM ".$db->nameQuote($this->table)." WHERE ".$db->nameQuote($field)." = ".$db->Quote($value);
			$db->setQuery($query);
			$result = $db->query();
			if(!$result){
				//something wrong
				if($db->getErrorNum()==0){
					//it is not an error, the problem is there is no record with the specified id
					$this->errMsg = false;
				}
				else{
					//MYSQL error
					$this->errMsg = $db->getErrorNum()." : ".$db->getErrorMsg();
					$mainframe->enqueueMessage(JText::_( 'QUERYKO' ),'error');
					$mainframe->enqueueMessage($this->errMsg,'error');
					$mainframe->enqueueMessage(get_class($this)."::deleteRecordsByField",'error');
					
				}
			}
			else{
				//clean messages
				$this->errMsg=false;
				return $result;
			}
		}
		else{
			$this->errMsg=JText::_( 'ARGNOVALID' );
			$mainframe->enqueueMessage(JText::_( 'ARGNOVALID' ),'error');
			$mainframe->enqueueMessage(get_class($this)."::deleteRecordsByField",'error');
			return false;
		}	
	}
	
	
	function deleteAllRecordsByDashboardID($dashboardID=false){
		global $mainframe;
		if($dashboardID!=false){
			$db =& JFactory::getDBO();
			$query = "DELETE FROM ".$db->nameQuote($this->table)." WHERE ".$db->nameQuote('dashboardid')." = ".$db->Quote($dashboardID);
			$db->setQuery($query);
			$result = $db->query();
			if(!$result){
				//something wrong
				if($db->getErrorNum()==0){
					//it is not an error, the problem is there is no record with the specified id
					$this->errMsg = false;
				}
				else{
					//MYSQL error
					$this->errMsg = $db->getErrorNum()." : ".$db->getErrorMsg();
					$mainframe->enqueueMessage(JText::_( 'QUERYKO' ),'error');
					$mainframe->enqueueMessage($this->errMsg,'error');
					$mainframe->enqueueMessage(get_class($this)."::deleteAllRecordsByDashboardID",'error');
					
				}
			}
			else{
				//clean messages
				$this->errMsg=false;
				return $result;
			}
		}
		else{
			$this->errMsg=JText::_( 'ARGNOVALID' );
			$mainframe->enqueueMessage(JText::_( 'ARGNOVALID' ),'error');
			$mainframe->enqueueMessage(get_class($this)."::deleteAllRecordsByDashboardID",'error');
			return false;
		}
	}
	
	/**
	 * 
	 * @return string the error message
	 */
	function getErrMsg(){
		return $this->errMsg;
	}
	
	
	/*
	 * 
	 * @param int $limitstart the number starting point record
	 * @param int $limit the number of records to show
	 * @param string $filter_order the field to order by
	 * @param string $filter_order_Dir the direction of order (asc or desc)
	 * @return array, the associative array of records
	 */
	function selectAllRecords($limitstart=false,$limit=false,$filter_order=false, $filter_order_Dir=false){		
		global $mainframe;
		
		$db =& JFactory::getDBO();
		//add order by clause to select
		$order="";
		if($filter_order && $filter_order_Dir) $order=" ORDER BY $filter_order $filter_order_Dir";
		
		
		$query='SELECT * FROM '.$db->nameQuote($this->table).$order;
		$db->setQuery($query,$limitstart,$limit);
		$list = $db->loadAssocList();
		if(!$list){
			//something wrong
			if($db->getErrorNum()==0){
				//it is not an error, the problem is there are no records into the table
				$this->errMsg = false;
			}
			else{
				//MYSQL error
				$this->errMsg = $db->getErrorNum()." : ".$db->getErrorMsg();
				$mainframe->enqueueMessage(JText::_( 'QUERYKO' ),'error');
				$mainframe->enqueueMessage($this->errMsg,'error');
				$mainframe->enqueueMessage(get_class($this)."::selectAllRecords",'error');
			}
		}
		else{
				
			//clean message
			$this->errMsg=false;			
			return $list;	
		}							
	}
	
	function selectAllRecordsByDashboardID($dashboardID=false){
		global $mainframe;
		
		if($dashboardID!=false){
			$db =& JFactory::getDBO();
			
			//to avoid problem with character set
			$db->setQuery($this->characterset);
			$db->query();
			
			
			$query='SELECT * FROM '.$db->nameQuote($this->table). ' WHERE '.$db->nameQuote('dashboardid').' = '.$db->Quote($dashboardID);
			$db->setQuery($query);
			$vet = $db->loadAssocList();
			
			if(!$vet){
				//something wrong
				if($db->getErrorNum()==0){
					//it is not an error, the problem is there is no record with the specified id
					$this->errMsg = false;
				}
				else{
					//MYSQL error
					$this->errMsg = $db->getErrorNum()." : ".$db->getErrorMsg();
					$mainframe->enqueueMessage(JText::_( 'QUERYKO' ),'error');
					$mainframe->enqueueMessage($this->errMsg,'error');
					$mainframe->enqueueMessage(get_class($this)."::selectAllRecordsByDashboardID",'error');
					
				}
			}			
			else{
				
			}
			
			return $vet;
		}
		else{
			$this->errMsg=JText::_( 'ARGNOVALID' );
			$mainframe->enqueueMessage(JText::_( 'ARGNOVALID' ),'error');
			$mainframe->enqueueMessage(get_class($this)."::selectAllRecordsByDashboardID",'error');
			return false;
		}	
	}
	
	
	/**
	 * Useful for JPagination
	 * @return the number of records in the table
	 */
	function countRecords(){
		global $mainframe;
		$db =& JFactory::getDBO();
		$query='SELECT count(*) FROM '.$db->nameQuote($this->table);
		$db->setQuery($query);		
		$result = $db->loadResult();
		if(!$result){
			//something wrong
			if($db->getErrorNum()==0){
				//it is not an error, the problem is there are no records into the table
				$this->errMsg = false;
			}
			else{
				//MYSQL error
				$this->errMsg = $db->getErrorNum()." : ".$db->getErrorMsg();
				$mainframe->enqueueMessage(JText::_( 'QUERYKO' ),'error');
				$mainframe->enqueueMessage($this->errMsg,'error');
				$mainframe->enqueueMessage(get_class($this)."::countRecords",'error');
			}	
		}
		else{
			//clean message
			$this->errMsg=false;
		}					
		return $result;		
	}
	
	
}
	
	
?>