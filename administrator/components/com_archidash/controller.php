<?php
/**
 * @version 1.0
 * @author  TobyTools.com (email:info@tobytools.com)(website: www.tobytools.com)
 * @copyright Copyright TobyTools.com 
 * @license GNU/GPL
 */


// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.controller');


class ArchiDashController extends JController{
	
	function ArchiDashController(){
		parent::__construct();
		
	}
	
	function display(){
		
		$this->setRedirect("index.php?option=com_archidash&controller=Welcome");
		
	}
}
?>
