<?php


header('Location: http://157.159.100.217/TrackIot/index.php?option=com_aikadmin&c=users&Itemid=7');
 
 
 
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport('joomla.application.component.view');
class AikViewCpanel extends JView
{
	function quickiconButton( $link, $image, $text )
	{
		$lang		=& JFactory::getLanguage();
		?>
		<div style="float:<?php echo ($lang->isRTL()) ? 'right' : 'left'; ?>;">
			<div class="icon">
				<a href="<?php echo $link; ?>">
					<?php echo JHTML::_('image.site',  $image, '/components/com_aikadmin/images/header/', NULL, NULL, $text ); ?>
				<span><?php echo $text; ?></span></a>
			</div>
		</div>
		<?php
	}
	function display($tpl=null)
	{
		JToolBarHelper::title( JText::_( 'CPanel' ), 'config.png' );
	?>
	<div id="cpanel">
		<?php
		$user = &JFactory::getUser();
		if ( $user->get('gid') >= 23 ) {
			$link = 'index.php?option=com_content&amp;view=article&amp;layout=form';
			$this->quickiconButton( $link, 'icon-48-article-add.png', JText::_( 'Add New Article' ) );
			
			$link = 'index.php?option=com_aikadmin&amp;c=content';
			$this->quickiconButton( $link, 'icon-48-article.png', JText::_( 'Article Manager' ) );

			$link = 'index.php?option=com_aikadmin&amp;c=frontpage';
			$this->quickiconButton( $link, 'icon-48-frontpage.png', JText::_( 'Front Page Manager' ) );
		
			$link = 'index.php?option=com_aikadmin&amp;c=admin';
			$this->quickiconButton( $link, 'icon-48-info.png', JText::_( 'Information' ) );
		
			$link = 'index.php?option=com_aikadmin&amp;c=checkin';
			$this->quickiconButton( $link, 'icon-48-checkin.png', JText::_( 'Global Check-in' ) );
		
			if ( $user->get('gid') > 23 ) {
				$link = 'index.php?option=com_aikadmin&amp;c=cache';
				$this->quickiconButton( $link, 'icon-48-config.png', JText::_( 'Cache Manager' ) );
				
				$link = 'index.php?option=com_aikadmin&amp;c=trash';
				$this->quickiconButton( $link, 'icon-48-trash.png', JText::_( 'Trash Manager' ) );
				
				$link = 'index.php?option=com_aikadmin&amp;c=categories';
				$this->quickiconButton( $link, 'icon-48-category.png', JText::_( 'Category Manager' ) );
			
				$link = 'index.php?option=com_aikadmin&amp;c=sections&amp;scope=content';
				$this->quickiconButton( $link, 'icon-48-section.png', JText::_( 'Section Manager' ) );
			
				$link = 'index.php?option=com_aikadmin&amp;c=cache&amp;task=purgeadmin';
				$this->quickiconButton( $link, 'icon-48-config.png', JText::_( 'Purge Expired Cache' ) );
		
				$link = 'index.php?option=com_aikadmin&amp;c=module';
				$this->quickiconButton( $link, 'icon-48-module.png', JText::_( 'Module Manager' ) );
		
				$link = 'index.php?option=com_aikadmin&amp;c=installer';
				$this->quickiconButton( $link, 'icon-48-extension.png', JText::_( 'Extension Manager' ) );
		
				$link = 'index.php?option=com_aikadmin&amp;c=menus';
				$this->quickiconButton( $link, 'icon-48-menumgr.png', JText::_( 'Menu Manager' ) );
			
				$link = 'index.php?option=com_aikadmin&amp;c=users';
				$this->quickiconButton( $link, 'icon-48-user.png', JText::_( 'User Manager' ) );
			}
			if ( $user->get('gid') > 24 ) {
				$link = 'index.php?option=com_aikadmin&amp;c=template';
				$this->quickiconButton( $link, 'icon-48-themes.png', JText::_( 'Template Manager' ) );
		
				$link = 'index.php?option=com_aikadmin&amp;c=plugin';
				$this->quickiconButton( $link, 'icon-48-plugin.png', JText::_( 'Plugin Manager' ) );
			
				$link = 'index.php?option=com_aikadmin&amp;c=language&amp;client=0';
				$this->quickiconButton( $link, 'icon-48-language.png', JText::_( 'Language Manager' ) );

				$link = 'index.php?option=com_aikadmin&amp;c=config';
				$this->quickiconButton( $link, 'icon-48-config.png', JText::_( 'Global Configuration' ) );
			}
		}
		?>
	</div>
	<?php
	}
}