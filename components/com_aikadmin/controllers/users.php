<?php
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport('joomla.application.component.controller');
class AikControllerUsers extends JController
{
	function __construct($config = array())
	{
		parent::__construct($config);
		$this->registerTask( 'add'  , 	'display'  );
		$this->registerTask( 'edit'  , 	'display'  );
		$this->registerTask( 'apply', 	'save'  );
		$this->registerTask( 'flogout', 'logout');
		$this->registerTask( 'unblock', 'block' );
	}
	function display( )
	{
		$view =& $this->getView( 'users' );
		switch($this->getTask())
		{
			case 'add':
			{
				JRequest::setVar( 'edit', false );
				$view->form('form');
			} break;
			case 'edit'    :
			{
				JRequest::setVar( 'edit', true );
				$view->form('form');
			} break;
			default:
			{
				$view->display();
			}
		}
	}
	function save()
	{
		global $mainframe;
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$option = JRequest::getCmd( 'option');

		$db			= & JFactory::getDBO();
		$me			= & JFactory::getUser();
		$acl			=& JFactory::getACL();
		$MailFrom	= $mainframe->getCfg('mailfrom');
		$FromName	= $mainframe->getCfg('fromname');
		$SiteName	= $mainframe->getCfg('sitename');

		$user = new JUser(JRequest::getVar( 'id', 0, 'post', 'int'));
		$original_gid = $user->get('gid');

		$post = JRequest::get('post');
		$post['username']	= JRequest::getVar('username', '', 'post', 'username');
		$post['password']	= JRequest::getVar('password', '', 'post', 'string', JREQUEST_ALLOWRAW);
		$post['password2']	= JRequest::getVar('password2', '', 'post', 'string', JREQUEST_ALLOWRAW);

		if (!$user->bind($post))
		{
			$mainframe->enqueueMessage(JText::_('CANNOT SAVE THE USER INFORMATION'), 'message');
			$mainframe->enqueueMessage($user->getError(), 'error');
			return $this->execute('edit');
		}

		$objectID 	= $acl->get_object_id( 'users', $user->get('id'), 'ARO' );
		$groups 	= $acl->get_object_groups( $objectID, 'ARO' );
		$this_group = strtolower( $acl->get_group_name( $groups[0], 'ARO' ) );


		if ( $user->get('id') == $me->get( 'id' ) && $user->get('block') == 1 )
		{
			$msg = JText::_( 'You cannot block Yourself!' );
			$mainframe->enqueueMessage($msg, 'message');
			return $this->execute('edit');
		}
		else if ( ( $this_group == 'super administrator' ) && $user->get('block') == 1 ) {
			$msg = JText::_( 'You cannot block a Super Administrator' );
			$mainframe->enqueueMessage($msg, 'message');
			return $this->execute('edit');
		}
		else if ( ( $this_group == 'administrator' ) && ( $me->get( 'gid' ) == 24 ) && $user->get('block') == 1 )
		{
			$msg = JText::_( 'WARNBLOCK' );
			$mainframe->enqueueMessage($msg, 'message');
			return $this->execute('edit');
		}
		else if ( ( $this_group == 'super administrator' ) && ( $me->get( 'gid' ) != 25 ) )
		{
			$msg = JText::_( 'You cannot edit a super administrator account' );
			$mainframe->enqueueMessage($msg, 'message');
			return $this->execute('edit');
		}
		$isNew 	= ($user->get('id') < 1);
		if (!$isNew)
		{
			if ( $user->get('gid') != $original_gid && $original_gid == 25 )
			{
				$query = 'SELECT COUNT( id )'
					. ' FROM #__users'
					. ' WHERE gid = 25'
					. ' AND block = 0'
				;
				$db->setQuery( $query );
				$count = $db->loadResult();

				if ( $count <= 1 )
				{
					$this->setRedirect( 'index.php?option=com_aikadmin&c=users', JText::_('WARN_ONLY_SUPER') );
					return false;
				}
			}
		}
		if (!$user->save())
		{
			$mainframe->enqueueMessage(JText::_('CANNOT SAVE THE USER INFORMATION'), 'message');
			$mainframe->enqueueMessage($user->getError(), 'error');
			return $this->execute('edit');
		}
		if ($isNew)
		{
			$adminEmail = $me->get('email');
			$adminName	= $me->get('name');

			$subject = JText::_('NEW_USER_MESSAGE_SUBJECT');
			$message = sprintf ( JText::_('NEW_USER_MESSAGE'), $user->get('name'), $SiteName, JURI::root(), $user->get('username'), $user->password_clear );

			if ($MailFrom != '' && $FromName != '')
			{
				$adminName 	= $FromName;
				$adminEmail = $MailFrom;
			}
			JUtility::sendMail( $adminEmail, $adminName, $user->get('email'), $subject, $message );
		}
		if ($user->get('id') == $me->get('id'))
		{
			$acl = &JFactory::getACL();
			$grp = $acl->getAroGroup($user->get('id'));
			$user->set('guest', 0);
			$user->set('aid', 1);

			if ($acl->is_group_child_of($grp->name, 'Registered')      ||
			    $acl->is_group_child_of($grp->name, 'Public Backend'))    {
				$user->set('aid', 2);
			}

			$user->set('usertype', $grp->name);

			$session = &JFactory::getSession();
			$session->set('user', $user);
		}

		switch ( $this->getTask() )
		{
			case 'apply':
				$msg = JText::sprintf( 'Successfully Saved changes to User', $user->get('name') );
				$this->setRedirect( 'index.php?option=com_aikadmin&c=users&view=user&task=edit&cid[]='. $user->get('id'), $msg );
				break;

			case 'save':
			default:
				$msg = JText::sprintf( 'Successfully Saved User', $user->get('name') );
				$this->setRedirect( 'index.php?option=com_aikadmin&c=users', $msg );
				break;
		}
	}
	function remove()
	{
		JRequest::checkToken() or jexit( 'Invalid Token' );

		$db 			=& JFactory::getDBO();
		$currentUser 	=& JFactory::getUser();
		$acl			=& JFactory::getACL();
		$cid 			= JRequest::getVar( 'cid', array(), '', 'array' );

		JArrayHelper::toInteger( $cid );

		if (count( $cid ) < 1) {
			JError::raiseError(500, JText::_( 'Select a User to delete', true ) );
		}

		foreach ($cid as $id)
		{
			$objectID 	= $acl->get_object_id( 'users', $id, 'ARO' );
			$groups 	= $acl->get_object_groups( $objectID, 'ARO' );
			$this_group = strtolower( $acl->get_group_name( $groups[0], 'ARO' ) );

			$success = false;
			if ( $this_group == 'super administrator' )
			{
				$msg = JText::_( 'You cannot delete a Super Administrator' );
			}
			else if ( $id == $currentUser->get( 'id' ) )
			{
				$msg = JText::_( 'You cannot delete Yourself!' );
			}
			else if ( ( $this_group == 'administrator' ) && ( $currentUser->get( 'gid' ) == 24 ) )
			{
				$msg = JText::_( 'WARNDELETE' );
			}
			else
			{
				$user =& JUser::getInstance((int)$id);
				$count = 2;

				if ( $user->get( 'gid' ) == 25 )
				{
					$query = 'SELECT COUNT( id )'
						. ' FROM #__users'
						. ' WHERE gid = 25'
						. ' AND block = 0'
					;
					$db->setQuery( $query );
					$count = $db->loadResult();
				}

				if ( $count <= 1 && $user->get( 'gid' ) == 25 )
				{
					$msg = "You cannot delete this Super Administrator as it is the only active Super Administrator for your site";
				}
				else
				{
					$user->delete();
					$msg = '';

					JRequest::setVar( 'task', 'remove' );
					JRequest::setVar( 'cid', $id );
					$this->logout();
				}
			}
		}
		$this->setRedirect( 'index.php?option=com_aikadmin&c=users', $msg);
	}
	function cancel( )
	{
		$this->setRedirect( 'index.php?option=com_aikadmin&c=users' );
	}
	function block( )
	{
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$db 			=& JFactory::getDBO();
		$acl			=& JFactory::getACL();
		$currentUser 	=& JFactory::getUser();

		$cid 	= JRequest::getVar( 'cid', array(), '', 'array' );
		$block  = $this->getTask() == 'block' ? 1 : 0;

		JArrayHelper::toInteger( $cid );

		if (count( $cid ) < 1) {
			JError::raiseError(500, JText::_( 'Select a User to '.$this->getTask(), true ) );
		}
		foreach ($cid as $id)
		{
			$objectID 	= $acl->get_object_id( 'users', $id, 'ARO' );
			$groups 	= $acl->get_object_groups( $objectID, 'ARO' );
			$this_group = strtolower( $acl->get_group_name( $groups[0], 'ARO' ) );

			$success = false;
			if ( $this_group == 'super administrator' )
			{
				$msg = JText::_( 'You cannot block a Super Administrator' );
			}
			else if ( $id == $currentUser->get( 'id' ) )
			{
				$msg = JText::_( 'You cannot block Yourself!' );
			}
			else if ( ( $this_group == 'administrator' ) && ( $currentUser->get( 'gid' ) == 24 ) )
			{
				$msg = JText::_( 'WARNBLOCK' );
			}
			else
			{
				$user =& JUser::getInstance((int)$id);
				$count = 2;

				if ( $user->get( 'gid' ) == 25 )
				{
					$query = 'SELECT COUNT( id )'
						. ' FROM #__users'
						. ' WHERE gid = 25'
						. ' AND block = 0'
					;
					$db->setQuery( $query );
					$count = $db->loadResult();
				}

				if ( $count <= 1 && $user->get( 'gid' ) == 25 )
				{
					$msg = "You cannot block this Super Administrator as it is the only active Super Administrator for your site";
				}
				else
				{
					$user =& JUser::getInstance((int)$id);
					$user->block = $block;
					$user->save();

					if($block)
					{
						JRequest::setVar( 'task', 'block' );
						JRequest::setVar( 'cid', array($id) );
						$this->logout();
					}
				}
			}
		}
		$this->setRedirect( 'index.php?option=com_aikadmin&c=users', $msg);
	}
	function logout( )
	{
		JRequest::checkToken() or jexit( 'Invalid Token' );
		global $mainframe;
		$db		=& JFactory::getDBO();
		$task 	= $this->getTask();
		$cids 	= JRequest::getVar( 'cid', array(), '', 'array' );
		$client = JRequest::getVar( 'client', 0, '', 'int' );
		$id 	= JRequest::getVar( 'id', 0, '', 'int' );

		JArrayHelper::toInteger($cids);
		if ( count( $cids ) < 1 ) {
			$this->setRedirect( 'index.php?option=com_aikadmin&c=users', JText::_( 'User Deleted' ) );
			return false;
		}
		foreach($cids as $cid)
		{
			$options = array();

			if ($task == 'logout' || $task == 'block') {
				$options['clientid'][] = 0; 
				$options['clientid'][] = 1; 
			} else if ($task == 'flogout') {
				$options['clientid'][] = $client;
			}

			$mainframe->logout((int)$cid, $options);
		}


		$msg = JText::_( 'User Session Ended' );
		switch ( $task )
		{
			case 'flogout':
				$this->setRedirect( 'index.php', $msg );
				break;

			case 'remove':
			case 'block':
				return;
				break;

			default:
				$this->setRedirect( 'index.php?option=com_aikadmin&c=users', $msg );
				break;
		}
	}
	function contact()
	{
		$contact_id = JRequest::getVar( 'contact_id', '', 'post', 'int' );
		$this->setRedirect( 'index.php?option=com_contact&task=edit&cid[]='. $contact_id );
	}
}