<?php
/**
 * @version		$Id: language.php 1.0 19-11-2009 Danijar
 * @package		Frontend Admin
 * @copyright	Copyright (C) 2009 CMSSpace. http://www.cmsspace.com
 * @license		GNU/GPL. http://www.gnu.org/licenses/gpl.html
 */
defined('_JEXEC') or die( 'Restricted access' );
jimport('joomla.application.component.controller');
$client	= JRequest::getVar('client', 0, '', 'int');
if ($client == 1) {
	JSubMenuHelper::addEntry(JText::_('Site'),'#" onclick="javascript:document.adminForm.client.value=\'0\';submitbutton(\'\');');
	JSubMenuHelper::addEntry(JText::_('Administrator'), '#" onclick="javascript:document.adminForm.client.value=\'1\';submitbutton(\'\');', true );
} else {
	JSubMenuHelper::addEntry(JText::_('Site'), '#" onclick="javascript:document.adminForm.client.value=\'0\';submitbutton(\'\');', true );
	JSubMenuHelper::addEntry(JText::_('Administrator'), '#" onclick="javascript:document.adminForm.client.value=\'1\';submitbutton(\'\');');
}

class AikControllerLanguage extends JController
{
	function __construct()
	{
		parent::__construct(array('default_task' => 'viewLanguages'));
	}
	function viewLanguages()
	{
		$view =& $this->getView( 'language' );
		$view->display();
	}
	function publishLanguage(  )
	{
		global $mainframe;
		$language 	= JRequest::getVar( 'cid', array(0), '', 'array' );
		$language	= array(JFilterInput::clean(@$language[0], 'cmd'));
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$client	=& JApplicationHelper::getClientInfo(JRequest::getVar('client', '0', '', 'int'));
	
		$params = JComponentHelper::getParams('com_languages');
		$params->set($client->name, $language);
	
		$table =& JTable::getInstance('component');
		$table->loadByOption( 'com_languages' );
	
		$table->params = $params->toString();
		if (!$table->check()) {
			JError::raiseWarning( 500, $table->getError() );
			return false;
		}
		if (!$table->store()) {
			JError::raiseWarning( 500, $table->getError() );
			return false;
		}
		$mainframe->redirect('index.php?option=com_aikadmin&c=language&client='.$client->id);
	}
}