<?php
/**
 * @version		$Id: cache.php 1.0 19-11-2009 Danijar
 * @package		Frontend Admin
 * @copyright	Copyright (C) 2009 CMSSpace. http://www.cmsspace.com
 * @license		GNU/GPL. http://www.gnu.org/licenses/gpl.html
 */
defined('_JEXEC') or die( 'Restricted access' );
jimport( 'joomla.application.component.controller' );
class AikControllerCategories extends JController
{
	function __construct( $default = array() )
	{
		parent::__construct( array('default_task' => 'showCategories') );
		$this->registerTask( 'add', 'editCategory' );
		$this->registerTask( 'edit', 'editCategory' );
		$this->registerTask( 'moveselect', 'moveCategorySelect' );
		$this->registerTask( 'movesave', 'moveCategorySave' );
		$this->registerTask( 'copyselect', 'copyCategorySelect' );
		$this->registerTask( 'copysave', 'copyCategorySave' );
		$this->registerTask( 'go2menu', 'saveCategory' );
		$this->registerTask( 'go2menuitem', 'saveCategory' );
		$this->registerTask( 'save', 'saveCategory' );
		$this->registerTask( 'apply', 'saveCategory' );
		$this->registerTask( 'remove', 'removeCategories' );
		$this->registerTask( 'publish', 'publishCategories' );
		$this->registerTask( 'unpublish', 'publishCategories' );
		$this->registerTask( 'cancel', 'cancelCategory' );
		$this->registerTask( 'orderup', 'orderCategory' );
		$this->registerTask( 'orderdown', 'orderCategory' );
		$this->registerTask( 'accesspublic', 'accessMenu' );
		$this->registerTask( 'accessregistered', 'accessMenu' );
		$this->registerTask( 'accessspecial', 'accessMenu' );
		$this->registerTask( 'saveorder', 'saveOrder' );
	}
	function showCategories()
	{
		$option = 'com_categories';
		$section = JRequest::getCmd( 'section', 'com_content' );
		global $mainframe;
	
		JToolBarHelper::title( JText::_( 'Category Manager' ) .': <small><small>[ '. JText::_(JString::substr($section, 4)).' ]</small></small>', 'categories.png' );
		JToolBarHelper::publishList();
		JToolBarHelper::unpublishList();

		if ( $section == 'com_content' || ( $section > 0 ) ) {
			JToolBarHelper::customX( 'moveselect', 'move.png', 'move_f2.png', 'Move', true );
			JToolBarHelper::customX( 'copyselect', 'copy.png', 'copy_f2.png', 'Copy', true );
		}
		JToolBarHelper::deleteList();
		JToolBarHelper::editListX();
		JToolBarHelper::addNewX();
		JToolBarHelper::help( 'screen.' . substr( $section, 4 ) . '.categories' );
		JToolBarHelper::cpanel();
		
		
		$db					=& JFactory::getDBO();
		$filter_order		= $mainframe->getUserStateFromRequest( 'com_categories.filter_order',					'filter_order',		'c.ordering',	'cmd' );
		$filter_order_Dir	= $mainframe->getUserStateFromRequest( 'com_categories.filter_order_Dir',				'filter_order_Dir',	'',				'word' );
		$filter_state		= $mainframe->getUserStateFromRequest( 'com_categories.'.$section.'.filter_state',	'filter_state',		'',				'word' );
		$sectionid			= $mainframe->getUserStateFromRequest( 'com_categories.'.$section.'.sectionid',		'sectionid',		0,				'int' );
		$search				= $mainframe->getUserStateFromRequest( 'com_categories.search',						'search',			'',				'string' );
		$search				= JString::strtolower( $search );
	
		$limit		= $mainframe->getUserStateFromRequest( 'global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int' );
		$limitstart	= $mainframe->getUserStateFromRequest( 'com_categories.limitstart', 'limitstart', 0, 'int' );
	
		$section_name 	= '';
		$content_add 	= '';
		$content_join 	= '';
		$order 			= ' ORDER BY '. $filter_order .' '. $filter_order_Dir .', c.ordering';
		if (intval( $section ) > 0) {
			$table = 'content';
	
			$query = 'SELECT title'
			. ' FROM #__sections'
			. ' WHERE id = '.(int) $section;
			$db->setQuery( $query );
			$section_name = $db->loadResult();
			$section_name = JText::sprintf( 'Content:', JText::_( $section_name ) );
			$where 	= ' WHERE c.section = '.$db->Quote($section);
			$type 	= 'content';
		} else if (strpos( $section, 'com_' ) === 0) {
			$table = substr( $section, 4 );
	
			$query = 'SELECT name'
			. ' FROM #__components'
			. ' WHERE link = '.$db->Quote('option='.$section);
			;
			$db->setQuery( $query );
			$section_name = $db->loadResult();
			$where 	= ' WHERE c.section = '.$db->Quote($section);
			$type 	= 'other';
			if ( $section == 'com_contact_details' ) {
				$section_name 	= JText::_( 'Contact' );
			}
			$section_name = JText::sprintf( 'Component:', $section_name );
		} else {
			$table 	= $section;
			$where 	= ' WHERE c.section = '.$db->Quote($section);
			$type 	= 'other';
		}
	
		$query = 'SELECT COUNT(*)'
		. ' FROM #__categories'
		;
		if ($section == 'com_content')
		{
			if($sectionid > 0)
			{
				$query .= ' WHERE section = '.(int) $sectionid;
			} else {
				$query .= ' WHERE section > 0';
			}
		} else {
			$query .= ' WHERE section = '.$db->quote($section);
		}
		if ( $filter_state ) {
			if ( $filter_state == 'P' ) {
				$query .= ' AND published = 1';
			} else if ($filter_state == 'U' ) {
				$query .= ' AND published = 0';
			}
		}
		$db->setQuery( $query );
		$total = $db->loadResult();
	
		if ( $section == 'com_content' ) {
			$table 			= 'content';
			$content_add 	= ' , z.title AS section_name';
			$content_join 	= ' LEFT JOIN #__sections AS z ON z.id = c.section';
			$where 			= ' WHERE c.section NOT LIKE "%com_%"';
			if ($filter_order == 'c.ordering'){
				$order 			= ' ORDER BY  z.title, c.ordering '. $filter_order_Dir;
			} else {
				$order 			= ' ORDER BY  '.$filter_order.' '. $filter_order_Dir.', z.title, c.ordering';
			}
	
			$section_name 	= JText::_( 'All Content:' );
	
			$type 			= 'content';
		}
		if ( $sectionid > 0 ) {
			$filter = ' AND c.section = '.$db->Quote($sectionid);
		} else {
			$filter = '';
		}
		if ( $filter_state ) {
			if ( $filter_state == 'P' ) {
				$filter .= ' AND c.published = 1';
			} else if ($filter_state == 'U' ) {
				$filter .= ' AND c.published = 0';
			}
		}
		if ($search) {
			$filter .= ' AND LOWER(c.title) LIKE '.$db->Quote( '%'.$db->getEscaped( $search, true ).'%', false );
		}
	
		jimport('joomla.html.pagination');
		$pageNav = new JPagination( $total, $limitstart, $limit );
	
		$tablesAllowed = $db->getTableList();
		if (!in_array($db->getPrefix().$table, $tablesAllowed)) {
			$table = 'content';
		}
	
		$query = 'SELECT  c.*, c.checked_out as checked_out_contact_category, g.name AS groupname, u.name AS editor, COUNT( DISTINCT s2.checked_out ) AS checked_out_count'
		. $content_add
		. ' FROM #__categories AS c'
		. ' LEFT JOIN #__users AS u ON u.id = c.checked_out'
		. ' LEFT JOIN #__groups AS g ON g.id = c.access'
		. ' LEFT JOIN #__'.$table.' AS s2 ON s2.catid = c.id AND s2.checked_out > 0'
		. $content_join
		. $where
		. $filter
		. ' AND c.published != -2'
		. ' GROUP BY c.id'
		. $order
		;
		$db->setQuery( $query, $pageNav->limitstart, $pageNav->limit );
		$rows = $db->loadObjectList();
		if ($db->getErrorNum()) {
			echo $db->stderr();
			return;
		}
	
		$count = count( $rows );
		for ( $i = 0; $i < $count; $i++ ) {
			$query = 'SELECT COUNT( a.id )'
			. ' FROM #__content AS a'
			. ' WHERE a.catid = '. (int) $rows[$i]->id
			. ' AND a.state <> -2'
			;
			$db->setQuery( $query );
			$active = $db->loadResult();
			$rows[$i]->active = $active;
		}
		for ( $i = 0; $i < $count; $i++ ) {
			$query = 'SELECT COUNT( a.id )'
			. ' FROM #__content AS a'
			. ' WHERE a.catid = '. (int) $rows[$i]->id
			. ' AND a.state = -2'
			;
			$db->setQuery( $query );
			$trash = $db->loadResult();
			$rows[$i]->trash = $trash;
		}
	
		$javascript = 'onchange="document.adminForm.submit();"';
		$lists['sectionid']	= JHTML::_('list.section',  'sectionid', $sectionid, $javascript, 'ordering', false );
		$lists['state']	= JHTML::_('grid.state',  $filter_state );
		$lists['order_Dir'] = $filter_order_Dir;
		$lists['order']		= $filter_order;
		$lists['search']= $search;
		$view =& $this->getView( 'categories' );
		$view->show( $rows, $section, $section_name, $pageNav, $lists, $type );
	}
	function editCategory()
	{
		global $mainframe;
		if(JRequest::getCmd( 'task' ) == 'edit')
			$edit = true;
		else 
			$edit = false;
		$text = ( $edit ? JText::_( 'Edit' ) : JText::_( 'New' ) );
		JToolBarHelper::title( JText::_( 'Category' ) .': <small><small>[ '. $text.' ]</small></small>', 'categories.png' );
		JToolBarHelper::save();
		JToolBarHelper::apply();
		if ($edit) {
			JToolBarHelper::cancel( 'cancel', 'Close' );
		} else {
			JToolBarHelper::cancel();
		}
		$section	= JRequest::getCmd( 'section', 'com_content' );
		JToolBarHelper::help( 'screen.' . substr( $section, 4 ) . '.categories.edit' );
		
		$db			=& JFactory::getDBO();
		$user 		=& JFactory::getUser();
		$uid		= $user->get('id');
	
		$type		= JRequest::getCmd( 'type' );
		$redirect	= JRequest::getCmd( 'section', 'com_content' );
		
		$cid		= JRequest::getVar( 'cid', array(0), '', 'array' );
	
		JArrayHelper::toInteger($cid, array(0));
		$query = 'SELECT COUNT( id )'
		. ' FROM #__sections'
		. ' WHERE scope = "content"'
		;
		$db->setQuery( $query );
		$sections = $db->loadResult();
		if (!$sections && $type != 'other'
				&& $section != 'com_weblinks'
				&& $section != 'com_newsfeeds'
				&& $section != 'com_contact_details'
				&& $section != 'com_banner') {
			$mainframe->redirect( 'index.php?option=com_aikadmin&c=categories&section='. $section, JText::_( 'WARNSECTION', true ) );
		}
	
		$row =& JTable::getInstance('category');
		if ($edit)
			$row->load( $cid[0] );
		if ( JTable::isCheckedOut($user->get ('id'), $row->checked_out )) {
			$msg = JText::sprintf( 'DESCBEINGEDITTED', JText::_( 'The category' ), $row->title );
			$mainframe->redirect( 'index.php?option=com_aikadmin&c=categories&section='. $row->section, $msg );
		}
	
		if ( $edit ) {
			$row->checkout( $user->get('id'));
		} else {
			$row->published 	= 1;
		}
	
		$order = array();
		$query = 'SELECT COUNT(*)'
		. ' FROM #__categories'
		. ' WHERE section = '.$db->Quote($row->section)
		;
		$db->setQuery( $query );
		$max = intval( $db->loadResult() ) + 1;
	
		for ($i=1; $i < $max; $i++) {
			$order[] = JHTML::_('select.option',  $i );
		}
		if ( $section == 'com_content' ) {
	
			if (!$row->section && JRequest::getInt('sectionid')) {
			    $row->section = JRequest::getInt('sectionid');
			}
	
			$query = 'SELECT s.id AS value, s.title AS text'
			. ' FROM #__sections AS s'
			. ' ORDER BY s.ordering'
			;
			$db->setQuery( $query );
			$sections = $db->loadObjectList();
			$lists['section'] = JHTML::_('select.genericlist',   $sections, 'section', 'class="inputbox" size="1"', 'value', 'text', $row->section );
		} else {
			if ( $type == 'other' ) {
				$section_name = JText::_( 'N/A' );
			} else {
				$temp =& JTable::getInstance('section');
				$temp->load( $row->section );
				$section_name = $temp->name;
			}
			if(!$section_name) $section_name = JText::_( 'N/A' );
			$row->section = $section;
			$lists['section'] = '<input type="hidden" name="section" value="'. $row->section .'" />'. $section_name;
		}
	
		$query = 'SELECT ordering AS value, title AS text'
		. ' FROM #__categories'
		. ' WHERE section = '.$db->Quote($row->section)
		. ' ORDER BY ordering'
		;
		if ($edit) {
			$lists['ordering'] 			= JHTML::_('list.specificordering',  $row, $cid[0], $query );
		}
		else {
			$lists['ordering'] 			= JHTML::_('list.specificordering',  $row, '', $query );
		}
		$active =  ( $row->image_position ? $row->image_position : 'left' );
		$lists['image_position'] 	= JHTML::_('list.positions',  'image_position', $active, NULL, 0, 0 );
		$lists['image'] 			= JHTML::_('list.images',  'image', $row->image );
		$lists['access'] 			= JHTML::_('list.accesslevel',  $row );
		$published = ($row->id) ? $row->published : 1;
		$lists['published'] 		= JHTML::_('select.booleanlist',  'published', 'class="inputbox"', $published );
	 	$view =& $this->getView( 'categories' );
		$view->edit( $row, $lists, $redirect );
	}
	function saveCategory()
	{
		global $mainframe;
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$db		 =& JFactory::getDBO();
		$menu 		= JRequest::getCmd( 'menu', 'mainmenu', 'post' );
		$menuid		= JRequest::getVar( 'menuid', 0, 'post', 'int' );
		$redirect 	= JRequest::getCmd( 'redirect', '', 'post' );
		$oldtitle 	= JRequest::getString( 'oldtitle', '', 'post' );
		$post		= JRequest::get( 'post' );
	
		$cid = JRequest::getVar( 'cid', array(0), '', 'array' );
		$section = JRequest::getCmd( 'section' );
		
		$text = ( $edit ? JText::_( 'Edit' ) : JText::_( 'New' ) );

		JToolBarHelper::title( JText::_( 'Category' ) .': <small><small>[ '. $text.' ]</small></small>', 'categories.png' );
		JToolBarHelper::save();
		JToolBarHelper::apply();
		if ($edit) {
			// for existing articles the button is renamed `close`
			JToolBarHelper::cancel( 'cancel', 'Close' );
		} else {
			JToolBarHelper::cancel();
		}
		JToolBarHelper::help( 'screen.' . substr( $section, 4 ) . '.categories.edit' );
		JToolBarHelper::cpanel();
		
		$post['description'] = JRequest::getVar( 'description', '', 'post', 'string', JREQUEST_ALLOWRAW );
		$row =& JTable::getInstance('category');
		if (!$row->bind( $post )) {
			JError::raiseError(500, $row->getError() );
		}
		if (!$row->check()) {
			JError::raiseError(500, $row->getError() );
		}
		if (!$row->id) {
			$where = "section = " . $db->Quote($row->section);
			$row->ordering = $row->getNextOrder( $where );
		}
	
		if (!$row->store()) {
			JError::raiseError(500, $row->getError() );
		}
		$row->checkin();
	
		if ($row->section > 0) {
			$query = 'UPDATE #__content'
					.' SET sectionid = '.$row->section
					.' WHERE catid = '.$row->id
			;
			$db->setQuery( $query );
			$db->query();
		}
	
		if ( $oldtitle ) {
			if ($oldtitle != $row->title) {
				$query = 'UPDATE #__menu'
				. ' SET name = '.$db->Quote($row->title)
				. ' WHERE name = '.$db->Quote($oldtitle)
				. ' AND type = "content_category"'
				;
				$db->setQuery( $query );
				$db->query();
			}
		}
		if ($row->section != 'com_contact_details' &&
			$row->section != 'com_newsfeeds' &&
			$row->section != 'com_weblinks') {
			$query = 'UPDATE #__sections SET count=count+1'
			. ' WHERE id = '.$db->Quote($row->section)
			;
			$db->setQuery( $query );
		}
	
		if (!$db->query()) {
			JError::raiseError(500, $db->getErrorMsg() );
		}
	
		switch ( JRequest::getCmd('task') )
		{
			case 'go2menu':
				$mainframe->redirect( 'index.php?option=com_aikadmin&c=menus&menutype='. $menu );
				break;
	
			case 'go2menuitem':
				$mainframe->redirect( 'index.php?option=com_aikadmin&c=menus&menutype='. $menu .'&task=edit&id='. $menuid );
				break;
	
			case 'apply':
				$msg = JText::_( 'Changes to Category saved' );
				$mainframe->redirect( 'index.php?option=com_aikadmin&c=categories&section='. $redirect .'&task=edit&cid[]='. $row->id, $msg );
				break;
	
			case 'save':
			default:
				$msg = JText::_( 'Category saved' );
				$mainframe->redirect( 'index.php?option=com_aikadmin&c=categories&section='. $redirect, $msg );
				break;
		}
	}
	function removeCategories()
	{
		global $mainframe;
		JRequest::checkToken() or jexit( 'Invalid Token' );
		
		$section 	= JRequest::getCmd( 'section', 'com_content' );
		$cid 		= JRequest::getVar( 'cid', array(0), '', 'array' );
		JArrayHelper::toInteger($cid, array(0));

		$db =& JFactory::getDBO();
	
		JArrayHelper::toInteger($cid);
	
		if (count( $cid ) < 1) {
			JError::raiseError(500, JText::_( 'Select a category to delete', true ));
		}
	
		$cids = implode( ',', $cid );
	
		if (intval( $section ) > 0) {
			$table = 'content';
		} else if (strpos( $section, 'com_' ) === 0) {
			$table = substr( $section, 4 );
		} else {
			$table = $section;
		}
	
		$tablesAllowed = $db->getTableList();
		if (!in_array($db->getPrefix().$table, $tablesAllowed)) {
			$table = 'content';
		}
	
		$query = 'SELECT c.id, c.name, c.title, COUNT( s.catid ) AS numcat'
		. ' FROM #__categories AS c'
		. ' LEFT JOIN #__'.$table.' AS s ON s.catid = c.id'
		. ' WHERE c.id IN ( '.$cids.' )'
		. ' GROUP BY c.id'
		;
		$db->setQuery( $query );
	
		if (!($rows = $db->loadObjectList())) {
			JError::raiseError( 500, $db->stderr() );
			return false;
		}
	
		$err = array();
		$cid = array();
		foreach ($rows as $row) {
			if ($row->numcat == 0) {
				$cid[] = (int) $row->id;
			} else {
				$err[] = $row->title;
			}
		}
	
		if (count( $cid )) {
			$cids = implode( ',', $cid );
			$query = 'DELETE FROM #__categories'
			. ' WHERE id IN ( '.$cids.' )'
			;
			$db->setQuery( $query );
			if (!$db->query()) {
				JError::raiseError( 500, $db->stderr() );
				return false;
			}
		}
	
		if (count( $err )) {
			$cids = implode( ", ", $err );
			$msg = JText::sprintf( 'WARNNOTREMOVEDRECORDS', $cids );
			$mainframe->redirect( 'index.php?option=com_aikadmin&c=categories&section='. $section, $msg );
		}
	
		$mainframe->redirect( 'index.php?option=com_aikadmin&c=categories&section='. $section );
	}
	function publishCategories()
	{
		global $mainframe;
		
		if(JRequest::getCmd( 'task' ) == 'publish')
			$publish = 1;
		else 
			$publish = 0;
		
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$section 	= JRequest::getCmd( 'section', 'com_content' );
		
		$db		=& JFactory::getDBO();
		$user	=& JFactory::getUser();
		$uid	= $user->get('id');
	
		JArrayHelper::toInteger($cid);
	
		if (count( $cid ) < 1) {
			$action = $publish ? 'publish' : 'unpublish';
			JError::raiseError(500, JText::_( 'Select a category to '.$action, true ) );
		}
	
		$cids = implode( ',', $cid );
	
		$query = 'UPDATE #__categories'
		. ' SET published = ' . (int) $publish
		. ' WHERE id IN ( '.$cids.' )'
		. ' AND ( checked_out = 0 OR ( checked_out = '.(int) $uid.' ) )'
		;
		$db->setQuery( $query );
		if (!$db->query()) {
			JError::raiseError(500, $db->getErrorMsg() );
		}
	
		if (count( $cid ) == 1) {
			$row =& JTable::getInstance('category');
			$row->checkin( $cid[0] );
		}
	
		$mainframe->redirect( 'index.php?option=com_aikadmin&c=categories&section='. $section );
	}
	function cancelCategory()
	{
		global $mainframe;
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$db =& JFactory::getDBO();
		$redirect = JRequest::getCmd( 'redirect', '', 'post' );
	
		$row =& JTable::getInstance('category');
		$row->bind( JRequest::get( 'post' ));
		$row->checkin();
		$mainframe->redirect( 'index.php?option=com_aikadmin&c=categories&section='. $redirect );
	}
	function orderCategory()
	{
		global $mainframe;
		$section 	= JRequest::getCmd( 'section', 'com_content' );
		$cid 		= JRequest::getVar( 'cid', array(0), '', 'array' );
		JArrayHelper::toInteger($cid, array(0));
		$uid = $cid[0];
		if(JRequest::getCmd( 'task' ) == 'orderup')
			$inc = -1;
		else 
			$inc = 1;
			
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$db		=& JFactory::getDBO();
		$row	=& JTable::getInstance('category' );
		$row->load( $uid );
		$row->move( $inc, 'section = '.$db->Quote($row->section) );
		$section = JRequest::getCmd('section');
		if($section) {
			$section = '&section='. $section;
		}
		$mainframe->redirect( 'index.php?option=com_aikadmin&c=categories'. $section );
	}
	function moveCategorySelect()
	{
		global $mainframe;
		$option = 'com_aikadmin';
		JRequest::checkToken() or jexit( 'Invalid Token' );

		$sectionOld 	= JRequest::getCmd( 'section', 'com_content' );
		$cid 		= JRequest::getVar( 'cid', array(0), '', 'array' );
		JArrayHelper::toInteger($cid, array(0));
		
		JToolBarHelper::title( JText::_( 'Category' ) .': <small><small>[ '. JText::_( 'Move' ).' ]</small></small>', 'categories.png' );
		JToolBarHelper::save( 'movesave' );
		JToolBarHelper::cancel();
		JToolBarHelper::cpanel();
		
		$db =& JFactory::getDBO();
		$redirect = JRequest::getCmd( 'section', 'com_content', 'post' );
	
		JArrayHelper::toInteger($cid);
	
		if (count( $cid ) < 1) {
			JError::raiseError(500, JText::_( 'Select an item to move', true ));
		}
	
		$cids = implode( ',', $cid );
		$query = 'SELECT a.title, a.section'
		. ' FROM #__categories AS a'
		. ' WHERE a.id IN ( '.$cids.' )'
		;
		$db->setQuery( $query );
		$items = $db->loadObjectList();
	
		$query = 'SELECT a.title'
		. ' FROM #__content AS a'
		. ' WHERE a.catid IN ( '.$cids.' )'
		. ' ORDER BY a.catid, a.title'
		;
		$db->setQuery( $query );
		$contents = $db->loadObjectList();
	
		$query = 'SELECT a.title AS text, a.id AS value'
		. ' FROM #__sections AS a'
		. ' WHERE a.published = 1'
		. ' ORDER BY a.title'
		;
		$db->setQuery( $query );
		$sections = $db->loadObjectList();
	
		$SectionList = JHTML::_('select.genericlist',   $sections, 'sectionmove', 'class="inputbox" size="10"', 'value', 'text', null );
		$view =& $this->getView( 'categories' );
		$view->moveCategorySelect( $option, $cid, $SectionList, $items, $sectionOld, $contents, $redirect );
	}
	
	function moveCategorySave()
	{
		global $mainframe;
		JRequest::checkToken() or jexit( 'Invalid Token' );
	
		$sectionOld 	= JRequest::getCmd( 'section', 'com_content' );
		$cid 		= JRequest::getVar( 'cid', array(0), '', 'array' );
		JArrayHelper::toInteger($cid, array(0));

		$db =& JFactory::getDBO();
		$sectionMove = JRequest::getCmd( 'sectionmove' );
		if (!$sectionMove)
		{
			$msg = JText::_('Please select a section from the list');
			$this->moveCategorySelect( 'com_aikadmin', $cid, $sectionOld );
			JError::raiseWarning(500, $msg);
			return;
		}
	
		JArrayHelper::toInteger($cid, array(0));
	
		$sectionNew =& JTable::getInstance('section');
	    $sectionNew->load( $sectionMove );
	
		$cids = implode( ',', $cid );
	
		$query = 'SELECT id, title'
		. ' FROM #__categories'
		. ' WHERE id IN ( '.$cids.' )'
		. ' AND section = '.$db->Quote($sectionMove)
		;
		$db->setQuery( $query );
	
		$scid   = $db->loadResultArray(0);
		$title  = $db->loadResultArray(1);
	
		$cid = array_diff($cid, $scid);
	
		if ( !empty($cid) ) {
		    $cids = implode( ',', $cid );
		    $total = count( $cid );
	
		    $query = 'UPDATE #__categories'
		    . ' SET section = '.$db->Quote($sectionMove)
		    . ' WHERE id IN ( '.$cids.' )'
		    ;
		    $db->setQuery( $query );
	    	if ( !$db->query() ) {
		    	JError::raiseError(500, $db->getErrorMsg() );
		    }
		    $query = 'UPDATE #__content'
		    . ' SET sectionid = '.$db->Quote($sectionMove)
		    . ' WHERE catid IN ( '.$cids.' )'
		    ;
		    $db->setQuery( $query );
		    if ( !$db->query() ) {
		    	JError::raiseError(500, $db->getErrorMsg());
		    }
	
			$msg = JText::sprintf( 'Categories moved to', $sectionNew->title );
			$mainframe->enqueueMessage($msg);
		}
		if ( !empty($title) && is_array($title) ) {
		    if ( count($title) == 1 ) {
			    $msg = JText::sprintf( 'Category already in', implode( ', ', $title ), $sectionNew->title );
		    } else {
			    $msg = JText::sprintf( 'Categories already in', implode( ', ', $title ), $sectionNew->title );
			}
			$mainframe->enqueueMessage($msg);
		}
	
		$mainframe->redirect( 'index.php?option=com_aikadmin&c=categories&section='. $sectionOld );
	}
	function copyCategorySelect()
	{
		global $mainframe;
		JRequest::checkToken() or jexit( 'Invalid Token' );
	
		JToolBarHelper::title( JText::_( 'Category' ) .': <small><small>[ '. JText::_( 'Copy' ).' ]</small></small>', 'categories.png' );

		JToolBarHelper::save( 'copysave' );
		JToolBarHelper::cancel();
		JToolBarHelper::cpanel();
		
		$option = 'com_aikadmin';
		$sectionOld 	= JRequest::getCmd( 'section', 'com_content' );
		$cid 		= JRequest::getVar( 'cid', array(0), '', 'array' );
		JArrayHelper::toInteger($cid, array(0));

		$db =& JFactory::getDBO();
		$redirect = JRequest::getCmd( 'section', 'com_content', 'post' );
	
		JArrayHelper::toInteger($cid);
	
		if (count( $cid ) < 1) {
			JError::raiseError(500, JText::_( 'Select an item to move', true ));
		}
		$cids = implode( ',', $cid );
		$query = 'SELECT a.title, a.section'
		. ' FROM #__categories AS a'
		. ' WHERE a.id IN ( '.$cids.' )'
		;
		$db->setQuery( $query );
		$items = $db->loadObjectList();
	
		$query = 'SELECT a.title, a.id'
		. ' FROM #__content AS a'
		. ' WHERE a.catid IN ( '.$cids.' )'
		. ' ORDER BY a.catid, a.title'
		;
		$db->setQuery( $query );
		$contents = $db->loadObjectList();
	
		$query = 'SELECT a.title AS `text`, a.id AS `value`'
		. ' FROM #__sections AS a'
		. ' WHERE a.published = 1'
		. ' ORDER BY a.name'
		;
		$db->setQuery( $query );
		$sections = $db->loadObjectList();
		$SectionList = JHTML::_('select.genericlist',   $sections, 'sectionmove', 'class="inputbox" size="10"', 'value', 'text', null );
	
		$view =& $this->getView( 'categories' );
		$view->copyCategorySelect( $option, $cid, $SectionList, $items, $sectionOld, $contents, $redirect );
	}
	function copyCategorySave()
	{
		global $mainframe;
		JRequest::checkToken() or jexit( 'Invalid Token' );
		
		$sectionOld 	= JRequest::getCmd( 'section', 'com_content' );
		$cid 		= JRequest::getVar( 'cid', array(0), '', 'array' );
		JArrayHelper::toInteger($cid, array(0));

		$db =& JFactory::getDBO();
	
		$sectionMove 	= JRequest::getInt( 'sectionmove' );
		if (!$sectionMove)
		{
			$msg = JText::_('Please select a section from the list');
			$this->copyCategorySelect( 'com_aikadmin', $cid, $sectionOld );
			JError::raiseWarning(500, $msg);
			return;
		}
	
		$contentid 		= JRequest::getVar( 'item', null, '', 'array' );
		JArrayHelper::toInteger($contentid);
	
		$category =& JTable::getInstance('category');
	
		foreach( $cid as $id )
		{
			$category->load( $id );
			$category->id 		= NULL;
			$category->title 	= JText::sprintf( 'Copy of', $category->title );
			$category->name 	= JText::sprintf( 'Copy of', $category->name );
			$category->section 	= $sectionMove;
			if (!$category->check()) {
				JError::raiseError(500, $category->getError());
			}
	
			if (!$category->store()) {
				JError::raiseError(500, $category->getError());
			}
			$category->checkin();
			$newcatids[]["old"] = $id;
			$newcatids[]["new"] = $category->id;
		}
	
		$content =& JTable::getInstance('content');
		foreach( $contentid as $id) {
			$content->load( $id );
			$content->id 		= NULL;
			$content->sectionid = $sectionMove;
			$content->hits 		= 0;
			foreach( $newcatids as $newcatid ) {
				if ( $content->catid == $newcatid["old"] ) {
					$content->catid = $newcatid["new"];
				}
			}
			if (!$content->check()) {
				JError::raiseError(500, $content->getError());
			}
	
			if (!$content->store()) {
				JError::raiseError(500, $content->getError());
			}
			$content->checkin();
		}
	
		$sectionNew =& JTable::getInstance('section');
		$sectionNew->load( $sectionMove );
	
		$msg = JText::sprintf( 'Categories copied to', count($cid), $sectionNew->title );
		$mainframe->redirect( 'index.php?option=com_aikadmin&c=categories&section='. $sectionOld, $msg );
	}
	function accessMenu()
	{
		global $mainframe;
		JRequest::checkToken() or jexit( 'Invalid Token' );
		
		$section 	= JRequest::getCmd( 'section', 'com_content' );
		$cid 		= JRequest::getVar( 'cid', array(0), '', 'array' );
		JArrayHelper::toInteger($cid, array(0));
		$uid = $cid[0];
		
		$db =& JFactory::getDBO();
	
		$row =& JTable::getInstance('category');
		$row->load( $uid );
		switch (JRequest::getCmd('task'))
		{
			case 'accesspublic':$row->access = 0;break;
			case 'accessregistered':$row->access = 1;break;
			case 'accessspecial':$row->access = 2;break;
		}
		if ( !$row->check() ) {
			return $row->getError();
		}
		if ( !$row->store() ) {
			return $row->getError();
		}
	
		$mainframe->redirect( 'index.php?option=com_aikadmin&c=categories&section='. $section );
	}
	
	function saveOrder()
	{
		global $mainframe;
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$db =& JFactory::getDBO();
	
		$section 	= JRequest::getCmd( 'section', 'com_content' );
		$cid 		= JRequest::getVar( 'cid', array(0), '', 'array' );
		JArrayHelper::toInteger($cid, array(0));

		$total		= count( $cid );
		$order 		= JRequest::getVar( 'order', array(0), 'post', 'array' );
		JArrayHelper::toInteger($order, array(0));
		$row		=& JTable::getInstance('category');
		$groupings = array();
	
		for( $i=0; $i < $total; $i++ ) {
			$row->load( (int) $cid[$i] );
			$groupings[] = $row->section;
			if ($row->ordering != $order[$i]) {
				$row->ordering = $order[$i];
				if (!$row->store()) {
					JError::raiseError(500, $db->getErrorMsg());
				}
			}
		}
		$groupings = array_unique( $groupings );
		foreach ($groupings as $group){
			$row->reorder('section = '.$db->Quote($group));
		}
	
		$msg 	= JText::_( 'New ordering saved' );
		$mainframe->redirect( 'index.php?option=com_aikadmin&c=categories&section='. $section, $msg );
	}
}