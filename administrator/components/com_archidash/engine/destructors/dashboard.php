<?php


defined('_JEXEC') or die('Restricted Access');
jimport('joomla.application.component.model');
require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'engine'.DS.'destructors'.DS.'destructor.php');


/**
 * The following class destroyes the dashboard and the resource related to it.
 * @version 1.0
 * @author  TobyTools.com (email:info@tobytools.com)(website: www.tobytools.com)
 * @copyright Copyright TobyTools.com 
 * @license GNU/GPL
 */


class ArchiDashDestructorDashboard extends ArchiDashDestructor{


	function ArchiDashDestructorDashboard($opts){
		parent::__construct();
		//$opts['dashboardid']
		$this->options = $opts;
		$this->table="";
		$this->options['characterset']=$this->characterset;//set characters
		
	}
	
	function preCondition(){
		//no precondition
		return true;
	}
	
	/**
	 * the destroy method follows a well defined order to destroy the resources
	 * @see admin/engine/destructors/ArchiDashDestructor#destroy()
	 */
	function destroy(){
		//Archi Modules
		require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'engine'.DS.'destructors'.DS.'archi.php');
		$classname = "ArchiDashDestructorArchi";
		$destructor = new $classname($this->options);
		$destructor->destroy();
		//Articles
		require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'engine'.DS.'destructors'.DS.'article.php');
		$classname = "ArchiDashDestructorArticle";
		$destructor = new $classname($this->options);
		$destructor->destroy();
		//MenuItems
		require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'engine'.DS.'destructors'.DS.'menu.php');
		$classname = "ArchiDashDestructorMenu";
		$destructor = new $classname($this->options);
		$destructor->destroy();
		//Menu + the related Module
		require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'engine'.DS.'destructors'.DS.'menutype.php');
		$classname = "ArchiDashDestructorMenuType";
		$destructor = new $classname($this->options);
		$destructor->destroy();
		//Categories
		require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'engine'.DS.'destructors'.DS.'category.php');
		$classname = "ArchiDashDestructorCategory";
		$destructor = new $classname($this->options);
		$destructor->destroy();
		//Section
		require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'engine'.DS.'destructors'.DS.'section.php');
		$classname = "ArchiDashDestructorSection";
		$destructor = new $classname($this->options);
		$destructor->destroy();
		//Archimede Datasources
		require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'engine'.DS.'destructors'.DS.'datasource.php');
		$classname = "ArchiDashDestructorDataSource";
		$destructor = new $classname($this->options);
		$destructor->destroy();
		//Dashboard
		$this->destroyDashboard();
		
	}

	private function destroyDashboard(){
		global $mainframe;
		if($this->preCondition() && isset($this->options['dashboardid'])){
			
			require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'models'.DS.'dashboard.php');
			$classname="ArchiDashModelDashboard";
			$model = new $classname();
			
			$model->deleteRecord($this->options['dashboardid']);
			
			
		}
		else{			
			$this->completed=false;
			$this->errMsg.=get_class($this)."::destroyDashboard ".JText::_('NODASHBOARDID');
			$mainframe->enqueueMessage($this->errMsg,'error');
		}
	}
}



