<?php
/**
 * @version		$Id: checin.php 1.0 19-11-2009 Danijar
 * @package		Frontend Admin
 * @copyright	Copyright (C) 2009 CMSSpace. http://www.cmsspace.com
 * @license		GNU/GPL. http://www.gnu.org/licenses/gpl.html
 */
defined('_JEXEC') or die( 'Restricted access' );
jimport( 'joomla.application.component.controller' );
class AikControllerCheckin extends JController
{
	function __construct()
	{
		parent::__construct(array('default_task' => 'Checkin'));
	}
	function Checkin()
	{
		JToolBarHelper::title( JText::_( 'Global Check-in' ), 'checkin.png' );
		JToolBarHelper::help( 'screen.checkin' );
		JToolBarHelper::cpanel();
		
		$view =& $this->getView( 'Checkin' );
		$view->Checkin();
	}
}