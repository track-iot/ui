<?php
/**
 * @version		$Id: controller.php 1.0 19-11-2009 Danijar
 * @package		Frontend Admin
 * @copyright	Copyright (C) 2009 CMSSpace. http://www.cmsspace.com
 * @license		GNU/GPL. http://www.gnu.org/licenses/gpl.html
 */
jimport('joomla.application.component.controller');
class AikController extends JController
{
	function display()
	{
		$view =& $this->getView( 'Cpanel');
		$view->display();
	}
}