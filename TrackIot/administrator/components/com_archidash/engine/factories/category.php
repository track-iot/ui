<?php


defined('_JEXEC') or die('Restricted Access');
jimport('joomla.application.component.model');
require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'engine'.DS.'factories'.DS.'factory.php');


/**
 * The following class create the category 
 * @version 1.0
 * @author  TobyTools.com (email:info@tobytools.com)(website: www.tobytools.com)
 * @copyright Copyright TobyTools.com 
 * @license GNU/GPL
 */


class ArchiDashFactoryCategory extends ArchiDashFactory{
	
	/*
	 *class constructor 
	 */
	function ArchiDashFactoryCategory($opts){		
		parent::__construct($opts);
		//check everything is allright
		if($this->completed){			
			$this->options['table']="#__categories";
			$this->options['tag']="category";		
		}
		else{
			global $mainframe;
			$mainframe->enqueueMessage(JText::_('NOOBJECT')." ".get_class($this),'error');
		}
	}
	
	
	/*
	 * create section if it does not exist
	 */
	function create(){
		if($this->completed){
			global $mainframe;
			$this->msg.="<h3>".JText::_('Category')."</h3>";
			/*Categories CREATION*/
			//find the list of categories definition for the specified dashboard
			$num = (int)$this->readDefinition($this->options['tag'],false,false,true);
			/*check there is at least one category*/
			if($num<=0){
				$this->completed=false;
				$this->errMsg.=get_class($this)."::create ".JText::_('DEFINITIONNOTVALID');
				$mainframe->enqueueMessage($this->errMsg,'error');
				return false;
			}			
			for ($i=1; $i<=$num; $i++){
				
				
				//category title
				$name = $this->readDefinition($this->options['tag'],$i,"name");
				//category alias
				$alias = $this->readDefinition($this->options['tag'],$i,"alias");
				//category description
				$description = $this->readDefinition($this->options['tag'],$i,"description");	
				
				//check the values are not empty
				if(strcasecmp(trim($name),"")==0 || strcasecmp(trim($alias),"")==0 || strcasecmp(trim($description),"")==0){
					$this->completed=false;
					$this->errMsg.=get_class($this)."::create ".JText::_('DEFINITIONNOTVALID');
					$mainframe->enqueueMessage($this->errMsg,'error');
					//exit from the foreach
					break;
				}
				
				//category creation
				$this->createCategory($name,$alias,$description,$i);		
			}
		}
		else{
			//no op
		}
		
	}
	
	/**
	 * given name, description and alias of category the method creates the record inside #__categories
	 * @param  $name
	 * @param  $alias
	 * @param  $description
	 * @return unknown_type
	 */
	protected function createCategory($name,$alias,$description,$ordering){
		global $mainframe;
		$categoryid=false;
		
		/*check if the category already exists.*/
		$db =& JFactory::getDBO();
			
		//to avoid problem with character set
		$db->setQuery($this->options['characterset']);
		$db->query();
		
		$query='SELECT * FROM '.$db->nameQuote($this->options['table']). ' WHERE '.$db->nameQuote('title').' = '.$db->Quote($name);
		$db->setQuery($query);
		$vet = $db->loadAssoc();
		if(!$vet){
			//the category needs to be created
			$query = "INSERT INTO ".$db->nameQuote($this->options['table'])." (title,alias,section,description,published,access,ordering) VALUES ('$name','$alias','{$this->options['sectionid']}','$description','1','{$this->options['accesslevel']}','$ordering')";			
			$db->setQuery($query);
			$result = $db->query();
			
			if(!$result){
				//something wrong
				$this->completed=false;
				$this->errMsg .= $db->getErrorNum()." : ".$db->getErrorMsg();
				$mainframe->enqueueMessage(JText::_( 'QUERYKO' ),'error');
				$mainframe->enqueueMessage($this->errMsg,'error');
				$mainframe->enqueueMessage(get_class($this)."::create",'error');
				return false;
			}
			else{
				//clean
				$this->errMsg .= false;
				//provide the element id
				$db->setQuery("SELECT LAST_INSERT_ID()");				
				$categoryid= $db->loadResult();
				
			}
				
			
			//output message
			$this->msg.="$name<br/>";			
			
		}
		else{
			//the section already exists
			$categoryid=$vet['id'];
			//output message
			$this->msg.="***$name<br/>";	
		}
		//save the information inside #__archidash_categories
		require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'models'.DS.'category.php');
		$classname="ArchiDashModelCategory";
		$model = new $classname();
		$vet = array();
			$vet['categoryid']=$categoryid;
			$vet['dashboardid']=$this->options['dashboardid'];
		
		$model->insertRecord($vet);
		//check there are errors for the creation of record with the model
		if($model->getErrMsg()!=false){
			$this->completed=false;
			$this->errMsg.=$model->getErrMsg();
		}
		
	}

}


