<?php


defined('_JEXEC') or die('Restricted Access');
jimport('joomla.application.component.model');
require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'models'.DS.'model.php');


/**
 * The following class manages all the CRUD operations on #__archidash_dashboards table
 * @version 1.0
 * @author  TobyTools.com (email:info@tobytools.com)(website: www.tobytools.com)
 * @copyright Copyright TobyTools.com 
 * @license GNU/GPL
 */


class ArchiDashModelDashboard extends ArchiDashModel{
	
	
	
	
		
	/**
	 * class constructor
	 * 
	 * */
	function ArchiDashModelDashboard(){
		parent::__construct();
		$this->table="#__archidash_dashboards";		
	}
	
	/**
	 * 
	 * Updates a record starting by the associative array. E.g.  Array ( [field1] => value1 [field2] => value2 ).
	 * @param array $vet the associative array with the new information of recod. 
	 * @return A database resource if successful, FALSE if not.
	 * 
	 * */
	
	function updateRecord($vet=false){
		global $mainframe;
		if($vet!=false){
			
			$db =& JFactory::getDBO();			
			
			//to avoid problem with character set
			$db->setQuery($this->characterset);
			$db->query();
			
			
			$query= "UPDATE ".$db->nameQuote($this->table)." SET ".$db->nameQuote('name')." = ".$db->Quote($vet['name']).", ".			
			$db->nameQuote('description')." = ".$db->Quote($vet['description']).
			" WHERE ".$db->nameQuote('id')." = ".$db->Quote($vet['id']);						
			$db->setQuery($query);
			$result = $db->query(); 
			if(!$result){
				//something wrong
				$this->errMsg = $db->getErrorNum()." : ".$db->getErrorMsg();
				$mainframe->enqueueMessage(JText::_( 'QUERYKO' ),'error');
				$mainframe->enqueueMessage($this->errMsg,'error');
				$mainframe->enqueueMessage(get_class($this)."::updateRecord",'error');
				return false;
			}
			else{
				//clean
				$this->errMsg = false;
				return $result;
			}		
		}
		else{
			$this->errMsg=JText::_( 'ARGNOVALID' );
			$mainframe->enqueueMessage(JText::_( 'ARGNOVALID' ),'error');
			$mainframe->enqueueMessage(get_class($this)."::updateRecord",'error');
			return false;
		}
				
	}
	
	/**
	 * 
	 * Insert a new record starting by the associative array.
	 * @param array $vet the associative array with the information of the new record. N.B. The id is useless, the function lets MYSQL manage the id auto increment 
	 * @return the record id if successful, FALSE if not.
	 * 
	 * */
	function insertRecord($vet=false){
		global $mainframe;
		if($vet!=false){						
			
			$db =& JFactory::getDBO();
			
			//to avoid problem with character set
			$db->setQuery($this->characterset);
			$db->query();
			
			$query = "INSERT INTO ".$db->nameQuote($this->table)." (name,description) VALUES ('".$vet['name']."','".$vet['description']."')";			
			$db->setQuery($query);
			$result = $db->query(); 
			if(!$result){
				//something wrong
				$this->errMsg = $db->getErrorNum()." : ".$db->getErrorMsg();
				$mainframe->enqueueMessage(JText::_( 'QUERYKO' ),'error');
				$mainframe->enqueueMessage($this->errMsg,'error');
				$mainframe->enqueueMessage(get_class($this)."::insertRecord",'error');
				return false;
			}
			else{
				//clean
				$this->errMsg = false;
				//provide the element id
				$db->setQuery("SELECT LAST_INSERT_ID()");				
				return $db->loadResult();
			}
		}
		else{
			$this->errMsg=JText::_( 'ARGNOVALID' );
			$mainframe->enqueueMessage(JText::_( 'ARGNOVALID' ),'error');
			$mainframe->enqueueMessage(get_class($this)."::insertRecord",'error');
			return false;
		}		
	}
	
	/**
	 * menu module name and id associated with dashboard
	 * @param $dashboardID
	 * @return unknown_type
	 */
	function getModuleInfo($dashboardID){
		
		global $mainframe;
		if($dashboardID!=false){

			$db =& JFactory::getDBO();
			
			//to avoid problem with character set
			$db->setQuery($this->characterset);
			$db->query();
			
			
			$query=	'	SELECT title as name, moduleid  
						FROM '.$db->nameQuote("#__archidash_modules").' as a , '.$db->nameQuote("#__modules").' as b'. 
					'	WHERE '.$db->nameQuote('dashboardid').' = '.$db->Quote($dashboardID).
					' 		AND '.$db->nameQuote('moduleid').' = '.$db->nameQuote('b.id');
			$db->setQuery($query);
			$vet = $db->loadAssoc();
			
			return $vet;						
			
		}
		else{
			$this->errMsg=JText::_( 'ARGNOVALID' );
			$mainframe->enqueueMessage(JText::_( 'ARGNOVALID' ),'error');
			$mainframe->enqueueMessage(get_class($this)."::getModuleInfo",'error');
			return false;
		}
		
		
			
	}
	
	/**
	 * provides all details
	 * @param $dashboardID
	 * @return unknown_type
	 */
	function getDetails($dashboardID){
		global $mainframe;
		if($dashboardID!=false){
			
			$result = array();
			
			$db =& JFactory::getDBO();			
			//to avoid problem with character set
			$db->setQuery($this->characterset);
			$db->query();
			
			/*DASHBOARD*/
			$query=	'	SELECT *  
						FROM '.$db->nameQuote("#__archidash_dashboards"). 
					'	WHERE '.$db->nameQuote('id').' = '.$db->Quote($dashboardID);
			$db->setQuery($query);
			$vet = $db->loadAssocList();
			
			if(!$vet){
				//something wrong
				if($db->getErrorNum()==0){
					//it is not an error, the problem is there is no record with the specified id
					$this->errMsg = false;
				}
				else{
					//MYSQL error
					$this->errMsg = $db->getErrorNum()." : ".$db->getErrorMsg();
					$mainframe->enqueueMessage(JText::_( 'QUERYKO' ),'error');
					$mainframe->enqueueMessage($this->errMsg,'error');
					$mainframe->enqueueMessage(get_class($this)."::getDetails",'error');
					
				}
			}
			else{
				$result['dashboard']=$vet;	
			}
			
			
			
			
			
			
			/*ARCHIMEDE MODULES*/
			$query=	'	SELECT title as name, moduleid  
						FROM '.$db->nameQuote("#__archidash_archies").' as a , '.$db->nameQuote("#__modules").' as b'. 
					'	WHERE '.$db->nameQuote('dashboardid').' = '.$db->Quote($dashboardID).
					' 		AND '.$db->nameQuote('moduleid').' = '.$db->nameQuote('b.id');
			$db->setQuery($query);
			$vet = $db->loadAssocList();
			if(!$vet){
				//something wrong
				if($db->getErrorNum()==0){
					//it is not an error, the problem is there is no record with the specified id
					$this->errMsg = false;
				}
				else{
					//MYSQL error
					$this->errMsg = $db->getErrorNum()." : ".$db->getErrorMsg();
					$mainframe->enqueueMessage(JText::_( 'QUERYKO' ),'error');
					$mainframe->enqueueMessage($this->errMsg,'error');
					$mainframe->enqueueMessage(get_class($this)."::getDetails",'error');
					
				}
			}
			else{
				$result['archies']=$vet;	
			}
			/*******************/
			
			/*ARTICLES*/
			$query=	'	SELECT title as name, articleid  
						FROM '.$db->nameQuote("#__archidash_articles").' as a , '.$db->nameQuote("#__content").' as b'. 
					'	WHERE '.$db->nameQuote('dashboardid').' = '.$db->Quote($dashboardID).
					' 		AND '.$db->nameQuote('articleid').' = '.$db->nameQuote('b.id');
			$db->setQuery($query);
			$vet = $db->loadAssocList();
			if(!$vet){
				//something wrong
				if($db->getErrorNum()==0){
					//it is not an error, the problem is there is no record with the specified id
					$this->errMsg = false;
				}
				else{
					//MYSQL error
					$this->errMsg = $db->getErrorNum()." : ".$db->getErrorMsg();
					$mainframe->enqueueMessage(JText::_( 'QUERYKO' ),'error');
					$mainframe->enqueueMessage($this->errMsg,'error');
					$mainframe->enqueueMessage(get_class($this)."::getDetails",'error');
					
				}
			}
			else{
				$result['articles']=$vet;	
			}
			/*******************/
			
			
			
			/*CATEGORIES*/
			$query=	'	SELECT title as name, categoryid  
						FROM '.$db->nameQuote("#__archidash_categories").' as a , '.$db->nameQuote("#__categories").' as b'. 
					'	WHERE '.$db->nameQuote('dashboardid').' = '.$db->Quote($dashboardID).
					' 		AND '.$db->nameQuote('categoryid').' = '.$db->nameQuote('b.id');
			$db->setQuery($query);
			$vet = $db->loadAssocList();
			if(!$vet){
				//something wrong
				if($db->getErrorNum()==0){
					//it is not an error, the problem is there is no record with the specified id
					$this->errMsg = false;
				}
				else{
					//MYSQL error
					$this->errMsg = $db->getErrorNum()." : ".$db->getErrorMsg();
					$mainframe->enqueueMessage(JText::_( 'QUERYKO' ),'error');
					$mainframe->enqueueMessage($this->errMsg,'error');
					$mainframe->enqueueMessage(get_class($this)."::getDetails",'error');
					
				}
			}
			else{
				$result['categories']=$vet;	
			}
			/*******************/
			
			/*MENUTYPE*/
			$query=	'	SELECT menutype as name, menutypeid  
						FROM '.$db->nameQuote("#__archidash_menu_types").' as a , '.$db->nameQuote("#__menu_types").' as b'. 
					'	WHERE '.$db->nameQuote('dashboardid').' = '.$db->Quote($dashboardID).
					' 		AND '.$db->nameQuote('menutypeid').' = '.$db->nameQuote('b.id');
			$db->setQuery($query);
			$vet = $db->loadAssocList();
			if(!$vet){
				//something wrong
				if($db->getErrorNum()==0){
					//it is not an error, the problem is there is no record with the specified id
					$this->errMsg = false;
				}
				else{
					//MYSQL error
					$this->errMsg = $db->getErrorNum()." : ".$db->getErrorMsg();
					$mainframe->enqueueMessage(JText::_( 'QUERYKO' ),'error');
					$mainframe->enqueueMessage($this->errMsg,'error');
					$mainframe->enqueueMessage(get_class($this)."::getDetails",'error');
					
				}
			}
			else{
				$result['menutype']=$vet;	
			}
			/*******************/
			
			
			/*MENU MODULE*/
			$query=	'	SELECT title as name, moduleid  
						FROM '.$db->nameQuote("#__archidash_modules").' as a , '.$db->nameQuote("#__modules").' as b'. 
					'	WHERE '.$db->nameQuote('dashboardid').' = '.$db->Quote($dashboardID).
					' 		AND '.$db->nameQuote('moduleid').' = '.$db->nameQuote('b.id');
			$db->setQuery($query);
			$vet = $db->loadAssocList();
			if(!$vet){
				//something wrong
				if($db->getErrorNum()==0){
					//it is not an error, the problem is there is no record with the specified id
					$this->errMsg = false;
				}
				else{
					//MYSQL error
					$this->errMsg = $db->getErrorNum()." : ".$db->getErrorMsg();
					$mainframe->enqueueMessage(JText::_( 'QUERYKO' ),'error');
					$mainframe->enqueueMessage($this->errMsg,'error');
					$mainframe->enqueueMessage(get_class($this)."::getDetails",'error');
					
				}
			}
			else{
				$result['module']=$vet;	
			}
			/*******************/
			
			/*SECTION*/
			$query=	'	SELECT title as name, sectionid  
						FROM '.$db->nameQuote("#__archidash_sections").' as a , '.$db->nameQuote("#__sections").' as b'. 
					'	WHERE '.$db->nameQuote('dashboardid').' = '.$db->Quote($dashboardID).
					' 		AND '.$db->nameQuote('sectionid').' = '.$db->nameQuote('b.id');
			$db->setQuery($query);
			$vet = $db->loadAssocList();
			if(!$vet){
				//something wrong
				if($db->getErrorNum()==0){
					//it is not an error, the problem is there is no record with the specified id
					$this->errMsg = false;
				}
				else{
					//MYSQL error
					$this->errMsg = $db->getErrorNum()." : ".$db->getErrorMsg();
					$mainframe->enqueueMessage(JText::_( 'QUERYKO' ),'error');
					$mainframe->enqueueMessage($this->errMsg,'error');
					$mainframe->enqueueMessage(get_class($this)."::getDetails",'error');
					
				}
			}
			else{
				$result['section']=$vet;	
			}
			/*******************/
			
			/*DATASOURCE SQL*/
			$query=	'	SELECT name, datasourceid  
						FROM '.$db->nameQuote("#__archidash_datasources").' as a , '.$db->nameQuote("#__arc_datasource_sql").' as b'. 
					'	WHERE '.$db->nameQuote('dashboardid').' = '.$db->Quote($dashboardID).
					'		AND '.$db->nameQuote('type').' = '.$db->Quote("SQL").
					' 		AND '.$db->nameQuote('datasourceid').' = '.$db->nameQuote('b.id');
			$db->setQuery($query);
			$vet = $db->loadAssocList();
			if(!$vet){
				//something wrong
				if($db->getErrorNum()==0){
					//it is not an error, the problem is there is no record with the specified id
					$this->errMsg = false;
				}
				else{
					//MYSQL error
					$this->errMsg = $db->getErrorNum()." : ".$db->getErrorMsg();
					$mainframe->enqueueMessage(JText::_( 'QUERYKO' ),'error');
					$mainframe->enqueueMessage($this->errMsg,'error');
					$mainframe->enqueueMessage(get_class($this)."::getDetails",'error');
					
				}
			}
			else{
				$result['ds_sql']=$vet;	
			}
			/*******************/
			
			/*DATASOURCE HTML*/
			$query=	'	SELECT name, datasourceid  
						FROM '.$db->nameQuote("#__archidash_datasources").' as a , '.$db->nameQuote("#__arc_datasource_html").' as b'. 
					'	WHERE '.$db->nameQuote('dashboardid').' = '.$db->Quote($dashboardID).
					'		AND '.$db->nameQuote('type').' = '.$db->Quote("HTML").
					' 		AND '.$db->nameQuote('datasourceid').' = '.$db->nameQuote('b.id');
			$db->setQuery($query);
			$vet = $db->loadAssocList();
			if(!$vet){
				//something wrong
				if($db->getErrorNum()==0){
					//it is not an error, the problem is there is no record with the specified id
					$this->errMsg = false;
				}
				else{
					//MYSQL error
					$this->errMsg = $db->getErrorNum()." : ".$db->getErrorMsg();
					$mainframe->enqueueMessage(JText::_( 'QUERYKO' ),'error');
					$mainframe->enqueueMessage($this->errMsg,'error');
					$mainframe->enqueueMessage(get_class($this)."::getDetails",'error');
					
				}
			}
			else{
				$result['ds_html']=$vet;	
			}
			/*******************/
			
			/*DATASOURCE Article*/
			$query=	'	SELECT name, datasourceid  
						FROM '.$db->nameQuote("#__archidash_datasources").' as a , '.$db->nameQuote("#__arc_datasource_article").' as b'. 
					'	WHERE '.$db->nameQuote('dashboardid').' = '.$db->Quote($dashboardID).
					'		AND '.$db->nameQuote('type').' = '.$db->Quote("Article").
					' 		AND '.$db->nameQuote('datasourceid').' = '.$db->nameQuote('b.id');
			$db->setQuery($query);
			$vet = $db->loadAssocList();
			if(!$vet){
				//something wrong
				if($db->getErrorNum()==0){
					//it is not an error, the problem is there is no record with the specified id
					$this->errMsg = false;
				}
				else{
					//MYSQL error
					$this->errMsg = $db->getErrorNum()." : ".$db->getErrorMsg();
					$mainframe->enqueueMessage(JText::_( 'QUERYKO' ),'error');
					$mainframe->enqueueMessage($this->errMsg,'error');
					$mainframe->enqueueMessage(get_class($this)."::getDetails",'error');
					
				}
			}
			else{
				$result['ds_article']=$vet;	
			}
			/*******************/
			
			/*DATASOURCE Snippet*/
			$query=	'	SELECT name, datasourceid  
						FROM '.$db->nameQuote("#__archidash_datasources").' as a , '.$db->nameQuote("#__arc_datasource_snippet").' as b'. 
					'	WHERE '.$db->nameQuote('dashboardid').' = '.$db->Quote($dashboardID).
					'		AND '.$db->nameQuote('type').' = '.$db->Quote("Snippet").
					' 		AND '.$db->nameQuote('datasourceid').' = '.$db->nameQuote('b.id');
			$db->setQuery($query);
			$vet = $db->loadAssocList();
			if(!$vet){
				//something wrong
				if($db->getErrorNum()==0){
					//it is not an error, the problem is there is no record with the specified id
					$this->errMsg = false;
				}
				else{
					//MYSQL error
					$this->errMsg = $db->getErrorNum()." : ".$db->getErrorMsg();
					$mainframe->enqueueMessage(JText::_( 'QUERYKO' ),'error');
					$mainframe->enqueueMessage($this->errMsg,'error');
					$mainframe->enqueueMessage(get_class($this)."::getDetails",'error');
					
				}
			}
			else{
				$result['ds_snippet']=$vet;	
			}
			/*******************/
			
			
			
			
			return $result;
			
			
			
			
			
		}
		else{
			$this->errMsg=JText::_( 'ARGNOVALID' );
			$mainframe->enqueueMessage(JText::_( 'ARGNOVALID' ),'error');
			$mainframe->enqueueMessage(get_class($this)."::getModuleInfo",'error');
			return false;
		}
		
	}
	
	
		
	
}
?>