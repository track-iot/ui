<?php 

// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.methods' );
jimport( 'joomla.html.pane' );
/**
 * @version 1.0
 * @author  TobyTools.com (email:info@tobytools.com)(website: www.tobytools.com)
 * @copyright Copyright TobyTools.com 
 * @license GNU/GPL
 */


class ArchiDashLayoutWelcome {
	
	function display(){
		//activation of mootols
		JHTML::_('behavior.mootools');
		
		//adding the css
		$css = JURI::base().'components/com_archidash/assets/css/welcome.css';
		$document =& JFactory::getDocument();
		$document->addStyleSheet($css);
		
		//setting the images paths
		$logoimg = JURI::base().'components/com_archidash/assets/images/logo128.png';
		
		
		
		
		//setting the hyperlinks and the <TD> table elements
		$welcomeLink = 'index.php?option=com_archidash&controller=Welcome';
		$dashboardLink = 'index.php?option=com_archidash&controller=Dashboard'; 
		$definitionLink = 'index.php?option=com_archidash&controller=Definition';
		
		$logoTD = "<a href='$dashboardLink'><img class='panel' src='$logoimg'><h2>".JText::_('GOTODASH')."</h2></a>";
		
		?>
		
		<center>
		<table class="adminlist">
		<tr>
		<td width="40%">
		<center>
			<table class="panel">
			<tr class="panel"><td id="sql" class="panel"><?php echo "";?></td> <td id="article" class="panel"><?php echo "";?></td></tr>			
			<tr><td colspan="2" class="panel"><?php echo $logoTD;?></td></tr>
			<tr class="panel"><td id="html" class="panel"><?php echo "";?></td> <td id="snippet" class="panel"><?php echo "";?></td></tr>
			</table>
		</center>
		</td>
		<td width="60%" valign="top">
			<center><h1><?php echo JText::_('WELCOMEMSG'); ?></h1></center>
			<br/>        
			<?php 
			//Preparing the panel
			$pane =& JPane::getInstance('Sliders');
			echo $pane->startPane('myPane');{
				echo $pane->startPanel(JText::_('WELCOME'), 'welcome');
				echo JText::_('WELCOMEADMPANEL');
				echo $pane->endPanel();
				echo "<br/>";
				echo $pane->startPanel(JText::_('More'), 'more');
				echo JText::_('MoreAdmPanel');
				echo $pane->endPanel();
								
			}
			echo $pane->endPane();
			?>
			<br/><br/>
			<center><h1><?php echo JText::_('ABOUTARCHIDASH'); ?></h1></center>
			<center>
			<table class="adminlist">
				<tr><td style="width:50%;text-align:right;"><b><?php echo JText::_('COMPONENTNAME');?></b></td><td style="width:50%;text-align:left;"><i>ArchiDash</i></td></tr>
				<tr><td style="width:50%;text-align:right;"><b><?php echo JText::_('CREATIONDATE');?></b></td><td style="width:50%;text-align:left;"><i><?php echo JText::_('April 2011');?></i></td></tr>
				<tr><td style="width:50%;text-align:right;"><b><?php echo JText::_('VERSION');?></b></td><td style="width:50%;text-align:left;"><i><?php echo JText::_('ARCHIDASHVERSION')?></i></td></tr>
				<tr><td style="width:50%;text-align:right;"><b><?php echo JText::_('LICENSE');?></b></td><td style="width:50%;text-align:left;"><i>GNU/GPL</i></td></tr>
				<tr><td style="width:50%;text-align:right;"><b><?php echo JText::_('AUTHOR');?></b></td><td style="width:50%;text-align:left;"><i>TobyTools.com</i></td></tr>
				<tr><td style="width:50%;text-align:right;"><b><?php echo JText::_('AUTHORWEBSITE');?></b></td><td style="width:50%;text-align:left;"><i><a href="http://www.tobytools.com" target="_blank">http://www.tobytools.com</a></i></td></tr>							
			</table>
			</center>
		</td>
		</tr>
		</table>
		</center>
		
		<?php
		//title for ArchiDash
		JToolBarHelper::title(   JText::_( 'ARCHIDASH' ),'logo');
		// add sub menu items
		JSubMenuHelper::addEntry(JText::_('WELCOME'), $welcomeLink,true);
		JSubMenuHelper::addEntry(JText::_('DASHBOARDS'), $dashboardLink,false);
		JSubMenuHelper::addEntry(JText::_('DEFINITIONS'), $definitionLink,false);
		
		
	}
}