<?php
function AikAdminBuildRoute( &$query )
{
	
	$segments = array();
	if(isset($query['c'])) {
		$segments[] = $query['c'];
		unset( $query['c'] );
	}
	return $segments; 
}
function AikAdminParseRoute( $segments )
{
	$menu =& JSite::getMenu();
	$item =& $menu->getActive();

	
	$vars = array();
	if(isset($segments[0])){
		$vars['c'] = $segments[0];
	}
	return $vars;
}