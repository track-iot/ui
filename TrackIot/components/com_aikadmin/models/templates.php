<?php
/**
 * @version		$Id: templates.php 1.0 19-11-2009 Danijar
 * @package		Frontend Admin
 * @copyright	Copyright (C) 2009 CMSSpace. http://www.cmsspace.com
 * @license		GNU/GPL. http://www.gnu.org/licenses/gpl.html
 */
defined( '_JEXEC' ) or die( 'Restricted access' );
require_once(dirname(__FILE__).DS.'extension.php');
jimport( 'joomla.filesystem.folder' );
class AikModelTemplates extends InstallerModel
{
	var $_type = 'template';
	function __construct()
	{
		global $mainframe;
		parent::__construct();
		$this->setState('filter.string', $mainframe->getUserStateFromRequest( "com_templates.installer.templates.string", 'filter', '', 'string' ));
		$this->setState('filter.client', $mainframe->getUserStateFromRequest( "com_templates.installer.templates.client", 'client', -1, 'int' ));
	}

	function _loadItems()
	{
		global $mainframe, $option;

		$db = &JFactory::getDBO();

		if ($this->_state->get('filter.client') < 0) {
			$client = 'all';
			$templateDirs = JFolder::folders(JPATH_SITE.DS.'templates');

			for ($i=0; $i < count($templateDirs); $i++) {
				$template = new stdClass();
				$template->folder = $templateDirs[$i];
				$template->client = 0;
				$template->baseDir = JPATH_SITE.DS.'templates';

				if ($this->_state->get('filter.string')) {
					if (strpos($template->folder, $this->_state->get('filter.string')) !== false) {
						$templates[] = $template;
					}
				} else {
					$templates[] = $template;
				}
			}
			$templateDirs = JFolder::folders(JPATH_ADMINISTRATOR.DS.'templates');

			for ($i=0; $i < count($templateDirs); $i++) {
				$template = new stdClass();
				$template->folder = $templateDirs[$i];
				$template->client = 1;
				$template->baseDir = JPATH_ADMINISTRATOR.DS.'templates';

				if ($this->_state->get('filter.string')) {
					if (strpos($template->folder, $this->_state->get('filter.string')) !== false) {
						$templates[] = $template;
					}
				} else {
					$templates[] = $template;
				}
			}
		} else {
			$clientInfo =& JApplicationHelper::getClientInfo($this->_state->get('filter.client'));
			$client = $clientInfo->name;
			$templateDirs = JFolder::folders($clientInfo->path.DS.'templates');

			for ($i=0; $i < count($templateDirs); $i++) {
				$template = new stdClass();
				$template->folder = $templateDirs[$i];
				$template->client = $clientInfo->id;
				$template->baseDir = $clientInfo->path.DS.'templates';

				if ($this->_state->get('filter.string')) {
					if (strpos($template->folder, $this->_state->get('filter.string')) !== false) {
						$templates[] = $template;
					}
				} else {
					$templates[] = $template;
				}
			}
		}
		$query = 'SELECT template' .
				' FROM #__templates_menu' .
				' WHERE 1';
		$db->setQuery($query);
		$activeList = $db->loadResultArray();

		$rows = array();
		$rowid = 0;
		foreach($templates as $template)
		{
			$dirName = $template->baseDir .DS. $template->folder;
			$xmlFilesInDir = JFolder::files($dirName,'.xml$');

			foreach($xmlFilesInDir as $xmlfile)
			{
				$data = JApplicationHelper::parseXMLInstallFile($dirName . DS. $xmlfile);

				$row = new StdClass();
				$row->id 		= $rowid;
				$row->client_id	= $template->client;
				$row->directory = $template->folder;
				$row->baseDir	= $template->baseDir;

				if (in_array($row->directory, $activeList)) {
					$row->active = true;
				} else {
					$row->active = false;
				}

				if ($data) {
					foreach($data as $key => $value) {
						$row->$key = $value;
					}
				}

				$row->checked_out = 0;
				$row->jname = JString::strtolower( str_replace( ' ', '_', $row->name ) );

				$rows[] = $row;
				$rowid++;
			}
		}
		$this->setState('pagination.total', count($rows));
		if($this->_state->get('pagination.offset') > $this->_state->get('pagination.total')) {
			$this->setState('pagination.offset',0);
		}

		if($this->_state->get('pagination.limit') > 0) {
			$this->_items = array_slice( $rows, $this->_state->get('pagination.offset'), $this->_state->get('pagination.limit') );
		} else {
			$this->_items = $rows;
		}
	}
}