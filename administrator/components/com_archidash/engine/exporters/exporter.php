<?php

defined('_JEXEC') or die('Restricted Access');




/**
 * Abstract class for the Exporter classes of ArchiDash
 * @version 1.0
 * @author  TobyTools.com (email:info@tobytools.com)(website: www.tobytools.com)
 * @copyright Copyright TobyTools.com 
 * @license GNU/GPL
 */


abstract class ArchiDashExporter {
	
	/**
	 * 
	 * @var string, the error message (if any) associated with the last operation performed
	 */
	protected $errMsg;
	
	
	/**
	 * associative array with the options for the class
	 * 
	 * @var unknown_type
	 */
	protected $options;
	
	/**
	 * This variable it's true only if the create operation is completed successfully.
	 * The export operation is a work unit performed against the Joomla database, 
	 * and treated in a coherent and reliable way independent of other ones.
	 * 
	 * 
	 * @var boolean
	 */
	
	protected $completed;
	
	
	
	protected function ArchiDashExporter($opt=false){
		if($opt){
			$this->options=$opt;
			$this->options['characterset']="SET NAMES 'utf8'";
			$this->errMsg="";
			$this->completed=true;
				
		}
		else{
			$this->completed=false;
			$this->options=false;
			$this->errMsg=false;
			
		}
		
	}
	/**
	 * 
	 * @param mixed $params
	 * @return string, the xml representation of element
	 */
	public abstract function export($params=false);
	
	protected function convertToHTMLEntities($string=false){
		if(!$string){
			return false;
		}
		else{
			//strip the slashes if any
			$string = stripslashes($string);
			
			
			$string = htmlspecialchars($string);/*added because there are problem with <>& when you write an xml document*/
			
			//$htmlentities = mb_convert_encoding($string, 'HTML-ENTITIES', mb_detect_encoding($string));
			$htmlentities = mb_convert_encoding($string, 'UTF-8', mb_detect_encoding($string));
			
			return trim($htmlentities);
		}
	}
	
}




?>
