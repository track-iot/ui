<?php
/**
 * @version		$Id: cache.class.php 1.0 19-11-2009 Danijar
 * @package		Frontend Admin
 * @copyright	Copyright (C) 2009 CMSSpace. http://www.cmsspace.com
 * @license		GNU/GPL. http://www.gnu.org/licenses/gpl.html
 */
defined( '_JEXEC' ) or die( 'Restricted access' );
class CacheData extends JObject
{
	var $_items = null;
	var $_path = null;
	function __construct( $path )
	{
		$this->_path = $path;
		$this->_parse();
	}
	function _parse()
	{
		jimport('joomla.filesystem.folder');
		jimport('joomla.filesystem.file');
		$folders = JFolder::folders($this->_path);

		foreach ($folders as $folder)
		{
			$files = array();
			$files = JFolder::files($this->_path.DS.$folder);
			$this->_items[$folder] = new CacheItem( $folder );

			foreach ($files as $file)
			{
				$this->_items[$folder]->updateSize( filesize( $this->_path.DS.$folder.DS.$file )/ 1024 );
			}
		}
	}
	function getGroupCount()
	{
		return count($this->_items);
	}
	function getRows( $start, $limit )
	{
		$i = 0;
		$rows = array();
		if (!is_array($this->_items)) {
			return null;
		}

		foreach ($this->_items as $item)
		{
			if ( (($i >= $start) && ($i < $start+$limit)) || ($limit == 0) ) {
				$rows[] = $item;
			}
			$i++;
		}
		return $rows;
	}
	function cleanCache( $group='' )
	{
		$cache =& JFactory::getCache('', 'callback', 'file');
		$cache->clean( $group );
	}

	function cleanCacheList( $array )
	{
		foreach ($array as $group) {
			$this->cleanCache( $group );
		}
	}
}
class CacheItem
{
	var $group 	= "";
	var $size 	= 0;
	var $count 	= 0;

	function CacheItem ( $group )
	{
		$this->group = $group;
	}

	function updateSize( $size )
	{
		$this->size = number_format( $this->size + $size, 2 );
		$this->count++;
	}
}