<?php

// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.controller');
/**
 * 
 * @version 1.0
 * @author  TobyTools.com (email:info@tobytools.com)(website: www.tobytools.com)
 * @copyright Copyright TobyTools.com 
 * @license GNU/GPL
 */

class ArchiDashControllerWelcome extends JController{
	
	
	function ArchiDashControllerWelcome(){
		
		//registerTask
		parent::__construct();
		
		
	}
	
	
	
	function display(){		
		
		/**
		 * example opts
		 * 
		 * Array ( [dashboard] => Array ( [name] => Some Name [description] => Some Desc [accesslevel] => 2 ) [sectionid] => 117 [menu] => Array ( [name] => ArchiDash Menu [description] => Some menu description ) [modules] => Array ( [0] => 1952 [1] => 1953 [2] => 1954 [3] => 1955 [4] => 1956 [5] => 1957 [6] => 1958 ) ) 
		 * 
		 */
		/*
		
		require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'models'.DS.'definition.php');
		$classname = "ArchiDashModelDefinition";		
		$model = new $classname();
		$model->deleteRecord(1);
		echo "ok";
		*/
		/*
		require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'engine'.DS.'exporters'.DS.'dashboard.php');
		
		$opts = array();
		
		$dashboard = array();
		$dashboard['name']="Some Name";
		$dashboard['description']="Some Desc";
		$dashboard['accesslevel']="2";
		
		$opts['dashboard']=$dashboard;
		$opts['sectionid']=117;
		
		$menu=array();
		$menu['name']="ArchiDash Menu";
		$menu['description']="Some menu description";
		$opts['menu']=$menu;
		
										
		$modules=array();
		$modules[]=1952;$modules[]=1953;$modules[]=1954;$modules[]=1955;$modules[]=1956;$modules[]=1957;$modules[]=1958;
		$opts['modules']=$modules;
		
		$classname = "ArchiDashExporterDashboard";		
		$exporter = new $classname($opts);
			
		$html = $exporter->export();
		
		$doc = new DOMDocument();
		$doc->formatOutput = true;
		$doc->preserveWhiteSpace = false;				
		@$doc->loadHTML($html);	//load html-entities			
		$xpath = new DOMXPath($doc);	
		$divTarget =  $xpath->query("//dashboard");
		foreach($divTarget as $trafficFlow){
			echo "<xmp>".$doc->saveXML($trafficFlow)."</xmp>";						
		}
		
		
		*/
		
		
		
		require_once( JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'views'.DS.'welcome'.DS.'view.html.php' );
		$classname = "ArchiDashViewWelcome";
		$view = new $classname();
		$view->display();
		
		/*
		require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'models'.DS.'archi.php');
		$classname = "ArchiDashModelArchi";		
		$model = new $classname();
		$res = $model->selectAllModules(false,false,"id","desc");
		echo "<h1>".count($res)."</h1>";
		print_r($res);
		
		require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'models'.DS.'section.php');
		$classname = "ArchiDashModelSection";		
		$model = new $classname();
		$res = $model->selectAllSections(false,false,"id","desc");
		echo "<h1>".count($res)."</h1>";
		print_r($res);
		*/
					
	}
	
	
}
?>
