<?php
/**
 * @version		$Id: item.php 1.0 19-11-2009 Danijar
 * @package		Frontend Admin
 * @copyright	Copyright (C) 2009 CMSSpace. http://www.cmsspace.com
 * @license		GNU/GPL. http://www.gnu.org/licenses/gpl.html
 */
defined('_JEXEC') or die( 'Restricted access' );
jimport( 'joomla.application.component.model' );
class AikModelItem extends JModel
{
	var $_id = null;
	var $_table = null;
	var $_url = null;
	function __construct()
	{
		parent::__construct();
		$url = JRequest::getVar('url', array(), '', 'array');
		if (isset($url['option']))
		 {
			$this->_url = 'index.php?option='.$url['option'];
			unset($url['option']);
			if (count($url)) {
				foreach ($url as $k => $v)
				{
					$this->_url .= '&'.$k.'='.$v;
				}
			}
		}
		
		$this->setId();
	}
	
	function setId()
	{
		$array = JRequest::getVar('cid', array(0), '', 'array');
		$this->_id = (int) $array[0];

		if (!$this->_id) {
		$this->_id = JRequest::getInt('id', 0);
		}
	}

	function &getItem()
	{
		static $item;
		if (isset($item)) {
			return $item;
		}

		$table =& $this->_getTable();
		$edit	= JRequest::getVar('edit',true);
		$cid = JRequest::getVar( 'cid', array(0), '', 'array' );
		JArrayHelper::toInteger($cid, array(0));
		if ($edit) {
			$table->load($cid[0]);
		}
		if ($type = JRequest::getString('type')) {
			$table->type = $type;
		}
		if ($menu_type = JRequest::getString('menutype')) {
			$table->menutype = $menu_type;
		}

		switch ($table->type)
		{
			case 'separator':
				$table->link = null;
				$table->componentid = 0;
				break;
			case 'url':
				$table->componentid = 0;
				break;
			case 'menulink':
				$table->componentid = 0;
				break;
			case 'component':
				if (!is_null($this->_url)) {
					$table->link = $this->_url;
				}
				$url = str_replace('index.php?', '', $table->link);
				$url = str_replace('&amp;', '&', $url);
				$table->linkparts = null;
				if(strpos($url, '&amp;') !== false)
				{
				   $url = str_replace('&amp;','&',$url);
				}

				parse_str($url, $table->linkparts);

				$db = &$this->getDBO();
				if ($component = @$table->linkparts['option']) {
					$query = 'SELECT `id`' .
							' FROM `#__components`' .
							' WHERE `link` <> \'\'' .
							' AND `parent` = 0' .
							' AND `option` = "'.$db->getEscaped($component).'"';
					$db->setQuery( $query );
					$table->componentid = $db->loadResult();
				}
				break;
		}

		$item = $table;
		return $item;
	}

	function &getExpansion()
	{
		$item				= &$this->getItem();
		$return['option']	= JRequest::getCmd('expand');
		$menutype			= JRequest::getString('menutype');

		if ($return['option'])
		{
			require_once(JPATH_SITE.DS.'components'.DS.'com_aikadmin'.DS.'helpers'.DS.'ilink.php');
			$handler		= new iLink($return['option'], $item->id, $menutype);
			$return['html'] = $handler->getTree();
			return $return;
		} else {
			$return['html'] = null;
		}
		return $return;
	}

	function &getUrlParams()
	{
		$item	=& $this->getItem();
		$params	= new JParameter('');

		if ($state =& $this->_getStateXML())
		{
			if (is_a($state, 'JSimpleXMLElement'))
			{
				$sp =& $state->getElementByPath('url');
				$params->setXML($sp);
				if (isset($item->linkparts) && is_array($item->linkparts)) {
					$params->loadArray($item->linkparts);
				}
			}
		}
		return $params;
	}

	function &getStateParams()
	{
		$item	=& $this->getItem();
		$params	= new JParameter($item->params);

		if ($state =& $this->_getStateXML())
		{
			if (is_a($state, 'JSimpleXMLElement'))
			{
				$sp =& $state->getElementByPath('params');
				$params->setXML($sp);
			}
		}
		return $params;
	}

	function &getAdvancedParams()
	{
		$item	=& $this->getItem();
		$params	= new JParameter($item->params);

		if ($state =& $this->_getStateXML())
		{
			if (is_a($state, 'JSimpleXMLElement'))
			{
				$ap =& $state->getElementByPath('advanced');
				$params->setXML($ap);
			}
		}
		return $params;
	}

	function &getComponentParams()
	{
		$params	= null;
		$item	= &$this->getItem();

		if ($item->type == 'component')
		{
			$comp	= &$this->getComponent();
			$option	= preg_replace( '#\W#', '', $comp->option );
			$path	= JPATH_SITE.DS.'components'.DS.$option.DS.'config.xml';

			$params = new JParameter( $item->params );
			if (file_exists( $path ))
			{
				$xml =& JFactory::getXMLParser('Simple');
				if ($xml->loadFile($path))
				{
					$document =& $xml->document;
					$menu = $document->attributes('menu');
					if ( isset($menu) && $menu == 'hide' )
					{
						$params = null;
						return $params;
					}

					if (isset($document->params[0]->param))
					{
						$hide	= array();
						for ($i=0,$n=count($document->params[0]->param); $i<$n; $i++)
						{
							if ($document->params[0]->param[$i]->attributes('menu') == 'hide')
							{
								$hide[]	= &$document->params[0]->param[$i];
							}
							else if ($document->params[0]->param[$i]->attributes('type') == 'radio' || $document->params[0]->param[$i]->attributes('type') == 'list') {
								$document->params[0]->param[$i]->addAttribute('default', '');
								$document->params[0]->param[$i]->addAttribute('type', 'list');
								$child = &$document->params[0]->param[$i]->addChild('option', array('value' => ''));
								$child->setData('Use Global');
							}
						}
						for ($i = 0, $n = count( $hide ); $i < $n; $i++) {
							$document->params[0]->removeChild( $hide[$i] );
						}
					}
					$params->setXML( $document->params[0] );
				}
			}
		}
		return $params;
	}

	function &getSystemParams()
	{
		$params	= null;
		$item	= &$this->getItem();

		$params = new JParameter( $item->params );
		if ($item->type == 'component') {
			$path = JPATH_BASE.DS.'components'.DS.'com_aikadmin'.DS.'models'.DS.'metadata'.DS.'component.xml';
			if (file_exists( $path )) {
				$xml =& JFactory::getXMLParser('Simple');
				if ($xml->loadFile($path)) {
					$document =& $xml->document;
					$params->setXML($document->getElementByPath('state/params'));
				}
			}
		}
		return $params;
	}
	function getStateName()
	{
		$state =& $this->_getStateXML();

		if ( ! is_a($state, 'JSimpleXMLElement'))
		{
			return null;
		}

		$name = null;
		$sn =& $state->getElementByPath('name');
		if ($sn) {
			$name = $sn->data();
		}

		return JText::_($name);
	}
	function getStateDescription()
	{
		$state =& $this->_getStateXML();


		if ( ! is_a($state, 'JSimpleXMLElement'))
		{
			return null;
		}

		$description = null;
		$sd =& $state->getElementByPath('description');
		if ($sd) {
			$description = $sd->data();
		}

		return JText::_($description);
	}
	function &getComponent()
	{
		$item		=& $this->getItem();
		$id			= $item->componentid;
		$component	= & JTable::getInstance( 'component');
		$component->load( $id );
		return $component;
	}

	function checkout($uid = null)
	{
		$id = JRequest::getVar('cid', array(0), '', 'array');
		JArrayHelper::toInteger( $id, array(0) );
		if (is_null($uid)) {
			$user	=& JFactory::getUser();
			$uid	= $user->get('id');
		}
		$item	=& $this->getItem();
		if(!$item->checkout($uid, $id[0])) {
			$this->setError($this->_db->getErrorMsg());
			return false;
		}

		return true;
	}
	
	function checkin()
	{
		if ($this->_id) {
			$item =& $this->_getTable();

			if(!$item->checkin($this->_id)) {
				$this->setError($this->_db->getErrorMsg());
				return false;
			}

			return true;
		}

		return false;
	}

	function store()
	{
		$db		=& JFactory::getDBO();
		$row	=& $this->getItem();
		$post	= $this->_state->get( 'request' );

		switch ($post['type'])
		{
			case 'separator':
				break;
			case 'url':
				break;
			case 'menulink':
				$post['link'] = 'index.php?Itemid='.$post['params']['menu_item'];
				break;
			case 'component':
				break;
		}
		if (!$row->bind( $post )) {
			echo "<script> alert('".$row->getError(true)."'); window.history.go(-1); </script>\n";
			return false;
		}

		if ($row->id > 0)
		{
			$query		= 'SELECT menutype FROM #__menu WHERE id = '.(int) $row->id;
			$this->_db->setQuery( $query );
			$oldType	= $this->_db->loadResult();
			if ($oldType != $row->menutype) {
				$row->parent = 0;
			}
			$query		= 'SELECT parent FROM #__menu WHERE id = '.(int) $row->id;
			$this->_db->setQuery( $query );
			$oldParent	= $this->_db->loadResult();
			if ($oldParent != $row->parent) {
				if ($row->parent != 0) {
					$query	= 'SELECT sublevel FROM #__menu WHERE id = '.(int) $row->parent;
					$this->_db->setQuery( $query );
					$sublevel = $this->_db->loadResult() + 1;
				} else {
					$sublevel = 0;
				}
				$row->sublevel = $sublevel;
				$this->_setSubLevel( array( (int) $row->id ), $sublevel );
			}
		}
		else
		{
			$where = "menutype = " . $db->Quote($row->menutype) . " AND published >= 0 AND parent = ".(int) $row->parent;
			$row->ordering = $row->getNextOrder( $where );

			if( $row->parent != 0 ) {
				$query = 'SELECT sublevel FROM #__menu WHERE id = '. (int) $row->parent;
				$this->_db->setQuery($query);
				$row->sublevel = $this->_db->loadResult() + 1;
			}
		}

		if (isset($post['urlparams']) && is_array($post['urlparams']))
		{
			$pos = strpos( $row->link, '?' );
			if ($pos !== false)
			{
				$prefix = substr( $row->link, 0, $pos );
				$query	= substr( $row->link, $pos+1 );

				$temp = array();
				if(strpos($query, '&amp;') !== false) {
					$query = str_replace('&amp;', '&', $query);
				}
				parse_str( $query, $temp );
				$temp2 = array_merge( $temp, $post['urlparams'] );

				$temp3 = array();
				foreach ($temp2 as $k => $v)
				{
					if ( $k && strlen($v) )
					{
						$temp3[] = $k.'='.$v;
					}
				}
				$url = null;
				$row->link = $prefix . '?' . implode( '&', $temp3 );
			}
		}

		if (!$row->check())
		{
			echo "<script> alert('".$row->getError(true)."'); window.history.go(-1); </script>\n";
			return false;
		}

		if (!$row->store())
		{
			echo "<script> alert('".$row->getError(true)."'); window.history.go(-1); </script>\n";
			return false;
		}

		$row->checkin();
		$row->reorder( 'menutype='.$db->Quote( $row->menutype ).' AND parent='.(int)$row->parent );
		MenusHelper::cleanCache();

		return true;
	}
	function delete( $ids )
	{
		JArrayHelper::toInteger($ids);

		$db = &$this->getDBO();

		if (count( $ids ))
		{
			$where = 'WHERE menuid = ' . implode( ' OR menuid = ', $ids );

			$query = 'DELETE FROM #__modules_menu '
				. $where;
			$db->setQuery( $query );
			if (!$db->query()) {
				$this->setError( $menuTable->getErrorMsg() );
				return false;
			}

			$query = 'DELETE FROM #__templates_menu '
				. $where;
			$db->setQuery( $query );
			if (!$db->query()) {
				$this->setError( $menuTable->getErrorMsg() );
				return false;
			}
			$query = 'UPDATE #__menu SET link = 0 WHERE type = \'menulink\' AND (link = '.implode( ' OR id = ', $ids ).')';
			$db->setQuery( $query );
			if (!$db->query()) {
				$this->setError( $db->getErrorMsg() );
				return false;
			}
			$where = 'WHERE id = ' . implode( ' OR id = ', $ids );

			$query = 'DELETE FROM #__menu ' . $where;
			$db->setQuery( $query );
			if (!$db->query()) {
				$this->setError( $db->getErrorMsg() );
				return false;
			}
		}
		MenusHelper::cleanCache();
		return true;
	}
	function deleteByType( $type = '' )
	{
		$db = &$this->getDBO();

		$query = 'SELECT id' .
				' FROM #__menu' .
				' WHERE menutype = ' . $db->Quote( $type );
		$db->setQuery( $query );
		$ids = $db->loadResultArray();

		if ($db->getErrorNum())
		{
			$this->setError( $db->getErrorMsg() );
			return false;
		}
		return $this->delete( $ids );
	}
	function &_getTable()
	{
		if ($this->_table == null) {
			$this->_table =& JTable::getInstance( 'menu');
		}
		return $this->_table;
	}

	function &_getStateXML()
	{
		static $xml;

		if (isset($xml)) {
			return $xml;
		}
		$xml = null;
		$xmlpath = null;
		$item 	= &$this->getItem();

		switch ($item->type)
		{
			case 'separator':
				$xmlpath = JPATH_BASE.DS.'components'.DS.'com_aikadmin'.DS.'models'.DS.'metadata'.DS.'separator.xml';
				break;
			case 'url':
				$xmlpath = JPATH_BASE.DS.'components'.DS.'com_aikadmin'.DS.'models'.DS.'metadata'.DS.'url.xml';
				break;
			case 'menulink':
				$xmlpath = JPATH_BASE.DS.'components'.DS.'com_aikadmin'.DS.'models'.DS.'metadata'.DS.'menulink.xml';
				break;
			case 'component':
			default:
				if (isset($item->linkparts['view']))
				{
					if (isset($item->linkparts['layout'])) {
						$layout = $item->linkparts['layout'];
					} else {
						$layout = 'default';
					}
					$lpath = JPATH_ROOT.DS.'components'.DS.$item->linkparts['option'].DS.'views'.DS.$item->linkparts['view'].DS.'tmpl'.DS.$layout.'.xml';
					$vpath = JPATH_ROOT.DS.'components'.DS.$item->linkparts['option'].DS.'views'.DS.$item->linkparts['view'].DS.'metadata.xml';
					if (file_exists($lpath)) {
						$xmlpath = $lpath;
					} elseif (file_exists($vpath)) {
						$xmlpath = $vpath;
					}
				}
				if (!$xmlpath && isset($item->linkparts['option'])) {
					$xmlpath = JPATH_ROOT.DS.'components'.DS.$item->linkparts['option'].DS.'metadata.xml';
					if(!file_exists($xmlpath)) {
						$xmlpath = JApplicationHelper::getPath('com_xml', $item->linkparts['option']);
					}
				}
				break;
		}

		if (file_exists($xmlpath))
		{
			$xml =& JFactory::getXMLParser('Simple');
			if ($xml->loadFile($xmlpath)) {
				$this->_xml = &$xml;
				$document =& $xml->document;
				$menus =& $document->getElementByPath('menu');
				if (is_a($menus, 'JSimpleXMLElement') && $menus->attributes('options') == 'none') {
					$xml =& $menus->getElementByPath('state');
				} else {
					$xml =& $document->getElementByPath('state');
				}
				if (!is_a($xml, 'JSimpleXMLElement')) {
					return $document;
				}
				if ($switch = $xml->attributes('switch'))
				{
					$default = $xml->attributes('default');
					$switchVal = (isset($item->linkparts[$switch]))? $item->linkparts[$switch] : 'default';
					$found = false;

					foreach ($xml->children() as $child) {
						if ($child->name() == $switchVal) {
							$xml =& $child;
							$found = true;
							break;
						}
					}

					if (!$found) {
						foreach ($xml->children() as $child) {
							if ($child->name() == $default) {
								$xml =& $child;
								break;
							}
						}
					}
				}
				$children = $xml->children();
				if (count($children) == 1)
				{
					if ($children[0]->name() == 'include') {
						$ret =& $this->_getIncludedParams($children[0]);
						if ($ret) {
							$xml =& $ret;
						}
					}
				}

				if ($switch = $xml->attributes('switch'))
				{
					$default = $xml->attributes('default');
					$switchVal = ($item->linkparts[$switch])? $item->linkparts[$switch] : 'default';
					$found = false;

					foreach ($xml->children() as $child) {
						if ($child->name() == $switchVal) {
							$xml =& $child;
							$found = true;
							break;
						}
					}

					if (!$found) {
						foreach ($xml->children() as $child) {
							if ($child->name() == $default) {
								$xml =& $child;
								break;
							}
						}
					}
				}
			}
		}
		return $xml;
	}

	function &_getIncludedParams($include)
	{
		$tags	= array();
		$state	= null;
		$source	= $include->attributes('source');
		$path	= $include->attributes('path');
		$item 	= &$this->getItem();

		preg_match_all( "/{([A-Za-z\-_]+)}/", $source, $tags);
		if (isset($tags[1])) {
			for ($i=0;$i<count($tags[1]);$i++) {
				$source = str_replace($tags[0][$i], @$item->linkparts[$tags[1][$i]], $source);
			}
		}
		if (file_exists( JPATH_ROOT.$source ))
		{
			$xml = & JFactory::getXMLParser('Simple');

			if ($xml->loadFile(JPATH_ROOT.$source)) {
				$document = &$xml->document;
				$state = $document->getElementByPath($path);
			}
		}
		return $state;
	}
	function _setSubLevel( $cid, $level )
	{
		JArrayHelper::toInteger($cid, array(0));

		$ids = implode( ',', $cid );

		$query	= 'UPDATE #__menu SET sublevel = '.(int) $level
				.' WHERE id IN ('.$ids.')';
		$this->_db->setQuery( $query );
		$this->_db->query();

		$query	= 'SELECT id FROM #__menu WHERE parent IN ('.$ids.')';
		$this->_db->setQuery( $query );
		$cids = $this->_db->loadResultArray( 0 );

		if (!empty( $cids )) {
			$this->_setSubLevel( $cids, $level + 1 );
		}
	}
}
