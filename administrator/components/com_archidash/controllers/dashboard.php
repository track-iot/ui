<?php

// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.controller');
require_once( JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'views'.DS.'dashboard'.DS.'view.html.php' );
require_once( JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'models'.DS.'dashboard.php' );
require_once( JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'engine'.DS.'factories'.DS.'dashboard.php' );
require_once( JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'engine'.DS.'destructors'.DS.'dashboard.php' );
/**
 * 
 * @version 1.0
 * @author  TobyTools.com (email:info@tobytools.com)(website: www.tobytools.com)
 * @copyright Copyright TobyTools.com 
 * @license GNU/GPL
 */

class ArchiDashControllerDashboard extends JController{
	
	
	protected $arcModel;
	protected $arcView;
	
	
	function ArchiDashControllerDashboard(){
		
		//registerTask
		parent::__construct();
		
		//call model		
		$classname = "ArchiDashModelDashboard";
		$this->arcModel = new $classname();
		//call view
		$classname = "ArchiDashViewDashboard";
		$this->arcView = new $classname();
		
		
		//task registration
		$this->registerTask('add','add');
		$this->registerTask('edit','edit');		
		$this->registerTask('remove','remove');
		
		$this->registerTask('cancel','cancel');
		$this->registerTask('saveedit','saveedit');
		$this->registerTask('applyedit','applyedit');
		
		$this->registerTask('save','save');
		
		$this->registerTask('details','details');
		
		
	}
	
	
	
	function display(){		
		
		$this->arcView->display();	
					
	}
	
	function add(){
		global $mainframe;
		//$classname = "ArchiDashFactoryDashboard";
		//$factory = new $classname();
		require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'models'.DS.'definition.php');
		$classname="ArchiDashModelDefinition";
		$model = new $classname;
		$vet = $model->selectAllRecords();
		$definitions = array();
		foreach($vet as $def){
			$definitions[]=$def['name'];
		}
		
		/*READ DIRECTORY
		$definitionDir = opendir(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'engine'.DS.'definitions');
		$definitions = array();
		while($entryName = readdir($definitionDir)) {
			if(strcasecmp(substr($entryName,-4),".xml")==0)$definitions[] = $entryName;
		}
		closedir($definitionDir);
		*/
				
		$this->arcView->add($definitions);
	}
	
	function edit(){
		//to call the edit user must specify a selection
		$cid = JRequest::getVar('cid');
		$id = $cid[0];
		//get the datasource
		$record = $this->arcModel->selectRecord($id);
		JRequest::setVar('hidemainmenu','1');//hide the main menu
		
		$this->arcView->edit($record);
	}
	
	
	function remove(){
		global $mainframe;
		//get selected elements
		$cid = JRequest::getVar('cid');
		$opt=array();
		foreach($cid as $id){
			$opt['dashboardid']=$id;
			$classname = "ArchiDashDestructorDashboard";
			$destructor = new $classname($opt);
			$destructor->destroy();

		}
		$mainframe->enqueueMessage(JText::_('OPERATIONOK'));
	
		
		//call the view
		$this->arcView->remove();
	}
	
	
	
	function save(){
		global $mainframe;
		$opts = array();
		$opts['definition'] =  JRequest::getVar('definition');
		$classname = "ArchiDashFactoryDashboard";
		$factory = new $classname($opts);
		$dashboardid = $factory->create();
		if($factory->isCompleted()){
			$mainframe->enqueueMessage(JText::_('OPERATIONOK'));
		}
		else{
			
			$mainframe->enqueueMessage(JText::_('OPERATIONKO'),'error');
			$opts = array();
			$opts['dashboardid']=$dashboardid;
			$classname = "ArchiDashDestructorDashboard";
			$destructor = new $classname($opts);
			$destructor->destroy();
		}
		$this->display();
	}
	
	function cancel(){
			
		$this->arcView->display();	
					
	}
	
	
	
	
	
	
	function applyedit(){
		global $mainframe;
		$vet = array();
		JRequest::setVar('hidemainmenu','1');//hide the main menu
		$cid = JRequest::getVar('cid');
		//collect the information
		$vet['id'] = $cid[0];
		$vet['name'] = JRequest::getVar('name','');
		$vet['description'] = JRequest::getVar('description','');
		
		
		

						
		
		
		$allright = $this->arcModel->updateRecord($vet);
		if($allright){
				$mainframe->enqueueMessage( JText::_( 'OPERATIONOK' ));
				//after updating the method re-select the datasource information
				$record = $this->arcModel->selectRecord($vet['id']);
				$this->arcView->edit($record);	
		}
		else{
				$mainframe->enqueueMessage( JText::_( 'OPERATIONKO' ));//details provided by the model
				//return to check what's wrong
				//after update failure the method returns the user information
				$this->arcView->edit($vet);
		}
		
	}
		
		
	function saveedit(){
		global $mainframe;
		$vet = array();
		
		$cid = JRequest::getVar('cid');
		//collect the datasource information
		$vet['id'] = $cid[0];
		$vet['name'] = JRequest::getVar('name','');
		$vet['description'] = JRequest::getVar('description','');
		
		
								
		
		//save
		$allright = $this->arcModel->updateRecord($vet);
		if($allright){
				$mainframe->enqueueMessage( JText::_( 'OPERATIONOK' ));
				$this->arcView->display();	
		}
		else{
				$mainframe->enqueueMessage( JText::_( 'OPERATIONKO' ));//the model provides the details
				JRequest::setVar('hidemainmenu','1');//hide the main menu
				//return to check what's wrong
				//after update failure the method returns the user information
				$this->arcView->edit($vet);	
		}
		
		
		
	}	
	
	
	function details(){
		$cid = JRequest::getVar('cid');		
		$details = $this->arcModel->getDetails($cid[0]);
				
		$this->arcView->details($details);
		
	}
	
	
	
}
?>

