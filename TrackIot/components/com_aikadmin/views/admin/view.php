<?php
/**
 * @version		$Id: view.php 1.0 19-11-2009 Danijar
 * @package		Frontend Admin
 * @copyright	Copyright (C) 2009 CMSSpace. http://www.cmsspace.com
 * @license		GNU/GPL. http://www.gnu.org/licenses/gpl.html
 */
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport('joomla.application.component.view');
class AikViewAdmin extends JView
{
	function get_php_setting($val)
	{
		$r =  (ini_get($val) == '1' ? 1 : 0);
		return $r ? JText::_( 'ON' ) : JText::_( 'OFF' ) ;
	}

	function get_server_software()
	{
		if (isset($_SERVER['SERVER_SOFTWARE'])) {
			return $_SERVER['SERVER_SOFTWARE'];
		} else if (($sf = getenv('SERVER_SOFTWARE'))) {
			return $sf;
		} else {
			return JText::_( 'n/a' );
		}
	}

	function system_info( )
	{
		global $mainframe;
		JHTML::_('behavior.switcher');

		$db =& JFactory::getDBO();
		JSubMenuHelper::addEntry(JText::_('System Info'),'#" id="site" class="active"');
		JSubMenuHelper::addEntry(JText::_('PHP Settings'),'#" id="phpsettings"');
		JSubMenuHelper::addEntry(JText::_('Configuration File'),'#" id="config"');
		JSubMenuHelper::addEntry(JText::_('Directory Permissions'),'#"  id="directory"');
		JSubMenuHelper::addEntry(JText::_('PHP Information'),'#"  id="phpinfo"');

		?>
		<form action="index.php" method="post" name="adminForm">

		<div id="config-document">
			<div id="page-site">
				<table class="noshow">
				<tr>
					<td>
						<?php require_once(JPATH_COMPONENT.DS.'views'.DS.'admin'.DS.'tmpl'.DS.'system.php'); ?>
					</td>
				</tr>
				</table>
			</div>

			<div id="page-phpsettings">
				<table class="noshow">
				<tr>
					<td>
						<?php require_once(JPATH_COMPONENT.DS.'views'.DS.'admin'.DS.'tmpl'.DS.'phpsettings.php'); ?>
					</td>
				</tr>
				</table>
			</div>

			<div id="page-config">
				<table class="noshow">
				<tr>
					<td>
						<?php require_once(JPATH_COMPONENT.DS.'views'.DS.'admin'.DS.'tmpl'.DS.'config.php'); ?>
					</td>
				</tr>
				</table>
			</div>

			<div id="page-directory">
				<table class="noshow">
				<tr>
					<td>
						<?php require_once(JPATH_COMPONENT.DS.'views'.DS.'admin'.DS.'tmpl'.DS.'directory.php'); ?>
					</td>
				</tr>
				</table>
			</div>

			<div id="page-phpinfo">
				<table class="noshow">
				<tr>
					<td>
						<?php require_once(JPATH_COMPONENT.DS.'views'.DS.'admin'.DS.'tmpl'.DS.'phpinfo.php'); ?>
					</td>
				</tr>
				</table>
			</div>
		</div>

		<div class="clr"></div>
		<?php
	}
	function help()
	{
		global $mainframe;
		jimport( 'joomla.filesystem.folder' );
		jimport( 'joomla.language.help' );
		$helpurl	= $mainframe->getCfg('helpurl');
		if ( $helpurl == 'http://help.mamboserver.com' ) {
			$helpurl = 'http://help.joomla.org';
		}
		$fullhelpurl = $helpurl . '/index2.php?option=com_content&amp;task=findkey&amp;pop=1&amp;keyref=';

		$helpsearch = JRequest::getString('helpsearch');
		$page		= JRequest::getCmd('page', 'joomla.whatsnew15.html');
		$toc		= AikViewAdmin::getHelpToc( $helpsearch );
		$lang		=& JFactory::getLanguage();
		$langTag = $lang->getTag();
		if( !JFolder::exists( JPATH_BASE.DS.'help'.DS.$langTag ) ) {
			$langTag = 'en-GB';		
		}

		if (!preg_match( '#\.html$#i', $page )) {
			$page .= '.xml';
		}
		?>
		<form action="index.php?option=com_admin&amp;task=help" method="post" name="adminForm">

		<table class="adminform" border="1">
		<tr>
			<td colspan="2">
				<table width="100%">
					<tr>
						<td>
							<strong><?php echo JText::_( 'Search' ); ?>:</strong>
							<input class="text_area" type="hidden" name="option" value="com_admin" />
							<input type="text" name="helpsearch" value="<?php echo $helpsearch;?>" class="inputbox" />
							<input type="submit" value="<?php echo JText::_( 'Go' ); ?>" class="button" />
							<input type="button" value="<?php echo JText::_( 'Clear Results' ); ?>" class="button" onclick="f=document.adminForm;f.helpsearch.value='';f.submit()" />
						</td>
						<td class="helpMenu">
							<?php
							if ($helpurl) {
							?>
							<?php echo JHTML::_('link', JHelp::createUrl( 'joomla.glossary' ), JText::_( 'Glossary' ), array('target' => 'helpFrame')) ?>
							|
							<?php echo JHTML::_('link', JHelp::createUrl( 'joomla.credits' ), JText::_( 'Credits' ), array('target' => 'helpFrame')) ?>
							|
							<?php echo JHTML::_('link', JHelp::createUrl( 'joomla.support' ), JText::_( 'Support' ), array('target' => 'helpFrame')) ?>
							<?php
							} else {
							?>
							<?php echo JHTML::_('link', 'http://docs.joomla.org/index.php?title=Glossary&printable=true', JText::_( 'Glossary' ), array('target' => 'helpFrame')) ?> |
							<?php echo JHTML::_('link', 'http://docs.joomla.org/index.php?title=Joomla.credits.15&printable=true', JText::_( 'Credits' ), array('target' => 'helpFrame')) ?> |
							<?php echo JHTML::_('link', 'http://docs.joomla.org/index.php?title=Joomla.support.15&printable=true', JText::_( 'Support' ), array('target' => 'helpFrame')) ?>
							<?php } ?>
							|
							<?php echo JHTML::_('link', 'http://www.gnu.org/licenses/gpl-2.0.html', JText::_( 'License' ), array('target' => 'helpFrame')) ?>
							|
							<?php echo JHTML::_('link', 'http://docs.joomla.org', 'docs.joomla.org', array('target' => 'helpFrame')) ?>
							|
							<?php echo JHTML::_('link', 'index.php?option=com_admin&amp;task=changelog&amp;tmpl=component', JText::_( 'Changelog' ), array('target' => 'helpFrame')) ?>
							|
							<?php echo JHTML::_('link', 'http://docs.joomla.org/index.php?title=Joomla_1.5_version_history&printable=true', JText::_( 'Latest Version Check' ), array('target' => 'helpFrame')) ?>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		</table>

		<div id="treecellhelp">
			<fieldset title="<?php echo JText::_( 'Alphabetical Index' ); ?>">
				<legend>
					<?php echo JText::_( 'Alphabetical Index' ); ?>
				</legend>

				<div class="helpIndex">
					<ul class="subext">
						<?php
						foreach ($toc as $k=>$v) {
							if ($helpurl) {
								echo '<li>';
								echo JHTML::_('link', JHelp::createUrl( $k ), $v, array('target' => 'helpFrame'));
								echo '</li>';
							} else {
								echo '<li>';
								echo JHTML::_('link', JURI::base() .'help/'.$langTag.'/'.$k, $v, array('target' => 'helpFrame'));
								echo '</li>';
							}
						}
						?>
					</ul>
				</div>
			</fieldset>
		</div>

		<div id="datacellhelp">
			<fieldset title="<?php echo JText::_( 'View' ); ?>">
				<legend>
					<?php echo JText::_( 'View' ); ?>
				</legend>
				<?php
				if ($helpurl && $page != 'joomla.whatsnew15.html') {
					?>
					<iframe name="helpFrame" src="<?php echo $fullhelpurl .preg_replace( '#\.xml$|\.html$#', '', $page );?>" class="helpFrame" frameborder="0"></iframe>
					<?php
				} else {
					?>
					<iframe name="helpFrame" src="<?php echo JURI::base() .'help/' .$langTag. '/' . $page;?>" class="helpFrame" frameborder="0"></iframe>
					<?php
				}
				?>
			</fieldset>
		</div>

		<input type="hidden" name="task" value="help" />
		</form>
		<?php
	}

	function writableCell( $folder, $relative=1, $text='', $visible=1 )
	{
		$writeable		= '<b><font color="green">'. JText::_( 'Writable' ) .'</font></b>';
		$unwriteable	= '<b><font color="red">'. JText::_( 'Unwritable' ) .'</font></b>';
	
		echo '<tr>';
		echo '<td class="item">';
		echo $text;
		if ( $visible ) {
			echo $folder . '/';
		}
		echo '</td>';
		echo '<td >';
		if ( $relative ) {
			echo is_writable( "../$folder" )	? $writeable : $unwriteable;
		} else {
			echo is_writable( "$folder" )		? $writeable : $unwriteable;
		}
		echo '</td>';
		echo '</tr>';
	}
	function getHelpTOC( $helpsearch )
	{
		global $mainframe;
	
		$lang =& JFactory::getLanguage();
		jimport( 'joomla.filesystem.folder' );
	
		$helpurl		= $mainframe->getCfg('helpurl');
	
		$langTag = $lang->getTag();
		if( !JFolder::exists( JPATH_BASE.DS.'help'.DS.$langTag ) ) {
			$langTag = 'en-GB';		
		}
		$files = JFolder::files( JPATH_BASE.DS.'administrator'.DS.'help'.DS.$langTag, '\.xml$|\.html$' );
	
		$toc = array();
		foreach ($files as $file) {
			$buffer = file_get_contents( JPATH_BASE.DS.'administrator'.DS.'help'.DS.$langTag.DS.$file );
			if (preg_match( '#<title>(.*?)</title>#', $buffer, $m )) {
				$title = trim( $m[1] );
				if ($title) {
					if ($helpurl) {
						$file = preg_replace( '#\.xml$|\.html$#', '', $file );
					}
					if ($helpsearch) {
						if (JString::strpos( strip_tags( $buffer ), $helpsearch ) !== false) {
							$toc[$file] = $title;
						}
					} else {
						$toc[$file] = $title;
					}
				}
			}
		}
		asort( $toc );
		return $toc;
	}
}
