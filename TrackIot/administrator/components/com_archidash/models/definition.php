<?php


defined('_JEXEC') or die('Restricted Access');
jimport('joomla.application.component.model');
require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'models'.DS.'model.php');


/**
 * The following class manages all the CRUD operations on #__archidash_dashboards table
 * @version 1.0
 * @author  TobyTools.com (email:info@tobytools.com)(website: www.tobytools.com)
 * @copyright Copyright TobyTools.com 
 * @license GNU/GPL
 */


class ArchiDashModelDefinition extends ArchiDashModel{
	
	
	protected $path;
	
		
	/**
	 * class constructor
	 * 
	 * */
	function ArchiDashModelDefinition(){
		parent::__construct();
		$this->table="#__archidash_definitions";		
		$this->path = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'engine'.DS;
	}
	
	/**
	 * override
	 */
	function selectAllRecordsByDashboardID($dashboardID=false){
		return false;
	}
	
	function deleteRecordsByField($field,$value){
		return false;
	}
	
	function deleteRecord($id=false){
		global $mainframe;
		if($id!=false){
			//get the file name
			$record = $this->selectRecord($id);
				if(!$record){
					$mainframe->enqueueMessage(JText::_( 'NODEFINITIONWITHID' ),'error');
					$mainframe->enqueueMessage(get_class($this)."::deleteRecord",'error');
					return false;
				}
							
			$db =& JFactory::getDBO();
			$query = "DELETE FROM ".$db->nameQuote($this->table)." WHERE ".$db->nameQuote('id')." = ".$db->Quote($id);
			$db->setQuery($query);
			$result = $db->query();
			if(!$result){
				//something wrong
				if($db->getErrorNum()==0){
					//it is not an error, the problem is there is no record with the specified id
					$this->errMsg = false;
				}
				else{
					//MYSQL error
					$this->errMsg = $db->getErrorNum()." : ".$db->getErrorMsg();
					$mainframe->enqueueMessage(JText::_( 'QUERYKO' ),'error');
					$mainframe->enqueueMessage($this->errMsg,'error');
					$mainframe->enqueueMessage(get_class($this)."::deleteRecord",'error');
					
				}
			}
			else{
				
				
				$delete = unlink($this->getPath().$record['name']);
				if(!$delete){
					$mainframe->enqueueMessage(JText::_( 'NODELETEFILE' ).":".$this->getPath().$record['name'],'error');
					$mainframe->enqueueMessage(get_class($this)."::deleteRecord",'error');
					return false;
				}
				
				//clean messages
				$this->errMsg=false;
				return $result;
			}
		}
		else{
			$this->errMsg=JText::_( 'ARGNOVALID' );
			$mainframe->enqueueMessage(JText::_( 'ARGNOVALID' ),'error');
			$mainframe->enqueueMessage(get_class($this)."::deleteRecord",'error');
			return false;
		}	
	}
	
	
	/**
	 * end override
	 */
	
	//insert the new record. no file management
	function insertRecord($vet=false){
		global $mainframe;
		if($vet!=false){						
			
			$db =& JFactory::getDBO();
			
			//to avoid problem with character set
			$db->setQuery($this->characterset);
			$db->query();
			
			$query = "INSERT INTO ".$db->nameQuote($this->table)." (name) VALUES ('".$vet['name']."')";			
			$db->setQuery($query);
			$result = $db->query(); 
			if(!$result){
				//something wrong
				$this->errMsg = $db->getErrorNum()." : ".$db->getErrorMsg();
				$mainframe->enqueueMessage(JText::_( 'QUERYKO' ),'error');
				$mainframe->enqueueMessage($this->errMsg,'error');
				$mainframe->enqueueMessage(get_class($this)."::insertRecord",'error');
				return false;
			}
			else{
				//clean
				$this->errMsg = false;
				//provide the element id
				$db->setQuery("SELECT LAST_INSERT_ID()");				
				return $db->loadResult();
			}
		}
		else{
			$this->errMsg=JText::_( 'ARGNOVALID' );
			$mainframe->enqueueMessage(JText::_( 'ARGNOVALID' ),'error');
			$mainframe->enqueueMessage(get_class($this)."::insertRecord",'error');
			return false;
		}		
	}
	
	function updateRecord($vet=false){
		return false;
	}
	
	function getPath(){
		return $this->path.JText::_('THEFOLDER').DS;
	}
	
}
