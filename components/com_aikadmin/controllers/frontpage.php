<?php
/**
 * @version		$Id: cache.php 1.0 19-11-2009 Danijar
 * @package		Frontend Admin
 * @copyright	Copyright (C) 2009 CMSSpace. http://www.cmsspace.com
 * @license		GNU/GPL. http://www.gnu.org/licenses/gpl.html
 */
defined('_JEXEC') or die( 'Restricted access' );
jimport( 'joomla.application.component.controller' );
class AikControllerFrontpage extends JController
{
	function __construct( $default = array() )
	{
		parent::__construct( array('default_task' => 'viewFrontPage') );
		$this->registerTask( 'publish', 'changeFrontPage' );
		$this->registerTask( 'unpublish', 'changeFrontPage' );
		$this->registerTask( 'archive', 'changeFrontPage' );
		$this->registerTask( 'remove', 'removeFrontPage' );
		$this->registerTask( 'orderup', 'orderFrontPage' );
		$this->registerTask( 'orderdown', 'orderFrontPage' );
		$this->registerTask( 'saveorder', 'saveOrder' );
		$this->registerTask( 'accesspublic', 'accessMenu' );
		$this->registerTask( 'accessregistered', 'accessMenu' );
		$this->registerTask( 'accessspecial', 'accessMenu' );
	}
	function viewFrontPage()
	{
		global $mainframe;
	
		$option		= JRequest::getCmd( 'option');
		$db					=& JFactory::getDBO();
		$filter_order		= $mainframe->getUserStateFromRequest( 'com_frontpage.filter_order',		'filter_order',		'fpordering',	'cmd' );
		$filter_order_Dir	= $mainframe->getUserStateFromRequest( 'com_frontpage.filter_order_Dir',	'filter_order_Dir',	'',				'word' );
		$filter_state		= $mainframe->getUserStateFromRequest( 'com_frontpage.filter_state',		'filter_state',		'',				'word' );
		$catid				= $mainframe->getUserStateFromRequest( 'com_frontpage.catid',			'catid',			0,				'int' );
		$filter_authorid	= $mainframe->getUserStateFromRequest( 'com_frontpage.filter_authorid',	'filter_authorid',	0,				'int' );
		$filter_sectionid	= $mainframe->getUserStateFromRequest( 'com_frontpage.filter_sectionid',	'filter_sectionid',	-1,				'int' );
		$search				= $mainframe->getUserStateFromRequest( 'com_frontpage.search',			'search',			'',				'string' );
		$search				= JString::strtolower( $search );
	
		$limit		= $mainframe->getUserStateFromRequest( 'global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int' );
		$limitstart	= $mainframe->getUserStateFromRequest( 'com_frontpage.limitstart', 'limitstart', 0, 'int' );
	
		JToolBarHelper::title( JText::_( 'Frontpage Manager' ), 'frontpage.png' );
		JToolBarHelper::archiveList();
		JToolBarHelper::publishList();
		JToolBarHelper::unpublishList();
		JToolBarHelper::custom('remove','delete.png','delete_f2.png','Remove', true);
		JToolBarHelper::help( 'screen.frontpage' );
		JToolBarHelper::cpanel();
	
		$where = array(
			"c.state >= 0"
		);
		if ( $filter_sectionid >= 0 ) {
			$where[] = 'c.sectionid = '.(int) $filter_sectionid;
		}
		if ( $catid > 0 ) {
			$where[] = 'c.catid = '.(int) $catid;
		}
		if ( $filter_authorid > 0 ) {
			$where[] = 'c.created_by = '. (int) $filter_authorid;
		}
		if ( $filter_state ) {
			if ( $filter_state == 'P' ) {
				$where[] = 'c.state = 1';
			} else if ($filter_state == 'U' ) {
				$where[] = 'c.state = 0';
			}
		}
	
		if ($search) {
			$where[] = 'LOWER( c.title ) LIKE '.$db->Quote( '%'.$db->getEscaped( $search, true ).'%', false );
		}
	
		$where 		= ( count( $where ) ? ' WHERE ' . implode( ' AND ', $where ) : '' );
		$orderby 	= ' ORDER BY '. $filter_order .' '. $filter_order_Dir .', fpordering';
	
		$query = 'SELECT count(*)'
		. ' FROM #__content AS c'
		. ' LEFT JOIN #__categories AS cc ON cc.id = c.catid'
		. ' LEFT JOIN #__sections AS s ON s.id = cc.section AND s.scope="content"'
		. ' INNER JOIN #__content_frontpage AS f ON f.content_id = c.id'
		. $where
		;
		$db->setQuery( $query );
		$total = $db->loadResult();
	
		jimport('joomla.html.pagination');
		$pageNav = new JPagination( $total, $limitstart, $limit );
	
		$query = 'SELECT c.*, g.name AS groupname, cc.title as name, s.title AS sect_name, u.name AS editor, f.ordering AS fpordering, v.name AS author'
		. ' FROM #__content AS c'
		. ' LEFT JOIN #__categories AS cc ON cc.id = c.catid'
		. ' LEFT JOIN #__sections AS s ON s.id = cc.section AND s.scope="content"'
		. ' INNER JOIN #__content_frontpage AS f ON f.content_id = c.id'
		. ' INNER JOIN #__groups AS g ON g.id = c.access'
		. ' LEFT JOIN #__users AS u ON u.id = c.checked_out'
		. ' LEFT JOIN #__users AS v ON v.id = c.created_by'
		. $where
		. $orderby
		;
		$db->setQuery( $query, $pageNav->limitstart,$pageNav->limit );
		$rows = $db->loadObjectList();
		if ($db->getErrorNum()) {
			echo $db->stderr();
			return false;
		}
	
		$query = 'SELECT cc.id AS value, cc.title AS text, section'
		. ' FROM #__categories AS cc'
		. ' INNER JOIN #__sections AS s ON s.id = cc.section '
		. ' ORDER BY s.ordering, cc.ordering'
		;
		$db->setQuery( $query );
		$categories[] 	= JHTML::_('select.option',  '-1', '- '. JText::_( 'Select Category' ) .' -' );
		$categories 	= array_merge( $categories, $db->loadObjectList() );
		$lists['catid'] = JHTML::_('select.genericlist',   $categories, 'catid', 'class="inputbox" size="1" onchange="document.adminForm.submit( );"', 'value', 'text', $catid );
	
		$javascript			= 'onchange="document.adminForm.submit();"';
		$lists['sectionid']	= JHTML::_('list.section',  'filter_sectionid', $filter_sectionid, $javascript );
	
		$query = 'SELECT c.created_by, u.name'
		. ' FROM #__content AS c'
		. ' INNER JOIN #__sections AS s ON s.id = c.sectionid'
		. ' LEFT JOIN #__users AS u ON u.id = c.created_by'
		. ' WHERE c.state <> -1'
		. ' AND c.state <> -2'
		. ' GROUP BY u.name'
		. ' ORDER BY u.name'
		;
		$db->setQuery( $query );
		$authors[] 			= JHTML::_('select.option',  '0', '- '. JText::_( 'Select Author' ) .' -', 'created_by', 'name' );
		$authors 			= array_merge( $authors, $db->loadObjectList() );
		$lists['authorid']	= JHTML::_('select.genericlist',   $authors, 'filter_authorid', 'class="inputbox" size="1" onchange="document.adminForm.submit( );"', 'created_by', 'name', $filter_authorid );
	
		$lists['state']	= JHTML::_('grid.state',  $filter_state );
		$lists['order_Dir']	= $filter_order_Dir;
		$lists['order']		= $filter_order;
		$lists['search']= $search;
	
		$view =& $this->getView( 'frontpage' );
		$view->showList( $rows, $pageNav, $option, $lists );
	}
	function changeFrontPage()
	{
		global $mainframe;
		JRequest::checkToken() or jexit( 'Invalid Token' );
	
		switch ( JRequest::getCmd( 'task' ) )
		{
			case 'publish':$state=1;break;
			case 'unpublish':$state=0;break;
			case 'archive':$state=-1;break;
		}
		$option		= JRequest::getCmd( 'option');
		$cid = JRequest::getVar( 'cid', array(0), 'post', 'array' );
		JArrayHelper::toInteger($cid, array(0));
		
		$db 	=& JFactory::getDBO();
		$user 	=& JFactory::getUser();
	
		if (count( $cid ) < 1) {
			$action = $state == 1 ? 'publish' : ($state == -1 ? 'archive' : 'unpublish');
			JError::raiseError(500, JText::_( 'Select an item to' .$action, true ) );
		}
	
		$cids = implode( ',', $cid );
	
		$query = 'UPDATE #__content'
		. ' SET state = '.(int) $state
		. ' WHERE id IN ( '. $cids .' )'
		. ' AND ( checked_out = 0 OR ( checked_out = ' .(int) $user->get('id'). ' ) )'
		;
		$db->setQuery( $query );
		if (!$db->query()) {
			JError::raiseError(500, $db->getErrorMsg() );
		}
	
		if (count( $cid ) == 1) {
			$row =& JTable::getInstance('content');
			$row->checkin( $cid[0] );
		}
	
		$cache = & JFactory::getCache('com_content');
		$cache->clean();
	
		$mainframe->redirect( 'index.php?option='.$option );
	}
	
	function removeFrontPage( )
	{
		global $mainframe;
		$option		= JRequest::getCmd( 'option');
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$cid = JRequest::getVar( 'cid', array(0), 'post', 'array' );
		JArrayHelper::toInteger($cid, array(0));
		
		$db =& JFactory::getDBO();
		if (!is_array( $cid ) || count( $cid ) < 1) {
			JError::raiseError(500, JText::_( 'Select an item to delete', true ) );
		}
		$fp =& JTable::getInstance('frontpage', 'Table');
		foreach ($cid as $id) {
			if (!$fp->delete( $id )) {
				JError::raiseError(500, $fp->getError() );
			}
			$obj =& JTable::getInstance('content');
			$obj->load( $id );
			$obj->mask = 0;
			if (!$obj->store()) {
				JError::raiseError(500, $fp->getError() );
			}
		}
		$fp->reorder();
	
		$cache = & JFactory::getCache('com_content');
		$cache->clean();
	
		$mainframe->redirect( 'index.php?option='.$option );
	}
	function orderFrontPage()
	{
		global $mainframe;
		JRequest::checkToken() or jexit( 'Invalid Token' );
	
		if(JRequest::getCmd( 'task' ) == 'orderup')
			$inc = -1;
		else 
			$inc = 1;
		$option		= JRequest::getCmd( 'option');
		$cid = JRequest::getVar( 'cid', array(0), 'post', 'array' );
		JArrayHelper::toInteger($cid, array(0));
		$uid = $cid[0];
		
		$db =& JFactory::getDBO();
	
		$fp =& JTable::getInstance('frontpage','Table');
		$fp->load( $uid );
		$fp->move( $inc );
	
		$cache = & JFactory::getCache('com_content');
		$cache->clean();
	
		$mainframe->redirect( 'index.php?option='.$option );
	}
	function accessMenu()
	{
		global $mainframe;
		JRequest::checkToken() or jexit( 'Invalid Token' );

		$cid = JRequest::getVar( 'cid', array(0), 'post', 'array' );
		JArrayHelper::toInteger($cid, array(0));
		$uid = $cid[0];
		
		$db = & JFactory::getDBO();
		$row =& JTable::getInstance('content');
		$row->load( $uid );
		
		switch ( JRequest::getCmd( 'task' ) )
		{
			case 'accesspublic':$row->access = 0;break;
			case 'accessregistered':$row->access = 1;break;
			case 'accessspecial':$row->access = 2;break;
		}
	
		if ( !$row->check() ) {
			return $row->getError();
		}
		if ( !$row->store() ) {
			return $row->getError();
		}
	
		$cache = & JFactory::getCache('com_content');
		$cache->clean();
	
		$mainframe->redirect( 'index.php?option=com_aikadmin&c=frontpage' );
	}
	function saveOrder()
	{
		global $mainframe;
		JRequest::checkToken() or jexit( 'Invalid Token' );
	
		$cid = JRequest::getVar( 'cid', array(0), 'post', 'array' );
		JArrayHelper::toInteger($cid, array(0));

		$db 	=& JFactory::getDBO();
		$total	= count( $cid );
		$order 	= JRequest::getVar( 'order', array(0), 'post', 'array' );
	
		for( $i=0; $i < $total; $i++ )
		{
			$query = 'UPDATE #__content_frontpage'
			. ' SET ordering = ' . (int) $order[$i]
			. ' WHERE content_id = ' . (int) $cid[$i];
			$db->setQuery( $query );
			if (!$db->query()) {
				JError::raiseError(500, $db->getErrorMsg() );
			}
		}
	
		$cache = & JFactory::getCache('com_content');
		$cache->clean();
	
		$msg 	= JText::_( 'New ordering saved' );
		$mainframe->redirect( 'index.php?option=com_aikadmin&c=frontpage', $msg );
	}
}