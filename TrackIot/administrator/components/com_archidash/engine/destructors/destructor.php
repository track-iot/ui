<?php

defined('_JEXEC') or die('Restricted Access');




/**
 * Abstract class for the Destructor classes of ArchiDash
 * @version 1.0
 * @author  TobyTools.com (email:info@tobytools.com)(website: www.tobytools.com)
 * @copyright Copyright TobyTools.com 
 * @license GNU/GPL
 */


abstract class ArchiDashDestructor {
	
	/**
	 * This variable it's true only if the create operation is completed successfully.
	 * The create operation is a unit of work performed against a Joomla installation, 
	 * and treated in a coherent and reliable way independent of other ones.
	 * 
	 * 
	 * @var boolean
	 */
	protected $completed;
	
	
	/**
	 * associative array with the options for the class (e.g. $options['dashboard']="STANDARD")
	 */
	protected $options;
	
	/**
	 * 
	 * @var string, the error message (if any) associated with the last operation performed
	 */
	protected $errMsg;
	
	
	/**
	 * 
	 * @var string, the messages associated with the last operation performed
	 */
	protected $msg;
	
	
	/**
	 * 
	 * @var string, the database table associated with the model class
	 */
	protected $table;
	
	/**
	 * 
	 * @var string, specifying the character set
	 */
	protected $characterset;
	
	
	
	
	protected function ArchiDashDestructor(){
		//is set to true, and if there is an error it is set to false
		$this->completed=true;
		$this->errMsg=false;
		$this->msg="";
		$this->table="";
		$this->characterset="SET NAMES 'utf8'";
	}
	
	
	abstract function destroy();
	
	
	
	
	/**
	 * check there are the preconditions to destroy the resource. For example a menu can be destroyed only if there are
	 * no more menu items related to it
	 * 
	 * @return boolean
	 */
	abstract function preCondition();
	
	
}

