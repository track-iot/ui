<?php

include ("jpgraph.php");
include ("jpgraph_pie.php");
include ("jpgraph_pie3d.php");
include ("jpgraph_bar.php");

define('MYSQL_HOST', 'localhost');
define('MYSQL_USER', 'root');
define('MYSQL_PASS', '');
define('MYSQL_DATABASE', 'tracklot');

$tableauAnnees = array();
$tableauNombreVentes = array();

// *****************************************************
// Extraction des donn�es dans la base de donn�es 
// **************************************************

$sql = <<<EOF
	SELECT  
		type AS TYPE,
		COUNT(id) AS NBR_READERS  
	FROM `Readers`
	GROUP BY type
EOF;

$mysqlCnx = @mysql_connect(MYSQL_HOST, MYSQL_USER, MYSQL_PASS) or die('Pb de connxion mysql');

@mysql_select_db(MYSQL_DATABASE) or die('Pb de s�lection de la base');

$mysqlQuery = @mysql_query($sql, $mysqlCnx) or die('Pb de requ�te');

while ($row = mysql_fetch_array($mysqlQuery,  MYSQL_ASSOC)) {
	// Ajouter ann�e devant, c'est pour la l�gende
	$tableauAnnees[] = "Type: " . $row['TYPE'];
	$tableauNombreVentes[] = $row['NBR_READERS'];
}

// *******************
// Cr�ation du graphique
// *******************


// Construction du conteneur
// Sp�cification largeur et hauteur
$graph = new Graph(400,250);

// R�pr�sentation lin�aire
$graph->SetScale("textlin");

// Ajouter une ombre au conteneur
$graph->SetShadow();

// Fixer les marges
$graph->img->SetMargin(40,30,25,40);

// Cr�ation du graphique histogramme
$bplot = new BarPlot($tableauNombreVentes);

// Sp�cification des couleurs des barres
$bplot->SetFillColor(array('red', 'green', 'blue'));
// Une ombre pour chaque barre
$bplot->SetShadow();

// Afficher les valeurs pour chaque barre
$bplot->value->Show();
// Fixer l'aspect de la police
$bplot->value->SetFont(FF_ARIAL,FS_NORMAL,9);
// Modifier le rendu de chaque valeur
$bplot->value->SetFormat('%d ventes');

// Ajouter les barres au conteneur
$graph->Add($bplot);

// Le titre
$graph->title->Set("Chart 'HISTOGRAM' : Readers by type");
$graph->title->SetFont(FF_FONT1,FS_BOLD);

// Titre pour l'axe horizontal(axe x) et vertical (axe y)
$graph->xaxis->title->Set("Type");
$graph->yaxis->title->Set("Number of the Readers");

$graph->yaxis->title->SetFont(FF_FONT1,FS_BOLD);
$graph->xaxis->title->SetFont(FF_FONT1,FS_BOLD);

// L�gende pour l'axe horizontal
$graph->xaxis->SetTickLabels($tableauAnnees);
$bplot->SetFillGradient("navy","#EEEEEE",GRAD_LEFT_REFLECTION);
$bplot->SetColor("white");
// Afficher le graphique
$graph->Stroke();




?>