<?php



// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view' );

/**
 * @version 1.0
 * @author  TobyTools.com (email:info@tobytools.com)(website: www.tobytools.com)
 * @copyright Copyright TobyTools.com 
 * @license GNU/GPL
 */

class ArchiDashViewWelcome extends JView{
	
	function display($tpl = null){
		require_once( JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'views'.DS.'welcome'.DS.'tmpl'.DS.'welcome.php' );
		$classname = "ArchiDashLayoutWelcome";
		$layout = new $classname();			
		$layout->display();
	}
}

?>
