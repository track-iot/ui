<?php
/**
 * @version 1.0
 * @author  TobyTools.com (email:info@tobytools.com)(website: www.tobytools.com)
 * @copyright Copyright TobyTools.com 
 * @license GNU/GPL
 */

// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

require_once( JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'controller.php' );

// Require specific controller if requested
if($controller = JRequest::getWord('controller')) {
	
	$path = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'controllers'.DS.strtolower($controller).'.php';
	if (file_exists($path)) {
		require_once $path;
	} else {
		$controller = '';
	}
}


// Create the controller
$classname	= 'ArchiDashController'.$controller;
$controller	= new $classname( );
if(!JRequest::getVar( 'task' )){
	JRequest::setVar('task','display');
}

$controller->execute( JRequest::getVar('task') );

// Redirect if set by the controller
$controller->redirect();


?>
