<?php


defined('_JEXEC') or die('Restricted Access');
jimport('joomla.application.component.model');
require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'engine'.DS.'exporters'.DS.'exporter.php');


/**
 * The following class export the section 
 * @version 1.0
 * @author  TobyTools.com (email:info@tobytools.com)(website: www.tobytools.com)
 * @copyright Copyright TobyTools.com 
 * @license GNU/GPL
 */


class ArchiDashExporterDatasource extends ArchiDashExporter{
	
	/*
	 *class constructor 
	 */
	function ArchiDashExporterDatasource($opts){
		parent::__construct($opts);
		//check everything is allright
		if($this->completed){
			//options['datasources']
			if($this->options['datasources']){
				$this->options['table']="";
				$this->options['tag']="";			
			}
			else{
				$this->completed=false;
				$this->options=false;
				$this->errMsg=false;
				global $mainframe;
				$mainframe->enqueueMessage(JText::_('NOSECTIONID')." ".get_class($this),'error');
			}
			
		}
		else{
			global $mainframe;
			$mainframe->enqueueMessage(JText::_('NOOBJECT')." ".get_class($this),'error');
		}
		
	}
	
	
	
	public function export($params=false){
		if($this->completed){
			global $mainframe;						
				$xml = "";
				$sqlIDs = $this->options['datasources']['sql'];
				$xml .= $this->exportSQLDatasources($sqlIDs);
				$articleIDs = $this->options['datasources']['article'];
				$xml .= $this->exportArticleDatasources($articleIDs);
				$htmlIDs = $this->options['datasources']['html'];
				$xml .= $this->exportHTMLDatasources($htmlIDs);
				$snippetIDs = $this->options['datasources']['snippet'];
				$xml .= $this->exportSnippetDatasources($snippetIDs);
				
				return $xml;
		}
		else{
			return "";
		}				
	}
	
	protected function exportSQLDatasources($datasourcesIDs){
		
		require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archimede'.DS.'models'.DS.'settings.php');
		$classname="ArchimedeModelSettings";
		$settings = new $classname();
		$tmp = $settings->getSetting('encryption_key');
		$encryption_key = !$tmp?"archimede":$tmp['value'];//if for some reason the setting is impossible to read, it sets a default useless value
		
		
		//table we are working on
		$targetTable="#__arc_datasource_sql";
		
		$db =& JFactory::getDBO();
			
		//to avoid problem with character set
		$db->setQuery($this->characterset);
		$db->query();
		
		//create the where clause
		$clause=" (0 ";
		foreach($datasourcesIDs as $key => $value){
				$clause.=" OR ".$db->nameQuote('id').' = '.$db->Quote($key)." ";
		}
		$clause.=" ) ";
		
		$query='SELECT name,description,driver,host,dbname as `database`,user,AES_DECRYPT(password,\''.$encryption_key.'\') as password, prefix, query, realtime, access_level as accesslevel  FROM '.$db->nameQuote($targetTable). ' WHERE '.$clause;
		
		$db->setQuery($query);
		$vet = $db->loadAssocList();
		$xml = "";
		foreach($vet as $datasource){
			$xml.=$this->exportSQL($datasource);
		} 
		return $xml;
				
	}
	
	protected function exportSQL($datasource){
		global $mainframe;
		/*read joomla database options these will be the default value if they are not specified for the sql datasource*/
		$driver=$mainframe->getCfg("dbtype");
		$prefix= $mainframe->getCfg("dbprefix");
		$host=$mainframe->getCfg("host");
		$database=$mainframe->getCfg("db");
		$user=$mainframe->getCfg("user");
		$password=$mainframe->getCfg("password");
		
		//if the references are related to local database they are removed
		if(strcasecmp(trim($datasource['driver']),trim($driver))==0){
			unset($datasource['driver']);
		}
		if(strcasecmp(trim($datasource['prefix']),trim($prefix))==0){
			unset($datasource['prefix']);
		}
		if(strcasecmp(trim($datasource['host']),trim($host))==0){
			unset($datasource['host']);
		}
		if(strcasecmp(trim($datasource['database']),trim($database))==0){
			unset($datasource['database']);
		}
		if(strcasecmp(trim($datasource['user']),trim($user))==0){
			unset($datasource['user']);
		}
		if(strcasecmp(trim($datasource['password']),trim($password))==0){
			unset($datasource['password']);
		}
		
		//ad as last the query
		$query = $datasource['query'];
		unset($datasource['query']);
		$datasource['query']=$query;
		$tag="sqlds";
		
		//create the xml string
		$xml="<$tag ";				
		foreach($datasource as $key => $value){
	

			//check value
			if(strcasecmp(trim($value),"")==0){
				//skip
			}
			else{					
				//from utf-8 to html entities
				$value=$this->convertToHTMLEntities($value);
				
				if(strcasecmp(trim($key),"query")==0){
					$xml.=">";//close the head tag
					$xml.="$value";
				}else{
					$xml.=" $key=\"$value\" ";	
				}									
			}
			
		}
		$xml.="</$tag>";				
		return $xml;		
	}
	
	protected function exportArticleDatasources($datasourceIDs){
		//table we are working on
		$targetTable="#__arc_datasource_article";
		
		$db =& JFactory::getDBO();
			
		//to avoid problem with character set
		$db->setQuery($this->characterset);
		$db->query();
		
		
		
		
		//create the where clause
		$clause=" (0 ";
		foreach($datasourceIDs as $key => $value){
		
				$clause.=" OR ".$db->nameQuote('a.id').' = '.$db->Quote($key)." ";
		}
		$clause.=" ) ";
		
		
		
		$query='SELECT a.name as name,a.description as description, a.access_level as accesslevel, a.tablenumber as tablenumber, a.tableid as tableid, a.stripped as stripped, a.realtime as realtime, b.title as articlename  
				FROM '.$db->nameQuote($targetTable). ' as a, '.$db->nameQuote("#__content").' as b    
				WHERE '.$clause.' AND '.$db->nameQuote('a.articleid').' = '.$db->nameQuote('b.id')
		;
		
	
		$db->setQuery($query);
		$vet = $db->loadAssocList();
		
		
		$xml = "";
		foreach($vet as $datasource){
			$xml.=$this->exportArticle($datasource);
		} 
		return $xml;
		
		
		
		
	}
	
	protected function exportArticle($datasource){
		$tag="articleds";
		
		//create the xml string
		$xml="<$tag ";				
		foreach($datasource as $key => $value){
			//check value
			if(strcasecmp(trim($value),"")==0){
				//skip
			}
			else{		
				//from utf-8 to html entities			
				$value=$this->convertToHTMLEntities($value);
				$xml.=" $key=\"$value\" ";
			}
		}
		$xml.=" />";				
		return $xml;
		
	}
	
	protected function exportHTMLDatasources($datasourceIDs){
		
		//table we are working on
		$targetTable="#__arc_datasource_html";
		
		$db =& JFactory::getDBO();
			
		//to avoid problem with character set
		$db->setQuery($this->characterset);
		$db->query();
		
		
		
		
		//create the where clause
		$clause=" (0 ";
		foreach($datasourceIDs as $key => $value){
		
				$clause.=" OR ".$db->nameQuote('id').' = '.$db->Quote($key)." ";
		}
		$clause.=" ) ";
		
		
		
		$query='SELECT name,description, access_level as accesslevel, tablenumber, tableid, stripped, realtime, address  
				FROM '.$db->nameQuote($targetTable). '     
				WHERE '.$clause;
		
	
		$db->setQuery($query);
		$vet = $db->loadAssocList();
		
		
		$xml = "";
		foreach($vet as $datasource){
			$xml.=$this->exportHTML($datasource);
		} 
		return $xml;
		
		
		
	
		
	}
	
	protected function exportHTML($datasource){
		
		$tag="htmlds";
		
		//create the xml string
		$xml="<$tag ";				
		foreach($datasource as $key => $value){
	

			//check value
			if(strcasecmp(trim($value),"")==0){
				//skip
			}
			else{					
				//from utf-8 to html entities
				$value=$this->convertToHTMLEntities($value);
				
				if(strcasecmp(trim($key),"address")==0){
					$xml.=">";//close the head tag
					$xml.="$value";
				}else{
					$xml.=" $key=\"$value\" ";	
				}									
			}
			
		}
		$xml.="</$tag>";				
		return $xml;		
		
	}
	protected function exportSnippetDatasources($datasourceIDs){
		
		//table we are working on
		$targetTable="#__arc_datasource_snippet";
		
		$db =& JFactory::getDBO();
			
		//to avoid problem with character set
		$db->setQuery($this->characterset);
		$db->query();
		
		
		
		
		//create the where clause
		$clause=" (0 ";
		foreach($datasourceIDs as $key => $value){
		
				$clause.=" OR ".$db->nameQuote('id').' = '.$db->Quote($key)." ";
		}
		$clause.=" ) ";
		
		
		
		$query='SELECT name,description, access_level as accesslevel, code  
				FROM '.$db->nameQuote($targetTable). '     
				WHERE '.$clause;
		
	
		$db->setQuery($query);
		$vet = $db->loadAssocList();
		
		
		$xml = "";
		foreach($vet as $datasource){
			$xml.=$this->exportSnippet($datasource);
		} 
		return $xml;
	
		
	}
	
	protected function exportSnippet($datasource){
		
		
		$tag="snippetds";
		
		//create the xml string
		$xml="<$tag ";				
		foreach($datasource as $key => $value){
	

			//check value
			if(strcasecmp(trim($value),"")==0){
				//skip
			}
			else{					
				//from utf-8 to html entities
				$value=$this->convertToHTMLEntities($value);
				
				if(strcasecmp(trim($key),"code")==0){
					$xml.=">";//close the head tag
					$xml.="$value";
				}else{
					$xml.=" $key=\"$value\" ";	
				}									
			}
			
		}
		$xml.="</$tag>";				
		return $xml;		
		
	}
		
	
}


