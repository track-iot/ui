<?php
/**
 * @version		$Id: submenu.php 1.0 19-11-2009 Danijar
 * @package		Frontend Admin
 * @copyright	Copyright (C) 2009 CMSSpace. http://www.cmsspace.com
 * @license		GNU/GPL. http://www.gnu.org/licenses/gpl.html
 */
$lang	=& JFactory::getLanguage();
$doc	=& JFactory::getDocument();
$user	=& JFactory::getUser();
class AikAdminSubMenu
{
	function get()
	{
		$menu = JToolBar::getInstance('submenu');
		$list = $menu->_bar;

		if (!is_array($list) || !count($list)) {
			return null;
		}

		$hide = JRequest::getInt('hidemainmenu');
		$txt = "<ul id=\"submenu\">\n";
		foreach ($list as $item)
		{
			$txt .= "<li>\n";
			if ($hide)
			{
				if (isset ($item[2]) && $item[2] == 1) {
					$txt .= "<span class=\"nolink active\">".$item[0]."</span>\n";
				}
				else {
					$txt .= "<span class=\"nolink\">".$item[0]."</span>\n";
				}
			}
			else
			{
				if (isset ($item[2]) && $item[2] == 1) {
					$txt .= "<a class=\"active\" href=\"".JFilterOutput::ampReplace($item[1])."\">".$item[0]."</a>\n";
				}
				else {
					$txt .= "<a href=\"".JFilterOutput::ampReplace($item[1])."\">".$item[0]."</a>\n";
				}
			}
			$txt .= "</li>\n";
		}
		$txt .= "</ul>\n";
		return $txt;
	}
	
	function enable()
	{
		$menu = JToolBar::getInstance('submenu');
		$list = $menu->_bar;

		if (!is_array($list) || !count($list)) {
			return false;
		}else {
			return true;
		}
	}
}