<?php
/**
 * @version		$Id: modules.php 1.0 19-11-2009 Danijar
 * @package		Frontend Admin
 * @copyright	Copyright (C) 2009 CMSSpace. http://www.cmsspace.com
 * @license		GNU/GPL. http://www.gnu.org/licenses/gpl.html
 */
defined( '_JEXEC' ) or die( 'Restricted access' );
require_once(dirname(__FILE__).DS.'extension.php');
class AikModelModules extends InstallerModel
{
	var $_type = 'module';
	function __construct()
	{
		global $mainframe;
		parent::__construct();

		$this->setState('filter.string', $mainframe->getUserStateFromRequest( 'com_modules.installer.modules.string', 'filter', '', 'string' ));
		$this->setState('filter.client', $mainframe->getUserStateFromRequest( 'com_modules.installer.modules.client', 'client', -1, 'int' ));
	}

	function _loadItems()
	{
		global $mainframe, $option;

		$db = &JFactory::getDBO();

		$and = null;
		if ($this->_state->get('filter.client') < 0) {
			if ($search = $this->_state->get('filter.string')) {
				$and = ' AND title LIKE '.$db->Quote( '%'.$db->getEscaped( $search, true ).'%', false );
			}
		} else {
			if ($search = $this->_state->get('filter.string'))
			{
				$and = ' AND client_id = '.(int)$this->_state->get('filter.client');
				$and .= ' AND title LIKE '.$db->Quote( '%'.$db->getEscaped( $search, true ).'%', false );
			}
			else {
				$and = ' AND client_id = '.(int)$this->_state->get('filter.client');
			}
		}

		$query = 'SELECT id, module, client_id, title, iscore' .
				' FROM #__modules' .
				' WHERE module LIKE "mod_%" ' .
				$and .
				' GROUP BY module, client_id' .
				' ORDER BY iscore, client_id, module';
		$db->setQuery($query);
		$rows = $db->loadObjectList();

		$n = count($rows);
		for ($i = 0; $i < $n; $i ++) {
			$row = & $rows[$i];

			if ($row->client_id == "1") {
				$moduleBaseDir = JPATH_ADMINISTRATOR.DS."modules";
			} else {
				$moduleBaseDir = JPATH_SITE.DS."modules";
			}

			$xmlfile = $moduleBaseDir . DS . $row->module .DS. $row->module.".xml";

			if (file_exists($xmlfile))
			{
				if ($data = JApplicationHelper::parseXMLInstallFile($xmlfile)) {
					foreach($data as $key => $value) {
						$row->$key = $value;
					}
				}
			}
		}
		$this->setState('pagination.total', $n);
		if($this->_state->get('pagination.offset') > $this->_state->get('pagination.total')) {
			$this->setState('pagination.offset',0);
		}

		if($this->_state->get('pagination.limit') > 0) {
			$this->_items = array_slice( $rows, $this->_state->get('pagination.offset'), $this->_state->get('pagination.limit') );
		} else {
			$this->_items = $rows;
		}
	}
}