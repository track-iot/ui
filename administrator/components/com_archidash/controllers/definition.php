<?php

// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.controller');
//Import filesystem libraries. Perhaps not necessary, but does not hurt
jimport('joomla.filesystem.file');

require_once( JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'views'.DS.'definition'.DS.'view.html.php' );
require_once( JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'models'.DS.'definition.php' );

/**
 * 
 * @version 1.0
 * @author  TobyTools.com (email:info@tobytools.com)(website: www.tobytools.com)
 * @copyright Copyright TobyTools.com 
 * @license GNU/GPL
 */

class ArchiDashControllerDefinition extends JController{
	
	
	
	protected $arcView;
	protected $arcModel;
	
	
	function ArchiDashControllerDefinition(){
		
		//registerTask
		parent::__construct();
		
		//call view
		$classname = "ArchiDashViewDefinition";
		$this->arcView = new $classname();
		
		//call model
		$classname = "ArchiDashModelDefinition";
		$this->arcModel = new $classname();
		
		
		//task registration
		$this->registerTask('download','download');
		$this->registerTask('remove','remove');
		$this->registerTask('cancel','cancel');
		$this->registerTask('import','import');
		$this->registerTask('export','export');
		$this->registerTask('saveexport','saveexport');
		$this->registerTask('upload','upload');
		
	}
	
	function export($params=false){
		$this->arcView->export($params);
	}
	
	
	function upload(){
		global $mainframe;
		$file = JRequest::getVar('file_upload', null, 'files', 'array');

		//$max = ini_get('upload_max_filesize');
		$directory = $this->arcModel->getPath();
		//only XML
		$file_type = "text/xml";
		if(isset($file)){ 
			//Clean up filename to get rid of strange characters like spaces etc
			$filename = JFile::makeSafe($file['name']);
	 
			//if($file['size'] > $max) $mainframe->enqueueMessage(JText::_('ONLYFILESUNDER').' '.$max,'error');
			//Set up the source and destination of the file
	 
			$src = $file['tmp_name'];
			$dest = $directory . $filename;
			//if file exists no operation
			if(@file_exists($dest)){
				 //Redirect to a page of your choice
				$mainframe->enqueueMessage(JText::_('FILEALREADYEXIST')." ".$dest,'error');
				$this->import();
			}
			else{
				//First check if the file has the right extension, we need jpg only
				if (strcasecmp($file['type'],$file_type)==0 || strcasecmp($file_type,'*')==0 ) { 
					   if ( JFile::upload($src, $dest) ) {		 
			  		    //Redirect to a page of your choice
						$mainframe->enqueueMessage(JText::_('FILESAVED')." ".$dest);
						$vet = array(); $vet['name']=$filename;
						$this->arcModel->insertRecord($vet);
						$this->display();					
					   } 
					   else {
						 	//Redirect and throw an error message
							$mainframe->enqueueMessage(JText::_('UPLOADERROR')." $directory",'error');
							$this->import();											
					   }
				} 
				else {
				   //Redirect and notify user file is not right extension
					$mainframe->enqueueMessage(JText::_('FILETYPEINVALID'),'error');
					$this->import();
				}
			}
		}
		else{
			$mainframe->enqueueMessage(JText::_('NOFILESPECIFIED'),'error');
			$this->import();
		}	
	}
	
	function display(){
		$this->arcView->display();
	}
	
	function download(){
		//to call the edit user must specify a selection
		$cid = JRequest::getVar('cid');
		$id = $cid[0];
		$record = $this->arcModel->selectRecord($id);
		$file = $this->arcModel->getPath().$record['name'];
		$this->arcView->download($file);
		
		
	}
	function remove(){
		global $mainframe;
		//get selected elements
		$cid = JRequest::getVar('cid');
		$opt=array();
		foreach($cid as $id){
			$this->arcModel->deleteRecord($id);
		}
		$mainframe->enqueueMessage(JText::_('OPERATIONOK'));
	
		
		//call the view
		$this->arcView->remove();
		
			
	}
	
	function cancel(){
			
		$this->arcView->display();	
					
	}
	
	function import(){
			
		$this->arcView->import();
	}
	
	function saveexport(){
		global $mainframe;
		
		$opts = array();
		
		$opts['definitionname']=JRequest::getVar('definitionname','');/*mandatory*/
		
		$dashboard = array();
		$dashboard['name']=JRequest::getVar('dashboardname','');/*mandatory*/
		$dashboard['description']=JRequest::getVar('dashboarddesc','');
		$dashboard['accesslevel']=JRequest::getVar('dashboardacl','0');
		
		$opts['dashboard']=$dashboard;
		$opts['sectionid']=JRequest::getVar('sectionid',false);/*mandatory*/
		
		$menu=array();
		$menu['name']=JRequest::getVar('menuname','');/*mandatory*/
		$menu['description']=JRequest::getVar('menudesc','');/*mandatory*/
		$opts['menu']=$menu;
		
										
		$modules=array();
		$modules=JRequest::getVar('moduleids',false);
		$opts['modules']=$modules;
		
		if(
			strcasecmp($opts['definitionname'],"")==0 ||
			strcasecmp($opts['dashboard']['name'],"")==0 ||
			$opts['sectionid']==false ||
			strcasecmp($opts['menu']['name'],"")==0||
			strcasecmp($opts['menu']['description'],"")==0			
		){
			$mainframe->enqueueMessage(JText::_('CHECKINFODEF'),'error');
			$this->export($opts);
		}
		else{
			require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'engine'.DS.'exporters'.DS.'dashboard.php');
			$classname = "ArchiDashExporterDashboard";		
			$exporter = new $classname($opts);			
			$xml="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
			
			
			//well known error with xml and &nbsp
			/**
			 * 	using nbsp in xml
				&nbsp; is defined in HTML, but not in XML. XML only defines:
				&lt;  &gt;  &apos;  &quot;  &amp;
				http://www.stylusstudio.com/xsllist/200211/post10600.html
				http://www.coderanch.com/t/129063/XML/entity-nbsp-was-referenced-but
				http://techtrouts.com/webkit-entity-nbsp-not-defined-convert-html-entities-to-xml/

			 * 
			 */
			 
			//$xml.="<!DOCTYPE some_name [<!ENTITY nbsp \"&#160;\">]>";
			$xml .= $exporter->export();
			
			
			
			 // send the header here
    		header('Content-type: text/xml');
    		header('Content-Disposition: attachment; filename="' . $opts['definitionname'].".xml");
    
		    // put the content in the file
	    	echo($xml);
	    
    		// stop processing the page
    		exit; 						
			$this->export($opts);
			
		}
		
		
	}

}