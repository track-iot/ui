<?php


defined('_JEXEC') or die('Restricted Access');
jimport('joomla.application.component.model');
require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'models'.DS.'model.php');


/**
 * The following class manages all the CRUD operations on #__archidash_articles table
 * @version 1.0
 * @author  TobyTools.com (email:info@tobytools.com)(website: www.tobytools.com)
 * @copyright Copyright TobyTools.com 
 * @license GNU/GPL
 */


class ArchiDashModelArticle extends ArchiDashModel{
	
	
	
	
		
	/**
	 * class constructor
	 * 
	 * */
	function ArchiDashModelArticle(){
		parent::__construct();
		$this->table="#__archidash_articles";		
	}
	
	/**
	 * 
	 * Updates a record starting by the associative array. E.g.  Array ( [field1] => value1 [field2] => value2 ).
	 * @param array $vet the associative array with the new information of recod. 
	 * @return A database resource if successful, FALSE if not.
	 * 
	 * */
	
	function updateRecord($vet=false){
		global $mainframe;
		if($vet!=false){
			
			$db =& JFactory::getDBO();			
			
			//to avoid problem with character set
			$db->setQuery($this->characterset);
			$db->query();
			
			
			$query= "UPDATE ".$db->nameQuote($this->table)." SET ".$db->nameQuote('articleid')." = ".$db->Quote($vet['articleid']).", ".			
			$db->nameQuote('dashboardid')." = ".$db->Quote($vet['dashboardid']).
			" WHERE ".$db->nameQuote('id')." = ".$db->Quote($vet['id']);						
			$db->setQuery($query);
			$result = $db->query(); 
			if(!$result){
				//something wrong
				$this->errMsg = $db->getErrorNum()." : ".$db->getErrorMsg();
				$mainframe->enqueueMessage(JText::_( 'QUERYKO' ),'error');
				$mainframe->enqueueMessage($this->errMsg,'error');
				$mainframe->enqueueMessage(get_class($this)."::updateRecord",'error');
				return false;
			}
			else{
				//clean
				$this->errMsg = false;
				return $result;
			}		
		}
		else{

			$this->errMsg=JText::_( 'ARGNOVALID' );
			$mainframe->enqueueMessage(JText::_( 'ARGNOVALID' ),'error');
			$mainframe->enqueueMessage(get_class($this)."::updateRecord",'error');
			return false;
		}
				
	}
	
	/**
	 * 
	 * Insert a new record starting by the associative array.
	 * @param array $vet the associative array with the information of the new record. N.B. The id is useless, the function lets MYSQL manage the id auto increment 
	 * @return the record id if successful, FALSE if not.
	 * 
	 * */
	function insertRecord($vet=false){
		global $mainframe;
		if($vet!=false){						
			
			$db =& JFactory::getDBO();
			
			//to avoid problem with character set
			$db->setQuery($this->characterset);
			$db->query();
			
			$query = "INSERT INTO ".$db->nameQuote($this->table)." (articleid,dashboardid) VALUES ('".$vet['articleid']."','".$vet['dashboardid']."')";			
			$db->setQuery($query);
			$result = $db->query(); 
			if(!$result){
				//something wrong
				$this->errMsg = $db->getErrorNum()." : ".$db->getErrorMsg();
				$mainframe->enqueueMessage(JText::_( 'QUERYKO' ),'error');
				$mainframe->enqueueMessage($this->errMsg,'error');
				$mainframe->enqueueMessage(get_class($this)."::insertRecord",'error');
				return false;
			}
			else{
				//clean
				$this->errMsg = false;
				//provide the element id
				$db->setQuery("SELECT LAST_INSERT_ID()");				
				return $db->loadResult();
			}
		}
		else{
			$this->errMsg=JText::_( 'ARGNOVALID' );
			$mainframe->enqueueMessage(JText::_( 'ARGNOVALID' ),'error');
			$mainframe->enqueueMessage(get_class($this)."::insertRecord",'error');
			return false;
		}		
	}
	
	
		
	
}
?>
