<?php


defined('_JEXEC') or die('Restricted Access');
jimport('joomla.application.component.model');
require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'engine'.DS.'destructors'.DS.'destructor.php');


/**
 * The following class destroy the menutype associated with the dashboard (+ the module by using ArchiDashDestructorModule)
 * @version 1.0
 * @author  TobyTools.com (email:info@tobytools.com)(website: www.tobytools.com)
 * @copyright Copyright TobyTools.com 
 * @license GNU/GPL
 */


class ArchiDashDestructorMenuType extends ArchiDashDestructor{
	
	/**
	 * 
	 * @var string, the name of MenuType
	 */
	private $menuName;

	function ArchiDashDestructorMenuType($opts){
		parent::__construct();
		//$opts['dashboardid']
		$this->options = $opts;
		$this->table="#__menu_types";
		$this->options['characterset']=$this->characterset;//set characters
		$this->menuName="";
		
	}
	
	function preCondition(){
		$db =& JFactory::getDBO();
		//delete the menutype ONLY IF there are no menu items associated with it				
		$query=	'SELECT * FROM '.$db->nameQuote('#__archidash_menu_types').' as a, '.$db->nameQuote('#__menu_types').' as b, '.$db->nameQuote('#__menu').' as c  '.
				' WHERE '.$db->nameQuote('dashboardid').' = '.$db->Quote($this->options['dashboardid']).
				' AND '.$db->nameQuote('a.menutypeid').' = '.$db->nameQuote('b.id').
				' AND '.$db->nameQuote('b.menutype').' = '.$db->nameQuote('c.menutype');
		$db->setQuery($query);
		$list = $db->loadAssocList();
		if(count($list)>0){
			//save the name of menutype. 0 element there is for sure
			$this->menuName = $list[0]['title'];
			return false;
		}
		else{
			return true;	
		}
		
	}

	function destroy(){
		global $mainframe;
		$preCondition = $this->preCondition();
		
		if(isset($this->options['dashboardid']) && $preCondition ){
			
			require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'models'.DS.'menutype.php');
			$classname="ArchiDashModelMenuType";
			$model = new $classname();
			
			/*MENUTYPE IDs related to dashboard*/
			$list = $model->selectAllRecordsByDashboardID($this->options['dashboardid']);
			if(!$list || count($list)<=0){
				//no element to destroy
				return;
			}
			
			
			/*DESTROY MENUTYPE*/
			//work on #__menu_types
			$db =& JFactory::getDBO();
			//to avoid problem with character set
			$db->setQuery($this->characterset);
			$db->query();
			
			$query = "DELETE FROM ".$db->nameQuote($this->table)." 
						WHERE 0 ";
			
			//create where clause
			$where=" ";
			foreach($list as $element){
				$id = $element['menutypeid'];
				$where.=" OR ".$db->nameQuote('id')." = ".$db->Quote($id);				
			}
			$where.=" ";
			$query.=$where;			
			$db->setQuery($query);
			$db->query();
			
			
		
			
			/*DELETE THE RELATED RECORDS INTO #__archidash_menutypes*/
			$model->deleteAllRecordsByDashboardID($this->options['dashboardid']);
			
			/*DELETE MODULE RELATED TO*/
			require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'engine'.DS.'destructors'.DS.'module.php');
			$classname = "ArchiDashDestructorModule";
			$destructor = new $classname($this->options);
			$destructor->destroy();
			
			
			
			
			
		}
		else{
			if(!isset($this->options['dashboardid'])){			
				$this->completed=false;
				$this->errMsg.=get_class($this)."::destroy ".JText::_('NODASHBOARDID');
				$mainframe->enqueueMessage($this->errMsg,'error');
			}
			if(!$preCondition){
				/**
				 * Even though there is no condition to delete the menutype, it is necessary clean the archidash tables
				 */
				require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'models'.DS.'menutype.php');
				$classname="ArchiDashModelMenuType";
				$model = new $classname();
				$model->deleteAllRecordsByDashboardID($this->options['dashboardid']);
				require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'models'.DS.'module.php');
				$classname="ArchiDashModelModule";
				$model = new $classname();
				$model->deleteAllRecordsByDashboardID($this->options['dashboardid']);
				/************************************************/
				
				//alert the user that is impossible to delete the menu
				$this->completed=false;
				$this->errMsg.=get_class($this)."::destroy (MENU ".$this->menuName.") ".JText::_('NOPRECMENUTYPE');
				$mainframe->enqueueMessage($this->errMsg,'notice');
			}
			
		}
	}
}


