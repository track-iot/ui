<?php
/**
 * @version		$Id: admin.php 1.0 19-11-2009 Danijar
 * @package		Frontend Admin
 * @copyright	Copyright (C) 2009 CMSSpace. http://www.cmsspace.com
 * @license		GNU/GPL. http://www.gnu.org/licenses/gpl.html
 */
jimport( 'joomla.application.component.controller' );
class AikControllerAdmin extends JController
{
	function __construct()
	{
		parent::__construct(array('default_task' => 'sysinfo'));
		
	}
	function sysinfo()
	{
		JToolBarHelper::title( JText::_( 'Information' ), 'systeminfo.png' );
		JToolBarHelper::help( 'screen.system.info' );
		JToolBarHelper::cpanel();
		
		$view =& $this->getView( 'admin' );
		$view->system_info();
	}
	function help()
	{
		JToolBarHelper::title( JText::_( 'Help' ), 'help_header.png' );
		JToolBarHelper::cpanel();
		
		$view =& $this->getView( 'admin' );
		//$view->help();
	}
}