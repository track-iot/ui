<?php
/**
 * @version		$Id: view.php 1.0 19-11-2009 Danijar
 * @package		Frontend Admin
 * @copyright	Copyright (C) 2009 CMSSpace. http://www.cmsspace.com
 * @license		GNU/GPL. http://www.gnu.org/licenses/gpl.html
 */
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.view');
class AikViewConfig extends JView
{
	function showConfig( &$row, &$lists )
	{
		global $mainframe;

		JHTML::_('behavior.tooltip');
		JHTML::_('behavior.switcher');

		JToolBarHelper::title( JText::_( 'Global Configuration' ), 'config.png' );
		JToolBarHelper::save();
		JToolBarHelper::apply();
		JToolBarHelper::cancel('cancel', 'Close');
		JToolBarHelper::cpanel();
		
		$table =& JTable::getInstance('component');
		$table->loadByOption( 'com_users' );
		$userparams = new JParameter( $table->params, JPATH_ADMINISTRATOR.DS.'components'.DS.'com_users'.DS.'config.xml' );
		$table->loadByOption( 'com_media' );
		$mediaparams = new JParameter( $table->params, JPATH_ADMINISTRATOR.DS.'components'.DS.'com_media'.DS.'config.xml' );

		$tmplpath = dirname(__FILE__).DS.'tmpl';

		JSubMenuHelper::addEntry(JText::_( 'Site' ),'#" id="site" class="active"');
		JSubMenuHelper::addEntry(JText::_( 'System' ),'#" id="system"');
		JSubMenuHelper::addEntry(JText::_( 'Server' ),'#" id="server"');
		
		jimport('joomla.client.helper');
		$ftp =& JClientHelper::setCredentialsFromRequest('ftp');
		?>
		<form action="index.php" method="post" name="adminForm" autocomplete="off">
		<?php if ($ftp) {
			require_once($tmplpath.DS.'ftp.php');
		} ?>
		<div id="config-document">
			<div id="page-site">
				<table class="noshow">
					<tr>
						<td width="100%">
							<?php require_once($tmplpath.DS.'config_site.php'); ?>
							<?php require_once($tmplpath.DS.'config_metadata.php'); ?>
							<?php require_once($tmplpath.DS.'config_seo.php'); ?>
						</td>
					</tr>
				</table>
			</div>
			<div id="page-system">
				<table class="noshow">
					<tr>
						<td width="100%">
							<?php require_once($tmplpath.DS.'config_system.php'); ?>
							<fieldset class="adminform">
								<legend><?php echo JText::_( 'User Settings' ); ?></legend>
								<?php echo $userparams->render('userparams'); ?>
							</fieldset>
							<fieldset class="adminform">
								<legend><?php echo JText::_( 'Media Settings' ); ?>
				<span class="error hasTip" title="<?php echo JText::_( 'Warning' );?>::<?php echo JText::_( 'WARNPATHCHANGES' ); ?>">
					<?php echo AikViewConfig::WarningIcon(); ?>
				</span>
								</legend>
								<?php echo $mediaparams->render('mediaparams'); ?>
							</fieldset>
							<?php require_once($tmplpath.DS.'config_debug.php'); ?>
							<?php require_once($tmplpath.DS.'config_cache.php'); ?>
							<?php require_once($tmplpath.DS.'config_session.php'); ?>
						</td>
					</tr>
				</table>
			</div>
			<div id="page-server">
				<table class="noshow">
					<tr>
						<td width="100%">
							<?php require_once($tmplpath.DS.'config_server.php'); ?>
							<?php require_once($tmplpath.DS.'config_locale.php'); ?>
							<?php require_once($tmplpath.DS.'config_ftp.php'); ?>
							<?php require_once($tmplpath.DS.'config_database.php'); ?>
							<?php require_once($tmplpath.DS.'config_mail.php'); ?>
						</td>
					</tr>
				</table>
			</div>
		</div>
		<div class="clr"></div>

		<input type="hidden" name="c" value="global" />
		<input type="hidden" name="live_site" value="<?php echo isset($row->live_site) ? $row->live_site : ''; ?>" />
		<input type="hidden" name="option" value="com_aikadmin" />
		<input type="hidden" name="c" value="config" />
		<input type="hidden" name="secret" value="<?php echo $row->secret; ?>" />
		<input type="hidden" name="task" value="" />
		<?php echo JHTML::_( 'form.token' ); ?>
		</form>
		<?php
	}

	function WarningIcon()
	{
		global $mainframe;

		$tip = '<img src="'.JURI::root().'includes/js/ThemeOffice/warning.png" border="0"  alt="" />';

		return $tip;
	}
}
