<?php


defined('_JEXEC') or die('Restricted Access');
jimport('joomla.application.component.model');
require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'engine'.DS.'exporters'.DS.'exporter.php');


/**
 * The following class export the categories 
 * @version 1.0
 * @author  TobyTools.com (email:info@tobytools.com)(website: www.tobytools.com)
 * @copyright Copyright TobyTools.com 
 * @license GNU/GPL
 */


class ArchiDashExporterCategory extends ArchiDashExporter{
	
	/*
	 *class constructor 
	 */
	function ArchiDashExporterCategory($opts){
		parent::__construct($opts);
		//check everything is allright
		if($this->completed){
			//options['sectionid'], since here you find all the categories related to
			if($this->options['sectionid']){
				$this->options['table']="#__categories";
				$this->options['tag']="category";			
			}
			else{
				$this->completed=false;
				$this->options=false;
				$this->errMsg=false;
				global $mainframe;
				$mainframe->enqueueMessage(JText::_('NOSECTIONID')." ".get_class($this),'error');
			}
			
		}
		else{
			global $mainframe;
			$mainframe->enqueueMessage(JText::_('NOOBJECT')." ".get_class($this),'error');
		}
		
	}
	
	public function export($params=false){
		if($this->completed){
			global $mainframe;						
			$db =& JFactory::getDBO();
			
			//to avoid problem with character set
			$db->setQuery($this->options['characterset']);
			$db->query();

			/*the fields are the xml attributes*/
			$query='SELECT title as name, alias, description  FROM '.$db->nameQuote($this->options['table']). ' WHERE '.$db->nameQuote('section').' = '.$db->Quote($this->options['sectionid']).' ORDER BY ordering ASC , Title ASC ';
			$db->setQuery($query);
			$vet = $db->loadAssocList();			
			
			if(!$vet){
				//something wrong
				if($db->getErrorNum()==0){
					//it is not an error, the problem is there is no record with the specified id
					$this->errMsg = false;
					$this->completed=false;
					$mainframe->enqueueMessage(JText::_( 'NOSECTION' ),'error');
					$mainframe->enqueueMessage(get_class($this)."::export",'error');	
				}
				else{
					//MYSQL error
					$this->completed=false;
					$this->errMsg = $db->getErrorNum()." : ".$db->getErrorMsg();
					$mainframe->enqueueMessage(JText::_( 'QUERYKO' ),'error');
					$mainframe->enqueueMessage($this->errMsg,'error');
					$mainframe->enqueueMessage(get_class($this)."::export",'error');					
				}
			}			
			else{	
				$xml="";			
				foreach($vet as $category){
					$xml.=$this->exportCategory($category);
				}
				return $xml;								
			}			
		}
		else{
			return "";
		}				
	}
	
	protected function exportCategory($vet){
		
		//create the xml string
		$xml="<{$this->options['tag']} ";				
		foreach($vet as $key => $value){
			//check value
			if(strcasecmp(trim($value),"")==0){
				//skip
			}
			else{		
				//from utf-8 to html entities			
				$value=$this->convertToHTMLEntities($value);
				$xml.=" $key=\"$value\" ";
			}
		}
		$xml.=" />";				
		return $xml;
	}
	

}
