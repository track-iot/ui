<?php



// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view' );

/**
 * @version 1.0
 * @author  TobyTools.com (email:info@tobytools.com)(website: www.tobytools.com)
 * @copyright Copyright TobyTools.com 
 * @license GNU/GPL
 */

class ArchiDashViewDefinition extends JView{
	
	function display($tpl = null){
		require_once( JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'views'.DS.'definition'.DS.'tmpl'.DS.'definition.php' );
		$classname = "ArchiDashLayoutDefinition";
		$layout = new $classname();			
		$layout->display();
	}
	
	function download($file){
		require_once( JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'views'.DS.'definition'.DS.'tmpl'.DS.'definition.php' );
		$classname = "ArchiDashLayoutDefinition";
		$layout = new $classname();			
		$layout->download($file);
	}
	
	function remove($tpl = null){
		require_once( JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'views'.DS.'definition'.DS.'tmpl'.DS.'definition.php' );
		$classname = "ArchiDashLayoutDefinition";
		$layout = new $classname();			
		$layout->remove();
	}
	
	function import($tpl = null){
		require_once( JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'views'.DS.'definition'.DS.'tmpl'.DS.'definition.php' );
		$classname = "ArchiDashLayoutDefinition";
		$layout = new $classname();			
		$layout->import();
	}
	
	function export($params=false){
		require_once( JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'views'.DS.'definition'.DS.'tmpl'.DS.'definition.php' );
		$classname = "ArchiDashLayoutDefinition";
		$layout = new $classname();			
		$layout->export($params);
	}
	
}
