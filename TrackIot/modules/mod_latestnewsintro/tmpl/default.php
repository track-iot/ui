<?php // no direct access
defined('_JEXEC') or die('Restricted access'); ?>
<ul class="latestnews<?php echo $params->get('moduleclass_sfx'); ?>">
<?php foreach ($list as $item) :  ?>
	<li class="latestnews<?php echo $params->get('moduleclass_sfx'); ?>">
		<a href="<?php echo $item->link; ?>" class="latestnews<?php echo $params->get('moduleclass_sfx'); ?>">
			<?php echo $item->text; ?></a>
            <br  />
            <?php
				if (!empty($item->intro)) {
					echo substr($item->intro, 0, $params->get('introlength'));
				} else {
					echo substr($item->full, 0, $params->get('introlength'));
				}
				?>
	</li>
<?php endforeach; ?>
</ul>