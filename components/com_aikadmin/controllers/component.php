<?php
/**
 * @version		$Id: config.php 1.0 19-11-2009 Danijar
 * @package		Frontend Admin
 * @copyright	Copyright (C) 2009 CMSSpace. http://www.cmsspace.com
 * @license		GNU/GPL. http://www.gnu.org/licenses/gpl.html
 */
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.controller' );
class AikControllerComponent extends JController
{
	function __construct( $default = array())
	{
		$default['default_task'] = 'edit';
		parent::__construct( $default );

		$this->registerTask( 'apply', 'save' );
	}
	function edit()
	{
		JRequest::setVar('tmpl', 'component');
		$component = JRequest::getCmd( 'component' );

		if (empty( $component ))
		{
			JError::raiseWarning( 500, 'Not a valid component' );
			return false;
		}
		$lang = & JFactory::getLanguage();
		$lang->load( $component );

		$model = $this->getModel('Component' );
		$table =& JTable::getInstance('component');

		if (!$table->loadByOption( $component ))
		{
			JError::raiseWarning( 500, 'Not a valid component' );
			return false;
		}

		$view =& $this->getView( 'component' );
		
		$view->assignRef('component', $table);
		$view->setModel( $model, true );
		$view->display();
	}
	function save()
	{
		JRequest::checkToken() or jexit( 'Invalid Token' );

		$component = JRequest::getCmd( 'component' );

		$table =& JTable::getInstance('component');
		if (!$table->loadByOption( $component ))
		{
			JError::raiseWarning( 500, 'Not a valid component' );
			return false;
		}

		$post = JRequest::get( 'post' );
		$post['option'] = $component;
		$table->bind( $post );

		if (!$table->check()) {
			JError::raiseWarning( 500, $table->getError() );
			return false;
		}
		if (!$table->store()) {
			JError::raiseWarning( 500, $table->getError() );
			return false;
		}
		$this->edit();
	}
	function cancel()
	{
		$this->setRedirect( 'index.php?option=com_aikadmin&c=userserrr' );
	}
}