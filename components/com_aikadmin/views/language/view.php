<?php
/**
 * @version		$Id: view.php 1.0 19-11-2009 Danijar
 * @package		Frontend Admin
 * @copyright	Copyright (C) 2009 CMSSpace. http://www.cmsspace.com
 * @license		GNU/GPL. http://www.gnu.org/licenses/gpl.html
 */
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport('joomla.application.component.view');
class AikViewLanguage extends JView
{
	function display($tpl=null)
	{
		global $mainframe, $option;
	$db		=& JFactory::getDBO();
	$client	=& JApplicationHelper::getClientInfo(JRequest::getVar('client', '0', '', 'int'));
	$rows	= array ();
	


	$limit		= $mainframe->getUserStateFromRequest( 'global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int' );
	$limitstart = $mainframe->getUserStateFromRequest( 'com_language.limitstart', 'limitstart', 0, 'int' );

	JToolBarHelper::title( JText::_( 'Language Manager' ), 'langmanager.png' );
	JToolBarHelper::makeDefault( 'publish' );
	JToolBarHelper::cpanel();
		
	$rowid = 0;

	jimport('joomla.client.helper');
	$ftp =& JClientHelper::setCredentialsFromRequest('ftp');

	jimport('joomla.filesystem.folder');
	$path = JLanguage::getLanguagePath($client->path);
	$dirs = JFolder::folders( $path );

	foreach ($dirs as $dir)
	{
		$files = JFolder::files( $path.DS.$dir, '^([-_A-Za-z]*)\.xml$' );
		foreach ($files as $file)
		{
			$data = JApplicationHelper::parseXMLLangMetaFile($path.DS.$dir.DS.$file);

			$row 			= new StdClass();
			$row->id 		= $rowid;
			$row->language 	= substr($file,0,-4);

			if (!is_array($data)) {
				continue;
			}
			foreach($data as $key => $value) {
				$row->$key = $value;
			}

			$params = JComponentHelper::getParams('com_languages');
			if ( $params->get($client->name, 'en-GB') == $row->language) {
				$row->published	= 1;
			} else {
				$row->published = 0;
			}

			$row->checked_out = 0;
			$row->mosname = JString::strtolower( str_replace( " ", "_", $row->name ) );
			$rows[] = $row;
			$rowid++;
		}
	}


	jimport('joomla.html.pagination');
	$page = new JPagination( $rowid, $limitstart, $limit );

	$rows = array_slice( $rows, $page->limitstart, $page->limit );
	
		$limitstart = JRequest::getVar('limitstart', '0', '', 'int');
		$user =& JFactory::getUser();

		JHTML::_('behavior.tooltip');
		?>
		<form action="index.php" method="post" name="adminForm">

			<?php if($ftp): ?>
			<fieldset title="<?php echo JText::_('DESCFTPTITLE'); ?>">
				<legend><?php echo JText::_('DESCFTPTITLE'); ?></legend>

				<?php echo JText::_('DESCFTP'); ?>

				<?php if(JError::isError($ftp)): ?>
					<p><?php echo JText::_($ftp->message); ?></p>
				<?php endif; ?>

				<table class="adminform nospace">
				<tbody>
				<tr>
					<td width="120">
						<label for="username"><?php echo JText::_('Username'); ?>:</label>
					</td>
					<td>
						<input type="text" id="username" name="username" class="input_box" size="70" value="" />
					</td>
				</tr>
				<tr>
					<td width="120">
						<label for="password"><?php echo JText::_('Password'); ?>:</label>
					</td>
					<td>
						<input type="password" id="password" name="password" class="input_box" size="70" value="" />
					</td>
				</tr>
				</tbody>
				</table>
			</fieldset>
			<?php endif; ?>

			<table class="adminlist">
			<thead>
			<tr>
				<th width="20">
					<?php echo JText::_( 'Num' ); ?>
				</th>
				<th width="30">
					&nbsp;
				</th>
				<th width="25%" class="title">
					<?php echo JText::_( 'Language' ); ?>
				</th>
				<th width="5%">
					<?php echo JText::_( 'Default' ); ?>
				</th>
				<th width="10%">
					<?php echo JText::_( 'Version' ); ?>
				</th>
				<th width="10%">
					<?php echo JText::_( 'Date' ); ?>
				</th>
				<th width="20%">
					<?php echo JText::_( 'Author' ); ?>
				</th>
				<th width="25%">
					<?php echo JText::_( 'Author Email' ); ?>
				</th>
			</tr>
			</thead>
			<tfoot>
			<tr>
				<td colspan="8">
					<?php echo $page->getListFooter(); ?>
				</td>
			</tr>
			</tfoot>
			<tbody>
			<?php
			$k = 0;
			for ($i=0, $n=count( $rows ); $i < $n; $i++) {
				$row = &$rows[$i];
				?>
				<tr class="<?php echo "row$k"; ?>">
					<td width="20">
						<?php echo $page->getRowOffset( $i ); ?>
					</td>
					<td width="20">
						<input type="radio" id="cb<?php echo $i;?>" name="cid[]" value="<?php echo $row->language; ?>" onclick="isChecked(this.checked);" />
					</td>
					<td width="25%">
						<?php echo $row->name;?>
					</td>
					<td width="5%" align="center">
						<?php
						if ($row->published == 1) {	 ?>
							<img src="<?php echo JURI::root();?>components/com_aikadmin/images/default.png" alt="<?php echo JText::_( 'Default' ); ?>" />
							<?php
						} else {
							?>
							&nbsp;
						<?php
						}
					?>
					</td>
					<td align="center">
						<?php echo $row->version; ?>
					</td>
					<td align="center">
						<?php echo $row->creationdate; ?>
					</td>
					<td align="center">
						<?php echo $row->author; ?>
					</td>
					<td align="center">
						<?php echo $row->authorEmail; ?>
					</td>
				</tr>
			<?php
			}
			?>
			</tbody>
			</table>

		<input type="hidden" name="option" value="<?php echo $option;?>" />
		<input type="hidden" name="c" value="language" />
		<input type="hidden" name="client" value="<?php echo $client->id;?>" />
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<?php echo JHTML::_( 'form.token' ); ?>
		</form>
		<?php
	}
}
