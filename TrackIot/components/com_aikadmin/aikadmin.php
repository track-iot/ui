<?php
/**
 * @version		$Id: aikadmin.php 1.0 19-11-2009 Danijar
 * @package		Frontend Admin
 * @copyright	Copyright (C) 2009 CMSSpace. http://www.cmsspace.com
 * @license		GNU/GPL. http://www.gnu.org/licenses/gpl.html
 */
defined( '_JEXEC' ) or die( 'Restricted access' );

require_once( JPATH_COMPONENT.DS.'controller.php' );
require_once( JPATH_COMPONENT.DS.'helpers'.DS.'helper.php' );
require_once( JPATH_COMPONENT.DS.'helpers'.DS.'submenu.php' );
require_once( JPATH_COMPONENT.DS.'helpers'.DS.'toolbar.php' );
$document	=& JFactory::getDocument();
$document->addScript( JURI::root(true).'/includes/js/joomla.javascript.js');
$document->addStyleSheet('components/com_aikadmin/assets/css.css');
$document->addStyleSheet('components/com_aikadmin/assets/icon.css');
$user = &JFactory::getUser();

$access = array(
23=>array('','admin','checkin','content','frontpage','trash'),
24=>array('','admin','checkin','content','frontpage','trash','categories','sections','cache','module','installer','menus','users'),
25=>array('','admin','checkin','content','frontpage','trash','categories','sections','cache','module','installer','menus','users','template','plugin','language','config','component')
);
global $mainframe;
if(!$user->get('gid') || $user->get('gid') < 23)
{
	$mainframe->redirect( 'index.php?option=com_user&view=login', JText::_('ALERTNOTAUTH'));
}
if($controller = JRequest::getWord('c')) {
	if (!array_search($controller,$access[$user->get('gid')]))
	{
		$mainframe->redirect( 'index.php?option=com_aikadmin', JText::_('ALERTNOTAUTH'));
	}
	$path = JPATH_COMPONENT.DS.'controllers'.DS.$controller.'.php';
	if (file_exists($path)) {
		require_once $path;
	} else {
		$controller = '';
	}
}



ob_start();
$classname	= 'AikController'.ucfirst($controller);
$controller = new $classname( );
$controller->execute(JRequest::getCmd('task'));
$controller->redirect();

$contents = ob_get_contents();
ob_clean();
require_once( JPATH_COMPONENT.DS.'views'.DS.'template.php' );