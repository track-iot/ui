<?php
/**
 * @version		$Id: admin.php 1.0 19-11-2009 Danijar
 * @package		Frontend Admin
 * @copyright	Copyright (C) 2009 CMSSpace. http://www.cmsspace.com
 * @license		GNU/GPL. http://www.gnu.org/licenses/gpl.html
 */
jimport( 'joomla.application.component.controller' );
class AikControllerTrash extends JController
{
	function __construct()
	{
		if ( JRequest::getCmd( 'return', 'viewContent', 'post' ) == 'viewMenu' ) {
			parent::__construct(array('default_task' => 'viewTrashMenu'));
		} else {
			parent::__construct(array('default_task' => 'viewTrashContent'));
		}
		$this->registerTask( 'deleteconfirm', 'viewdeleteTrash' );
		$this->registerTask( 'delete', 'deleteTrash' );
		$this->registerTask( 'restoreconfirm', 'viewrestoreTrash' );
		$this->registerTask( 'restore', 'restoreTrash' );
		$this->registerTask( 'viewMenu', 'viewTrashMenu' );
		$this->registerTask( 'viewContent', 'viewTrashContent' );
	}
	function viewTrashContent( )
	{
		global $mainframe;
	
		$text = ': <small><small>['. JText::_( 'Articles' ) .']</small></small>';
		JToolBarHelper::title( JText::_( 'Trash Manager' ) . $text, 'trash.png' );
		JToolBarHelper::custom('restoreconfirm','restore.png','restore_f2.png', 'Restore', true);
		JToolBarHelper::custom('deleteconfirm','delete.png','delete_f2.png', 'Delete', true);
		JToolBarHelper::help( 'screen.trashmanager' );
		JToolBarHelper::cpanel();
		
		$option		= JRequest::getCmd( 'option');
		$db					=& JFactory::getDBO();
		$filter_order		= $mainframe->getUserStateFromRequest( "com_trash.viewContent.filter_order",		'filter_order',		'sectname', 'cmd' );
		$filter_order_Dir	= $mainframe->getUserStateFromRequest( "com_trash.viewContent.filter_order_Dir",	'filter_order_Dir',	'',			'word' );
		$search				= $mainframe->getUserStateFromRequest( "com_trash.search",						'search', 			'',			'string' );
		$search				= JString::strtolower( $search );
	
		$limit		= $mainframe->getUserStateFromRequest( 'global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int' );
		$limitstart = $mainframe->getUserStateFromRequest( 'com_trash.limitstart', 'limitstart', 0, 'int' );
	
		$where[] = 'c.state = -2';
	
		if ($search) {
			$where[] = 'LOWER(c.title) LIKE '.$db->Quote( '%'.$db->getEscaped( $search, true ).'%', false );
		}
	
		$where 		= ( count( $where ) ? ' WHERE ' . implode( ' AND ', $where ) : '' );
		$orderby = ' ORDER BY '. $filter_order .' '. $filter_order_Dir .', s.name, cc.name, c.title';
	
		// get the total number of content
		$query = 'SELECT count(c.id)'
		. ' FROM #__content AS c'
		. ' LEFT JOIN #__categories AS cc ON cc.id = c.catid'
		. ' LEFT JOIN #__sections AS s ON s.id = cc.section AND s.scope = "content"'
		. ' LEFT JOIN #__groups AS g ON g.id = c.access'
		. ' LEFT JOIN #__users AS u ON u.id = c.checked_out'
		. $where
		;
		$db->setQuery( $query );
		$total = $db->loadResult();
	
		jimport('joomla.html.pagination');
		$pageNav = new JPagination( $total, $limitstart, $limit );
	
		// Query articles
		$query = 'SELECT c.title, c.id, c.sectionid, c.catid, g.name AS groupname, cc.title AS catname, s.title AS sectname'
		. ' FROM #__content AS c'
		. ' LEFT JOIN #__categories AS cc ON cc.id = c.catid'
		. ' LEFT JOIN #__sections AS s ON s.id = cc.section AND s.scope="content"'
		. ' LEFT JOIN #__groups AS g ON g.id = c.access'
		. ' LEFT JOIN #__users AS u ON u.id = c.checked_out'
		. $where
		. $orderby
		;
		$db->setQuery( $query, $pageNav->limitstart, $pageNav->limit );
		$contents = $db->loadObjectList();
	
		for ( $i = 0; $i < count($contents); $i++ ) {
			if ( ( $contents[$i]->sectionid == 0 ) && ( $contents[$i]->catid == 0 ) ) {
				$contents[$i]->sectname = JText::_('UNCATEGORIZED');
			}
		}
		$lists['order_Dir']	= $filter_order_Dir;
		$lists['order']		= $filter_order;
		$lists['search']= $search;
	
		$view =& $this->getView( 'trash' );
		$view->showListContent( $option, $contents, $pageNav, $lists );
	}
	function viewTrashMenu( )
	{
		global $mainframe;
	
		$task	= JRequest::getCmd('task', 'viewMenu');
		$return	= JRequest::getCmd('return', 'viewContent', 'post');

		$text = ': <small><small>['. JText::_( 'Menu Items' ) .']</small></small>';
		JToolBarHelper::title( JText::_( 'Trash Manager' ) . $text, 'trash.png' );
		JToolBarHelper::custom('restoreconfirm','restore.png','restore_f2.png', 'Restore', true);
		JToolBarHelper::custom('deleteconfirm','delete.png','delete_f2.png', 'Delete', true);
		JToolBarHelper::help( 'screen.trashmanager' );
		JToolBarHelper::cpanel();
		
		$option		= JRequest::getCmd( 'option');
		$db					=& JFactory::getDBO();
		$filter_order		= $mainframe->getUserStateFromRequest( "com_trash.viewMenu.filter_order",		'filter_order',		'm.menutype',	'cmd' );
		$filter_order_Dir	= $mainframe->getUserStateFromRequest( "com_trash.viewMenu.filter_order_Dir",	'filter_order_Dir',	'',				'word' );
		$limit				= $mainframe->getUserStateFromRequest( "limit",								'limit',			$mainframe->getCfg('list_limit'), 'int' );
		$limitstart 		= $mainframe->getUserStateFromRequest( "com_trash.viewMenu.limitstart",		'limitstart', 		0,				'int' );
		$search				= $mainframe->getUserStateFromRequest( "com_trash.search",					'search',			'',				'string' );
		$search				= JString::strtolower( $search );
	
		$where[] = 'm.published = -2';
	
		if ($search) {
			$where[] = 'LOWER(m.name) LIKE '.$db->Quote( '%'.$db->getEscaped( $search, true ).'%', false );
		}
	
		$where 		= ( count( $where ) ? ' WHERE ' . implode( ' AND ', $where ) : '' );
		$orderby 	= ' ORDER BY '. $filter_order . ' ' . $filter_order_Dir .', m.menutype, m.ordering, m.ordering,  m.name';
	
		$query = 'SELECT count(*)'
		. ' FROM #__menu AS m'
		. ' LEFT JOIN #__users AS u ON u.id = m.checked_out'
		. $where
		;
		$db->setQuery( $query );
		$total = $db->loadResult();
	
		jimport('joomla.html.pagination');
		$pageNav = new JPagination( $total, $limitstart, $limit );
		$query = 'SELECT m.name, m.id, m.menutype, m.type, com.name AS com_name'
		. ' FROM #__menu AS m'
		. ' LEFT JOIN #__users AS u ON u.id = m.checked_out'
		. ' LEFT JOIN #__components AS com ON com.id = m.componentid AND m.type = "components"'
		. $where
		. $orderby
		;
		$db->setQuery( $query, $pageNav->limitstart, $pageNav->limit );
		$menus = $db->loadObjectList();
	
		$lists['order_Dir']	= $filter_order_Dir;
		$lists['order']		= $filter_order;
		$lists['search']= $search;
	
		$view =& $this->getView( 'trash' );
		$view->showListMenu( $option, $menus, $pageNav, $lists );
	}
	function viewdeleteTrash()
	{
		global $mainframe;
		$option		= JRequest::getCmd( 'option');
		
		JToolBarHelper::title( JText::_( 'Delete Items' ), 'delete_f2.png' );
		JToolBarHelper::cancel();
		JToolBarHelper::cpanel();
		
		$db =& JFactory::getDBO();
		$return = JRequest::getCmd( 'return', 'viewContent', 'post' );
	
		$cid = JRequest::getVar( 'cid', array(0), 'post', 'array' );
		$mid = JRequest::getVar( 'mid', array(0), 'post', 'array' );
		
		JArrayHelper::toInteger($cid, array(0));
		JArrayHelper::toInteger($mid, array(0));
	
		$cids = implode( ',', $cid );
		$mids = implode( ',', $mid );
	
		if ( $cids ) {
			$query = 	'SELECT a.title AS name'
			. ' FROM #__content AS a'
			. ' WHERE ( a.id IN ( '.$cids.' ) )'
			. ' ORDER BY a.title'
			;
			$db->setQuery( $query );
			$items = $db->loadObjectList();
			$id = $cid;
			$type = "content";
		} else if ( $mids ) {
			$query = 	'SELECT a.name'
			. ' FROM #__menu AS a'
			. ' WHERE ( a.id IN ( '.$mids.' ) )'
			. ' ORDER BY a.name'
			;
			$db->setQuery( $query );
			$items = $db->loadObjectList();
			$id = $mid;
			$type = "menu";
		}
	
		$view =& $this->getView( 'trash' );
		$view->showDelete( $option, $id, $items, $type, $return );
	}
	function deleteTrash()
	{
		global $mainframe;
	
		$option		= JRequest::getCmd( 'option');
		JRequest::checkToken() or jexit( 'Invalid Token' );
	
		$db		=& JFactory::getDBO();
		$return	= JRequest::getCmd( 'return', 'viewContent', 'post' );
		$type	= JRequest::getCmd( 'type', '', 'post' );
	
		$cid = JRequest::getVar( 'cid', array(0), 'post', 'array' );

		JArrayHelper::toInteger($cid, array(0));
		$total = count( $cid );
		if ( $type == 'content' )
		{
			$obj =& JTable::getInstance('content');
	
			require_once (JPATH_COMPONENT.DS.'tables'.DS.'frontpage.php');
			$fp = new TableFrontPage( $db );
			foreach ( $cid as $id ) {
				$id = intval( $id );
				$obj->delete( $id );
				$fp->delete( $id );
			}
		} else if ( $type == "menu" ) {
			$obj =& JTable::getInstance('menu');
			foreach ( $cid as $id ) {
				$id = intval( $id );
				$obj->delete( $id );
			}
		}
	
		$msg = JText::sprintf( 'Item(s) successfully Deleted', $total );
		$mainframe->redirect( 'index.php?option='.$option.'&task='.$return, $msg );
	}
	function viewrestoreTrash() {
		global $mainframe;
	
		$option		= JRequest::getCmd( 'option');
		
		JToolBarHelper::title( JText::_( 'Restore Items' ), 'restoredb.png' );
		JToolBarHelper::cancel();
		JToolBarHelper::cpanel();
		
		$db		=& JFactory::getDBO();
		$return = JRequest::getCmd( 'return', 'viewContent', 'post' );
	
		$cid = JRequest::getVar( 'cid', array(0), 'post', 'array' );
		$mid = JRequest::getVar( 'mid', array(0), 'post', 'array' );
		
		JArrayHelper::toInteger($cid, array(0));
		JArrayHelper::toInteger($mid, array(0));
	
		$cids = implode( ',', $cid );
		$mids = implode( ',', $mid );
	
		if ( $cids ) {
			$query = 'SELECT a.title AS name'
			. ' FROM #__content AS a'
			. ' WHERE ( a.id IN ( '.$cids.' ) )'
			. ' ORDER BY a.title'
			;
			$db->setQuery( $query );
			$items = $db->loadObjectList();
			$id = $cid;
			$type = "content";
		} else if ( $mids ) {
			$query = 'SELECT a.name'
			. ' FROM #__menu AS a'
			. ' WHERE ( a.id IN ( '.$mids.' ) )'
			. ' ORDER BY a.name'
			;
			$db->setQuery( $query );
			$items = $db->loadObjectList();
			$id = $mid;
			$type = "menu";
		}
		$view =& $this->getView( 'trash' );
		$view->showRestore( $option, $id, $items, $type, $return );
	}
	function restoreTrash()
	{
		global $mainframe;
	
		$option		= JRequest::getCmd( 'option');
		JRequest::checkToken() or jexit( 'Invalid Token' );
	
		$db		= & JFactory::getDBO();
		$type	= JRequest::getCmd( 'type', '', 'post' );
		$cid = JRequest::getVar( 'cid', array(0), 'post', 'array' );
		JArrayHelper::toInteger($cid, array(0));

		$total = count( $cid );
		$state 		= 0;
		$ordering 	= 9999;
	
		if ( $type == 'content' ) {
			$return = 'viewContent';
			JArrayHelper::toInteger($cid, array(0));
			$cids = implode( ',', $cid );
	
			$query = 'UPDATE #__content'
			. ' SET state = '.(int) $state.', ordering = '.(int) $ordering
			. ' WHERE id IN ( '.$cids.' )'
			;
			$db->setQuery( $query );
			if ( !$db->query() ) {
				JError::raiseError(500, $db->getErrorMsg() );
			}
		} else if ( $type == 'menu' ) {
			$return = 'viewMenu';
	
			jimport('joomla.application.component.model');
			JModel::addIncludePath(JPATH_BASE.DS.'components'.DS.'com_menus'.DS.'models');
			$model =& JModel::getInstance('List', 'MenusModel');
			$total = $model->fromTrash($cid);
	
			if (!$total) {
				JError::raiseError(500, $db->getErrorMsg() );
			}
		}
	
		$msg = JText::sprintf( 'Item(s) successfully Restored', $total );
		$mainframe->redirect( 'index.php?option='.$option.'&task='.$return, $msg );
	}
}