<?php
/**
 * @version		$Id: cache.php 1.0 19-11-2009 Danijar
 * @package		Frontend Admin
 * @copyright	Copyright (C) 2009 CMSSpace. http://www.cmsspace.com
 * @license		GNU/GPL. http://www.gnu.org/licenses/gpl.html
 */
defined('_JEXEC') or die( 'Restricted access' );
jimport( 'joomla.application.component.controller' );
include_once(dirname(__FILE__).DS.'..'.DS.'helpers'.DS.'cache.class.php');
class AikControllerCache extends JController
{
	function __construct()
	{
		parent::__construct(array('default_task' => 'showCache'));
	}
	function showCache()
	{
		global $mainframe, $option;
		$submenu = JRequest::getVar('client', '0', '', 'int');
		$client	 =& JApplicationHelper::getClientInfo($submenu);
		if ($submenu == 1) {
			JSubMenuHelper::addEntry(JText::_('Site'), 'index.php?option=com_aikadmin&c=cache&client=0');
			JSubMenuHelper::addEntry(JText::_('Administrator'), 'index.php?option=com_aikadmin&c=cache&client=1', true);
		} else {
			JSubMenuHelper::addEntry(JText::_('Site'), 'index.php?option=com_aikadmin&c=cache&client=0', true);
			JSubMenuHelper::addEntry(JText::_('Administrator'), 'index.php?option=com_aikadmin&c=cache&client=1');
		}
		
		JToolBarHelper::title( JText::_( 'Cache Manager - Clean Cache Admin' ), 'checkin.png' );
		JToolBarHelper::custom( 'delete', 'delete.png', 'delete_f2.png', 'Delete', true );
		JToolBarHelper::help( 'screen.cache' );
		JToolBarHelper::cpanel();

		$limit		= $mainframe->getUserStateFromRequest( 'global.list.limit', 'limit', $mainframe->getCfg('list_limit'));
		$limitstart = $mainframe->getUserStateFromRequest( 'com_cache.limitstart', 'limitstart', 0 );

		$cmData = new CacheData($client->path.DS.'cache');

		jimport('joomla.html.pagination');
		$pageNav = new JPagination( $cmData->getGroupCount(), $limitstart, $limit );

		$view =& $this->getView( 'cache' );
		$view->displayCache( $cmData->getRows( $limitstart, $limit ), $client, $pageNav );
	}

	function delete()
	{
		$cid = JRequest::getVar( 'cid', array(0), 'post', 'array' );
		JRequest::checkToken() or jexit( 'Invalid Token' );

		$client	=& JApplicationHelper::getClientInfo(JRequest::getVar('client', '0', '', 'int'));

		$cmData = new CacheData($client->path.DS.'cache');
		$cmData->cleanCacheList( $cid );
		$this->showCache();
	}
	function purgeadmin()
	{	
		JToolBarHelper::title( JText::_( 'Cache Manager - Purge Cache Admin' ), 'checkin.png' );
		JToolBarHelper::custom( 'purge', 'delete.png', 'delete_f2.png', 'Purge expired', false );
		JToolBarHelper::help( 'screen.cache' );
		JToolBarHelper::cpanel();
		
		$view =& $this->getView( 'cache' );
		$view->showPurgeExecute();
	}
	function purge()
	{	
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$cache =& JFactory::getCache('');
		$cache->gc();
		CacheView::purgeSuccess();
	}
}
