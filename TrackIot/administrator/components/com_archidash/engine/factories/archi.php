<?php


defined('_JEXEC') or die('Restricted Access');
jimport('joomla.application.component.model');
require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'engine'.DS.'factories'.DS.'factory.php');


/**
 * The following class create the archi modules
 * @version 1.0
 * @author  TobyTools.com (email:info@tobytools.com)(website: www.tobytools.com)
 * @copyright Copyright TobyTools.com 
 * @license GNU/GPL
 */


class ArchiDashFactoryArchi extends ArchiDashFactory{
	
	/*
	 *class constructor 
	 *$opts must be for example
	 *	$opts['dashboard']="STANDARD"
	 *	$opts['dashboardid']=1
	 *
	 *
	 */
	function ArchiDashFactoryArchi($opts){
		parent::__construct($opts);
		//check everything is allright
		if($this->completed){
			
			$this->options['table']="#__modules";
			$this->options['tag']="module";		
		}
		else{
			global $mainframe;
			$mainframe->enqueueMessage(JText::_('NOOBJECT')." ".get_class($this),'error');
		}
				
	}
	/*
	 * create all the defined archi modules for the specified dashboard
	 */
	
	function create(){
		if($this->completed){
			$this->msg="<h2>".JText::_('Archimede')." ".JText::_('Modules')."</h2>";
			/*Archimede MODULES CREATION*/
			//find the list of modules for the specified dashboard
			$num = (int)$this->readDefinition($this->options['tag'],false,false,true);
			//foreach name
			for ($i=1; $i<=$num; $i++){
	
				//create the module by reading the definition
				$tmp = array();
					$tmp['name']=$this->readDefinition($this->options['tag'],$i,"name");
					$tmp['position']=$this->readDefinition($this->options['tag'],$i,"position");
					$tmp['type']=$this->readDefinition($this->options['tag'],$i,"type");
					$tmp['moddsname']=$this->readDefinition($this->options['tag'],$i,"datasourcename");
					$tmp['moddstype']=$this->readDefinition($this->options['tag'],$i,"datasourcetype");
					$tmp['params']=$this->readDefinition($this->options['tag'],$i);
					
				//create the module
				$id = $this->insertModule($tmp);
				//if some error then skip to next one
				if($this->completed==false){
					continue;
				}
				
				//save the id in #_archidash_archies table
				require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'models'.DS.'archi.php');
				$classname="ArchiDashModelArchi";
				$model = new $classname();
				$vet = array();
					$vet['moduleid']=$id;
					$vet['dashboardid']=$this->options['dashboardid'];
	
				$model->insertRecord($vet);
				//check there are errors for the creation of record with the model
				if($model->getErrMsg()!=false){
					$this->completed=false;
					$this->errMsg.=$model->getErrMsg();
				}
				
				//message
				$this->msg.=$tmp['name']."<br/>";			
			}
		}
		else{
			//no op
		}
	}
	
	/**
	 * create a new module archisimpletable
	 * @param $title
	 * @param $position
	 * @param $params
	 * @param $datasourceName
	 * @param $datasourceType
	 * @return unknown_type
	 */
	protected function insertModule($vet){
		global $mainframe;
		
		//check the module definition is valid
		$config = parse_ini_file("config.ini");
		$acceptedModuleTypes = $config['module'];
		
		//check module values
		if(	strcasecmp(trim($vet['name']),"")==0 ||
			strcasecmp(trim($vet['position']),"")==0 ||
			strcasecmp(trim($vet['type']),"")==0 ||
			strcasecmp(trim($vet['params']),"")==0 ||
			strcasecmp(trim($vet['moddsname']),"")==0 ||
			strcasecmp(trim($vet['moddstype']),"")==0 
		){
			
			$this->completed=false;
			$mainframe->enqueueMessage(get_class($this)."::insertModule",'error');
			$mainframe->enqueueMessage(JText::_('MODTYPENOTVALID')." <br/></br>{$vet['name']}<br/>{$vet['type']}<br/></br>",'error');
			return false;
			
		}
		
		if(in_array(strtolower($vet['type']),$acceptedModuleTypes)==false){
			$this->completed=false;
			$mainframe->enqueueMessage(get_class($this)."::insertModule",'error');
			$mainframe->enqueueMessage(JText::_('MODTYPENOTVALID')." <br/></br>{$vet['name']}<br/>{$vet['type']}<br/></br>",'error');
			return false;
		} 
		
		
		/*FIND DATASOURCE ID*/
		//find the right table
		$vet['targetTable']="";
		if(strcasecmp(trim($vet['moddstype']),"SQL")==0)$vet['targetTable']="#__arc_datasource_sql";
		if(strcasecmp(trim($vet['moddstype']),"HTML")==0)$vet['targetTable']="#__arc_datasource_html";
		if(strcasecmp(trim($vet['moddstype']),"Snippet")==0)$vet['targetTable']="#__arc_datasource_snippet";
		if(strcasecmp(trim($vet['moddstype']),"Article")==0)$vet['targetTable']="#__arc_datasource_article";
		
		
		$db =& JFactory::getDBO();
			
		//to avoid problem with character set
		$db->setQuery($this->options['characterset']);
		$db->query();
		
		$query='SELECT * FROM '.$db->nameQuote($vet['targetTable']). ' WHERE '.$db->nameQuote('name').' = '.$db->Quote($vet['moddsname']);
		$db->setQuery($query);
		$com = $db->loadAssoc();
		$datasourceID = $com['id'];
		
		if($datasourceID==false || strcasecmp(trim($datasourceID),"")==0){
			$this->completed=false;
			$this->errMsg.=get_class($this)."::insertModule ";
			$this->errMsg.=JText::_('DSNAMENOTVALID').": {$vet['moddsname']} {$vet['moddstype']}";
			return false;
		}
		
		//add the datasource id to params
		$vet['params'].="\ndatasourceid=".$datasourceID;
		
		/***************/
		
		$query="
			INSERT INTO {$this->options['table']}(title, position,params, published, module, access, showtitle)
			VALUES
				('{$vet['name']}','{$vet['position']}','{$vet['params']}', '1', '{$vet['type']}', '{$this->options['accesslevel']}','1')
		";
		
				
		$db->setQuery($query);
		$db->query();
		$result = $db->insertid();
		if(!$result){	
			//something wrong
			$this->completed=false;
			$this->errMsg .= $db->getErrorNum()." : ".$db->getErrorMsg();
			$mainframe->enqueueMessage(JText::_( 'QUERYKO' ),'error');
			$mainframe->enqueueMessage($this->errMsg,'error');
			$mainframe->enqueueMessage(get_class($this)."::insertArchiSimpleTable",'error');
			return false;
		}
		else{
			//add #__modules_menu entry to put the menu visible on every page
			
			$query="
				INSERT INTO #__modules_menu(moduleid, menuid)
				VALUES
					('$result','0')
			";
			
					
			$db->setQuery($query);
			$db->query();
			$this->errMsg .= false;
			return $result;	
		
		}
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
	
	


?>



