<?php
defined( '_JEXEC' ) or die( 'Restricted access' );



/**
 * Browse and Read the definition file and provide the associated information.
 * 
 * @version 1.0
 * @author  TobyTools.com (email:info@tobytools.com)(website: www.tobytools.com)
 * @copyright Copyright TobyTools.com 
 * @license GNU/GPL
 */
class ArchiDashDefinitionReader {

	/**
	 * 
	 * @var string, the file name of definition that should be read (e.g. standard.xml) 
	 */
	private $definition;
	

	/**
	 * 
	 * @var string, the $xml of $definition file
	 */
	private $xml;
	
	/**
	 * 
	 * @var string, the path where you can find the xml definition files. 
	 */
	private $path;
	
	/**
	 * 
	 * @param $def, the name of definition file (e.g standard.xml)
	 * 
	 */
	function ArchiDashDefinitionReader($def=false){
		$this->definition=$def;
		$this->path = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'engine'.DS.'definitions'.DS;
		//check the file exist
		if(file_exists($this->path.$this->definition)){
			$tmp = implode('',@file($this->path.$this->definition));
			$this->xml = $this->convertToHTMLEntities($tmp);			
		}
		else{
			//file does not exist
			$this->definition = false;
			$this->xml=false;
			$this->path=false;
		}
		
	}
	
	/**
	 * 
	 * provide the value of a specific tag (or attribute)
	 * 
	 * @param $tag, the tag you wish to read
	 * @param $index, the tag are always at least 1. With index you specify which tag you wish to read (first=1, second=2, etc...)
	 * @param $attribute, if you wish to read a specific attribut of a tag
	 * @return unknown_type
	 */
	public function readDefinition($tag=false,$index=1,$attribute=false,$nested=false){
		if($this->xml && $tag){
			
			//define the index inside the xpath query. $index always >0
			if($index<=0)$index=1;
			$index = "[$index]";
			//define the attribute (if any) inside the xpath query
			if($attribute)$attribute="/@$attribute";
			else $attribute="";
			
			//define the nested tag (if any) inside the xpath query
			if($nested)$nested="/$nested";
			else $nested="";
			
			
			$doc = new DOMDocument();
			$doc->formatOutput = true;
			$doc->preserveWhiteSpace = false;				
			@$doc->loadXML($this->xml);	//load html-entities			
			$xpath = new DOMXPath($doc);
			
			
			
			$tmp = $xpath->query("//$tag$index$nested$attribute");
			$value = false;
			if($tmp) $value =  $tmp->item(0)->nodeValue;//only one element
			return $this->convertToUTF8($value);//provide the value as UTF-8
		}
		else{
			return false;
		}
	}
	/**
	 * 
	 * @param  $tag
	 * @return how many occurences of $tag are present inside the definition
	 */
	public function getLength($tag=false){
		if($this->xml && $tag){
			$doc = new DOMDocument();
			$doc->formatOutput = true;
			$doc->preserveWhiteSpace = false;				
			@$doc->loadXML($this->xml);	//load html-entities			
			$xpath = new DOMXPath($doc);
			$value =  $xpath->query("//$tag")->length;//only one attribute of $attribute
			return $value;
		}
		else{
			return 0;
		}
	}	

	
	
	/**
	 * This method is useful to convert the string in a format which is browser friendly
	 * 
	 * @param a string
	 * @return a string composed by html entities
	 */
	public function convertToHTMLEntities($string=false,$htmlSpecialChar=false){
		if(!$string){
			return false;
		}
		else{
			//strip the slashes if any
			$string = stripslashes($string);
			
			if($htmlSpecialChar)$string = htmlspecialchars($string);/*added because there are problem with <>& when you write an xml document*/
			
			//$htmlentities = mb_convert_encoding($string, 'HTML-ENTITIES', mb_detect_encoding($string));
			$htmlentities = mb_convert_encoding($string, 'UTF-8', mb_detect_encoding($string));
			
			
			
			return trim($htmlentities);
		}
	}
	
	/**
	 * To convert html-entities string in a utf-8 compliant string
	 *   
	 * @param html-entities $string
	 * @return an utf-8 string
	 */
	public function convertToUTF8($string=false){
		if(!$string){
			return false;
		}
		else{
			
			
			
			//$utf8 = mb_convert_encoding($string, "UTF-8",'HTML-ENTITIES');
			$utf8 = mb_convert_encoding($string, "UTF-8",mb_detect_encoding($string));
			//return trim($utf8);
			//add slashes
			return addslashes(trim($utf8));
		}
	}
	
	/**
	 * 
	 * @return the xml definition
	 */
	public function getDefinitionXML(){
		return $this->xml;
	}
	
	
	
	
}
