<?php
/**
 * @version		$Id: template.php 1.0 19-11-2009 Danijar
 * @package		Frontend Admin
 * @copyright	Copyright (C) 2009 CMSSpace. http://www.cmsspace.com
 * @license		GNU/GPL. http://www.gnu.org/licenses/gpl.html
 */
?>
<div class="padding" style="padding: 10px 10px 0 10px;">
	<div id="toolbar-box">
		<div class="t">
			<div class="t">
				<div class="t"></div>
			</div>
		</div>
		<div class="m">
			<?php 
			//toolbar
			jimport('joomla.html.toolbar');
			$bar = & JToolBar::getInstance('toolbar');
			echo $bar->render('toolbar');
			
			//title
			$title = $mainframe->get('JComponentTitle');
			if (!empty($title)) {
				echo $title;
			}
			?>
			<div class="clr"></div>
		</div>
		<div class="b">
			<div class="b">
				<div class="b"></div>
			</div>
		</div>
	</div>
	<div class="clr"></div>
	<?php if(AikAdminSubMenu::enable()) { ?>
	<div id="submenu-box">
		<div class="t">
			<div class="t">
				<div class="t"></div>
			</div>
 		</div>
		<div class="m">
			<?php echo AikAdminSubMenu::get();?>
			<div class="clr"></div>
		</div>
		<div class="b">
			<div class="b">
	 			<div class="b"></div>
			</div>
		</div>
	</div>
	<?php } ?>
	<div id="element-box">
		<div class="t">
			<div class="t">
				<div class="t"></div>
			</div>
		</div>
		<div class="m">
			<?php echo $contents;?>
			<div class="clr"></div>
		</div>
		<div class="b">
			<div class="b">
				<div class="b"></div>
			</div>
		</div>
	</div>
</div>