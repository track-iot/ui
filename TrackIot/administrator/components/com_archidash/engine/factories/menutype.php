<?php


defined('_JEXEC') or die('Restricted Access');
jimport('joomla.application.component.model');
require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'engine'.DS.'factories'.DS.'factory.php');


/**
 * The following class create the menutype 
 * @version 1.0
 * @author  TobyTools.com (email:info@tobytools.com)(website: www.tobytools.com)
 * @copyright Copyright TobyTools.com 
 * @license GNU/GPL
 */


class ArchiDashFactoryMenuType extends ArchiDashFactory{
	
	/*
	 *class constructor 
	 */
	function ArchiDashFactoryMenuType($opts){
		parent::__construct($opts);
		//check everything is allright
		if($this->completed){			
			$this->options['table']="#__menu_types";
			$this->options['tag']="menu";		
		}
		else{
			global $mainframe;
			$mainframe->enqueueMessage(JText::_('NOOBJECT')." ".get_class($this),'error');
		}
	}
	/*
	 * create  menutype if it does not exist
	 */
	function create(){
		if($this->completed){
			global $mainframe;
			/*check if the menutype already exists.*/
			$db =& JFactory::getDBO();
				
			//to avoid problem with character set
			$db->setQuery($this->options['characterset']);
			$db->query();
			
			//menutype title
			$mtTitle = $this->readDefinition($this->options['tag'],1,"name"); 
			//menu type attribute
			$mtMT = $this->readDefinition($this->options['tag'],1,"menutype");
			//menutype description
			$mtDescription = $this->readDefinition($this->options['tag'],1,"description");
			
			//validation check
			if(strcasecmp(trim($mtTitle),"")==0 || strcasecmp(trim($mtMT),"")==0 || strcasecmp(trim($mtDescription),"")==0 ){
				$this->completed=false;
				$this->errMsg.=get_class($this)."::create ".JText::_('DEFINITIONNOTVALID');
				$mainframe->enqueueMessage($this->errMsg,'error');
				return false;
			}
			
			
			$query='SELECT * FROM '.$db->nameQuote($this->options['table']). ' WHERE '.$db->nameQuote('title').' = '.$db->Quote($mtTitle);
			$db->setQuery($query);
			$vet = $db->loadAssoc();
			if(!$vet){
				//the menutype needs to be created
				$query = "INSERT INTO ".$db->nameQuote($this->options['table'])." (menutype,title,description) VALUES ('$mtMT','$mtTitle','$mtDescription')";			
				$db->setQuery($query);
				$result = $db->query();
				
				if(!$result){
					//something wrong
					$this->completed=false;
					$this->errMsg .= $db->getErrorNum()." : ".$db->getErrorMsg();
					$mainframe->enqueueMessage(JText::_( 'QUERYKO' ),'error');
					$mainframe->enqueueMessage($this->errMsg,'error');
					$mainframe->enqueueMessage(get_class($this)."::create",'error');
					return false;
				}
				else{
					//clean
					$this->errMsg .= false;
					//provide the element id
					$db->setQuery("SELECT LAST_INSERT_ID()");				
					$this->options['menutypeid']= $db->loadResult();
				}
				
				
				
				
				
				//output message
				$this->msg.="<h3>".JText::_('Menu')."</h3>$mtTitle<br/>";			
				
			}
			else{
				//the section already exists
				$this->options['menutypeid']=$vet['id'];
				//output message
				$this->msg.="<h3>".JText::_('Menu')."</h3>***$mtTitle<br/>";
			}
			//save the information inside #__archidash_sections
			require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'models'.DS.'menutype.php');
			$classname="ArchiDashModelMenuType";
			$model = new $classname();
			$vet = array();
				$vet['menutypeid']=$this->options['menutypeid'];
				$vet['dashboardid']=$this->options['dashboardid'];
			
			$model->insertRecord($vet);
			//check there are errors for the creation of record with the model
			if($model->getErrMsg()!=false){
				$this->completed=false;
				$this->errMsg.=$model->getErrMsg();
			}
		}
		else{
			//no op	
		}
	}

}


