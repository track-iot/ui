<?php
/**
 * @version		$Id: view.php 1.0 19-11-2009 Danijar
 * @package		Frontend Admin
 * @copyright	Copyright (C) 2009 CMSSpace. http://www.cmsspace.com
 * @license		GNU/GPL. http://www.gnu.org/licenses/gpl.html
 */
defined( '_JEXEC' ) or die( 'Restricted access' );
include_once(dirname(__FILE__).DS.'..'.DS.'default'.DS.'view.php');
class AikViewInstall extends AikViewDefault
{
	function display($tpl=null)
	{
		JToolBarHelper::help( 'screen.installer' );
		$paths = new stdClass();
		$paths->first = '';

		$this->assignRef('paths', $paths);
		$this->assignRef('state', $this->get('state'));

		parent::display($tpl);
	}

}