<?php
defined('_JEXEC') or die( 'Restricted access' );
jimport('joomla.application.component.controller');
class AikControllerContent extends JController
{
	function __construct( $default = array() )
	{
		parent::__construct( array('default_task' => 'viewContent') );
		$this->registerTask( 'add', 'editContent' );
		$this->registerTask( 'new', 'editContent' );
		$this->registerTask( 'edit', 'editContent' );
		$this->registerTask( 'go2menu', 'saveContent' );
		$this->registerTask( 'go2menuitem', 'saveContent' );
		$this->registerTask( 'resethits', 'saveContent' );
		$this->registerTask( 'menulink', 'saveContent' );
		$this->registerTask( 'apply', 'saveContent' );
		$this->registerTask( 'save', 'saveContent' );
		$this->registerTask( 'remove', 'removeContent' );
		$this->registerTask( 'publish', 'changeContent' );
		$this->registerTask( 'unpublish', 'changeContent' );
		$this->registerTask( 'archive', 'changeContent' );
		$this->registerTask( 'unarchive', 'changeContent' );
		$this->registerTask( 'toggle_frontpage', 'toggleFrontPage' );
		$this->registerTask( 'cancel', 'cancelContent' );
		$this->registerTask( 'orderup', 'orderContent' );
		$this->registerTask( 'orderdown', 'orderContent' );
		$this->registerTask( 'movesect', 'moveSection' );
		$this->registerTask( 'movesectsave', 'moveSectionSave' );
		$this->registerTask( 'copy', 'copyItem' );
		$this->registerTask( 'copysave', 'copyItemSave' );
		$this->registerTask( 'accesspublic', 'accessMenu' );
		$this->registerTask( 'accessregistered', 'accessMenu' );
		$this->registerTask( 'accessspecial', 'accessMenu' );
		$this->registerTask( 'saveorder', 'saveOrder' );
		$this->registerTask( 'preview', 'previewContent' );
		$this->registerTask( 'ins_pagebreak', 'insertPagebreak' );
	}
	function element()
	{
		$model	= &$this->getModel( 'element' );
		$view	= &$this->getView( 'element');
		$view->setModel( $model, true );
		$view->display();
	}
	function viewContent()
	{
		global $mainframe;

				global $filter_state;

		JToolBarHelper::title( JText::_( 'Article Manager' ), 'article.png' );
		if ($filter_state == 'A' || $filter_state == NULL) {
			JToolBarHelper::unarchiveList();
		}
		if ($filter_state != 'A') {
			JToolBarHelper::archiveList();
		}
		JToolBarHelper::publishList();
		JToolBarHelper::unpublishList();
		JToolBarHelper::customX( 'movesect', 'move.png', 'move_f2.png', 'Move' );
		JToolBarHelper::customX( 'copy', 'copy.png', 'copy_f2.png', 'Copy' );
		JToolBarHelper::trash();
		JToolBarHelper::editListX();
		JToolBarHelper::addNewX();
		JToolBarHelper::preferences('com_content', '550');
		JToolBarHelper::help( 'screen.content' );
		JToolBarHelper::cpanel();
		// Initialize variables
		$db			=& JFactory::getDBO();
		$filter		= null;

		// Get some variables from the request
		$sectionid			= JRequest::getVar( 'sectionid', -1, '', 'int' );
		$redirect			= $sectionid;
		$option				= JRequest::getCmd( 'option' );
		$context			= 'com_content.viewcontent';
		$filter_order		= $mainframe->getUserStateFromRequest( $context.'filter_order',		'filter_order',		'',	'cmd' );
		$filter_order_Dir	= $mainframe->getUserStateFromRequest( $context.'filter_order_Dir',	'filter_order_Dir',	'',	'word' );
		$filter_state		= $mainframe->getUserStateFromRequest( $context.'filter_state',		'filter_state',		'',	'word' );
		$catid				= $mainframe->getUserStateFromRequest( $context.'catid',			'catid',			0,	'int' );
		$filter_authorid	= $mainframe->getUserStateFromRequest( $context.'filter_authorid',	'filter_authorid',	0,	'int' );
		$filter_sectionid	= $mainframe->getUserStateFromRequest( $context.'filter_sectionid',	'filter_sectionid',	-1,	'int' );
		$search				= $mainframe->getUserStateFromRequest( $context.'search',			'search',			'',	'string' );
		$search				= JString::strtolower($search);

		$limit		= $mainframe->getUserStateFromRequest('global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int');
		$limitstart	= $mainframe->getUserStateFromRequest($context.'limitstart', 'limitstart', 0, 'int');

		$limitstart = ( $limit != 0 ? (floor($limitstart / $limit) * $limit) : 0 );

		$where[] = 'c.state != -2';

		if (!$filter_order) {
			$filter_order = 'section_name';
		}
		if ($filter_order == 'c.ordering') {
			$order = ' ORDER BY section_name, cc.title, c.ordering '. $filter_order_Dir;
		} else {
			$order = ' ORDER BY '. $filter_order .' '. $filter_order_Dir .', section_name, cc.title, c.ordering';
		}
		$all = 1;

		if ($filter_sectionid >= 0) {
			$filter = ' WHERE cc.section = '. (int) $filter_sectionid;
		}
		$section->title = 'All Articles';
		$section->id = 0;

		if ($filter_sectionid >= 0) {
			$where[] = 'c.sectionid = ' . (int) $filter_sectionid;
		}
		if ($catid > 0) {
			$where[] = 'c.catid = ' . (int) $catid;
		}
		if ($filter_authorid > 0) {
			$where[] = 'c.created_by = ' . (int) $filter_authorid;
		}
		if ($filter_state) {
			if ($filter_state == 'P') {
				$where[] = 'c.state = 1';
			} else {
				if ($filter_state == 'U') {
					$where[] = 'c.state = 0';
				} else if ($filter_state == 'A') {
					$where[] = 'c.state = -1';
				} else {
					$where[] = 'c.state != -2';
				}
			}
		}
		if ($search) {
			$where[] = '(LOWER( c.title ) LIKE '.$db->Quote( '%'.$db->getEscaped( $search, true ).'%', false ) .
				' OR c.id = ' . (int) $search . ')';
		}

		$where = (count($where) ? ' WHERE '.implode(' AND ', $where) : '');

		$query = 'SELECT COUNT(*)' .
				' FROM #__content AS c' .
				' LEFT JOIN #__categories AS cc ON cc.id = c.catid' .
				' LEFT JOIN #__sections AS s ON s.id = c.sectionid' .
				$where;
		$db->setQuery($query);
		$total = $db->loadResult();


		jimport('joomla.html.pagination');
		$pagination = new JPagination($total, $limitstart, $limit);

		$query = 'SELECT c.*, g.name AS groupname, cc.title AS name, u.name AS editor, f.content_id AS frontpage, s.title AS section_name, v.name AS author' .
				' FROM #__content AS c' .
				' LEFT JOIN #__categories AS cc ON cc.id = c.catid' .
				' LEFT JOIN #__sections AS s ON s.id = c.sectionid' .
				' LEFT JOIN #__groups AS g ON g.id = c.access' .
				' LEFT JOIN #__users AS u ON u.id = c.checked_out' .
				' LEFT JOIN #__users AS v ON v.id = c.created_by' .
				' LEFT JOIN #__content_frontpage AS f ON f.content_id = c.id' .
				$where .
				$order;
		$db->setQuery($query, $pagination->limitstart, $pagination->limit);
		$rows = $db->loadObjectList();

		if ($db->getErrorNum()) {
			JError::raiseError( 500, $db->stderr() );
			return false;
		}

		$query = 'SELECT cc.id AS value, cc.title AS text, section' .
				' FROM #__categories AS cc' .
				' INNER JOIN #__sections AS s ON s.id = cc.section ' .
				$filter .
				' ORDER BY s.ordering, cc.ordering';
		$lists['catid'] = ContentHelper::filterCategory($query, $catid);

		$javascript = 'onchange="document.adminForm.submit();"';
		$lists['sectionid'] = JHTML::_('list.section', 'filter_sectionid', $filter_sectionid, $javascript);

		$query = 'SELECT c.created_by, u.name' .
				' FROM #__content AS c' .
				' INNER JOIN #__sections AS s ON s.id = c.sectionid' .
				' LEFT JOIN #__users AS u ON u.id = c.created_by' .
				' WHERE c.state <> -1' .
				' AND c.state <> -2' .
				' GROUP BY u.name' .
				' ORDER BY u.name';
		$authors[] = JHTML::_('select.option', '0', '- '.JText::_('Select Author').' -', 'created_by', 'name');
		$db->setQuery($query);
		$authors = array_merge($authors, $db->loadObjectList());
		$lists['authorid'] = JHTML::_('select.genericlist',  $authors, 'filter_authorid', 'class="inputbox" size="1" onchange="document.adminForm.submit( );"', 'created_by', 'name', $filter_authorid);

		$lists['state'] = JHTML::_('grid.state', $filter_state, 'Published', 'Unpublished', 'Archived');

		$lists['order_Dir']	= $filter_order_Dir;
		$lists['order']		= $filter_order;

		$lists['search'] = $search;

		$view =& $this->getView( 'Content' );
		$view->showContent($rows, $lists, $pagination, $redirect);
	}
	function viewArchive()
	{
		global $mainframe;
		$db						=& JFactory::getDBO();

		$sectionid				= JRequest::getVar( 'sectionid', 0, '', 'int' );
		$option					= JRequest::getCmd( 'option' );

		$filter_order			= $mainframe->getUserStateFromRequest("com_content.$sectionid.viewarchive.filter_order",		'filter_order',		'sectname',	'cmd');
		$filter_order_Dir		= $mainframe->getUserStateFromRequest("com_content.$sectionid.viewarchive.filter_order_Dir",	'filter_order_Dir',	'',			'word');
		$catid					= $mainframe->getUserStateFromRequest("com_content.$sectionid.viewarchive.catid",				'catid',			0,			'int');
		$limit					= $mainframe->getUserStateFromRequest('global.list.limit',									'limit',			$mainframe->getCfg('list_limit'), 'int');
		$limitstart				= $mainframe->getUserStateFromRequest("com_content.$sectionid.viewarchive.limitstart",			'limitstart',		0,			'int');
		$filter_authorid		= $mainframe->getUserStateFromRequest("com_content.$sectionid.viewarchive.filter_authorid",		'filter_authorid',	0,			'int');
		$filter_sectionid		= $mainframe->getUserStateFromRequest("com_content.$sectionid.viewarchive.filter_sectionid",	'filter_sectionid',	0,			'int');
		$search					= $mainframe->getUserStateFromRequest("com_content.$sectionid.viewarchive.search",				'search',			'',			'string');
		$search					= JString::strtolower($search);
		$redirect				= $sectionid;

		if ($sectionid == 0)
		{
			$where = array ('c.state 	= -1', 'c.catid	= cc.id', 'cc.section = s.id', 's.scope  	= "content"');
			$filter = ' , #__sections AS s WHERE s.id = c.section';
			$all = 1;
		}
		else
		{
			$where = array ('c.state 	= -1', 'c.catid	= cc.id', 'cc.section	= s.id', 's.scope	= "content"', 'c.sectionid= '.(int) $sectionid);
			$filter = ' WHERE section = '.$db->Quote($sectionid);
			$all = NULL;
		}
		if ($filter_sectionid > 0)
		{
			$where[] = 'c.sectionid = ' . (int) $filter_sectionid;
		}
		if ($filter_authorid > 0)
		{
			$where[] = 'c.created_by = ' . (int) $filter_authorid;
		}
		if ($catid > 0)
		{
			$where[] = 'c.catid = ' . (int) $catid;
		}
		if ($search)
		{
			$where[] = 'LOWER( c.title ) LIKE '.$db->Quote( '%'.$db->getEscaped( $search, true ).'%', false );
		}

		$filter_order_Dir = ($filter_order_Dir == 'ASC' ? 'ASC' : 'DESC');
		$orderby = ' ORDER BY '. $filter_order .' '. $filter_order_Dir .', sectname, cc.name, c.ordering';
		$where = (count($where) ? ' WHERE '.implode(' AND ', $where) : '');

		$query = 'SELECT COUNT(*)' .
				' FROM #__content AS c' .
				' LEFT JOIN #__categories AS cc ON cc.id = c.catid' .
				' LEFT JOIN #__sections AS s ON s.id = c.sectionid' .
				$where;
		$db->setQuery($query);
		$total = $db->loadResult();

		jimport('joomla.html.pagination');
		$pagination = new JPagination($total, $limitstart, $limit);

		$query = 'SELECT c.*, g.name AS groupname, cc.name, v.name AS author, s.title AS sectname' .
				' FROM #__content AS c' .
				' LEFT JOIN #__categories AS cc ON cc.id = c.catid' .
				' LEFT JOIN #__sections AS s ON s.id = c.sectionid' .
				' LEFT JOIN #__groups AS g ON g.id = c.access' .
				' LEFT JOIN #__users AS v ON v.id = c.created_by' .
				$where .
				$orderby;
		$db->setQuery($query, $pagination->limitstart, $pagination->limit);
		$rows = $db->loadObjectList();

		if ($db->getErrorNum())
		{
			JError::raiseError( 500, $db->stderr() );
			return false;
		}

		$query = 'SELECT c.id AS value, c.title AS text' .
				' FROM #__categories AS c' .
				$filter .
				' ORDER BY c.ordering';
		$lists['catid'] = ContentHelper::filterCategory($query, $catid);

		$javascript = 'onchange="document.adminForm.submit();"';
		$lists['sectionid'] = JAdminMenus::SelectSection('filter_sectionid', $filter_sectionid, $javascript);

		$section = & JTable::getInstance('section');
		$section->load($sectionid);

		$query = 'SELECT c.created_by, u.name' .
				' FROM #__content AS c' .
				' INNER JOIN #__sections AS s ON s.id = c.sectionid' .
				' LEFT JOIN #__users AS u ON u.id = c.created_by' .
				' WHERE c.state = -1' .
				' GROUP BY u.name' .
				' ORDER BY u.name';
		$db->setQuery($query);
		$authors[] = JHTML::_('select.option', '0', '- '.JText::_('Select Author').' -', 'created_by', 'name');
		$authors = array_merge($authors, $db->loadObjectList());
		$lists['authorid'] = JHTML::_('select.genericlist',  $authors, 'filter_authorid', 'class="inputbox" size="1" onchange="document.adminForm.submit( );"', 'created_by', 'name', $filter_authorid);

		$lists['order_Dir']	= $filter_order_Dir;
		$lists['order']		= $filter_order;
		$lists['search'] = $search;

		$view =& $this->getView( 'Content' );
		$view->showArchive($rows, $section, $lists, $pagination, $option, $all, $redirect);
	}

	function editContent()
	{
		global $mainframe;

		$task	= JRequest::getCmd( 'task' );
		
		if($task == 'edit')
			$edit = true;
		else 
			$edit = false;
		
		$cid = JRequest::getVar( 'cid', array(0), '', 'array' );
		$cid = intval($cid[0]);

		$text = ( $edit ? JText::_( 'Edit' ) : JText::_( 'New' ) );

		JToolBarHelper::title( JText::_( 'Article' ).': <small><small>[ '. $text.' ]</small></small>', 'addedit.png' );
		JToolBarHelper::preview( 'index.php?option=com_aikadmin&c=content&id='.$cid.'&tmpl=component', true );
		JToolBarHelper::save();
		JToolBarHelper::apply();
		if ( $edit ) {
			JToolBarHelper::cancel( 'cancel', 'Close' );
		} else {
			JToolBarHelper::cancel();
		}
		JToolBarHelper::help( 'screen.content.edit' );
		JToolBarHelper::cpanel();
		
		$db				= & JFactory::getDBO();
		$user			= & JFactory::getUser();

		$cid			= JRequest::getVar( 'cid', array(0), '', 'array' );
		JArrayHelper::toInteger($cid, array(0));
		$id				= JRequest::getVar( 'id', $cid[0], '', 'int' );
		$option			= JRequest::getCmd( 'option' );
		$nullDate		= $db->getNullDate();
		$contentSection	= '';
		$sectionid		= 0;

		$row = & JTable::getInstance('content');
		if($edit)
			$row->load($id);

		if ($id) {
			$sectionid = $row->sectionid;
			if ($row->state < 0) {
				$mainframe->redirect('index.php?option=com_aikadmin&c=content', JText::_('You cannot edit an archived item'));
			}
		}
		if ($sectionid == 0) {
			$where = ' WHERE section NOT LIKE "%com_%"';
		} else {
			$where = ' WHERE section = '. $db->Quote( $sectionid );
		}
		if ( JTable::isCheckedOut($user->get ('id'), $row->checked_out ))
		{
			$msg = JText::sprintf('DESCBEINGEDITTED', JText::_('The item'), $row->title);
			$mainframe->redirect('index.php?option=com_aikadmin&c=content', $msg);
		}

		if ($id)
		{
			$row->checkout($user->get('id'));

			if (trim($row->images)) {
				$row->images = explode("\n", $row->images);
			} else {
				$row->images = array ();
			}

			$query = 'SELECT name' .
					' FROM #__users'.
					' WHERE id = '. (int) $row->created_by;
			$db->setQuery($query);
			$row->creator = $db->loadResult();

			if ($row->created_by == $row->modified_by) {
				$row->modifier = $row->creator;
			} else {
				$query = 'SELECT name' .
						' FROM #__users' .
						' WHERE id = '. (int) $row->modified_by;
				$db->setQuery($query);
				$row->modifier = $db->loadResult();
			}

			$query = 'SELECT COUNT(content_id)' .
					' FROM #__content_frontpage' .
					' WHERE content_id = '. (int) $row->id;
			$db->setQuery($query);
			$row->frontpage = $db->loadResult();
			if (!$row->frontpage) {
				$row->frontpage = 0;
			}
		}
		else
		{
			if (!$sectionid && JRequest::getInt('filter_sectionid')) {
				$sectionid =JRequest::getInt('filter_sectionid');
			}

			if (JRequest::getInt('catid'))
			{
				$row->catid	 = JRequest::getInt('catid');
				$category 	 = & JTable::getInstance('category');
				$category->load($row->catid);
				$sectionid = $category->section;
			} else {
				$row->catid = NULL;
			}
			$createdate =& JFactory::getDate();
			$row->sectionid = $sectionid;
			$row->version = 0;
			$row->state = 1;
			$row->ordering = 0;
			$row->images = array ();
			$row->publish_up = $createdate->toUnix();
			$row->publish_down = JText::_('Never');
			$row->creator = '';
			$row->created = $createdate->toUnix();
			$row->modified = $nullDate;
			$row->modifier = '';
			$row->frontpage = 0;

		}

		$javascript = "onchange=\"changeDynaList( 'catid', sectioncategories, document.adminForm.sectionid.options[document.adminForm.sectionid.selectedIndex].value, 0, 0);\"";

		$query = 'SELECT s.id, s.title' .
				' FROM #__sections AS s' .
				' ORDER BY s.ordering';
		$db->setQuery($query);

		$sections[] = JHTML::_('select.option', '-1', '- '.JText::_('Select Section').' -', 'id', 'title');
		$sections[] = JHTML::_('select.option', '0', JText::_('Uncategorized'), 'id', 'title');
		$sections = array_merge($sections, $db->loadObjectList());
		$lists['sectionid'] = JHTML::_('select.genericlist',  $sections, 'sectionid', 'class="inputbox" size="1" '.$javascript, 'id', 'title', intval($row->sectionid));

		foreach ($sections as $section)
		{
			$section_list[] = (int) $section->id;
			if ($row->sectionid) {
				if ($section->id == $row->sectionid) {
					$contentSection = $section->title;
				}
			} else {
				if ($section->id == $sectionid) {
					$contentSection = $section->title;
				}
			}
		}

		$sectioncategories = array ();
		$sectioncategories[-1] = array ();
		$sectioncategories[-1][] = JHTML::_('select.option', '-1', JText::_( 'Select Category' ), 'id', 'title');
		$section_list = implode('\', \'', $section_list);

		$query = 'SELECT id, title, section' .
				' FROM #__categories' .
				' WHERE section IN ( \''.$section_list.'\' )' .
				' ORDER BY ordering';
		$db->setQuery($query);
		$cat_list = $db->loadObjectList();

		$uncat = new stdClass();
		$uncat->id = 0;
		$uncat->title = JText::_('Uncategorized');
		$uncat->section = 0;
		$cat_list[] = $uncat;
		foreach ($sections as $section)
		{
			$sectioncategories[$section->id] = array ();
			$rows2 = array ();
			foreach ($cat_list as $cat)
			{
				if ($cat->section == $section->id) {
					$rows2[] = $cat;
				}
			}
			foreach ($rows2 as $row2) {
				$sectioncategories[$section->id][] = JHTML::_('select.option', $row2->id, $row2->title, 'id', 'title');
			}
		}
		$sectioncategories['-1'][] = JHTML::_('select.option', '-1', JText::_( 'Select Category' ), 'id', 'title');
		$categories = array();
		foreach ($cat_list as $cat) {
			if($cat->section == $row->sectionid)
				$categories[] = $cat;
		}

		$categories[] = JHTML::_('select.option', '-1', JText::_( 'Select Category' ), 'id', 'title');
		$lists['catid'] = JHTML::_('select.genericlist',  $categories, 'catid', 'class="inputbox" size="1"', 'id', 'title', intval($row->catid));

		$query = 'SELECT ordering AS value, title AS text' .
				' FROM #__content' .
				' WHERE catid = ' . (int) $row->catid .
				' AND state >= 0' .
				' ORDER BY ordering';
		if($edit)
			$lists['ordering'] = JHTML::_('list.specificordering', $row, $id, $query, 1);
		else
			$lists['ordering'] = JHTML::_('list.specificordering', $row, '', $query, 1);

		$lists['frontpage'] = JHTML::_('select.booleanlist', 'frontpage', '', $row->frontpage);
		$lists['state'] = JHTML::_('select.booleanlist', 'state', '', $row->state);
		if (JString::strlen($row->fulltext) > 1) {
			$row->text = $row->introtext . "<hr id=\"system-readmore\" />" . $row->fulltext;
		} else {
			$row->text = $row->introtext;
		}

		$form = new JParameter('', JPATH_COMPONENT.DS.'models'.DS.'article.xml');

		$active = (intval($row->created_by) ? intval($row->created_by) : $user->get('id'));
		$form->set('created_by', $active);
		$form->set('access', $row->access);
		$form->set('created_by_alias', $row->created_by_alias);

		$form->set('created', JHTML::_('date', $row->created, '%Y-%m-%d %H:%M:%S'));
		$form->set('publish_up', JHTML::_('date', $row->publish_up, '%Y-%m-%d %H:%M:%S'));
		if (JHTML::_('date', $row->publish_down, '%Y') <= 1969 || $row->publish_down == $db->getNullDate()) {
			$form->set('publish_down', JText::_('Never'));
		} else {
			$form->set('publish_down', JHTML::_('date', $row->publish_down, '%Y-%m-%d %H:%M:%S'));
		}

		$form->loadINI($row->attribs);

		$form->set('description', $row->metadesc);
		$form->set('keywords', $row->metakey);
		$form->loadINI($row->metadata);

		$view =& $this->getView( 'Content' );
		$view->editContent($row, $contentSection, $lists, $sectioncategories, $option, $form);
	}
	function saveContent()
	{
		global $mainframe;
		JRequest::checkToken() or jexit( 'Invalid Token' );

		$db		= & JFactory::getDBO();
		$user		= & JFactory::getUser();
		$dispatcher 	= & JDispatcher::getInstance();
		JPluginHelper::importPlugin('content');

		$details	= JRequest::getVar( 'details', array(), 'post', 'array');
		$option		= JRequest::getCmd( 'option' );
		$task		= JRequest::getCmd( 'task' );
		$sectionid	= JRequest::getVar( 'sectionid', 0, '', 'int' );
		$redirect	= JRequest::getVar( 'redirect', $sectionid, 'post', 'int' );
		$menu		= JRequest::getVar( 'menu', 'mainmenu', 'post', 'cmd' );
		$menuid		= JRequest::getVar( 'menuid', 0, 'post', 'int' );
		$nullDate	= $db->getNullDate();

		$row = & JTable::getInstance('content');
		if (!$row->bind(JRequest::get('post'))) {
			JError::raiseError( 500, $db->stderr() );
			return false;
		}
		$row->bind($details);

		$row->id = (int) $row->id;

		$isNew = true;
		if ($row->id) {
			$isNew = false;
			$datenow =& JFactory::getDate();
			$row->modified 		= $datenow->toMySQL();
			$row->modified_by 	= $user->get('id');
		}

		$row->created_by 	= $row->created_by ? $row->created_by : $user->get('id');

		if ($row->created && strlen(trim( $row->created )) <= 10) {
			$row->created 	.= ' 00:00:00';
		}

		$config =& JFactory::getConfig();
		$tzoffset = $config->getValue('config.offset');
		$date =& JFactory::getDate($row->created, $tzoffset);
		$row->created = $date->toMySQL();

		if (strlen(trim($row->publish_up)) <= 10) {
			$row->publish_up .= ' 00:00:00';
		}

		$date =& JFactory::getDate($row->publish_up, $tzoffset);
		$row->publish_up = $date->toMySQL();

		if (trim($row->publish_down) == JText::_('Never') || trim( $row->publish_down ) == '')
		{
			$row->publish_down = $nullDate;
		}
		else
		{
			if (strlen(trim( $row->publish_down )) <= 10) {
				$row->publish_down .= ' 00:00:00';
			}
			$date =& JFactory::getDate($row->publish_down, $tzoffset);
			$row->publish_down = $date->toMySQL();
		}

		$row->state	= JRequest::getVar( 'state', 0, '', 'int' );
		$params		= JRequest::getVar( 'params', null, 'post', 'array' );

		if (is_array($params))
		{
			$txt = array ();
			foreach ($params as $k => $v) {
				$txt[] = "$k=$v";
			}
			$row->attribs = implode("\n", $txt);
		}
		$metadata = JRequest::getVar( 'meta', null, 'post', 'array');
		if (is_array($metadata))
		{
			$txt = array();
			foreach ($metadata as $k => $v) {
				if ($k == 'description') {
					$row->metadesc = $v;
				} elseif ($k == 'keywords') {
					$row->metakey = $v;
				} else {
					$txt[] = "$k=$v";
				}
			}
			$row->metadata = implode("\n", $txt);
		}

		ContentHelper::saveContentPrep( $row );
		if (!$row->check()) {
			JError::raiseError( 500, $db->stderr() );
			return false;
		}

		$row->version++;

		$result = $dispatcher->trigger('onBeforeContentSave', array(&$row, $isNew));
		if(in_array(false, $result, true)) {
			JError::raiseError(500, $row->getError());
			return false;
		}
		if (!$row->store()) {
			JError::raiseError( 500, $db->stderr() );
			return false;
		}

		$row->checkin();
		$row->reorder('catid = '.(int) $row->catid.' AND state >= 0');
		require_once (JPATH_COMPONENT.DS.'tables'.DS.'frontpage.php');
		$fp = new TableFrontPage($db);

		if (JRequest::getVar( 'frontpage', 0, '', 'int' ))
		{
			if (!$fp->load($row->id))
			{
				$query = 'INSERT INTO #__content_frontpage' .
						' VALUES ( '. (int) $row->id .', 1 )';
				$db->setQuery($query);
				if (!$db->query())
				{
					JError::raiseError( 500, $db->stderr() );
					return false;
				}
				$fp->ordering = 1;
			}
		}
		else
		{
			if (!$fp->delete($row->id)) {
				$msg .= $fp->stderr();
			}
			$fp->ordering = 0;
		}
		$fp->reorder();

		$cache = & JFactory::getCache('com_content');
		$cache->clean();

		$dispatcher->trigger('onAfterContentSave', array(&$row, $isNew));

		switch ($task)
		{
			case 'go2menu' :
				$mainframe->redirect('index.php?option=com_aikadmin&c=menus&menutype='.$menu);
				break;

			case 'go2menuitem' :
				$mainframe->redirect('index.php?option=com_aikadmin&c=menus&menutype='.$menu.'&task=edit&id='.$menuid);
				break;

			case 'menulink' :
				ContentHelper::menuLink($redirect, $row->id);
				break;

			case 'resethits' :
				ContentHelper::resetHits($redirect, $row->id);
				break;

			case 'apply' :
				$msg = JText::sprintf('SUCCESSFULLY SAVED CHANGES TO ARTICLE', $row->title);
				$mainframe->redirect('index.php?option=com_aikadmin&c=content&sectionid='.$redirect.'&task=edit&cid[]='.$row->id, $msg);
				break;

			case 'save' :
			default :
				$msg = JText::sprintf('Successfully Saved Article', $row->title);
				$mainframe->redirect('index.php?option=com_aikadmin&c=content&sectionid='.$redirect, $msg);
				break;
		}
	}
	function changeContent()
	{
		global $mainframe;
		$task	= JRequest::getCmd( 'task' );
		switch ($task)
		{
			case 'publish' :$state = 1;	break;
			case 'unpublish' :$state = 0;break;
			case 'archive' :$state = -1;break;
			case 'unarchive' :$state = 0;break;
		}
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$db		= & JFactory::getDBO();
		$user	= & JFactory::getUser();

		$cid	= JRequest::getVar( 'cid', array(), 'post', 'array' );
		JArrayHelper::toInteger($cid);
		$option	= JRequest::getCmd( 'option' );
		
		$rtask	= JRequest::getCmd( 'returntask', '', 'post' );
		if ($rtask) {
			$rtask = '&task='.$rtask;
		}

		if (count($cid) < 1) {
			$redirect	= JRequest::getVar( 'redirect', '', 'post', 'int' );
			$action		= ($state == 1) ? 'publish' : ($state == -1 ? 'archive' : 'unpublish');
			$msg		= JText::_('Select an item to') . ' ' . JText::_($action);
			$mainframe->redirect('index.php?option='.$option.$rtask.'&sectionid='.$redirect, $msg, 'error');
		}

		$uid	= $user->get('id');
		$total	= count($cid);
		$cids	= implode(',', $cid);

		$query = 'UPDATE #__content' .
				' SET state = '. (int) $state .
				' WHERE id IN ( '. $cids .' ) AND ( checked_out = 0 OR (checked_out = '. (int) $uid .' ) )';
		$db->setQuery($query);
		if (!$db->query()) {
			JError::raiseError( 500, $db->getErrorMsg() );
			return false;
		}

		if (count($cid) == 1) {
			$row = & JTable::getInstance('content');
			$row->checkin($cid[0]);
		}

		switch ($state)
		{
			case -1 :
				$msg = JText::sprintf('Item(s) successfully Archived', $total);
				break;

			case 1 :
				$msg = JText::sprintf('Item(s) successfully Published', $total);
				break;

			case 0 :
			default :
				if ($task == 'unarchive') {
					$msg = JText::sprintf('Item(s) successfully Unarchived', $total);
				} else {
					$msg = JText::sprintf('Item(s) successfully Unpublished', $total);
				}
				break;
		}

		$cache = & JFactory::getCache('com_content');
		$cache->clean();

		$redirect	= JRequest::getVar( 'redirect', $row->sectionid, 'post', 'int' );
		$mainframe->redirect('index.php?option='.$option.$rtask.'&sectionid='.$redirect, $msg);
	}
	function toggleFrontPage()
	{
		global $mainframe;
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$db		=& JFactory::getDBO();

		$cid	= JRequest::getVar( 'cid', array(), 'post', 'array' );
		$option	= JRequest::getCmd( 'option' );
		$msg	= null;

		JArrayHelper::toInteger($cid);

		if (count($cid) < 1) {
			$msg = JText::_('Select an item to toggle');
			$mainframe->redirect('index.php?option='.$option, $msg, 'error');
		}
		require_once (JPATH_COMPONENT.DS.'tables'.DS.'frontpage.php');
		$fp = new TableFrontPage($db);

		foreach ($cid as $id)
		{
			if ($fp->load($id)) {
				if (!$fp->delete($id)) {
					$msg .= $fp->stderr();
				}
				$fp->ordering = 0;
			} else {
				$query = 'INSERT INTO #__content_frontpage' .
						' VALUES ( '. (int) $id .', 0 )';
				$db->setQuery($query);
				if (!$db->query()) {
					JError::raiseError( 500, $db->stderr() );
					return false;
				}
				$fp->ordering = 0;
			}
			$fp->reorder();
		}

		$cache = & JFactory::getCache('com_aikadmin');
		$cache->clean();

		$mainframe->redirect('index.php?option='.$option, $msg);
	}

	function removeContent()
	{
		global $mainframe;
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$db			= & JFactory::getDBO();

		$cid		= JRequest::getVar( 'cid', array(), 'post', 'array' );
		$option		= JRequest::getCmd( 'option' );
		$return		= JRequest::getCmd( 'returntask', '', 'post' );
		$nullDate	= $db->getNullDate();

		JArrayHelper::toInteger($cid);

		if (count($cid) < 1) {
			$msg =  JText::_('Select an item to delete');
			$mainframe->redirect('index.php?option='.$option, $msg, 'error');
		}
		$state		= '-2';
		$ordering	= '0';
		$cids = implode(',', $cid);
		$query = 'UPDATE #__content' .
				' SET state = '.(int) $state .
				', ordering = '.(int) $ordering .
				', checked_out = 0, checked_out_time = '.$db->Quote($nullDate).
				' WHERE id IN ( '. $cids. ' )';
		$db->setQuery($query);
		if (!$db->query())
		{
			JError::raiseError( 500, $db->getErrorMsg() );
			return false;
		}

		$cache = & JFactory::getCache('com_aikadmin');
		$cache->clean();

		$msg = JText::sprintf('Item(s) sent to the Trash', count($cid));
		$mainframe->redirect('index.php?option='.$option.'&task='.$return, $msg);
	}
	function cancelContent()
	{
		global $mainframe;
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$db	= & JFactory::getDBO();
		$row = & JTable::getInstance('content');
		$row->bind(JRequest::get('post'));
		$row->checkin();

		$mainframe->redirect('index.php?option=com_aikadmin&c=content');
	}
	function orderContent()
	{
		global $mainframe;
		$task	= JRequest::getCmd( 'task' );
		if($task == 'orderup')
			$direction = -1;
		else 
			$direction = 1;
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$db		= & JFactory::getDBO();
		$cid	= JRequest::getVar( 'cid', array(), 'post', 'array' );

		if (isset( $cid[0] ))
		{
			$row = & JTable::getInstance('content');
			$row->load( (int) $cid[0] );
			$row->move($direction, 'catid = ' . (int) $row->catid . ' AND state >= 0' );

			$cache = & JFactory::getCache('com_aikadmin');
			$cache->clean();
		}

		$mainframe->redirect('index.php?option=com_aikadmin&c=content');
	}
	function moveSection()
	{
		JToolBarHelper::title( JText::_( 'Move Articles' ), 'move_f2.png' );
		JToolBarHelper::custom( 'movesectsave', 'save.png', 'save_f2.png', 'Save', false );
		JToolBarHelper::cancel();
		JToolBarHelper::cpanel();
		
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$db			=& JFactory::getDBO();

		$cid		= JRequest::getVar( 'cid', array(), 'post', 'array' );
		$sectionid	= JRequest::getVar( 'sectionid', 0, '', 'int' );

		JArrayHelper::toInteger($cid);

		if (count($cid) < 1) {
			$msg = JText::_('Select an item to move');
			$mainframe->redirect('index.php?option=com_aikadmin&c=content', $msg, 'error');
		}

		$cids = implode(',', $cid);
		$query = 'SELECT a.title' .
				' FROM #__content AS a' .
				' WHERE ( a.id IN ( '. $cids .' ) )' .
				' ORDER BY a.title';
		$db->setQuery($query);
		$items = $db->loadObjectList();

		$query = 'SELECT CONCAT_WS( ", ", s.id, c.id ) AS `value`, CONCAT_WS( " / ", s.title, c.title ) AS `text`' .
				' FROM #__sections AS s' .
				' INNER JOIN #__categories AS c ON c.section = s.id' .
				' WHERE s.scope = "content"' .
				' ORDER BY s.title, c.title';
		$db->setQuery($query);
		$rows[] = JHTML::_('select.option', "0, 0", JText::_('UNCATEGORIZED'));
		$rows = array_merge($rows, $db->loadObjectList());
		$sectCatList = JHTML::_('select.genericlist',  $rows, 'sectcat', 'class="inputbox" size="8"', 'value', 'text', null);

		$view =& $this->getView( 'Content' );
		$view->moveSection($cid, $sectCatList, 'com_aikadmin', $sectionid, $items);
	}
	function moveSectionSave()
	{
		global $mainframe;
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$db			= & JFactory::getDBO();
		$user		= & JFactory::getUser();

		$cid		= JRequest::getVar( 'cid', array(0), 'post', 'array' );
		$sectionid	= JRequest::getVar( 'sectionid', 0, '', 'int' );
		$option		= JRequest::getCmd( 'option' );

		JArrayHelper::toInteger($cid, array(0));

		$sectcat = JRequest::getVar( 'sectcat', '', 'post', 'string' );
		$sectcat = explode(',', $sectcat);
		$newsect = (int) @$sectcat[0];
		$newcat = (int) @$sectcat[1];

		if ((!$newsect || !$newcat) && ($sectcat !== array('0', ' 0'))) {
			$mainframe->redirect("index.php?option=com_aikadmin&c=content&sectionid=$sectionid", JText::_('An error has occurred'));
		}

		$query = 'SELECT a.title' .
				' FROM #__sections AS a' .
				' WHERE a.id = '. (int) $newsect;
		$db->setQuery($query);
		$section = $db->loadResult();

		$query = 'SELECT a.title' .
				' FROM #__categories AS a' .
				' WHERE a.id = '. (int) $newcat;
		$db->setQuery($query);
		$category = $db->loadResult();

		$total	= count($cid);
		$cids		= implode(',', $cid);
		$uid		= $user->get('id');

		$row = & JTable::getInstance('content');
		foreach ($cid as $id)
		{
			$row->load(intval($id));
			$row->ordering = 0;
			$row->store();
			$row->reorder('catid = '.(int) $row->catid.' AND state >= 0');
		}

		$query = 'UPDATE #__content SET sectionid = '.(int) $newsect.', catid = '.(int) $newcat.
				' WHERE id IN ( '.$cids.' )' .
				' AND ( checked_out = 0 OR ( checked_out = '.(int) $uid.' ) )';
		$db->setQuery($query);
		if (!$db->query())
		{
			JError::raiseError( 500, $db->getErrorMsg() );
			return false;
		}

		foreach ($cid as $id)
		{
			$row->load(intval($id));
			$row->ordering = 0;
			$row->store();
			$row->reorder('catid = '.(int) $row->catid.' AND state >= 0');
		}

		if ($section && $category) {
			$msg = JText::sprintf('Item(s) successfully moved to Section', $total, $section, $category);
		} else {
			$msg = JText::sprintf('ITEM(S) SUCCESSFULLY MOVED TO UNCATEGORIZED', $total);
		}

		$mainframe->redirect('index.php?option='.$option.'&sectionid='.$sectionid, $msg);
	}
	function copyItem()
	{
		
		JRequest::checkToken() or jexit( 'Invalid Token' );

		JToolBarHelper::title( JText::_( 'Copy Articles' ), 'copy_f2.png' );
		JToolBarHelper::custom( 'copysave', 'save.png', 'save_f2.png', 'Save', false );
		JToolBarHelper::cancel();
		JToolBarHelper::cpanel();
		
		$db			= & JFactory::getDBO();

		$cid		= JRequest::getVar( 'cid', array(), 'post', 'array' );
		$sectionid	= JRequest::getVar( 'sectionid', 0, '', 'int' );
		$option		= JRequest::getCmd( 'option' );

		JArrayHelper::toInteger($cid);

		if (count($cid) < 1) {
			$msg = JText::_('Select an item to move');
			$mainframe->redirect('index.php?option='.$option, $msg, 'error');
		}

		$cids = implode(',', $cid);
		$query = 'SELECT a.title' .
				' FROM #__content AS a' .
				' WHERE ( a.id IN ( '. $cids .' ) )' .
				' ORDER BY a.title';
		$db->setQuery($query);
		$items = $db->loadObjectList();

		$query = 'SELECT CONCAT_WS(",",s.id,c.id) AS `value`, CONCAT_WS(" / ", s.title, c.title) AS `text`' .
				' FROM #__sections AS s' .
				' INNER JOIN #__categories AS c ON c.section = s.id' .
				' WHERE s.scope = "content"' .
				' ORDER BY s.title, c.title';
		$db->setQuery($query);

		$uncat	= JHTML::_('select.option', '0,0', JText::_('UNCATEGORIZED'));
		$rows	= $db->loadObjectList();
		array_unshift($rows, $uncat);
		$sectCatList = JHTML::_('select.genericlist', $rows, 'sectcat', 'class="inputbox" size="10"', 'value', 'text', NULL);

		$view =& $this->getView( 'Content' );
		$view->copySection($option, $cid, $sectCatList, $sectionid, $items);
	}
	function copyItemSave()
	{
		global $mainframe;

		JRequest::checkToken() or jexit( 'Invalid Token' );
		$db			= & JFactory::getDBO();

		$cid		= JRequest::getVar( 'cid', array(), 'post', 'array' );
		$sectionid	= JRequest::getVar( 'sectionid', 0, '', 'int' );
		$option		= JRequest::getCmd( 'option' );

		JArrayHelper::toInteger($cid);

		$item	= null;
		$sectcat = JRequest::getVar( 'sectcat', '-1,-1', 'post', 'string' );
		$sectcat = explode(',', $sectcat);
		$newsect = (int) @$sectcat[0];
		$newcat = (int) @$sectcat[1];

		if (($newsect == -1) || ($newcat == -1)) {
			$mainframe->redirect('index.php?option=com_aikadmin&c=content&sectionid='.$sectionid, JText::_('An error has occurred'));
		}
		$query = 'SELECT a.title' .
				' FROM #__sections AS a' .
				' WHERE a.id = '. (int) $newsect;
		$db->setQuery($query);
		$section = $db->loadResult();

		$query = 'SELECT a.title' .
				' FROM #__categories AS a' .
				' WHERE a.id = '. (int) $newcat;
		$db->setQuery($query);
		$category = $db->loadResult();

		if (($newsect == 0) && ($newcat == 0))
		{
			$section	= JText::_('UNCATEGORIZED');
			$category	= JText::_('UNCATEGORIZED');
		}

		$total = count($cid);
		for ($i = 0; $i < $total; $i ++)
		{
			$row = & JTable::getInstance('content');

			$query = 'SELECT a.*' .
					' FROM #__content AS a' .
					' WHERE a.id = '.(int) $cid[$i];
			$db->setQuery($query, 0, 1);
			$item = $db->loadObject();

			$row->id						= NULL;
			$row->sectionid					= $newsect;
			$row->catid						= $newcat;
			$row->hits						= '0';
			$row->ordering					= '0';
			$row->title						= $item->title;
			$row->alias						= $item->alias;
			$row->title_alias				= $item->title_alias;
			$row->introtext					= $item->introtext;
			$row->fulltext					= $item->fulltext;
			$row->state						= $item->state;
			$row->mask						= $item->mask;
			$row->created					= $item->created;
			$row->created_by				= $item->created_by;
			$row->created_by_alias			= $item->created_by_alias;
			$row->modified					= $item->modified;
			$row->modified_by				= $item->modified_by;
			$row->checked_out				= $item->checked_out;
			$row->checked_out_time			= $item->checked_out_time;
			$row->publish_up				= $item->publish_up;
			$row->publish_down				= $item->publish_down;
			$row->images					= $item->images;
			$row->attribs					= $item->attribs;
			$row->version					= $item->parentid;
			$row->parentid					= $item->parentid;
			$row->metakey					= $item->metakey;
			$row->metadesc					= $item->metadesc;
			$row->access					= $item->access;
			$row->metadata					= $item->metadata;

			if (!$row->check()) {
				JError::raiseError( 500, $row->getError() );
				return false;
			}

			if (!$row->store()) {
				JError::raiseError( 500, $row->getError() );
				return false;
			}
			$row->reorder('catid='.(int) $row->catid.' AND state >= 0');
		}

		$msg = JText::sprintf('Item(s) successfully copied to Section', $total, $section, $category);
		$mainframe->redirect('index.php?option='.$option.'&sectionid='.$sectionid, $msg);
	}
	function accessMenu()
	{
		global $mainframe;

		$task	= JRequest::getCmd( 'task' );
		switch ($task)
		{
			case 'accesspublic' :$access = 0;	break;
			case 'accessregistered' :$access = 1;break;
			case 'accessspecial' :$access = 2;break;
		}
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$db		= & JFactory::getDBO();

		$cid	= JRequest::getVar( 'cid', array(0), 'post', 'array' );
		$option	= JRequest::getCmd( 'option' );
		$cid	= $cid[0];

		$row = & JTable::getInstance('content');
		$row->load($cid);
		$row->access = $access;

		if (!$row->check()) {
			JError::raiseError( 500, $row->getError() );
			return false;
		}

		if (!$row->store()) {
			JError::raiseError( 500, $row->getError() );
			return false;
		}

		$cache = & JFactory::getCache('com_aikadmin');
		$cache->clean();

		$mainframe->redirect('index.php?option='.$option);
	}

	function saveOrder()
	{
		global $mainframe;
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$db			= & JFactory::getDBO();

		$cid		= JRequest::getVar( 'cid', array(0), 'post', 'array' );
		$order		= JRequest::getVar( 'order', array (0), 'post', 'array' );
		$redirect	= JRequest::getVar( 'redirect', 0, 'post', 'int' );
		$rettask	= JRequest::getVar( 'returntask', '', 'post', 'cmd' );
		$total		= count($cid);
		$conditions	= array ();

		JArrayHelper::toInteger($cid, array(0));
		JArrayHelper::toInteger($order, array(0));

		$row = & JTable::getInstance('content');
		for ($i = 0; $i < $total; $i ++)
		{
			$row->load( (int) $cid[$i] );
			if ($row->ordering != $order[$i]) {
				$row->ordering = $order[$i];
				if (!$row->store()) {
					JError::raiseError( 500, $db->getErrorMsg() );
					return false;
				}
				$condition = 'catid = '.(int) $row->catid.' AND state >= 0';
				$found = false;
				foreach ($conditions as $cond)
					if ($cond[1] == $condition) {
						$found = true;
						break;
					}
				if (!$found)
					$conditions[] = array ($row->id, $condition);
			}
		}
		foreach ($conditions as $cond)
		{
			$row->load($cond[0]);
			$row->reorder($cond[1]);
		}

		$cache = & JFactory::getCache('com_aikadmin');
		$cache->clean();

		$msg = JText::_('New ordering saved');
		switch ($rettask)
		{
			case 'showarchive' :
				$mainframe->redirect('index.php?option=com_aikadmin&c=content&task=showarchive&sectionid='.$redirect, $msg);
				break;

			default :
				$mainframe->redirect('index.php?option=com_aikadmin&c=content&sectionid='.$redirect, $msg);
				break;
		}
	}

	function previewContent()
	{
		$document		=& JFactory::getDocument();
		$db 			=& JFactory::getDBO();
		$id				= JRequest::getVar( 'id', 0, '', 'int' );
		$option			= JRequest::getCmd( 'option' );
		$query = 'SELECT template' .
				' FROM #__templates_menu' .
				' WHERE client_id = 0' .
				' AND menuid = 0';
		$db->setQuery($query);
		$template = $db->loadResult();

		if (!file_exists( JPATH_SITE.DS.'templates'.DS.$template.DS.'css'.DS.'editor.css' )) {
			$template = 'system';
		}

		$document->setTitle(JText::_('Article Preview'));
		$document->addStyleSheet(JURI::root() . 'templates/'.$template.'/css/editor.css');
		$document->setBase(JUri::root());

		$view =& $this->getView( 'Content' );
		$view->previewContent();
	}

	function insertPagebreak()
	{
		$document =& JFactory::getDocument();
		$document->setTitle(JText::_('PGB ARTICLE PAGEBRK'));
		$view =& $this->getView( 'Content' );
		$view->insertPagebreak();
	}
}