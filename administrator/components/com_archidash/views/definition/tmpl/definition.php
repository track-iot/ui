<?php 

// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.methods' );
jimport( 'joomla.html.pane' );
jimport( 'joomla.html.pagination' );
jimport('joomla.environment.request');/**
 * @version 1.0
 * @author  TobyTools.com (email:info@tobytools.com)(website: www.tobytools.com)
 * @copyright Copyright TobyTools.com 
 * @license GNU/GPL
 */


class ArchiDashLayoutDefinition {
	
	private function prepareDisplayToolbar(){
		//adding the css
		$css = JURI::base().'components/com_archidash/assets/css/definition.css';
		$document =& JFactory::getDocument();
		$document->addStyleSheet($css); 
		//custom title for Paris
		JToolBarHelper::title(   JText::_( 'ARCHIDASH' ),'logo');
		//add toolbars buttons		
		JToolBarHelper::custom("import","import32","import32",JText::_("Import"),false,false);
		JToolBarHelper::custom("export","export32","export32",JText::_("Export"),false,false);
		JToolBarHelper::deleteList();
	}
	
	private function prepareSubmenu(){
		$welcomeLink = 'index.php?option=com_archidash&controller=Welcome';
		$dashboardLink = 'index.php?option=com_archidash&controller=Dashboard';
		$definitionLink = 'index.php?option=com_archidash&controller=Definition';	
		// add sub menu items
		JSubMenuHelper::addEntry(JText::_('WELCOME'), $welcomeLink,false);
		JSubMenuHelper::addEntry(JText::_('DASHBOARDS'), $dashboardLink,false);
		JSubMenuHelper::addEntry(JText::_('DEFINITIONS'), $definitionLink,true);
		
	}
	
	
	
	
	function display(){
				
		
		global $mainframe;
		//get the model
		require_once( JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'models'.DS.'definition.php' );
		$classname = "ArchiDashModelDefinition";
		$model  = new $classname();
		
		$this->prepareDisplayToolbar();
		$this->prepareSubmenu();
		
		
		//prepare the page
		$limit = JRequest::getVar('limit',$mainframe->getCfg('list_limit'));
		$limitstart = JRequest::getVar('limitstart',0);
/*************************************************************************************************************************/		
		/*WATCH OUT!!!!!!!!*/
		/*******************/
		//experienced an issue with these cases, apparently joomla! does not manage these scenarios automatically
		if($limit==0 && $limitstart>0){
			
			$limit=0; 
			$limitstart=0;
		}
		/*******************/
		if($limit>$limitstart && $limitstart>0){
			$limitstart=0;
		}
		/*******************/
/*************************************************************************************************************************/		
		$total = $model->countRecords();
		$filter_order = JRequest::getVar('filter_order','id');
		$filter_order_Dir = JRequest::getVar('filter_order_Dir','desc');
		$pageNav = new JPagination($total,$limitstart,$limit);
		
		$records = $model->selectAllRecords($limitstart,$limit,$filter_order,$filter_order_Dir);
		if(!$records && strcmp($model->getErrMsg(),"")!=0){//something is strange
			$mainframe->enqueueMessage( JText::_( 'OPERATIONKO' )."<br/>".$model->getErrMsg(),'error');
		}
		
		?>
		<form action="index.php" method="post" name="adminForm">
		<table class="adminlist">
				<thead>					
					<tr>
						<th width="3%"><input type="checkbox" name="toggle" value="" onclick="checkAll(<?php  echo count($records);?>);" /></th>
						<th ><?php echo JHTML::_('grid.sort',JText::_('NAME'),'name',$filter_order_Dir,$filter_order);?></th>
						
						
					</tr>
				</thead>
				<tbody>
		<?php 
				$k=0;
				$i=0;
				//http://php.net/manual/en/control-structures.foreach.php
				/*
				 * 	Warning: Invalid argument supplied for foreach()
					You can prevent this error by type-casting the foreach variable as an array type using "(array)" before the array variable name.
				 * */
				foreach((array)$records as $currentRec){
					$checked = JHTML::_('grid.id',$i, $currentRec['id'],false,'cid');
		?>		
					<tr class="<?php echo "row$k"; ?>">
						<td align="center" width="3%"><?php echo $checked;?></td>
						<td ><center><h3><a href="<?php echo JURI::base().'index.php?option=com_archidash&controller=Definition&task=download&cid[]='.$currentRec['id'];?>"><?php echo $currentRec['name'];?></a></h3></center></td>
					</tr>
		<?php
					$k=1-$k;
					$i++; 		
				}						
		?>				
				</tbody>
				<tfoot>
					<tr>
						<td colspan="8"><center><?php echo $pageNav->getListFooter();?></center></td>
					</tr>
				</tfoot>
		
		</table>
		<input type="hidden" name="option" value="com_archidash"/>
		<input type="hidden" name="controller" value="Definition"/>
		<input type="hidden" name="task" value=""/>				
		<input type="hidden" name="filter_order" value="<?php echo $filter_order; ?>"/>
		<input type="hidden" name="filter_order_Dir" value="<?php echo $filter_order_Dir; ?>"/>
		<input type="hidden" name="boxchecked" value="0"/>
		<input type="hidden" name="hidemainmenu" value=""/>
		<input type="hidden" name="next_level_id" value=""/>

		</form>
		<?php 
	}
	
	function download($fullPath){
		global $mainframe;
		if(file_exists($fullPath)){
			if ($fd = @fopen ($fullPath, "r")) {
			    $fsize = @filesize($fullPath);
			    $path_parts = @pathinfo($fullPath);
			    header("Content-type: application/xml"); // add here more headers for diff. extensions
			    header("Content-Disposition: attachment; filename=\"".$path_parts["basename"]."\""); // use 'attachment' to force a download
			    header("Content-length: $fsize");
			    header("Cache-control: private"); //use this to open files directly
			    while(!@feof($fd)) {
			        $buffer = @fread($fd, 2048);
			        echo $buffer;
			    }
			}
			@fclose ($fd);
			exit;
		}
		else{
			$mainframe->enqueueMessage(JText::_( 'FILENOTFOUND' ).":".$fullPath,'error');
		}
			
		$this->display();	
	}
	
	function remove(){
		$this->display();
	}
	
	private function prepareImportToolbar(){
		//adding the css
		$css = JURI::base().'components/com_archidash/assets/css/definition.css';
		$document =& JFactory::getDocument();
		$document->addStyleSheet($css); 
		//custom title for Paris
		JToolBarHelper::title(   JText::_( 'Import' ),'logo');
		//add toolbars buttons		
		JToolBarHelper::cancel();
	}
	
	
	function import(){
		$this->prepareImportToolbar();
		$this->prepareSubmenu();
		$document =& JFactory::getDocument();
		$document->addScript(JURI::base() . 'components/com_archidash/views/definition/tmpl/definition.js');
	?>
		<form action="index.php" method="post" name="adminForm" enctype="multipart/form-data">
		<!-- The hidden inputs at the begin are very useful in the case of error, file not found.
		At the end of forum, if there is some error, they are not written by the browser.-->
		<input type="hidden" name="option" value="com_archidash"/>
		<input type="hidden" name="controller" value="Definition"/>
		<input type="hidden" name="task" value=""/>								
		
		<fieldset class="adminform">
			<legend><?php echo JText::_('UPLOADFILE');?></legend>
			<table class="admintable" width="100%">								
				<tr>
					
					<td width="20%" align="right" class="key"><?php	echo JHTML::_('tooltip',JText::_('TOOLTIPFILE') ,null,null, JText::_('DEFFILE'));?></td>
					<td width="80%">
						
						<input class="input_box" id="file_upload" type="file" name="file_upload" size="100" />
						<input class="button" onclick="if(file_upload.value=='') {alert('<?php echo JText::_('CHOOSEAFILE');?>');return false;}else{submitForm();}" name="upload" type="button" value="<?php echo JText::_('UPLOADFILE');?>" />
					
					</td>
					
					
				</tr>
			</table>
		</fieldset>					
	</form>

		
	
	<?php 	
	}
	
	private function prepareSelectSection($sectionID="0"){
		require_once( JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'models'.DS.'section.php' );
		$classname = "ArchiDashModelSection";
		$model  = new $classname();
		$sections = $model->selectAllSections(false,false,"id","desc");
		
		$selectLength=20;
		$select = "<select  style='width:412px;' size='$selectLength' name='sectionid' >";
		$chosen="";
		
		foreach($sections as $elem){
			
			if(strcasecmp($sectionID,$elem['id'])==0){
				$chosen="selected='selected'";	
			}
			else{
				$chosen="";
			}
			$select.="<option $chosen value='{$elem['id']}'>{$elem['id']} {$elem['title']}</option>";
			
		}
		$select.="</select>";
		
		return $select;
		
	}
	
	private function prepareSelectModules($moduleIDs=false){
		require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'models'.DS.'archi.php');
		$classname = "ArchiDashModelArchi";		
		$model = new $classname();
		$modules = $model->selectAllModules(false,false,"id","desc");
		$selectLength=20;
		$select = "<select style='width:412px;' size='$selectLength' multiple='multiple' name='moduleids[]' >";
		$chosen="";
		
		foreach($modules as $elem){
			
			if(@in_array($elem['id'],$moduleIDs)){
				$chosen="selected='selected'";	
			}
			else{
				$chosen="";
			}
			$select.="<option $chosen value='{$elem['id']}'>{$elem['id']} {$elem['title']}</option>";
			
		}
		$select.="</select>";
		
		return $select;
		
	}
	
	
	private function prepareExportToolbar(){
		//adding the css
		$css = JURI::base().'components/com_archidash/assets/css/definition.css';
		$document =& JFactory::getDocument();
		$document->addStyleSheet($css); 
		JToolBarHelper::title(   JText::_( 'Export' ),'logo');
		JToolBarHelper::save('saveexport');
		//add toolbars buttons		
		JToolBarHelper::cancel();
	}
	
	
	public function export($params=false){
		$this->prepareExportToolbar();
		$this->prepareSubmenu();
		
		
	?>	
	
		<form action="index.php" method="post" name="adminForm" enctype="multipart/form-data">
		<!-- The hidden inputs at the begin are very useful in the case of error, file not found.
		At the end of forum, if there is some error, they are not written by the browser.-->
		<input type="hidden" name="option" value="com_archidash"/>
		<input type="hidden" name="controller" value="Definition"/>
		<input type="hidden" name="task" value=""/>								
		
			<fieldset class="adminform">
				<legend><?php echo JText::_('EXPORTDEFINITION');?></legend>
				<table class="admintable" width="100%">								
					<tr>
						<td width="20%"  align="right" class="key"><?php	echo JHTML::_('tooltip',JText::_('TOOLTIPDEFNAME') ,null,null, JText::_('DEFNAME'));?></td>
						<td><input style='width:412px;' class="text_area" type="text" name="definitionname" id="definitionname" size="76" maxlength="250" value="<?php echo JRequest::getVar('definitionname','');?>" /></td>
					</tr>
					
					<tr><td><br/></td><td><br/></td></tr>
					
					<tr>
						<td width="20%"  align="right" class="key"><?php	echo JHTML::_('tooltip',JText::_('TOOLTIPDASHNAME') ,null,null, JText::_('DASHNAME'));?></td>
						<td><input style='width:412px;' class="text_area" type="text" name="dashboardname" id="dashboardname" size="76" maxlength="250" value="<?php echo JRequest::getVar('dashboardname','');?>" /></td>
					</tr>
					
					<tr>
						<td width="20%" align="right" class="key"><?php	echo JHTML::_('tooltip',JText::_('TOOLTIPDASHDESC') ,null,null, JText::_('DASHDESC'));?></td>
						<td><input style='width:412px;' class="text_area" type="text" name="dashboarddesc" id="dashboarddesc" size="76" maxlength="250" value="<?php echo JRequest::getVar('dashboarddesc','');?>" /></td>
					</tr>
					
					<tr>
						<td width="20%" align="right" class="key"><?php	echo JHTML::_('tooltip',JText::_('TOOLTIPDASHACL') ,null,null, JText::_('DASHACL'));?></td>
						<td>
							<select  style='width:412px;' name="dashboardacl" size="1">
								<option <?php $chosen=strcasecmp("0",JRequest::getVar('dashboardacl','0'))==0?"selected='selected'":""; echo $chosen; ?> value="0"><?php echo JText::_('Public');?></option>
								<option <?php $chosen=strcasecmp("1",JRequest::getVar('dashboardacl','0'))==0?"selected='selected'":""; echo $chosen; ?> value="1"><?php echo JText::_('Registered');?></option>
								<option <?php $chosen=strcasecmp("2",JRequest::getVar('dashboardacl','0'))==0?"selected='selected'":""; echo $chosen; ?> value="2"><?php echo JText::_('Special');?></option>							
							</select>	
						</td>
					</tr>
					
					<tr><td><br/></td><td><br/></td></tr>
					
					<tr>
						<td width="20%" align="right" class="key"><?php	echo JHTML::_('tooltip',JText::_('TOOLTIPSECTION') ,null,null, JText::_('SECTIONNAME'));?></td>
						<td width="80%"><?php echo $this->prepareSelectSection(JRequest::getVar('sectionid',false));?></td>										
					</tr>
					
					<tr><td><br/></td><td><br/></td></tr>
					
					<tr>
						<td width="20%"  align="right" class="key"><?php	echo JHTML::_('tooltip',JText::_('TOOLTIPMENUNAME') ,null,null, JText::_('MENUNAME'));?></td>
						<td><input style='width:412px;' class="text_area" type="text" name="menuname" id="menuname" size="76" maxlength="250" value="<?php echo JRequest::getVar('menuname','');?>" /></td>
					</tr>
					
					<tr>
						<td width="20%" align="right" class="key"><?php	echo JHTML::_('tooltip',JText::_('TOOLTIPMENUDESC') ,null,null, JText::_('MENUDESC'));?></td>
						<td><input style='width:412px;' class="text_area" type="text" name="menudesc" id="menudesc" size="76" maxlength="250" value="<?php echo JRequest::getVar('menudesc','');?>" /></td>
					</tr>
					
					<tr><td><br/></td><td><br/></td></tr>
					
					<tr>
						<td width="20%" align="right" class="key"><?php	echo JHTML::_('tooltip',JText::_('TOOLTIPMODULE') ,null,null, JText::_('MODULES'));?></td>
						<td width="80%"><?php echo $this->prepareSelectModules(JRequest::getVar('moduleids',false));?></td>										
					</tr>
					
				</table>
			</fieldset>					
		</form>
	
	<?php 	
		
		
	}	
		
}
