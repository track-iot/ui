Joomla! is always accessed through a single point of entry: index.php for the Site Application or administrator/index.php for the Administrator Application. The application will then load the required component, based on the value of 'option' in the URL or in the POST data. For our component, the URL would be:

index.php?option=com_hello&view=hello

This will load our main file, which can be seen as the single point of entry for our component: components/com_hello/hello.php.

The code for this file is fairly typical across components.

site/hello.php:

<?php
/**
 * @package    Joomla.Tutorials
 * @subpackage Components
 * components/com_hello/hello.php
 * @link http://docs.joomla.org/Developing_a_Model-View-Controller_Component_-_Part_1
 * @license    GNU/GPL
*/
 
// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
 
// Require the base controller
 
require_once( JPATH_COMPONENT.DS.'controller.php' );
 
// Require specific controller if requested
if($controller = JRequest::getWord('controller')) {
    $path = JPATH_COMPONENT.DS.'controllers'.DS.$controller.'.php';
    if (file_exists($path)) {
        require_once $path;
    } else {
        $controller = '';
    }
}
 
// Create the controller
$classname    = 'HelloController'.$controller;
$controller   = new $classname( );
 
// Perform the Request task
$controller->execute( JRequest::getWord( 'task' ) );
 
// Redirect if set by the controller
$controller->redirect();

The first statement is a security check.

JPATH_COMPONENT is the absolute path to the current component, in our case components/com_hello. If you specifically need either the Site component or the Administrator component, you can use JPATH_COMPONENT_SITE or JPATH_COMPONENT_ADMINISTRATOR.

DS is the directory separator of your system: either '/' or '\'. This is automatically set by the framework so the developer doesn't have to worry about developing different versions for different server OSs. The 'DS' constant should always be used when referring to files on the local server.

After loading the base controller, we check if a specific controller is needed. In this component, the base controller is the only controller, but we will leave this conditional check "in place" for future use.

JRequest::getWord() finds a word variable in the URL or the POST data. So if our URL is index.php?option=com_hello&controller=controller_name, then we can retrieve our controller name in our component using: echo JRequest::getWord('controller');

Now we have our base controller 'HelloController' in com_hello/controller.php, and, if needed, additional controllers like 'HelloControllerController1' in com_hello/controllers/controller1.php. Using this standard naming scheme will make things easy later on: '{Componentname}{Controller}{Controllername}'

After the controller is created, we instruct the controller to execute the task, as defined in the URL: index.php?option=com_hello&task=sometask. If no task is set, the default task 'display' will be assumed. When display is used, the 'view' variable will decide what will be displayed. Other common tasks are save, edit, new...

The controller might decide to redirect the page, usually after a task like 'save' has been completed. This last statement takes care of the actual redirection.

The main entry point (hello.php) essentially passes control to the controller, which handles performing the task that was specified in the request.

Note that we don't use a closing php tag in this file: ?>. The reason for this is that we will not have any unwanted whitespace in the output code. This is default practice since Joomla! 1.5, and will be used for all php-only files.
Creating the Controller

Our component only has one task - greet the world. Therefore, the controller will be very simple. No data manipulation is required. All that needs to be done is the appropriate view loaded. We will have only one method in our controller: display(). Most of the required functionality is built into the JController class, so all that we need to do is invoke the JController::display() method.

The code for the base controller site/controller.php is:

<?php
/**
 * @package    Joomla.Tutorials
 * @subpackage Components
 * @link http://docs.joomla.org/Developing_a_Model-View-Controller_Component_-_Part_1
 * @license    GNU/GPL
 */
 
// No direct access
 
defined( '_JEXEC' ) or die( 'Restricted access' );
 
jimport('joomla.application.component.controller');
 
/**
 * Hello World Component Controller
 *
 * @package    Joomla.Tutorials
 * @subpackage Components
 */
class HelloController extends JController
{
    /**
     * Method to display the view
     *
     * @access    public
     */
    function display()
    {
        parent::display();
    }
 
}

The JController constructor will always register a display() task and unless otherwise specified (using the registerDefaultTask() method), it will set it as the default task.

This barebones display() method isn't really even necessary since all it does is invoke the parent constructor. However, it is a good visual clue to indicate what is happening in the controller.

The JController::display() method will determine the name of the view and layout from the request and load that view and set the layout. When you create a menu item for your component, the menu manager will allow the administrator to select the view that they would like the menu link to display and to specify the layout. A view usually refers to a view of a certain set of data (i.e. a list of cars, a list of events, a single car, a single event). A layout is a way that that view is organized.

In our component, we will have a single view called hello, and a single layout (default).
Creating the View

The task of the view is very simple: It retrieves the data to be displayed and pushes it into the template. Data is pushed into the template using the JView::assignRef method. (Note: The key (the first argument) passed to the assignRef method cannot be preceded by an underscore i.e. $this->assignRef('_greeting',$greeting). Doing so will cause the assignRef method to return false and your variable will not be pushed into the template.)

The code for the view at site/views/hello/view.html.php:

<?php
/**
 * @package    Joomla.Tutorials
 * @subpackage Components
 * @link http://docs.joomla.org/Developing_a_Model-View-Controller_Component_-_Part_1
 * @license    GNU/GPL
*/
 
// no direct access
 
defined( '_JEXEC' ) or die( 'Restricted access' );
 
jimport( 'joomla.application.component.view');
 
/**
 * HTML View class for the HelloWorld Component
 *
 * @package    HelloWorld
 */
 
class HelloViewHello extends JView
{
    function display($tpl = null)
    {
        $greeting = "Hello World!";
        $this->assignRef( 'greeting', $greeting );
 
        parent::display($tpl);
    }
}

Creating the Template

Joomla! templates/layouts are regular PHP files that are used to layout the data from the view in a particular manner. The variables assigned by the JView::assignRef method can be accessed from the template using $this->{propertyname} (see the template code below for an example).

Our template is very simple: we only want to display the greeting that was passed in from the view - this file is:
site/views/hello/tmpl/default.php:

<?php
 
// No direct access
 
defined('_JEXEC') or die('Restricted access'); ?>
<h1><?php echo $this->greeting; ?></h1>

Wrapping It All Up - Creating the hello.xml File

It is possible to install a component manually by copying the files using an FTP client and modifying the database tables. It is more efficient to create a package file that will allow the Joomla! Installer to do this for you. This package file contains a variety of information:

    * basic descriptive details about your component (i.e. name), and optionally, a description, copyright and license information.
    * a list of files that need to be copied.
    * optionally, a PHP file that performs additional install and uninstall operations.
    * optionally, an SQL file which contains database queries that should be executed upon install/uninstall

The format of the XML file at hello.xml is as follows:

<?xml version="1.0" encoding="utf-8"?>
<install type="component" version="1.5.0">
 <name>Hello</name>
 <!-- The following elements are optional and free of formatting constraints -->
 <creationDate>2007-02-22</creationDate>
 <author>John Doe</author>
 <authorEmail>john.doe@example.org</authorEmail>
 <authorUrl>http://www.example.org</authorUrl>
 <copyright>Copyright Info</copyright>
 <license>License Info</license>
 <!--  The version string is recorded in the components table -->
 <version>1.01</version>
 <!-- The description is optional and defaults to the name -->
 <description>Description of the component ...</description>
 
 <!-- Site Main File Copy Section -->
 <!-- Note the folder attribute: This attribute describes the folder
      to copy FROM in the package to install therefore files copied
      in this section are copied from /site/ in the package -->
 <files folder="site">
  <filename>controller.php</filename>
  <filename>hello.php</filename>
  <filename>index.html</filename>
  <filename>views/index.html</filename>
  <filename>views/hello/index.html</filename>
  <filename>views/hello/view.html.php</filename>
  <filename>views/hello/tmpl/default.php</filename>
  <filename>views/hello/tmpl/index.html</filename>
 </files>
 
 <administration>
  <!-- Administration Menu Section -->
  <menu>Hello World!</menu>
 
  <!-- Administration Main File Copy Section -->
  <files folder="admin">
   <filename>hello.php</filename>
   <filename>index.html</filename>
  </files>
 
 </administration>
</install>

Info non-talk.png
	Put this xml file, also called manifest, in the root of your package (because the installer will take its path as the root path for all other files).
Don't include itself under <files>...

You may have noticed the manifest source above mentioned files we have not discussed. These are the index.html files and the admin files. An index.html file is placed in each directory to prevent prying users from getting a directory listing. If there is no index.html file, some web servers will list the directory contents. This is often undesirable. These files have the simple line:

<html><body bgcolor="#FFFFFF"></body></html>

It will simply display a blank page.

The hello.php file in the admin folder is the entry point for the our component's admin section. Since our component has no administrator needs (yet), this file will have the same content as the index.html files for now.

If you've followed along, you can visit URL index.php?option=com_hello to see your work. To do this, zip up the main folder and install via the extensions manager. If it still does not work, download the package at the bottom of this page and install for a working example.