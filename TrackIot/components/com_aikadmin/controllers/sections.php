<?php
/**
 * @version		$Id: cache.php 1.0 19-11-2009 Danijar
 * @package		Frontend Admin
 * @copyright	Copyright (C) 2009 CMSSpace. http://www.cmsspace.com
 * @license		GNU/GPL. http://www.gnu.org/licenses/gpl.html
 */
defined('_JEXEC') or die( 'Restricted access' );
jimport( 'joomla.application.component.controller' );
class AikControllerSections extends JController
{
	function __construct( $default = array() )
	{

		parent::__construct( array('default_task' => 'showSections') );
		$this->registerTask( 'add', 'editSection' );
		$this->registerTask( 'edit', 'editSection' );
		$this->registerTask( 'go2menu', 'saveSection' );
		$this->registerTask( 'go2menuitem', 'saveSection' );
		$this->registerTask( 'save', 'saveSection' );
		$this->registerTask( 'apply', 'saveSection' );
		$this->registerTask( 'remove', 'removeSections' );
		$this->registerTask( 'copyselect', 'copySectionSelect' );
		$this->registerTask( 'copysave', 'copySectionSave' );
		$this->registerTask( 'publish', 'publishSections' );
		$this->registerTask( 'unpublish', 'publishSections' );
		$this->registerTask( 'cancel', 'cancelSection' );
		$this->registerTask( 'orderup', 'orderSection' );
		$this->registerTask( 'orderdown', 'orderSection' );
		$this->registerTask( 'accesspublic', 'accessMenu' );
		$this->registerTask( 'accessregistered', 'accessMenu' );
		$this->registerTask( 'accessspecial', 'accessMenu' );
		$this->registerTask( 'saveorder', 'saveOrder' );
	}

	function showSections()
	{
		global $mainframe;
	
		JToolBarHelper::title( JText::_( 'Section Manager' ), 'sections.png' );
		JToolBarHelper::publishList();
		JToolBarHelper::unpublishList();
		JToolBarHelper::customX( 'copyselect', 'copy.png', 'copy_f2.png', 'Copy', true );
		JToolBarHelper::deleteList();
		JToolBarHelper::editListX();
		JToolBarHelper::addNewX();
		JToolBarHelper::help( 'screen.sections' );
		JToolBarHelper::cpanel();
		
		$db					=& JFactory::getDBO();
		$user				=& JFactory::getUser();
		$option		= JRequest::getCmd( 'option');
		$scope 		= JRequest::getCmd( 'scope' );
		$filter_order		= $mainframe->getUserStateFromRequest( 'com_sections.filter_order',		'filter_order',		's.ordering',	'cmd' );
		$filter_order_Dir	= $mainframe->getUserStateFromRequest( 'com_sections.filter_order_Dir',	'filter_order_Dir',	'',				'word' );
		$filter_state		= $mainframe->getUserStateFromRequest( 'com_sections.filter_state',		'filter_state',		'',				'word' );
		$search				= $mainframe->getUserStateFromRequest( 'com_sections.search',			'search',			'',				'string' );
		$search				= JString::strtolower( $search );
	
		$limit		= $mainframe->getUserStateFromRequest( 'global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int' );
		$limitstart	= $mainframe->getUserStateFromRequest( 'com_sections.limitstart', 'limitstart', 0, 'int' );
	
		$where[] = 's.scope = '.$db->Quote($scope);
	
		if ( $filter_state ) {
			if ( $filter_state == 'P' ) {
				$where[] = 's.published = 1';
			} else if ($filter_state == 'U' ) {
				$where[] = 's.published = 0';
			}
		}
		if ($search) {
			$where[] = 'LOWER(s.title) LIKE '.$db->Quote( '%'.$db->getEscaped( $search, true ).'%', false );
		}
	
		$where 		= ( count( $where ) ? ' WHERE ' . implode( ' AND ', $where ) : '' );
		$orderby 	= ' ORDER BY '.$filter_order.' '. $filter_order_Dir .', s.ordering';
	
		$query = 'SELECT COUNT(*)'
		. ' FROM #__sections AS s'
		. $where
		;
		$db->setQuery( $query );
		$total = $db->loadResult();
	
		jimport('joomla.html.pagination');
		$pageNav = new JPagination( $total, $limitstart, $limit );
	
		$query = 'SELECT s.*, g.name AS groupname, u.name AS editor'
		. ' FROM #__sections AS s'
		. ' LEFT JOIN #__content AS cc ON s.id = cc.sectionid'
		. ' LEFT JOIN #__users AS u ON u.id = s.checked_out'
		. ' LEFT JOIN #__groups AS g ON g.id = s.access'
		. $where
		. ' GROUP BY s.id'
		. $orderby
		;
		$db->setQuery( $query, $pageNav->limitstart, $pageNav->limit );
		$rows = $db->loadObjectList();
		if ($db->getErrorNum()) {
			echo $db->stderr();
			return false;
		}
	
		$count = count( $rows );
		for ( $i = 0; $i < $count; $i++ ) {
			$query = 'SELECT COUNT( a.id )'
			. ' FROM #__categories AS a'
			. ' WHERE a.section = '.$db->Quote($rows[$i]->id)
			. ' AND a.published <> -2'
			;
			$db->setQuery( $query );
			$active = $db->loadResult();
			$rows[$i]->categories = $active;
		}
		for ( $i = 0; $i < $count; $i++ ) {
			$query = 'SELECT COUNT( a.id )'
			. ' FROM #__content AS a'
			. ' WHERE a.sectionid = '.(int) $rows[$i]->id
			. ' AND a.state <> -2'
			;
			$db->setQuery( $query );
			$active = $db->loadResult();
			$rows[$i]->active = $active;
		}
		for ( $i = 0; $i < $count; $i++ ) {
			$query = 'SELECT COUNT( a.id )'
			. ' FROM #__content AS a'
			. ' WHERE a.sectionid = '.(int) $rows[$i]->id
			. ' AND a.state = -2'
			;
			$db->setQuery( $query );
			$trash = $db->loadResult();
			$rows[$i]->trash = $trash;
		}
		$lists['state']	= JHTML::_('grid.state',  $filter_state );
		$lists['order_Dir']	= $filter_order_Dir;
		$lists['order']		= $filter_order;
		$lists['search']= $search;
	
		$view =& $this->getView( 'sections' );
		$view->show( $rows, $scope, $user->get('id'), $pageNav, $option, $lists );
	}
	function editSection()
	{
		global $mainframe;
		if(JRequest::getCmd( 'task' ) == 'edit')
			$edit = true;
		else 
			$edit = false;
			
		$db			=& JFactory::getDBO();
		$user 		=& JFactory::getUser();
	
		$option		= JRequest::getCmd( 'option');
		$scope		= JRequest::getCmd( 'scope' );
		$cid		= JRequest::getVar( 'cid', array(0), '', 'array' );
		JArrayHelper::toInteger($cid, array(0));
	
		$cid = JRequest::getVar('cid', array(0), '', 'array');
		JArrayHelper::toInteger($cid, array(0));

		$text = ( $edit ? JText::_( 'Edit' ) : JText::_( 'New' ) );

		JToolBarHelper::title( JText::_( 'Section' ).': <small><small>[ '. $text.' ]</small></small>', 'sections.png' );
		JToolBarHelper::save();
		JToolBarHelper::apply();
		if ( $edit ) {
			JToolBarHelper::cancel( 'cancel', 'Close' );
		} else {
			JToolBarHelper::cancel();
		}
		JToolBarHelper::help( 'screen.sections.edit' );
		JToolBarHelper::cpanel();
		
		$row =& JTable::getInstance('section');
		if ($edit)
			$row->load( $cid[0] );
	
		if ($row->isCheckedOut( $user->get('id') )) {
			$msg = JText::sprintf( 'DESCBEINGEDITTED', JText::_( 'The section' ), $row->title );
			$mainframe->redirect( 'index.php?option='. $option .'&scope='. $row->scope, $msg );
		}
	
		if ( $edit ) {
			$row->checkout( $user->get('id') );
		} else {
			$row->scope 		= $scope;
			$row->published 	= 1;
		}
	
		$query = 'SELECT ordering AS value, title AS text'
		. ' FROM #__sections'
		. ' WHERE scope='.$db->Quote($row->scope).' ORDER BY ordering'
		;
		if($edit)
			$lists['ordering'] 			= JHTML::_('list.specificordering',  $row, $cid[0], $query );
		else
			$lists['ordering'] 			= JHTML::_('list.specificordering',  $row, '', $query );
		$active =  ( $row->image_position ? $row->image_position : 'left' );
		$lists['image_position'] 	= JHTML::_('list.positions',  'image_position', $active, NULL, 0 );
		$lists['image'] 			= JHTML::_('list.images',  'image', $row->image );
		$lists['access'] 			= JHTML::_('list.accesslevel',  $row );
		$lists['published'] 		= JHTML::_('select.booleanlist',  'published', 'class="inputbox"', $row->published );
		$view =& $this->getView( 'sections' );
		$view->edit( $row, $option, $lists );
	}
	function saveSection()
	{
		global $mainframe;
		JRequest::checkToken() or jexit( 'Invalid Token' );
		
		$db			=& JFactory::getDBO();
		$option		= JRequest::getCmd( 'option');
		$task = JRequest::getCmd( 'task' );
		$scope 		= JRequest::getCmd( 'scope' );
		$menu		= JRequest::getVar( 'menu', 'mainmenu', 'post', 'string' );
		$menuid		= JRequest::getVar( 'menuid', 0, 'post', 'int' );
		$oldtitle	= JRequest::getVar( 'oldtitle', '', '', 'post', 'string' );
	
		$post = JRequest::get('post');
		$post['description'] = JRequest::getVar( 'description', '', 'post', 'string', JREQUEST_ALLOWRAW );
	
		$row =& JTable::getInstance('section');
		if (!$row->bind($post)) {
			JError::raiseError(500, $row->getError() );
		}
		if (!$row->check()) {
			JError::raiseError(500, $row->getError() );
		}
		if ( $oldtitle ) {
			if ( $oldtitle <> $row->title ) {
				$query = 'UPDATE #__menu'
				. ' SET name = '.$db->Quote($row->title)
				. ' WHERE name = '.$db->Quote($oldtitle)
				. ' AND type = "content_section"'
				;
				$db->setQuery( $query );
				$db->query();
			}
		}
		if (!$row->id) {
			$row->ordering = $row->getNextOrder();
		}
	
		if (!$row->store()) {
			JError::raiseError(500, $row->getError() );
		}
		$row->checkin();
	
		switch ( $task )
		{
			case 'go2menu':
				$mainframe->redirect( 'index.php?option=com_aikadmin&c=menus&menutype='. $menu );
				break;
	
			case 'go2menuitem':
				$mainframe->redirect( 'index.php?option=com_aikadmin&c=menus&menutype='. $menu .'&task=edit&id='. $menuid );
				break;
	
			case 'apply':
				$msg = JText::_( 'Changes to Section saved' );
				$mainframe->redirect( 'index.php?option='. $option .'&scope='. $scope .'&task=edit&cid[]='. $row->id, $msg );
				break;
	
			case 'save':
			default:
				$msg = JText::_( 'Section saved' );
				$mainframe->redirect( 'index.php?option='. $option .'&scope='. $scope, $msg );
				break;
		}
	}
	function removeSections()
	{
		global $mainframe;
		JRequest::checkToken() or jexit( 'Invalid Token' );
	
		$cid 		= JRequest::getVar( 'cid', array(0), '', 'array' );
		JArrayHelper::toInteger($cid, array(0));
		$option		= JRequest::getCmd( 'option');
		$scope 		= JRequest::getCmd( 'scope' );
		$db =& JFactory::getDBO();
		if (count( $cid ) < 1) {
			JError::raiseError(500, JText::_( 'Select a section to delete', true ) );
		}
	
		JArrayHelper::toInteger( $cid );
		$cids = implode( ',', $cid );
	
		$query = 'SELECT s.id, s.title, COUNT(c.id) AS numcat'
		. ' FROM #__sections AS s'
		. ' LEFT JOIN #__categories AS c ON c.section=s.id'
		. ' WHERE s.id IN ( '.$cids.' )'
		. ' GROUP BY s.id'
		;
		$db->setQuery( $query );
		if (!($rows = $db->loadObjectList())) {
			echo "<script> alert('".$db->getErrorMsg(true)."'); window.history.go(-1); </script>\n";
		}
	
		$name = array();
		$err = array();
		$cid = array();
		foreach ($rows as $row) {
			if ($row->numcat == 0) {
				$cid[]	= (int) $row->id;
				$name[]	= $row->title;
			} else {
				$err[]	= $row->title;
			}
		}
	
		if (count( $cid ))
		{
			$cids = implode( ',', $cid );
			$query = 'DELETE FROM #__sections'
			. ' WHERE id IN ( '.$cids.' )'
			;
			$db->setQuery( $query );
			if (!$db->query()) {
				echo "<script> alert('".$db->getErrorMsg(true)."'); window.history.go(-1); </script>\n";
			}
		}
	
		if (count( $err ))
		{
			$cids = implode( ', ', $err );
			$msg = JText::sprintf( 'DESCCANNOTBEREMOVED', $cids );
			$mainframe->redirect( 'index.php?option='. $option .'&scope='. $scope, $msg );
		}
	
		$names = implode( ', ', $name );
		$msg = JText::sprintf( 'Sections successfully deleted', $names );
		$mainframe->redirect( 'index.php?option='. $option .'&scope='. $scope, $msg );
	}
	function publishSections()
	{
		global $mainframe;
		JRequest::checkToken() or jexit( 'Invalid Token' );
	
		$cid 		= JRequest::getVar( 'cid', array(0), '', 'array' );
		JArrayHelper::toInteger($cid, array(0));

		if(JRequest::getCmd( 'task' ) == 'publish')
			$publish = 1;
		else 
			$publish = 0;
			
		$scope 		= JRequest::getCmd( 'scope' );
		$option		= JRequest::getCmd( 'option');
		$db 	=& JFactory::getDBO();
		$user 	=& JFactory::getUser();
		if ( count( $cid ) < 1 ) {
			$action = $publish ? 'publish' : 'unpublish';
			JError::raiseError(500, JText::_( 'Select a section to '.$action, true ) );
		}
	
		$cids = implode( ',', $cid );
		$count = count( $cid );
		$query = 'UPDATE #__sections'
		. ' SET published = '.(int) $publish
		. ' WHERE id IN ( '.$cids.' )'
		. ' AND ( checked_out = 0 OR ( checked_out = '.(int) $user->get('id').' ) )'
		;
		$db->setQuery( $query );
		if (!$db->query()) {
			JError::raiseError(500, $db->getErrorMsg() );
		}
	
		if ( $count == 1 ) {
			$row =& JTable::getInstance('section');
			$row->checkin( $cid[0] );
		}
		if ( $publish == 0 ) {
			$query = 'SELECT id'
			. ' FROM #__menu'
			. ' WHERE type = "content_section"'
			. ' AND componentid IN ( '.$cids.' )'
			;
			$db->setQuery( $query );
			$menus = $db->loadObjectList();
	
			if ($menus) {
				foreach ($menus as $menu) {
					$query = 'UPDATE #__menu'
					. ' SET published = '.(int) $publish
					. ' WHERE id = '.(int) $menu->id
					;
					$db->setQuery( $query );
					$db->query();
				}
			}
		}
		$mainframe->redirect( 'index.php?option='. $option .'&scope='. $scope );
	}
	function cancelSection()
	{
		global $mainframe;
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$option		= JRequest::getCmd( 'option');
		$scope 		= JRequest::getCmd( 'scope' );
		$db =& JFactory::getDBO();
		$row =& JTable::getInstance('section');
		$row->bind(JRequest::get('post'));
		$row->checkin();
	
		$mainframe->redirect( 'index.php?option='. $option .'&c=sections&scope='. $scope );
	}
	function orderSection()
	{
		global $mainframe;
		$cid 		= JRequest::getVar( 'cid', array(0), '', 'array' );
		JArrayHelper::toInteger($cid, array(0));
		$uid = $cid[0];
		
		if(JRequest::getCmd( 'task' ) == 'orderup')
			$inc = -1;
		else 
			$inc = 1;
			
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$option		= JRequest::getCmd( 'option');
		$scope 		= JRequest::getCmd( 'scope' );
		
		$db =& JFactory::getDBO();
		$row =& JTable::getInstance('section');
		$row->load( $uid );
		$row->move( $inc, 'scope = '.$db->Quote($row->scope) );
	
		$mainframe->redirect( 'index.php?option='. $option .'&scope='. $scope );
	}
	function copySectionSelect( $option, $section )
	{
		global $mainframe;
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$cid 		= JRequest::getVar( 'cid', array(0), '', 'array' );
		JArrayHelper::toInteger($cid, array(0));
			
		JToolBarHelper::title( JText::_( 'Section' ) .': <small><small>[ '. JText::_( 'Copy' ).' ]</small></small>', 'section.png' );
		JToolBarHelper::save( 'copysave' );
		JToolBarHelper::cancel();
		JToolBarHelper::cpanel();

		$db =& JFactory::getDBO();
		$option		= JRequest::getCmd( 'option');
		JArrayHelper::toInteger($cid);
	
		if ( count( $cid ) < 1) {
			JError::raiseError(500, JText::_( 'Select an item to move', true ) );
		}
		$cids = implode( ',', $cid );
		$query = 'SELECT a.title, a.id'
		. ' FROM #__categories AS a'
		. ' WHERE a.section IN ( '.$cids.' )'
		;
		$db->setQuery( $query );
		$categories = $db->loadObjectList();
		$query = 'SELECT a.title, a.id'
		. ' FROM #__content AS a'
		. ' WHERE a.sectionid IN ( '.$cids.' )'
		. ' ORDER BY a.sectionid, a.catid, a.title'
		;
		$db->setQuery( $query );
		$contents = $db->loadObjectList();
	
		$view =& $this->getView( 'sections' );
		$view->copySectionSelect( $option, $cid, $categories, $contents, $section );
	}
	function copySectionSave( $sectionid )
	{
		global $mainframe;
		JRequest::checkToken() or jexit( 'Invalid Token' );
	
		$db			=& JFactory::getDBO();
		$scope 		= JRequest::getCmd( 'scope' );
		$title		= JRequest::getString( 'title' );
		$contentid	= JRequest::getVar( 'content' );
		$categoryid = JRequest::getVar( 'category' );
		JArrayHelper::toInteger($contentid);
		JArrayHelper::toInteger($categoryid);
		$section =& JTable::getInstance('section');
		foreach( $sectionid as $id ) {
			$section->load( $id );
			$section->id 	= NULL;
			$section->title = $title;
			$section->name 	= $title;
			if ( !$section->check() ) {
				copySectionSelect('com_sections', $sectionid, $scope );
				JError::raiseWarning(500, $section->getError() );
				return;
			}
	
			if ( !$section->store() ) {
				JError::raiseError(500, $section->getError() );
			}
			$section->checkin();
			$section->reorder( 'scope = '.$db->Quote($section->scope) );
			$newsectids[]["old"] = $id;
			$newsectids[]["new"] = $section->id;
		}
		$sectionMove = $section->id;
		$category =& JTable::getInstance('category');
		foreach( $categoryid as $id ) {
			$category->load( $id );
			$category->id = NULL;
			$category->section = $sectionMove;
			foreach( $newsectids as $newsectid ) {
				if ( $category->section == $newsectid["old"] ) {
					$category->section = $newsectid["new"];
				}
			}
			if (!$category->check()) {
				JError::raiseError(500, $category->getError() );
			}
	
			if (!$category->store()) {
				JError::raiseError(500, $category->getError() );
			}
			$category->checkin();
			$category->reorder( 'section = '.$db->Quote($category->section) );
			$newcatids[]["old"] = $id;
			$newcatids[]["new"] = $category->id;
		}
	
		$content =& JTable::getInstance('content');
		foreach( $contentid as $id) {
			$content->load( $id );
			$content->id = NULL;
			$content->hits = 0;
			foreach( $newsectids as $newsectid ) {
				if ( $content->sectionid == $newsectid["old"] ) {
					$content->sectionid = $newsectid["new"];
				}
			}
			foreach( $newcatids as $newcatid ) {
				if ( $content->catid == $newcatid["old"] ) {
					$content->catid = $newcatid["new"];
				}
			}
			if (!$content->check()) {
				JError::raiseError(500, $content->getError() );
			}
	
			if (!$content->store()) {
				JError::raiseError(500, $content->getError() );
			}
			$content->checkin();
		}
		$sectionOld =& JTable::getInstance('section');
		$sectionOld->load( $sectionMove );
	
		$msg = JText::sprintf( 'DESCCATANDITEMSCOPIED', $sectionOld-> name, $title );
		$mainframe->redirect( 'index.php?option=com_aikadmin&c=sections&scope=content', $msg );
	}
	function accessMenu( $access )
	{
		global $mainframe;
		JRequest::checkToken() or jexit( 'Invalid Token' );
	
		
		$cid 		= JRequest::getVar( 'cid', array(0), '', 'array' );
		JArrayHelper::toInteger($cid, array(0));
		$uid = $cid[0];
		$db	=& JFactory::getDBO();
		$option		= JRequest::getCmd( 'option');
		$row =& JTable::getInstance('section');
		$row->load( $uid );
		$task = JRequest::getCmd( 'task' );
		
		switch ($task)
		{
			case 'accesspublic':$row->access = 0;break;
			case 'accessregistered':$row->access = 1;break;
			case 'accessspecial':$row->access = 2;break;
		}
	
		if ( !$row->check() ) {
			return $row->getError();
		}
		if ( !$row->store() ) {
			return $row->getError();
		}
	
		$mainframe->redirect( 'index.php?option='. $option .'&scope='. $row->scope );
	}
	
	function saveOrder()
	{
		global $mainframe;
		JRequest::checkToken() or jexit( 'Invalid Token' );
	
		$db			=& JFactory::getDBO();
		$row		=& JTable::getInstance('section');
	
		$cid 		= JRequest::getVar( 'cid', array(0), '', 'array' );
		JArrayHelper::toInteger($cid, array(0));
		$total		= count( $cid );
		$order		= JRequest::getVar( 'order', array(0), 'post', 'array' );
		JArrayHelper::toInteger($order, array(0));
	
		for( $i=0; $i < $total; $i++ )
		{
			$row->load( (int) $cid[$i] );
			if ($row->ordering != $order[$i]) {
				$row->ordering = $order[$i];
				if (!$row->store()) {
					JError::raiseError(500, $db->getErrorMsg() );
				}
			}
		}
	
		$row->reorder( );
	
		$msg 	= JText::_( 'New ordering saved' );
		$mainframe->redirect( 'index.php?option=com_aikadmin&c=sections&scope=content', $msg );
	}
}