<?php 

// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.methods' );
jimport( 'joomla.html.pane' );
jimport( 'joomla.html.pagination' );
jimport('joomla.environment.request');/**
 * @version 1.0
 * @author  TobyTools.com (email:info@tobytools.com)(website: www.tobytools.com)
 * @copyright Copyright TobyTools.com 
 * @license GNU/GPL
 */


class ArchiDashLayoutDashboard {
	
		
	private function prepareDisplayToolbar(){
		//adding the css
		$css = JURI::base().'components/com_archidash/assets/css/dashboard.css';
		$document =& JFactory::getDocument();
		$document->addStyleSheet($css); 
		//custom title for Paris
		JToolBarHelper::title(   JText::_( 'ARCHIDASH' ),'logo');
		//add toolbars buttons		
		JToolBarHelper::addNew('add');
		JToolBarHelper::editList();
		JToolBarHelper::deleteList();
	}
	
	private function prepareSubmenu(){
		$welcomeLink = 'index.php?option=com_archidash&controller=Welcome';
		$dashboardLink = 'index.php?option=com_archidash&controller=Dashboard';
		$definitionLink = 'index.php?option=com_archidash&controller=Definition';	
		// add sub menu items
		JSubMenuHelper::addEntry(JText::_('WELCOME'), $welcomeLink,false);
		JSubMenuHelper::addEntry(JText::_('DASHBOARDS'), $dashboardLink,true);
		JSubMenuHelper::addEntry(JText::_('DEFINITIONS'), $definitionLink,false);
		
	}
	
	
	function display(){
				
		
		global $mainframe;
		//get the model
		require_once( JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'models'.DS.'dashboard.php' );
		$classname = "ArchiDashModelDashboard";
		$model  = new $classname();
		
		$this->prepareDisplayToolbar();
		$this->prepareSubmenu();
		
		
		//prepare the page
		$limit = JRequest::getVar('limit',$mainframe->getCfg('list_limit'));
		$limitstart = JRequest::getVar('limitstart',0);
/*************************************************************************************************************************/		
		/*WATCH OUT!!!!!!!!*/
		/*******************/
		//experienced an issue with these cases, apparently joomla! does not manage these scenarios automatically
		if($limit==0 && $limitstart>0){
			
			$limit=0; 
			$limitstart=0;
		}
		/*******************/
		if($limit>$limitstart && $limitstart>0){
			$limitstart=0;
		}
		/*******************/
/*************************************************************************************************************************/		
		$total = $model->countRecords();
		$filter_order = JRequest::getVar('filter_order','id');
		$filter_order_Dir = JRequest::getVar('filter_order_Dir','desc');
		$pageNav = new JPagination($total,$limitstart,$limit);
		
		$records = $model->selectAllRecords($limitstart,$limit,$filter_order,$filter_order_Dir);
		if(!$records && strcmp($model->getErrMsg(),"")!=0){//something is strange
			$mainframe->enqueueMessage( JText::_( 'OPERATIONKO' )."<br/>".$model->getErrMsg(),'error');
		}
		
		?>
		<form action="index.php" method="post" name="adminForm">
		<table class="adminlist">
				<thead>					
					<tr>
						<th width="3%"><input type="checkbox" name="toggle" value="" onclick="checkAll(<?php  echo count($records);?>);" /></th>
						<th width="20%"><?php echo JHTML::_('grid.sort',JText::_('NAME'),'name',$filter_order_Dir,$filter_order);?></th>
						<th><?php echo JHTML::_('grid.sort',JText::_('DESCRIPTION'),'description',$filter_order_Dir,$filter_order);?></th>
						<th width="5%"><?php echo JText::_('Details');?></th>
						<th width="10%"><?php echo JText::_('Module');?></th>
						<th width="5%"><?php echo JHTML::_('grid.sort',JText::_('ID'),'id',$filter_order_Dir,$filter_order);?></th>
						
					</tr>
				</thead>
				<tbody>
		<?php 
				$k=0;
				$i=0;
				//http://php.net/manual/en/control-structures.foreach.php
				/*
				 * 	Warning: Invalid argument supplied for foreach()
					You can prevent this error by type-casting the foreach variable as an array type using "(array)" before the array variable name.
				 * */
				foreach((array)$records as $currentRec){
					$checked = JHTML::_('grid.id',$i, $currentRec['id'],false,'cid');
		?>		
					<tr class="<?php echo "row$k"; ?>">
						<td align="center" width="3%"><?php echo $checked;?></td>
						<td width="20%"><center><h3><a href="<?php echo JURI::base().'index.php?option=com_archidash&controller=Dashboard&task=edit&cid[]='.$currentRec['id'];?>"><?php echo $currentRec['name'];?></a></h3></center></td>
						<td><h4><?php echo $currentRec['description'];?></h4></td>
						
						<td  width="5%"><center><a href="<?php echo JURI::base().'index.php?option=com_archidash&controller=Dashboard&task=details&cid[]='.$currentRec['id'];  ?>"><img src="<?php echo JURI::base().'components/com_archidash/assets/images/logo32.png';?>"/></a></center></td>
						
						<td width="10%">
							<?php $dashmod = $model->getModuleInfo($currentRec['id']);?>
							
							<center><h4>
							<a href="<?php echo JURI::base().'index.php?option=com_modules&client=0&task=edit&cid[]='.$dashmod['moduleid'];?>"><?php echo $dashmod['name'];?></a>
							</h4></center>						
						</td>
						<td align="center" width="5%"><h3><?php echo $currentRec['id'];?></h3></td>				
					</tr>
		<?php
					$k=1-$k;
					$i++; 		
				}						
		?>				
				</tbody>
				<tfoot>
					<tr>
						<td colspan="8"><center><?php echo $pageNav->getListFooter();?></center></td>
					</tr>
				</tfoot>
		
		</table>
		<input type="hidden" name="option" value="com_archidash"/>
		<input type="hidden" name="controller" value="Dashboard"/>
		<input type="hidden" name="task" value=""/>				
		<input type="hidden" name="filter_order" value="<?php echo $filter_order; ?>"/>
		<input type="hidden" name="filter_order_Dir" value="<?php echo $filter_order_Dir; ?>"/>
		<input type="hidden" name="boxchecked" value="0"/>
		<input type="hidden" name="hidemainmenu" value=""/>
		<input type="hidden" name="next_level_id" value=""/>

		</form>
		<?php 
	}
	
	function remove(){
		$this->display();
	}
	
	
	
	private function prepareEditToolbar(){
		//adding the css
		$css = JURI::base().'components/com_archidash/assets/css/dashboard.css';
		$document =& JFactory::getDocument();
		$document->addStyleSheet($css); 
		//custom title for Paris
		JToolBarHelper::title(   JText::_( 'ARCHIDASH' ),'logo');
		//add toolbars buttons		

		JToolBarHelper::apply('applyedit');
		JToolBarHelper::save('saveedit');
		JToolBarHelper::cancel();
	}
	
	
	/**
	 * Print the edit view
	 */
	function edit($record=false){
		$this->prepareEditToolbar();
		$this->prepareSubmenu();
		//$editor =& JFactory::getEditor();
		?>
		<form action="index.php" method="post" name="adminForm" enctype="multipart/form-data">
		<!-- The hidden inputs at the begin are very useful in the case of error, file not found.
		At the end of forum, if there is some error, they are not written by the browser.-->
		<input type="hidden" name="option" value="com_archidash"/>
		<input type="hidden" name="controller" value="Dashboard"/>
		<input type="hidden" name="task" value=""/>
		<input type="hidden" name="cid[]" value="<?php echo $record['id'];?>"/>
		
						
		<!--  -->
		<fieldset class="adminform">
			<legend><?php echo JText::_('DSINF');?></legend>
			<table class="admintable" width="100%">
								
				<tr>
					
					<td width="20%" align="right" class="key"><?php	echo JHTML::_('tooltip',JText::_('TOOLTIPNAME') ,null,null, JText::_('NAME'));?></td>
					<td width="80%"><input style="width: 410px;" class="text_area" type="text" name="name" id="name" size="76" maxlength="250" value="<?php echo $record['name'];?>" /></td>
				</tr>
				<tr>
					<td width="20%" align="right" class="key"><?php	echo JHTML::_('tooltip',JText::_('TOOLTIPDESCRIPTION') ,null,null, JText::_('DESCRIPTION'));?></td>
					<td width="80%"><textarea style="width: 100%; height: 100%;" rows="5" cols="50" id="description" name="description"><?php echo $record['description'];?></textarea></td>
					
				</tr>
						</table>
		</fieldset>					
	</form>
		
		<?php 
		
	}
	
	
	
	private function prepareAddToolbar(){
		//adding the css
		$css = JURI::base().'components/com_archidash/assets/css/dashboard.css';
		$document =& JFactory::getDocument();
		$document->addStyleSheet($css); 
		//custom title for Paris
		JToolBarHelper::title(   JText::_( 'ARCHIDASH' ),'logo');
		//add toolbars buttons		

		JToolBarHelper::save();
		JToolBarHelper::cancel();
	}
	

	function add($definitions=false){
		$this->prepareAddToolbar();
		$this->prepareSubmenu();
		
		$arr = array();
		foreach($definitions as $d){
			$arr[] = JHTML::_('select.option', $d, $d );
		}		
		
		
		
		?>
		
		<form action="index.php" method="post" name="adminForm" enctype="multipart/form-data">
		<!-- The hidden inputs at the begin are very useful in the case of error, file not found.
		At the end of form, if there is some error, they are not written by the browser.-->
		<input type="hidden" name="option" value="com_archidash"/>
		<input type="hidden" name="controller" value="Dashboard"/>
		
		<input type="hidden" name="task" value=""/>	
		
		<!--  -->
		<fieldset class="adminform">
			<legend><?php echo JText::_('DSINF');?></legend>
			<table class="admintable" width="100%">
								
				<tr>
					<td width="20%" align="right" class="key"><?php	echo JHTML::_('tooltip',JText::_('TOOLTIPDEFINITION') ,null,null, JText::_('DEFINITION'));?></td>
					<td width="80%"><?php echo JHTML::_('select.genericlist', $arr, 'definition', null, 'value', 'text', 0);?></td>
				</tr>
						</table>
		</fieldset>
			
		
	</form>
		
		
		<?php 
	}
	
	
	
	private function prepareDetailsToolbar(){
		//adding the css
		$css = JURI::base().'components/com_archidash/assets/css/dashboard.css';
		$document =& JFactory::getDocument();
		$document->addStyleSheet($css); 
		//custom title for Paris
		JToolBarHelper::title(   JText::_( 'ARCHIDASH' ),'logo');
		
	}
	
	
	function details($details=false){
		$this->prepareDetailsToolbar();
		$this->prepareSubmenu();

	?>
	
		<fieldset class="adminform">
			<legend><?php echo JText::_('DSINF');?></legend>
			<table class="admintable" width="100%">
			<?php echo $this->printDashboard($details);?>
			<?php echo $this->printSection($details);?>
			<?php echo $this->printCategories($details);?>
			<?php echo $this->printArticles($details);?>
			<?php echo $this->printArchimedeModules($details);?>
			<?php echo $this->printModule($details);?>
			<?php echo $this->printMenuType($details);?>
			<?php echo $this->printSQLDatasource($details);?>
			<?php echo $this->printHTMLDatasource($details);?>
			<?php echo $this->printArticleDatasource($details);?>
			<?php echo $this->printSnippetDatasource($details);?>					
				
			</table>
		</fieldset>
	
	<?php 
		
		
	}
	
	private function printDashboard($details=false){		
		if(isset($details['dashboard'])){
			$dashboard = $details['dashboard'][0];
			$result="
				<tr>
					<td width='20%' align='right' class='key'>{$dashboard['name']} (id: {$dashboard['id']})</td>
					<td width='80%'>{$dashboard['description']}</td>
				</tr>
			";
			return $result;
			
		}
		else{
			return "";
		}
		
	}
	
	private function printArchimedeModules($details=false){		
		if(isset($details['archies'])){
			$archies = $details['archies'];
			$result="
				<tr>
					<td width='20%' align='right' class='key'>".JText::_('Archimede Modules')."</td>
					<td width='80%'>
			";
			foreach($archies as $vet){
				$result.="
					<a href='".JURI::base()."index.php?option=com_modules&client=0&task=edit&cid[]={$vet['moduleid']}'>{$vet['name']}</a><br/>
				";
			}
			
			
			$result.="</td></tr>";
			return $result;
		}
		else{
			return "";
		}	
	}
	
	
	
	private function printArticles($details=false){		
		if(isset($details['articles'])){
			$articles = $details['articles'];
			$result="
				<tr>
					<td width='20%' align='right' class='key'>".JText::_('Articles')."</td>
					<td width='80%'>
			";
			foreach($articles as $vet){
				$result.="
					<a href='".JURI::base()."index.php?option=com_content&sectionid=-1&task=edit&cid[]={$vet['articleid']}'>{$vet['name']}</a><br/>
				";
			}
			
			
			$result.="</td></tr>";
			return $result;
		}
		else{
			return "";
		}	
	}
	
	private function printCategories($details=false){		
		if(isset($details['categories'])){
			$categories = $details['categories'];
			$result="
				<tr>
					<td width='20%' align='right' class='key'>".JText::_('Categories')."</td>
					<td width='80%'>
			";
			foreach($categories as $vet){
				$result.="
					<a href='".JURI::base()."index.php?option=com_categories&section=com_content&task=edit&cid[]={$vet['categoryid']}&type=content'>{$vet['name']}</a><br/>
				";
			}
			
			
			$result.="</td></tr>";
			return $result;
		}
		else{
			return "";
		}	
	}
	
	private function printSection($details=false){		
		if(isset($details['section'])){
			$section = $details['section'];
			$result="
				<tr>
					<td width='20%' align='right' class='key'>".JText::_('Section')."</td>
					<td width='80%'>
			";
			foreach($section as $vet){
				$result.="
					<a href='".JURI::base()."index.php?option=com_sections&scope=content&task=edit&cid[]={$vet['sectionid']}'>{$vet['name']}</a><br/>
				";
			}
			
			
			$result.="</td></tr>";
			return $result;
		}
		else{
			return "";
		}	
	}
	
	private function printModule($details=false){		
		if(isset($details['module'])){
			$module = $details['module'];
			$result="
				<tr>
					<td width='20%' align='right' class='key'>".JText::_('Menu Module')."</td>
					<td width='80%'>
			";
			foreach($module as $vet){
				$result.="
					<a href='".JURI::base()."index.php?option=com_modules&client=0&task=edit&cid[]={$vet['moduleid']}'>{$vet['name']}</a><br/>
				";
			}
			
			
			$result.="</td></tr>";
			return $result;
		}
		else{
			return "";
		}	
	}
	
	private function printMenuType($details=false){		
		if(isset($details['menutype'])){
			$menutype = $details['menutype'];
			$result="
				<tr>
					<td width='20%' align='right' class='key'>".JText::_('Menu')."</td>
					<td width='80%'>
			";
			foreach($menutype as $vet){
				$result.="
					<a href='".JURI::base()."index.php?option=com_menus&task=view&menutype={$vet['name']}'>{$vet['name']}</a><br/>
				";
			}
			
			
			$result.="</td></tr>";
			return $result;
		}
		else{
			return "";
		}	
	}
	
	private function printSQLDatasource($details=false){		
		if(isset($details['ds_sql'])){
			$datasources = $details['ds_sql'];
			$result="
				<tr>
					<td width='20%' align='right' class='key'>".JText::_('SQL Data Sources')."</td>
					<td width='80%'>
			";
			foreach($datasources as $vet){
				$result.="
					<a href='".JURI::base()."index.php?option=com_archimede&controller=SQL&task=edit&cid[]={$vet['datasourceid']}'>{$vet['name']}</a><br/>
				";
			}
			
			
			$result.="</td></tr>";
			return $result;
		}
		else{
			return "";
		}	
	}
	
	private function printHTMLDatasource($details=false){		
		if(isset($details['ds_html'])){
			$datasources = $details['ds_html'];
			$result="
				<tr>
					<td width='20%' align='right' class='key'>".JText::_('HTML Data Sources')."</td>
					<td width='80%'>
			";
			foreach($datasources as $vet){
				$result.="
					<a href='".JURI::base()."index.php?option=com_archimede&controller=HTML&task=edit&cid[]={$vet['datasourceid']}'>{$vet['name']}</a><br/>
				";
			}
			
			
			$result.="</td></tr>";
			return $result;
		}
		else{
			return "";
		}	
	}
	
	private function printArticleDatasource($details=false){		
		if(isset($details['ds_article'])){
			$datasources = $details['ds_article'];
			$result="
				<tr>
					<td width='20%' align='right' class='key'>".JText::_('Article Data Sources')."</td>
					<td width='80%'>
			";
			foreach($datasources as $vet){
				$result.="
					<a href='".JURI::base()."index.php?option=com_archimede&controller=Article&task=edit&cid[]={$vet['datasourceid']}'>{$vet['name']}</a><br/>
				";
			}
			
			
			$result.="</td></tr>";
			return $result;
		}
		else{
			return "";
		}	
	}
	
	private function printSnippetDatasource($details=false){		
		if(isset($details['ds_snippet'])){
			$datasources = $details['ds_snippet'];
			$result="
				<tr>
					<td width='20%' align='right' class='key'>".JText::_('Snippet Data Sources')."</td>
					<td width='80%'>
			";
			foreach($datasources as $vet){
				$result.="
					<a href='".JURI::base()."index.php?option=com_archimede&controller=Snippet&task=edit&cid[]={$vet['datasourceid']}'>{$vet['name']}</a><br/>
				";
			}
			
			
			$result.="</td></tr>";
			return $result;
		}
		else{
			return "";
		}	
	}
	
	
	
	
	
	
		
	
}

