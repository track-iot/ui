<?php
/**
 * @version		$Id: plugins.php 1.0 19-11-2009 Danijar
 * @package		Frontend Admin
 * @copyright	Copyright (C) 2009 CMSSpace. http://www.cmsspace.com
 * @license		GNU/GPL. http://www.gnu.org/licenses/gpl.html
 */
defined( '_JEXEC' ) or die( 'Restricted access' );
require_once(dirname(__FILE__).DS.'extension.php');
class AikModelPlugins extends InstallerModel
{
	var $_type = 'plugin';
	function __construct()
	{
		global $mainframe;
		parent::__construct();
		$this->setState('filter.group', $mainframe->getUserStateFromRequest( "com_modules.installer.plugins.group", 'group', '', 'cmd' ));
		$this->setState('filter.string', $mainframe->getUserStateFromRequest( "com_modules.installer.plugins.string", 'filter', '', 'string' ));
	}

	function &getGroups()
	{
		$db = &$this->getDBO();

		$query = 'SELECT folder AS value, folder AS text' .
				' FROM #__plugins' .
				' GROUP BY folder' .
				' ORDER BY folder';
		$db->setQuery( $query );

		$types[] = JHTML::_('select.option',  '', JText::_( 'All' ) );
		$types = array_merge( $types, $db->loadObjectList() );

		return $types;
	}

	function _loadItems()
	{
		global $mainframe, $option;
		$db = & JFactory::getDBO();

		$where = null;
		if ($this->_state->get('filter.group')) {
			if ($search = $this->_state->get('filter.string'))
			{
				$where = ' WHERE folder = "'.$db->getEscaped($this->_state->get('filter.group')).'"';
				$where .= ' AND name LIKE '.$db->Quote( '%'.$db->getEscaped( $search, true ).'%', false );
			}
			else {
				$where = ' WHERE folder = "'.$db->getEscaped($this->_state->get('filter.group')).'"';
			}
		} else {
			if ($search = $this->_state->get('filter.string')) {
				$where .= ' WHERE name LIKE '.$db->Quote( '%'.$db->getEscaped( $search, true ).'%', false );
			}
		}

		$query = 'SELECT id, name, folder, element, client_id, iscore' .
				' FROM #__plugins' .
				$where .
				' ORDER BY iscore, folder, name';
		$db->setQuery($query);
		$rows = $db->loadObjectList();
		$baseDir = JPATH_ROOT.DS.'plugins';

		$numRows = count($rows);
		for ($i = 0; $i < $numRows; $i ++) {
			$row = & $rows[$i];
			$xmlfile = $baseDir.DS.$row->folder.DS.$row->element.".xml";

			if (file_exists($xmlfile)) {
				if ($data = JApplicationHelper::parseXMLInstallFile($xmlfile)) {
					foreach($data as $key => $value)
					{
						$row->$key = $value;
					}
				}
			}
		}

		$this->setState('pagination.total', $numRows);
		if($this->_state->get('pagination.offset') > $this->_state->get('pagination.total')) {
			$this->setState('pagination.offset',0);
		}

		if($this->_state->get('pagination.limit') > 0) {
			$this->_items = array_slice( $rows, $this->_state->get('pagination.offset'), $this->_state->get('pagination.limit') );
		} else {
			$this->_items = $rows;
		}
	}
}