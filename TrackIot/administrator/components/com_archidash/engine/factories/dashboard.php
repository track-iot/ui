<?php


defined('_JEXEC') or die('Restricted Access');
jimport('joomla.application.component.model');
require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'engine'.DS.'factories'.DS.'factory.php');


/**
 * The following class create the dashboards
 * @version 1.0
 * @author  TobyTools.com (email:info@tobytools.com)(website: www.tobytools.com)
 * @copyright Copyright TobyTools.com 
 * @license GNU/GPL
 */


class ArchiDashFactoryDashboard extends ArchiDashFactory{
	
	/*
	 *class constructor 
	 */
	function ArchiDashFactoryDashboard($opts){
		parent::__construct($opts);
		//check everything is allright
		if($this->completed){			
			if($this->preRequirementsCheck()){
				$this->options['tag']="dashboard";
			}	
			else{
				$this->completed=false;
				global $mainframe;
				$mainframe->enqueueMessage(JText::_( 'PREREQFAILED' ),'error');
			}	
		}
		else{
			//no op
		}
		
	}
	
	
	
	
	
	public function create(){
		if($this->completed){
			global $mainframe;
			//Invoke ArchiDashModelDashboard class
			require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'models'.DS.'dashboard.php');
			$classname = "ArchiDashModelDashboard";
			$model = new $classname();
			//write into the #__archidash_dashboards table
			
			$vet = array();
			$vet['name']=$this->readDefinition($this->options['tag'],1,"name");
			$vet['description']=$this->readDefinition($this->options['tag'],1,"description");
			$vet['accesslevel']=$this->readDefinition($this->options['tag'],1,"accesslevel");
			
			//check the value is not empty
			if(strcasecmp(trim($vet['name']),"")==0){
				$this->completed=false;
				$mainframe->enqueueMessage(JText::_('BUILDFAILED')." ".get_class($this)."::create Dashboard",'error');
				$mainframe->enqueueMessage(JText::_('PLSCHECK')." ".JText::_('Name'),'error');
				$mainframe->enqueueMessage($this->errMsg,'error');
				//at this moment, no dashboard id
				//false create The argument is not valid error for destructor
				return -1;
			}
			
			
			
			$this->options['dashboardid'] = $model->insertRecord($vet);
			$this->options['accesslevel'] = $vet['accesslevel'];
			//check if the creation was successfully completed
			/*ERRORS MANAGEMENT*/
			if($model->getErrMsg()!=false){
						$this->completed=false;
						$this->errMsg.=$nodel->getErrMsg();
						return $this->options['dashboardid'];
			}				
			$this->msg.="<h2>{$vet['name']}</h2>";
			
			
			/*Invoke the factory classes*/
			
			/*DATASOURCE FACTORY*/
		
			require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'engine'.DS.'factories'.DS.'datasource.php');
			$classname="ArchiDashFactoryDataSource";
			$datasourcefactory = new $classname($this->options);
			$datasourcefactory->create();
			//there are errors?
			
			if($datasourcefactory->isCompleted()==false){
				$this->completed=false;
				$this->errMsg.=$datasourcefactory->getErrMsg();
				return $this->options['dashboardid'];
			}
			
			$this->msg.=$datasourcefactory->getMsg();
		
			/*******************************************************************************************************************/
			
			
			
			
			/*SECTION FACTORY - one for dashboard*/
			
	
			require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'engine'.DS.'factories'.DS.'section.php');
			$classname="ArchiDashFactorySection";
			$sectionfactory = new $classname($this->options);
			$sectionfactory->create();
			
			//there are errors?
		
			if($sectionfactory->isCompleted()==false){
				$this->completed=false;
				$this->errMsg.=$sectionfactory->getErrMsg();
				$mainframe->enqueueMessage(JText::_('BUILDFAILED')." ".get_class($this)."::create Section",'error');
				return $this->options['dashboardid'];
			}
			
			$this->msg.=$sectionfactory->getMsg();
			$tmp = $sectionfactory->getOptions();
			//save the section id, it's necessary to create the category
			$this->options['sectionid']=$tmp['sectionid'];
			
			/*CATEGORY FACTORY*/
			require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'engine'.DS.'factories'.DS.'category.php');
			$classname="ArchiDashFactoryCategory";
			$categoryfactory = new $classname($this->options);
			$categoryfactory->create();
			
			//there are errors?
			/*ERRORS MANAGEMENT*/
			if($categoryfactory->isCompleted()==false){
				$this->completed=false;
				$this->errMsg.=$categoryfactory->getErrMsg();
				$mainframe->enqueueMessage(JText::_('BUILDFAILED')." ".get_class($this)."::create Categories",'error');
				return $this->options['dashboardid'];
			}
			
			$this->msg.=$categoryfactory->getMsg();
			
			
			/*MENUTYPE FACTORY*/
		
			require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'engine'.DS.'factories'.DS.'menutype.php');
			$classname="ArchiDashFactoryMenuType";
			$menutypefactory = new $classname($this->options);
			$menutypefactory->create();
			if($menutypefactory->isCompleted()==false){
				$this->completed=false;
				$this->errMsg.=$menutypefactory->getErrMsg();
				$mainframe->enqueueMessage(JText::_('BUILDFAILED')." ".get_class($this)."::create Menu Type",'error');
				return $this->options['dashboardid'];
			}
			$this->msg.=$menutypefactory->getMsg();	
			
						
			/*MODULE FACTORY*/
		
			require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'engine'.DS.'factories'.DS.'module.php');
			$classname="ArchiDashFactoryModule";
			$modulefactory = new $classname($this->options);
			$modulefactory->create();
			if($modulefactory->isCompleted()==false){
				$this->completed=false;
				$this->errMsg.=$modulefactory->getErrMsg();
				$mainframe->enqueueMessage(JText::_('BUILDFAILED')." ".get_class($this)."::create Module",'error');
				return $this->options['dashboardid'];
			}
			$this->msg.=$modulefactory->getMsg();
			
			
			/*MENUITEMS FACTORY*/
		
		
			require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'engine'.DS.'factories'.DS.'menu.php');
			$classname="ArchiDashFactoryMenu";
			$menufactory = new $classname($this->options);
			$menufactory->create();
			if($menufactory->isCompleted()==false){
				$this->completed=false;
				$this->errMsg.=$menufactory->getErrMsg();
				$mainframe->enqueueMessage(JText::_('BUILDFAILED')." ".get_class($this)."::create Menu Items",'error');
				return $this->options['dashboardid'];
			}
			$this->msg.=$menufactory->getMsg();
				
			/*ARTICLE FACTORY*/
			require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'engine'.DS.'factories'.DS.'article.php');
			$classname="ArchiDashFactoryArticle";
			$articlefactory = new $classname($this->options);
			$articlefactory->create();
			if($articlefactory->isCompleted()==false){
				$this->completed=false;
				$this->errMsg.=$articlefactory->getErrMsg();
				$mainframe->enqueueMessage(JText::_('BUILDFAILED')." ".get_class($this)."::create Articles",'error');
				return $this->options['dashboardid'];
			}
			$this->msg.=$articlefactory->getMsg();
				
			
			
			/*Archi Modules FACTORY*/
			require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_archidash'.DS.'engine'.DS.'factories'.DS.'archi.php');
			$classname="ArchiDashFactoryArchi";
			$archifactory = new $classname($this->options);
			$archifactory->create();
			if($archifactory->isCompleted()==false){
				$this->completed=false;
				$this->errMsg.=$archifactory->getErrMsg();
				$mainframe->enqueueMessage(JText::_('BUILDFAILED')." ".get_class($this)."::create Archimede Modules",'error');
				return $this->options['dashboardid'];
			}
			$this->msg.=$archifactory->getMsg();
			
				
			
			
			
			return $this->options['dashboardid'];	
			
		}
		else{
			//no op
			//at this moment, no dashboard id
			//false create The argument is not valid error for destructor
			return -1;
		}
			
	}

	
}
?>

